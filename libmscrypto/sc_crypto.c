/*
*  Implementation file for Secure Chorus crypto library.
*/


/* ==== Header file inclusions ============================================== */

#include <ctype.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

#include <openssl/aes.h>
#include <openssl/bn.h>
#include <openssl/ec.h>
#include <openssl/ecdsa.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <openssl/sha.h>

#include "sc_libmscrypto.h"

#include "sc_context.h"
#include "sc_dataref.h"
#include "sc_errno.h"
#include "sc_keystore.h"
#include "sc_misc.h"
#include "sc_types.h"


/* ==== Constant definitions ================================================ */

/*
*  The following two blocks of constant definitions relate to the storage of
*  data references. The secure data storage stores blocks of data each with
*  a handle of type DataRef (which is assumed to be a copyable and assignable
*  type, including being bitwise copyable). Any complex storage will use
*  multiple data references recorded in an array, that array itself being
*  stored similarly. This organisation may be, and is, used recursively.
*  The top level of the organisation is the context, whose data reference is
*  reported outside this library as an object of type SecCtx, which is actually
*  the same type as a data reference in this implementation.
*
*  There are two forms of such arrays of data references. The first is a form
*  of fixed length. There are two such types of array, indicated by the first
*  two sets of constant definitions. The first is for a security context, the
*  second is for a data store. The other form is of dynamically variable length
*  where multiple objects of a type may be stored.
*
*  Both forms may contain cases that are not allocated, either because the
*  single object has not been stored, or because a list currently has length
*  zero. The data reference value of zero (strictly, any value that compares
*  equal to zero) is used for this case.
*
*  There is one other form of multiple object storage, a certificate. This is
*  stored as received, as a sequence of length-value fields (except the last
*  which is a number, then that many length-value fields). The third block of
*  constant definitions below indicate the fields in a certificate.
*
*  The recursive structure of arrays from a security context is as follows.
*  Unmentioned indices are single strings/buffers (including certificates).
*  The first two arrays mentioned (transport key IDs and transport keys) have
*  the same length and matching elements (ID and key).
*
*  TK_ID_INDEX     - Array of data references for transport key IDs.
*  TK_INDEX        - Array of data references for transport keys.
*  EXT_CERTS_INDEX - Array of data references for external certificates.
*  KEY_PACKS_INDEX - Array of data references for key packs, each of
*                    which is itself an array of data references.
*/

/*
*  Number of data references recorded for a security context.
*  Meanings of array elements recorded are:
*  0 - KMS URI.
*  1 - Data references for transport key IDs.
*  2 - Data references for transport key.
*  3 - Root certificate.
*  4 - Data references for external certificates.
*  5 - Data references for key packs.
*/
#define CONTEXT_LENGTH   6
#define KMS_URI_INDEX    0
#define TK_ID_INDEX      1
#define TK_INDEX         2
#define ROOT_CERT_INDEX  3
#define EXT_CERTS_INDEX  4
#define KEY_PACKS_INDEX  5


/*
*  Number of data references recorded for a key pack.
*  Meanings of array elements recorded are:
*  0 - User URI.
*  1 - User ID (UID).
*  2 - Transport key ID.
*  3 - Key pack valid from timestamp.
*  4 - Key pack valid to timestamp.
*  5 - SAKKE UDK.
*  6 - ECCSI SSK.
*  7 - ECCSI PVT.
*/
#define KEY_PACK_LENGTH     8
#define KP_URI_INDEX        0
#define KP_UID_INDEX        1
#define KP_TK_ID_INDEX      2
#define KP_FROM_INDEX       3
#define KP_TO_INDEX         4
#define KP_SAKKE_UDK_INDEX  5
#define KP_ECCSI_SSK_INDEX  6
#define KP_ECCSI_PVT_INDEX  7


/*
*  Certificate fields. All are length-value pairs, except final
*  one which is a number followed by that many length-value pairs.
*/
#define VERSION_FIELD          0
#define ROLE_FIELD             1
#define CERT_URI_FIELD         2
#define KMS_URI_FIELD          3
#define ISSUER_FIELD           4
#define VALID_FROM_FIELD       5
#define VALID_TO_FIELD         6
#define REVOKED_FIELD          7
#define USER_ID_FORMAT_FIELD   8
#define PUB_ENC_KEY_FIELD      9
#define PUB_AUTH_KEY_FIELD     10
#define KMS_DOMAIN_LIST_FIELD  11


/*
*  Timestamp length.
*/
#define TIMESTAMP_LENGTH 16


/*
*  AES block size (in bytes) for AES-128, except last.
*/
#define AES_LENGTH	       8
#define AES_KEY_SIZE      16
#define AES_SALT_SIZE     14
#define AES_IV_SIZE       16
#define AES_256_KEY_SIZE  32


/*
*  MAC size (in bytes)
*/
#define MAC_SIZE 32


/*
*  Elliptic curve point label.
*/
#define EC_LABEL 0x04


/*
*  ECCSI big number size.
*/
#define ECCSI_SIZE 32


/*
*  SAKKE big number size.
*/
#define SAKKE_SIZE 128


/*
*  SAKKE key size.
*/
#define SAKKE_KEY_SIZE 16


/*
*  Maximum HMAC key size.
*/
#define MAX_HMAC_KEY_SIZE 512


/*
*  Transport key sizes.
*/
#define TRK_AES_128_SIZE 16
#define TRK_AES_256_SIZE 32


/*
*  KEMAC parameters.
*/
#define KEMAC_LABEL_SIZE        25
#define KEMAC_CONSTANT_LOCATION  0
#define KEMAC_CS_ID_LOCATION     4
#define KEMAC_CSB_ID_LOCATION    5
#define KEMAC_RAND_LOCATION      9
#define KEMAC_RAND_SIZE         16

#define PRF_KEY_SIZE 16

#define KEMAC_SALT_CSB_ID_LOCATION     2
#define KEMAC_SALT_TIMESTAMP_LOCATION  6

#define KEMAC_CS_ID 0xFF


/*
*  Timestamp margin allowed, in seconds, to count as within reasonable bounds.
*  This allows timestap to be up to a 5 minutes before current time, and no more
*  than a minute after current time. REAL_TIME_CHECK is for cases where messages
*  are expected to arrive instantaneously, and therefore checked at sender.
*/
#define TIMESTAMP_RT_LESS (60 * 5)
#define TIMESTAMP_RT_MORE (60 * 1)
#define REAL_TIME_CHECK   0


/*
*  Timestamp certificate jitter margin allowed, in seconds, to count as within
*  reasonable bounds. This allows timestap to be up to 1 hour before current
*  time, and no more than 1 hour after current time.
*/
#define TIMESTAMP_CERT_PAST_JITTER   (60 * 60 * 1)
#define TIMESTAMP_CERT_FUTURE_JITTER (60 * 60 * 1) 


/*
*  Timestamp keypack jitter margin allowed, in seconds, to count as within
*  reasonable bounds. This allows timestap to be up to 5 minutes before current
*  time, and no more than 5 minutes after current time.
*/
#define TIMESTAMP_KEYPACK_PAST_JITTER   (60 * 5)
#define TIMESTAMP_KEYPACK_FUTURE_JITTER (60 * 5)


/*
*  Maximum allowed string length
*/
#define LENGTH_LIMIT 10000000

/*
*  Constants for key derivation from RFC 3830 Tables 4.1.3 and 4.1.4.
*/
static const uint8_t TGK_TEK[]            = { 0x2A, 0xD0, 0x1C, 0x64 };
static const uint8_t TGK_AITHENTICATION[] = { 0x1B, 0x5C, 0x79, 0x73 };
static const uint8_t TGK_ENCRYPTION[]     = { 0x15, 0x79, 0x8C, 0xEF };
static const uint8_t TGK_SALTING[]        = { 0x39, 0xA2, 0xC1, 0x4B };

static const uint8_t ENV_PSK_ENCRYPTION[]     = { 0x15, 0x05, 0x33, 0xE1 };
static const uint8_t ENV_PSK_AUTHENTICATION[] = { 0x2D, 0x22, 0xAC, 0x75 };
static const uint8_t ENV_PSK_SALTING[]        = { 0x29, 0xB8, 0x89, 0x16 };


/* ==== Static data storage ================================================= */

/*
*  Static array used for current context data, used only until next function
*  call.
*/
static DataRef context_array[CONTEXT_LENGTH];


/*
*  Dynamically allocated memory used for function results, valid until next
*  function call.
*/
static void *result_p = NULL;


/* ==== Local (static) functions ============================================ */

/* ---- Function declarations ----------------------------------------------- */

/*
 *  Declarations of functions used before defined.
 */
static String user_id(String timestamp, String uri, String format);
static String get_domain(String uri);


/* ---- Network byte order functions ---------------------------------------- */

/*
*  Write unsigned number in network byte order (big endian) at pointer.
*  Args: pointer - Pointer to write at. Assumed non-null with sufficient memory.
*        number  - Number to be written.
*/
static void write32(unsigned char *pointer, uint32_t number)
{
	unsigned char *ptr = pointer + sizeof(number);

	while (ptr != pointer)
	{
		*--ptr = number % 256;
		number /= 256;
	}
}


/*
*  Write unsigned number in network byte order (big endian) at pointer.
*  Args: pointer - Pointer to write at. Assumed non-null with sufficient memory.
*        number  - Number to be written.
*/
static void write64(unsigned char *pointer, uint64_t number)
{
	unsigned char *ptr = pointer + sizeof(number);

	while (ptr != pointer)
	{
		*--ptr = number % 256;
		number /= 256;
	}
}


/* ---- Buffer and String basic functions ----------------------------------- */

/*
*   Check string is hexadecimal.
*   Args: length - Expected length of string.
*         string - String.
*   Rets: True if string is hexadecimal up to first null character or length
*         characters, false otherwise including string is null pointer.
*/
static Boolean check_hex(const size_t length, const char *string)
{
	size_t i;

	if (string == NULL)
	{
		return false;
	}

	for (i = 0; i != LENGTH_LIMIT; ++i)
	{
		if (string[i] == '\0' || i == length)
		{
			return true;
		}

		if (!isxdigit(string[i]))
		{
			return false;
		}
	}

	return false;
}


/*
*   Checks a String.
*   Args: string - String to be checked.
*   Rets: True if pointer is not null and length is not greater than
*         LENGTH_LIMIT.
*/
static Boolean check_string(String string)
{
	return string.pointer != NULL && string.length <= LENGTH_LIMIT;
}


/*
*   Checks a Buffer.
*   Args: buffer - Buffer to be checked.
*   Rets: True if pointer is not null and length is not greater than
*         LENGTH_LIMIT.
*/
static Boolean check_buffer(Buffer buffer)
{
	return buffer.pointer != NULL && buffer.length <= LENGTH_LIMIT;
}


/*
*   Convert Buffer to String.
*   Args: buffer - Buffer to be converted.
*   Rets: String; if Buffer pointer is null then given zero length.
*/
static String to_string(Buffer buffer)
{
	String string;
	string.length = (buffer.pointer != NULL ? buffer.length : 0);
	string.pointer = (const char *)buffer.pointer;
	return string;
}


/*
*   Convert String to Buffer.
*   Args: string - String to be converted.
*   Rets: Buffer; if pointer is null then given zero length.
*/
static Buffer to_buffer(String string)
{
	Buffer buffer;
	buffer.length = (string.pointer != NULL ? string.length : 0);
	buffer.pointer = (const unsigned char *)string.pointer;
	return buffer;
}


/*
*  Convert length and pointer to Buffer.
*  Args: length  - Length.
*        pointer - Pointer.
*  Rets: Buffer containing length and pointer. Null pointer is
*        assigned a length of zero, rather than input length.
*/
static Buffer fixed_buffer(size_t length, const void *pointer)
{
	Buffer buffer;
	buffer.length = (pointer != NULL ? length : 0);
	buffer.pointer = (const unsigned char *)pointer;
	return buffer;
}


/*
*  Convert length and pointer to String.
*  Args: length  - Length.
*        pointer - Pointer.
*  Rets: String containing length and pointer.
*/
static String fixed_string(size_t length, const void *pointer)
{
	String string;
	string.length = (pointer != NULL ? length : 0);
	string.pointer = (const char *)pointer;
	return string;
}


/*
*  Purge dynamically allocated buffer, overwriting contents before freeing.
*  Args: buffer_p - *buffer_p is buffer to be overwritten and freed.
*/
static void purge_buffer(Buffer *buffer_p)
{
	memset((void *)buffer_p->pointer, 0, buffer_p->length);
	free((void *)buffer_p->pointer);
	buffer_p->length = 0;
	buffer_p->pointer = NULL;
}


/* ---- Big number functions ------------------------------------------------ */

/*
*  Working version of BN CTX object, owned by this function (and only
*  allocated once per lifetime of this library code).
*  Rets: Pointer to BN CTX object for reuse, null if unable to allocate.
*/
static BN_CTX *bn_ctx(void)
{
	static BN_CTX *ctx_ = NULL;

	if (ctx_ == NULL)
	{
		ctx_ = BN_CTX_new();
	}

	return ctx_;
}


/*
*  Big number creation from null terminated hex string.
*  Args: hex - Hexadecimal string.
*  Rets: Pointer to newly created big number, null if failure.
*        The calling function must free this number.
*/
static BIGNUM *string_big(const char *hex)
{
	/*
	*  Variable used below.
	*/
	BIGNUM *big;

	/*
	*  Verify hexadecimal string.
	*/
	if (hex == NULL || !check_hex(LENGTH_LIMIT, hex))
	{
		return NULL;
	}

	/*
	*  Create big number from hex string, verifying if successful.
	*/
	big = BN_new();

	if (!BN_hex2bn(&big, hex))
	{
	    BN_free(big);
	    return NULL;
	}

	/*
	*  Successful, return big number.
	*/
	return big;
}


/*
*  Big number setting from length specified hex string.
*  Args: big     - Pointer to big number to be set.
*        length  - Length of hexadecimal string.
*        pointer - Pointer to hexadecimal string.
*  Rets: True if successful, false if not.
*/
static Boolean hex_big(BIGNUM *big, size_t length, const char *pointer)
{
	/*
	*  Variables used below.
	*/
	Boolean success;
	char *string;

	/*
	*  Verify length before allocating memory.
	*/
	if (length > LENGTH_LIMIT)
	{
		return false;
	}

	/*
	*  Need copy of number as null-terminated string.
	*/
	string = (char *)malloc(length + 1);

	if (string == NULL)
	{
		return false;
	}

	memcpy(string, pointer, length);
	string[length] = '\0';

	/*
	*  Verify string is hexadecimal.
	*/
	if (!check_hex(length, string))
	{
		free(string);
		return false;
	}

	/*
	*  Determine big number, recording success, free string before
	*  returning.
	*/
	success = BN_hex2bn(&big, string) != 0;
	free(string);
	return success;
}


/*
*  Big number creation from long integer.
*  Args: number - Number to set big number to.
*  Rets: Pointer to newly created big number, null if failure.
*        It is up to the calling routine to free this number.
*/
static BIGNUM *long_big(long number)
{
	/*
	*  Allocate memory for big number.
	*/
	BIGNUM * const big = BN_new();

	/*
	*  Separate cases for positive and negative numbers. Return
	*  allocated number if successful.
	*/
	if (number >= 0)
	{
		if (BN_set_word(big, number))
		{
			return big;
		}
	}
	else
	{
		if (BN_set_word(big, -number))
		{
			BN_set_negative(big, 1);
			return big;
		}
	}

	/*
	*  Not successful, free big number and return null pointer.
	*/
	BN_free(big);
	return NULL;
}


/*
*  Convert big number to binary.
*  Args: big    - Big number to be put into hash.
*        buffer - Buffer into which number is to be written, with length
*                 equal to required length.
*  Rets: True if successful, false if not.
*/
static Boolean big_binary(const BIGNUM *big, Buffer buffer)
{
	/*
	*  Verify buffer is valid and has required size.
	*/
	const size_t size = BN_num_bytes(big);
	unsigned char *ptr = (unsigned char *)buffer.pointer;

	if (big == NULL || buffer.pointer == NULL || size > buffer.length)
	{
		return false;
	}

	/*
	*  Zero out start of buffer, representing most significant bytes
	*  of binary number.
	*/
	if (size < buffer.length)
	{
		ptr += buffer.length - size;
		memset((void *)buffer.pointer, 0, ptr - buffer.pointer);
	}

	/*
	*  Put conversion into least significant byte end of buffer.
	*/
	return BN_bn2bin(big, ptr) == (int)size;
}


/*
*  Convert elliptic curve point to binary.
*  Args: curve  - Curve.
*        ctx    - CTX object.
*        ec     - Point to be put into hash.
*        x      - Big number used by this function.
*        y      - Big number used by this function.
*        buffer - Buffer into which point is to be written, with length
*                 equal to required length (assumed valid). Must allow
*                 initial byte for label.
*  Rets: True if successful, false if not.
*/
static Boolean ec_binary(const EC_GROUP *curve, BN_CTX *ctx, const EC_POINT *ec,
	                     BIGNUM *x, BIGNUM *y, Buffer buffer)
{
	/*
	*  Check valid buffer; must have odd buffer size.
	*/
	size_t length = buffer.length;

	if (buffer.pointer == NULL || curve == NULL || ctx == NULL || x == NULL
		|| y == NULL || ec == NULL || length % 2 != 1)
	{
		return false;
	}

	length /= 2;

	/*
	*  Put elliptic curve point into buffer.
	*/
	*(unsigned char *)buffer.pointer = EC_LABEL;

	return EC_POINT_get_affine_coordinates_GFp(curve, ec, x, y, ctx)
		   && big_binary(x, fixed_buffer(length, buffer.pointer + 1))
		   && big_binary(y, fixed_buffer(length, buffer.pointer + 1 + length));
}


/*
*  Allocate temporary big number variables.
*  Args: number - Number of temporary big numbers to be allocated.
*        big_p  - Pointer to array of big numbers to be allocated,
*                 must be of length number (assumed).
*  Rets: True if success, false otherwise.
*/
static Boolean big_alloc(size_t number, BIGNUM **big_p)
{
	size_t i;
	size_t j;

	if (big_p == NULL || *big_p == NULL)
	{
		return false;
	}

	for (i = 0; i != number; ++i)
	{
		big_p[i] = BN_new();

		if (big_p[i] == NULL)
		{
			for (j = 0; j != i; ++j)
			{
				BN_free(big_p[j]);
			}

			return false;
		}
	}

	return true;
}


/*
*  Deallocate temporary big number variables, after clearing.
*  Args: number - Number of temporary big numbers to be freed.
*        big_p  - Pointer to array of big numbers to be freed,
*                 must be of length number (assumed).
*/
static void big_free(size_t number, BIGNUM **big_p)
{
	size_t i;

	for (i = 0; i != number; ++i)
	{
		BN_clear_free(big_p[i]);
	}
}


/* ---- Elliptic curve functions -------------------------------------------- */

/*
*  Set elliptic curve point from big integer coordinates, and verify is on
*  curve.
*  Args: ec    - Pointer to elliptic curve point to be set.
*        x     - Pointer to x coordinate.
*        y     - Pointer to y coordinate.
*        curve - Pointer to elliptic curve object.
*        ctx   - Temporary storage.
*  Rets: True if set elliptic curve point successfully, otherwise false.
*/
static Boolean big_ec(EC_POINT *ec, BIGNUM *x, BIGNUM *y, const EC_GROUP *curve,
	                  BN_CTX *ctx)
{
	return EC_POINT_set_affine_coordinates_GFp(curve, ec, x, y, ctx)
		   && EC_POINT_is_on_curve(curve, ec, ctx);
}


/*
*  Get ECCSI elliptic curve point from buffer.
*  Args: curve  - Pointer to elliptic curve, assumed non-null.
*        buffer - Buffer from which point is to be determined.
*        label  - If true then buffer must start with curve
*                 label, and buffer is in hex, if false then
*                 there is no curve label and buffer is in binary.
*  Rets: Elliptic curve point, allocated by this function, null
*        if unable to determine point (including if point is
*        not on curve). The calling function must free this point.
*/
static EC_POINT *ec_point(const EC_GROUP *curve, Buffer buffer,
	                      Boolean label)
{
	/*
	*  Variables used below.
	*/
	BN_CTX *ctx;
	BIGNUM *x;
	BIGNUM *y;
	EC_POINT *point;
	const unsigned char *x_p;
	const unsigned char *y_p;

	/*
	*  Length and offset will be set to ignore label, if present, and length
	*  will be per component. Initialised to values without label, for both
	*  components.
	*/
	size_t length = buffer.length;
	size_t offset = 0;

	/*
	*  Verify curve and buffer.
	*/
	if (curve == NULL || buffer.pointer == NULL)
	{
		return NULL;
	}

	/*
	*  Verify buffer is in correct format and get pointers to
	*  coordinate strings and their lengths. Assumes these are
	*  of equal length, but does not validate length against
	*  curve, that is left to checking point is on curve.
	*  Verification of that buffer is in hexadecimal is also
	*  postponed to conversion to big numbers. Assumes that
	*  EC_LABEL has no non-decimal digits (it is actually 0x04).
	*/
	if (label)
	{
		if (length < 2 || buffer.pointer[0] != '0' + EC_LABEL / 16
			|| buffer.pointer[1] != '0' + EC_LABEL % 16)
		{
			return NULL;
		}

		offset = 2;
		length -= 2;

		if (length % 4 != 0)
		{
			return NULL;
		}
	}
	else
	{
		if (length % 2 != 0)
		{
			return NULL;
		}
	}

	/*
	*  Convert length to per component and Verify, as will be done below,
	*  but avoids allocation and freeing. Also ensures length can be
	*  converted to int.
	*/
	length /= 2;

	if (length > LENGTH_LIMIT)
	{
	    return NULL;
	}

	/*
	*  Set pointers to each component hex string.
	*/
	x_p = buffer.pointer + offset;
	y_p = x_p + length;

	/*
	*  Get and verify CTX object.
	*/
	ctx = bn_ctx();

	if (ctx == NULL)
	{
		return NULL;
	}

	/*
	*  Allocate and verify big numbers for x and y coordinates of point.
	*/
	x = BN_new();

	if (x == NULL)
	{
		return NULL;
	}

	y = BN_new();

	if (y == NULL)
	{
		BN_free(x);
		return NULL;
	}

	/*
	*  Set coordinate points.
	*/
	if (label)
	{
		if (!hex_big(x, length, (const char *)x_p)
			|| !hex_big(y, length, (const char *)y_p))
		{
			BN_clear_free(x);
			BN_clear_free(y);
			return NULL;
		}
	}
	else
	{
		if (!BN_bin2bn(x_p, (int)length, x) || !BN_bin2bn(y_p, (int)length, y))
		{
			BN_clear_free(x);
			BN_clear_free(y);
			return NULL;
		}
	}

	/*
	*  Allocate and verify elliptic curve point.
	*/
	point = EC_POINT_new(curve);

	if (point == NULL)
	{
		BN_clear_free(x);
		BN_clear_free(y);
		return NULL;
	}

	/*
	*  Set elliptic curve point, to null if not successful.
	*/
	if (!big_ec(point, x, y, curve, ctx))
	{
		BN_clear_free(x);
		BN_clear_free(y);
		EC_POINT_clear_free(point);
		point = NULL;
		return NULL;
	}

	/*
	*  Free temporary variables and return point, successful or not.
	*/
	BN_clear_free(x);
	BN_clear_free(y);
	return point;
}


/*
*  EC point number creation from two hex strings.
*  Args: x_str - Hexadecimal string for x coordinate.
*        y_str - Hexadecimal string for y coordinate.
*        curve - Pointer to elliptic curve object.
*        ctx   - CTX object.
*  Rets: Pointer to newly created elliptic curve point,
*        null if not a valid elliptic curve point.
*/

static EC_POINT *string_ec(const char *x_str, const char *y_str,
	                       const EC_GROUP *curve, BN_CTX *ctx)
{
	/*
	*  Variables used below.
	*/
	BIGNUM *x;
	BIGNUM *y;
	EC_POINT *ec;

	/*
	*  Create components of curve point.
	*/
	x = string_big(x_str);

	if (x == NULL)
	{
		return NULL;
	}

	y = string_big(y_str);

	if (y == NULL)
	{
		BN_clear_free(x);
		return NULL;
	}

	/*
	*  Create and set curve point.
	*/
	ec = EC_POINT_new(curve);

	if (ec == NULL)
	{
		BN_clear_free(x);
		BN_clear_free(y);
		return NULL;
	}

	if (big_ec(ec, x, y, curve, ctx))
	{
		BN_clear_free(x);
		BN_clear_free(y);
		return ec;
	}
	else
	{
		EC_POINT_clear_free(ec);
		BN_clear_free(x);
		BN_clear_free(y);
		return NULL;
	}
}


/* ---- Hash functions ------------------------------------------------------ */

/*
*  SHA-256 hash of data.
*  Args: data - Data to be hashed.
*        hash - Buffer into which hash is put, must have size as indicated.
*               May overlap data.
*  Rets: True if successful, false if not.
*/
static Boolean sha256(Buffer data, unsigned char hash[SHA256_DIGEST_LENGTH])
{
	/*
	*  Hash function is in three parts, using indicated object.
	*/
	SHA256_CTX ctx;
	return SHA256_Init(&ctx)
		   && SHA256_Update(&ctx, data.pointer, data.length)
		   && SHA256_Final(hash, &ctx);
}


/*
*  Add a big number point to SHA-256 hash.
*  Args: hash   - Hash object to be updated.
*        big    - Big number to be put into hash.
*        buffer - Working buffer, with length equal to required
*                 length (assumed valid).
*  Rets: True if successful, false if not.
*/
static Boolean sha256_big(SHA256_CTX *hash, const BIGNUM *big, Buffer buffer)
{
	return big_binary(big, buffer)
		   && SHA256_Update(hash, buffer.pointer, buffer.length);
}


/*
*  Add an elliptic curve point to SHA-256 hash.
*  Args: hash   - Hash object to be updated.
*        curve  - Elliptic curve object.
*        ec     - Elliptic curve point to be put into hash.
*        buffer - Working buffer, with length equal to required
*                 length of single octet plus two coordinates
*                 (assumed valid).
*  Rets: True if successful, false if not.
*/
static Boolean sha256_ec(SHA256_CTX *hash, const EC_GROUP *curve,
	                     const EC_POINT *ec, Buffer buffer)
{
	/*
	*  Object required for conversion.
	*/
	BN_CTX *ctx = bn_ctx();

	if (ctx == NULL)
	{
		return false;
	}

	/*
	*  Create uncompressed coordinate conversion. Need to override label.
	*/
	if (EC_POINT_point2oct(curve, ec, POINT_CONVERSION_UNCOMPRESSED,
		                   (unsigned char *)buffer.pointer, buffer.length, ctx)
		!= buffer.length)
	{
		return false;
	}

	*(unsigned char *)buffer.pointer = EC_LABEL;

	/*
	*  Hash and return success indication.
	*/
	return SHA256_Update(hash, buffer.pointer, buffer.length);
}


/* ---- Random functions ---------------------------------------------------- */

/*
*  Get range limited random big number.
*  Args: limit  - Random number must be less that this limit.
*                 Assumed to be valid.
*        number - Used to return random big number such that
*                 0 < number < limit.
*  Rets: Standard error code.
*  Note: This implementation of this function assumes that repeated generation
*        of a random number until it is less than limit is acceptable.
*/
static ScErrno random_big(const BIGNUM *limit, BIGNUM *big)
{
	/*
	*  Variables used below.
	*/
	Buffer buffer;
	ScErrno error;
	void *pointer;

	/*
	*  Set buffer to store random bytes.
	*/
	buffer.length = BN_num_bytes(limit);
	pointer = realloc(result_p, buffer.length);
	buffer.pointer = (const unsigned char *)pointer;

	if (buffer.pointer == NULL || buffer.length > INT_MAX)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	result_p = pointer;

	/*
	*  Loop until acceptable big number.
	*/
	do
	{
		/*
		*  Get appropriate length of random bytes.
		*/
		error = GetSecureRandom(buffer);

		if (error)
		{
			return error;
		}

		/*
		*  Convert bytes to big number.
		*/
		if (BN_bin2bn(buffer.pointer, (int)buffer.length, big) == NULL)
		{
			return RESOURCE_NOT_ALLOCATED;
		}
	}
	while (BN_is_zero(big) || BN_cmp(big, limit) >= 0);

	/*
	*  Random number successfully generated.
	*/
	return SUCCESS;
}


/* ---- ECCSI functions ----------------------------------------------------- */

/*
*  Determine ECCSI curve, owned by this function (and only allocated and
*  evaluated once per lifetime of this library code).
*  Rets: Pointer to curve (group) object.
*/
static const EC_GROUP *eccsi_curve(void)
{
	/*
	*  Elliptic curve, set unitialised.
	*/
	static const EC_GROUP *curve_ = NULL;

	/*
	*  Initialise elliptic curve if necessary.
	*/
	if (curve_ == NULL)
	{
		/*
		*  Variables used below, last one initialised and verified.
		*/
		BIGNUM *p;
		BIGNUM *a;
		BIGNUM *b;
		BN_CTX * const ctx = bn_ctx();

		if (ctx == NULL)
		{
			return NULL;
		}

		/*
		*  Initialise and verify p, a, b that define curve.
		*/
		p = string_big("FFFFFFFF000000010000000000000000"
			           "00000000FFFFFFFFFFFFFFFFFFFFFFFF");

		if (p == NULL)
		{
			return NULL;
		}

		a = long_big(-3);

		if (a == NULL)
		{
			BN_free(p);
			return NULL;
		}

		b = string_big("5AC635D8AA3A93E7B3EBBD55769886BC"
			           "651D06B0CC53B0F63BCE3C3E27D2604B");

		if (b == NULL)
		{
			BN_free(p);
			BN_free(a);
			return NULL;
		}

		/*
		*  Create elliptic curve, and free temporary variables.
		*/
		curve_ = EC_GROUP_new_curve_GFp(p, a, b, ctx);
		BN_free(p);
		BN_free(a);
		BN_free(b);
	}

	/*
	*  Return pointer to elliptic curve, owned by this function.
	*/
	return curve_;
}


/*
*  Determine ECCSI curve group order, owned by this function (and only
*  allocated and evaluated once per lifetime of this library code).
*  Rets: Pointer to big number.
*/
static const BIGNUM *eccsi_order(void)
{
	static const BIGNUM *q_ = NULL;

	if (q_ == NULL)
	{
		q_ = string_big("FFFFFFFF00000000FFFFFFFFFFFFFFFF"
			            "BCE6FAADA7179E84F3B9CAC2FC632551");
	}

	return q_;
}


/*
*  Determine ECCSI generating point, owned by this function (and only
*  allocated and evaluated once per lifetime of this library code).
*  Also sets as curve generating function.
*  Rets: Pointer to elliptic curve point.
*/
static const EC_POINT *eccsi_generator(void)
{
	/*
	*  Generating point, set uninitialised.
	*/
	static const EC_POINT *g_ = NULL;

	/*
	*  Initialise generating point if necessary.
	*/
	if (g_ == NULL)
	{
		/*
		*  Variables used below.
		*/
		const EC_GROUP *curve;
		const BIGNUM *q;
		BIGNUM *c;
		BN_CTX *ctx;

		/*
		*  Curve, order and cofactor.
		*/
		curve = eccsi_curve();

		if (curve == NULL)
		{
			return NULL;
		}

		q = eccsi_order();

		if (q == NULL)
		{
			return NULL;
		}

		c = long_big(1);

		if (c == NULL)
		{
			return NULL;
		}

		/*
		*  CTX object.
		*/
		ctx = bn_ctx();

		if (ctx == NULL)
		{
			BN_free(c);
			return NULL;
		}

		/*
		*  Set generating point.
		*/
		g_ = string_ec("6B17D1F2E12C4247F8BCE6E563A440F2"
			           "77037D812DEB33A0F4A13945D898C296",
			           "4FE342E2FE1A7F9B8EE7EB4A7C0F9E16"
			           "2BCE33576B315ECECBB6406837BF51F5",
			           curve, ctx);

		if (!EC_GROUP_set_generator((EC_GROUP *)curve, g_, q, c))
		{
			EC_POINT_free((EC_POINT *)g_);
			g_ = NULL;
		}

		/*
		*  Free temporary variable.
		*/
		BN_free(c);
	}

	/*
	*  Return generating point, success or failure.
	*/
	return g_;
}


/*
*  Get ECCSI elliptic curve point from buffer.
*  Args: buffer - Buffer from which point is to be determined.
*        label  - If true then buffer must start with curve
*                 label, and buffer is in hex, if false then
*                 there is no curve label and buffer is in binary.
*  Rets: Elliptic curve point, allocated by this function, null
*        if unable to determine point (including if point is
*        not on curve). The calling function must free this point.
*/
static EC_POINT *eccsi_point(Buffer buffer, Boolean label)
{
	/*
	*  Get and verify elliptic curve.
	*/
	const EC_GROUP *const curve = eccsi_curve();

	if (curve == NULL)
	{
		return NULL;
	}

	/*
	*  Return elliptic curve point.
	*/
	return ec_point(curve, buffer, label);
}


/*
*  Calculate HS as used by ECCSI.
*  Args: identity - Identity.
*        kpak     - KPAK.
*        pvt      - PVT.
*        hs       - HS, set by this function.
*  Rets: True if HS successfully calculated, false otherwise.
*/
static Boolean eccsi_hs_calc(String identity, const EC_POINT *kpak,
	                         const EC_POINT *pvt, BIGNUM *hs)
{
	/*
	*  Variables used below. Buffer is initialised.
	*/
	SHA256_CTX ctx;
	unsigned char array[1 + 2 * ECCSI_SIZE];
	Buffer buffer = { 1 + 2 * ECCSI_SIZE, array };
	unsigned char hash[SHA256_DIGEST_LENGTH];
	const EC_POINT *g;
	Boolean success;

	/*
	*  Elliptic curve.
	*/
	const EC_GROUP *curve = eccsi_curve();

	if (curve == NULL)
	{
		return false;
	}

	/*
	*  Curve generating point.
	*/
	g = eccsi_generator();

	if (g == NULL)
	{
		return false;
	}

	/*
	*  Calculate hash, including initialising and terminating hash
	*  object.
	*/
	if (!SHA256_Init(&ctx))
	{
		return false;
	}

	success = sha256_ec(&ctx, curve, g, buffer)
		      && sha256_ec(&ctx, curve, kpak, buffer)
		      && SHA256_Update(&ctx, identity.pointer, identity.length)
		      && sha256_ec(&ctx, curve, pvt, buffer);
	return SHA256_Final(hash, &ctx) && success
		   && BN_bin2bn(hash, SHA256_DIGEST_LENGTH, hs) != NULL;
}


/*
*  Calculate HE as used by ECCSI.
*  Args: hs   - HS.
*        r    - r.
*        data - Data (Message).
*        he   - HE, set by this function.
*  Rets: True if HE successfully calculated, false otherwise.
*/
static Boolean eccsi_he_calc(const BIGNUM *hs, const BIGNUM *r,
	                         Buffer data, BIGNUM *he)
{
	/*
	*  Variables used below. Buffer is initialised.
	*/
	SHA256_CTX ctx;
	unsigned char array[ECCSI_SIZE];
	Buffer buffer = { ECCSI_SIZE, array };
	unsigned char hash[SHA256_DIGEST_LENGTH];
	Boolean success;

	if (!SHA256_Init(&ctx))
	{
		return false;
	}

	success = sha256_big(&ctx, hs, buffer)
		      && sha256_big(&ctx, r, buffer)
		      && SHA256_Update(&ctx, data.pointer, data.length);

	return SHA256_Final(hash, &ctx) && success
		   && BN_bin2bn(hash, SHA256_DIGEST_LENGTH, he) != NULL;
}


/*
*  Verify KPAK, SSK and PVT have correct relationship.
*  Args: identity - Identity.
*        kpak     - KPAK.
*        ssk      - SSK.
*        pvt      - PVT.
*  Rets: True if valid, false if not.
*/
static Boolean eccsi_verify_key(String identity, const EC_POINT *kpak,
	                            const BIGNUM *ssk, const EC_POINT *pvt)
{
	/*
	*  Variables used below.
	*/
	Boolean success;
	const EC_POINT *g;
	BN_CTX *ctx;
	BIGNUM *hs;
	EC_POINT *e;
	EC_POINT *f;

	/*
	*  Elliptic curve and generator and CTX object.
	*/
	const EC_GROUP *curve = eccsi_curve();

	if (curve == NULL)
	{
		return false;
	}

	g = eccsi_generator();

	if (g == NULL)
	{
		return false;
	}

	ctx = bn_ctx();

	if (ctx == NULL)
	{
		return false;
	}

	/*
	*  Evaluate HS.
	*/
	hs = BN_new();

	if (!eccsi_hs_calc(identity, kpak, pvt, hs))
	{
		BN_clear_free(hs);
		return false;
	}

	/*
	*  Verify that KPAK = [SSK]G - [HS]PVT, here reoganised as
	*  KPAK + [HS]PVT = [SSK]G.
	*/
	e = EC_POINT_new(curve);

	if (e == NULL)
	{
		BN_clear_free(hs);
		return false;
	}

	f = EC_POINT_new(curve);

	if (f == NULL)
	{
		BN_clear_free(hs);
		EC_POINT_clear_free(e);
		return false;
	}

	success = EC_POINT_mul(curve, f, 0, pvt, hs, ctx)
		      && EC_POINT_add(curve, e, kpak, f, ctx)
		      && EC_POINT_mul(curve, f, 0, g, ssk, ctx)
		      && EC_POINT_cmp(curve, e, f, ctx) == 0;

	/*
	*  Free temporary variables and exit.
	*/
	EC_POINT_clear_free(e);
	EC_POINT_clear_free(f);
	BN_clear_free(hs);
	return success;
}


/*
*  ECCSI verify data.
*  Args: kpak       - ECCSI KPAK.
*        identity   - Signer identity.
*        data       - Data to be verified.
*        signature  - Signature.
*  Rets: Standard error code.
*/
static ScErrno eccsi_verify(const EC_POINT *kpak, String identity,
	                        Buffer data, Buffer signature)
{
	/*
	*  Variables used below.
	*/
	const unsigned char *r_p;
	const unsigned char *s_p;
	const unsigned char *pvt_x_p;
	const unsigned char *pvt_y_p;
	BN_CTX *ctx;
	const EC_GROUP *curve;
	const EC_POINT *g;
	BIGNUM *r;
	BIGNUM *s;
	BIGNUM *hs;
	BIGNUM *he;
	EC_POINT *pvt;
	EC_POINT *y;
	EC_POINT *j;
	Boolean success;

	/*
	*  Verify input formats.
	*/
	if (identity.pointer == NULL || identity.length == 0)
	{
		return ECCSI_NOT_VERIFIED;
	}

	if (data.pointer == NULL || data.length == 0)
	{
		return ECCSI_NOT_VERIFIED;
	}

	if (signature.length != 4 * ECCSI_SIZE + 1
		|| signature.pointer[2 * ECCSI_SIZE] != EC_LABEL)
	{
		return ECCSI_NOT_VERIFIED;
	}

	/*
	*  Pointers to components of signature.
	*/
	r_p = signature.pointer;
	s_p = signature.pointer + ECCSI_SIZE;
	pvt_x_p = signature.pointer + 2 * ECCSI_SIZE + 1;
	pvt_y_p = pvt_x_p + ECCSI_SIZE;

	/*
	*  CTX object, curve and generator.
	*/
	ctx = bn_ctx();

	if (ctx == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	curve = eccsi_curve();

	if (curve == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	g = eccsi_generator();

	if (g == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	/*
	*  Temporary variables.
	*/
	r = BN_new();

	if (r == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	s = BN_new();

	if (s == NULL)
	{
		BN_free(r);
		return RESOURCE_NOT_ALLOCATED;
	}

	hs = BN_new();

	if (hs == NULL)
	{
		BN_free(r);
		BN_free(s);
		return RESOURCE_NOT_ALLOCATED;
	}

	he = BN_new();

	if (he == NULL)
	{
		BN_free(r);
		BN_free(s);
		BN_free(hs);
		return RESOURCE_NOT_ALLOCATED;
	}

	pvt = EC_POINT_new(curve);

	if (pvt == NULL)
	{
		BN_free(r);
		BN_free(s);
		BN_free(hs);
		BN_free(he);
		return RESOURCE_NOT_ALLOCATED;
	}

	y = EC_POINT_new(curve);

	if (y == NULL)
	{
		BN_free(r);
		BN_free(s);
		BN_free(hs);
		BN_free(he);
		EC_POINT_free(pvt);
		return RESOURCE_NOT_ALLOCATED;
	}

	j = EC_POINT_new(curve);

	if (j == NULL)
	{
		BN_free(r);
		BN_free(s);
		BN_free(hs);
		BN_free(he);
		EC_POINT_free(pvt);
		EC_POINT_free(y);
		return RESOURCE_NOT_ALLOCATED;
	}

	/*
	*  Determine if signature matches data. Note initial use of hs and he
	*  are as temporary variables.
	*/
	success = BN_bin2bn(pvt_x_p, ECCSI_SIZE, hs) != NULL
		      && BN_bin2bn(pvt_y_p, ECCSI_SIZE, he) != NULL
		      && big_ec(pvt, hs, he, curve, ctx)
		      && BN_bin2bn(r_p, ECCSI_SIZE, r) != NULL
		      && !BN_is_zero(r)
		      && BN_bin2bn(s_p, ECCSI_SIZE, s) != NULL
		      && eccsi_hs_calc(identity, kpak, pvt, hs)
		      && eccsi_he_calc(hs, r, data, he)
		      && EC_POINT_mul(curve, y, 0, pvt, hs, ctx)
		      && EC_POINT_add(curve, y, y, kpak, ctx)
		      && EC_POINT_mul(curve, y, 0, y, r, ctx)
		      && EC_POINT_mul(curve, j, 0, g, he, ctx)
		      && EC_POINT_add(curve, j, j, y, ctx)
		      && EC_POINT_mul(curve, j, 0, j, s, ctx)
		      && EC_POINT_get_affine_coordinates_GFp(curve, j, s, 0, ctx)
		      && BN_cmp(s, r) == 0;

	/*
	*  Clear temporaries and return if successful.
	*/
	BN_clear_free(r);
	BN_clear_free(s);
	BN_clear_free(hs);
	BN_clear_free(he);
	EC_POINT_clear_free(pvt);
	EC_POINT_clear_free(y);
	EC_POINT_clear_free(j);
	return success ? SUCCESS : ECCSI_NOT_VERIFIED;
}


/*
*  ECCSI signature.
*  Args: curve       - Curve.
*        ctx         - CTX object.
*        kpak        - KPAK.
*        ssk         - SSK.
*        pvt         - PVT.
*        r           - Passed as available big number, suitable for use as r.
*        s           - Passed as available big number, suitable for use as s.
*        identity    - Signing identity.
*        data        - Data to be signed (input).
*        signature_p - *signature_p is set to signature.
*  Rets: Standard error code.
*/
static ScErrno eccsi_sign(const EC_GROUP *curve, BN_CTX *ctx,
	                      const EC_POINT *kpak, const BIGNUM *ssk,
	                      const EC_POINT *pvt, BIGNUM *r, BIGNUM *s,
	                      String identity, Buffer data, Buffer *signature_p)
{
	/*
	*  Variables used below.
	*/
	const BIGNUM *q;
	const EC_POINT *g;
	BIGNUM *j;
	EC_POINT *jj;
	BIGNUM *hs;
	BIGNUM *he;
	void *pointer;
	ScErrno error;

	/*
	*  Group order and generator.
	*/
	q = eccsi_order();

	if (q == NULL)
	{
		return RESOURCE_NOT_EXIST;
	}

	g = eccsi_generator();

	if (g == NULL)
	{
		return RESOURCE_NOT_EXIST;
	}

	/*
	*  Initialise variables.
	*/
	j = BN_new();

	if (j == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	jj = EC_POINT_new(curve);

	if (jj == NULL)
	{
		BN_free(j);
		return RESOURCE_NOT_ALLOCATED;
	}

	hs = BN_new();

	if (hs == NULL)
	{
		BN_free(j);
		EC_POINT_free(jj);
		return RESOURCE_NOT_ALLOCATED;
	}

	he = BN_new();

	if (he == NULL)
	{
		BN_free(j);
		EC_POINT_free(jj);
		BN_free(hs);
		return RESOURCE_NOT_ALLOCATED;
	}

	/*
	*  Loop because reject some cases (see while condition).
	*  Convenient to handle success and failure after loop.
	*/
	error = SUCCESS;

	do
	{
		/*
		*  Calculate j, r and s (other variables are temporary use only).
		*/
		error = random_big(q, j);

		if (error)
		{
			break;
		}

		if (!EC_POINT_mul(curve, jj, 0, g, j, ctx)
			|| !EC_POINT_get_affine_coordinates_GFp(curve, jj, r, 0, ctx)
			|| !eccsi_hs_calc(identity, kpak, pvt, hs)
			|| !eccsi_he_calc(hs, r, data, he)
			|| !BN_mod_mul(s, r, ssk, q, ctx)
			|| !BN_mod_add(s, he, s, q, ctx))
		{
			error = GENERAL_FAILURE;
			break;
		}
	}
	while (BN_is_zero(s));

	/*
	*  Variables no longer needed, even if success.
	*/
	EC_POINT_clear_free(jj);
	BN_clear_free(hs);
	BN_clear_free(he);

	if (error)
	{
		return error;
	}

	/*
	*  Final part of calculation, after which j is no longer required.
	*  Note that for P-256 curve, where q < 2^N, s is always equal to s'.
	*/
	if (!BN_mod_inverse(s, s, q, ctx) || !BN_mod_mul(s, s, j, q, ctx))
	{
		BN_clear_free(j);
		return GENERAL_FAILURE;
	}

	BN_clear_free(j);

	/*
	*  Form signature as concatenation of r, s and PVT, with total size
	*  4 * ECCSI_SIZE + 1.
	*/
	signature_p->length = 4 * ECCSI_SIZE + 1;
	pointer = realloc(result_p, signature_p->length);
	signature_p->pointer = (const unsigned char *)pointer;

	if (signature_p->pointer == NULL)
	{
		signature_p->length = 0;
		return RESOURCE_NOT_ALLOCATED;
	}

	result_p = pointer;

	((unsigned char *)signature_p->pointer)[2 * ECCSI_SIZE] = EC_LABEL;

	if (!big_binary(r, fixed_buffer(ECCSI_SIZE, signature_p->pointer))
		|| !big_binary(s, fixed_buffer(ECCSI_SIZE, 
		               signature_p->pointer + ECCSI_SIZE))
		|| !ec_binary(curve, ctx, pvt, r, s,
		              fixed_buffer(2 * ECCSI_SIZE + 1,
		                           signature_p->pointer + 2 * ECCSI_SIZE)))
	{
		return GENERAL_FAILURE;
	}

	return SUCCESS;
}


/* ---- SAKKE functions ----------------------------------------------------- */

/*
*  Determine SAKKE curve, owned by this function (and only allocated and
*  evaluated once per lifetime of this library code).
*  Rets: Pointer to curve (group) object.
*/
static const EC_GROUP *sakke_curve(void)
{
	/*
	*  Elliptic curve, set uninitialised.
	*/
	static const EC_GROUP *curve_ = NULL;

	/*
	*  Initialise elliptic curve if necessary.
	*/
	if (curve_ == NULL)
	{
		/*
		*  Variables used below, last one initialised and verified.
		*/
		BIGNUM *p;
		BIGNUM *a;
		BIGNUM *b;
		BN_CTX * const ctx = bn_ctx();

		if (ctx == NULL)
		{
			return NULL;
		}

		/*
		*  Initialise and verify p, a, b that define curve.
		*/
		p = string_big("997ABB1F0A563FDA65C61198DAD0657A"
			           "416C0CE19CB48261BE9AE358B3E01A2E"
			           "F40AAB27E2FC0F1B228730D531A59CB0"
			           "E791B39FF7C88A19356D27F4A666A6D0"
			           "E26C6487326B4CD4512AC5CD65681CE1"
			           "B6AFF4A831852A82A7CF3C521C3C09AA"
			           "9F94D6AF56971F1FFCE3E82389857DB0"
			           "80C5DF10AC7ACE87666D807AFEA85FEB");

		if (p == NULL)
		{
			return NULL;
		}

		a = long_big(-3);

		if (a == NULL)
		{
			BN_free(p);
			return NULL;
		}

		b = BN_new();

		if (b == NULL)
		{
			BN_free(p);
			BN_free(a);
			return NULL;
		}

		if (!BN_zero(b))
		{
			BN_free(p);
			BN_free(a);
			BN_free(b);
			return NULL;
		}

		/*
		*  Create elliptic curve, and free temporary variables.
		*/
		curve_ = EC_GROUP_new_curve_GFp(p, a, b, ctx);
		BN_free(p);
		BN_free(a);
		BN_free(b);
	}

	/*
	*  Return pointer to elliptic curve, owned by this function.
	*/
	return curve_;
}


/*
*  Determine SAKKE curve prime, owned by this function (and only
*  allocated and evaluated once per lifetime of this library code).
*  Rets: Pointer to big number.
*/
static const BIGNUM *sakke_prime(void)
{
	static const BIGNUM *p_ = NULL;

	if (p_ == NULL)
	{
		p_ = string_big("997ABB1F0A563FDA65C61198DAD0657A"
			            "416C0CE19CB48261BE9AE358B3E01A2E"
			            "F40AAB27E2FC0F1B228730D531A59CB0"
			            "E791B39FF7C88A19356D27F4A666A6D0"
			            "E26C6487326B4CD4512AC5CD65681CE1"
		                "B6AFF4A831852A82A7CF3C521C3C09AA"
			            "9F94D6AF56971F1FFCE3E82389857DB0"
			            "80C5DF10AC7ACE87666D807AFEA85FEB");
	}

	return p_;
}


/*
*  Determine SAKKE curve group order, owned by this function (and only
*  allocated and evaluated once per lifetime of this library code).
*  Rets: Pointer to big number.
*/
static const BIGNUM *sakke_order(void)
{
	static const BIGNUM *q_ = NULL;

	if (q_ == NULL)
	{
		q_ = string_big("265EAEC7C2958FF69971846636B4195E"
			            "905B0338672D20986FA6B8D62CF8068B"
			            "BD02AAC9F8BF03C6C8A1CC354C69672C"
			            "39E46CE7FDF222864D5B49FD2999A9B4"
			            "389B1921CC9AD335144AB173595A0738"
			            "6DABFD2A0C614AA0A9F3CF14870F026A"
			            "A7E535ABD5A5C7C7FF38FA08E2615F6C"
			            "203177C42B1EB3A1D99B601EBFAA17FB");
	}

	return q_;
}


/*
*  Determine SAKKE generating point, owned by this function (and only
*  allocated and evaluated once per lifetime of this library code).
*  Rets: Pointer to elliptic curve point.
*/
static const EC_POINT *sakke_generator(void)
{
	/*
	*  Generating point, set uninitialised.
	*/
	static const EC_POINT *g_ = NULL;

	/*
	*  Initialise generating point if necessary.
	*/
	if (g_ == NULL)
	{
		/*
		*  Variables used below.
		*/
		const EC_GROUP *curve;
		const BIGNUM *q;
		BIGNUM *c;
		BN_CTX *ctx;

		/*
		*  Curve, order and cofactor.
		*/
		curve = sakke_curve();

		if (curve == NULL)
		{
			return NULL;
		}

		q = sakke_order();

		if (q == NULL)
		{
			return NULL;
		}

		c = long_big(4);

		if (c == NULL)
		{
			return NULL;
		}

		/*
		*  CTX object.
		*/
		ctx = bn_ctx();

		if (ctx == NULL)
		{
			BN_free(c);
			return NULL;
		}

		/*
		*  Set generating point.
		*/
		g_ = string_ec("53FC09EE332C29AD0A7990053ED9B52A"
			           "2B1A2FD60AEC69C698B2F204B6FF7CBF"
			           "B5EDB6C0F6CE2308AB10DB9030B09E10"
			           "43D5F22CDB9DFA55718BD9E7406CE890"
			           "9760AF765DD5BCCB337C86548B72F2E1"
			           "A702C3397A60DE74A7C1514DBA66910D"
			           "D5CFB4CC80728D87EE9163A5B63F73EC"
			           "80EC46C4967E0979880DC8ABEAE63895",
			           "0A8249063F6009F1F9F1F0533634A135"
			           "D3E82016029906963D778D821E141178"
			           "F5EA69F4654EC2B9E7F7F5E5F0DE55F6"
			           "6B598CCF9A140B2E416CFF0CA9E032B9"
			           "70DAE117AD547C6CCAD696B5B7652FE0"
			           "AC6F1E80164AA989492D979FC5A4D5F2"
			           "13515AD7E9CB99A980BDAD5AD5BB4636"
			           "ADB9B5706A67DCDE75573FD71BEF16D7",
			           curve, ctx);

		if (!EC_GROUP_set_generator((EC_GROUP *)curve, g_, q, c))
		{
			EC_POINT_free((EC_POINT *)g_);
			g_ = NULL;
		}

		/*
		*  Free temporary variable.
		*/
		BN_free(c);
	}

	/*
	*  Return generating point, success or failure.
	*/
	return g_;
}

/*
*  Determine SAKKE paired generator, owned by this function (and only
*  allocated and evaluated once per lifetime of this library code).
*  Rets: Pointer to big number.
*/
static const BIGNUM *sakke_gen_pair(void)
{
	static const BIGNUM *gg_ = NULL;

	if (gg_ == NULL)
	{
		gg_ = string_big("66FC2A432B6EA392148F15867D623068"
			             "C6A87BD1FB94C41E27FABE658E015A87"
			             "371E94744C96FEDA449AE9563F8BC446"
			             "CBFDA85D5D00EF577072DA8F541721BE"
			             "EE0FAED1828EAB90B99DFB0138C78433"
			             "55DF0460B4A9FD74B4F1A32BCAFA1FFA"
			             "D682C033A7942BCCE3720F20B9B7B040"
			             "3C8CAE87B7A0042ACDE0FAB36461EA46");
	}

	return gg_;
}

/*
*  Get SAKKE elliptic curve point from buffer.
*  Args: buffer - Buffer from which point is to be determined.
*        label  - If true then buffer must start with curve
*                 label, and buffer is in hex, if false then
*                 there is no curve label and buffer is in binary.
*  Rets: Elliptic curve point, allocated by this function, null
*        if unable to determine point (including if point is
*        not on curve). The calling function must free this point.
*/
static EC_POINT *sakke_point(Buffer buffer, Boolean label)
{
	/*
	*  Get and verify elliptic curve.
	*/
	const EC_GROUP *const curve = sakke_curve();

	if (curve == NULL)
	{
		return NULL;
	}

	/*
	*  Return elliptic curve point.
	*/
	return ec_point(curve, buffer, label);
}


/*
*  Square function of pair (x,y) used by SAKKE.
*  Args: x_out - Output x. May be the same as x_in.
*        y_out - Output y. May be the same as y_in.
*        x_in  - Input x.
*        y_in  - Input y.
*        x1    - Temporary variable. Must not be same as any other variable.
*        y1    - Temporary variable. Must not be same as any other variable.
*        x2    - Temporary variable. Must not be same as any other variable.
*        y2    - Temporary variable. Must not be same as any other variable.
*  Rets: True if successful, false if not.
*/
static Boolean sakke_sqr(BIGNUM *x_out, BIGNUM *y_out, const BIGNUM *x_in,
	                     const BIGNUM *y_in, BIGNUM *x1, BIGNUM *y1,
	                     BIGNUM *x2, BIGNUM *y2)
{
	BN_CTX *ctx = bn_ctx();
	const BIGNUM *p = sakke_prime();

	if (ctx == NULL || p == NULL)
	{
		return false;
	}

	return BN_copy(x1, x_in) && BN_copy(y1, y_in)
		   && BN_add(x2, x1, y1)
		   && BN_sub(y2, x1, y1)
		   && BN_mod_mul(x_out, x2, y2, p, ctx)
		   && BN_mod_mul(y_out, x1, y1, p, ctx)
		   && BN_lshift1(y_out, y_out)
		   && BN_nnmod(y_out, y_out, p, ctx);
}


/*
*  Multiply function of pairs (x,y) used by SAKKE.
*  Args: x_out - Output x. May be the same as x_in1 or x_in2.
*        y_out - Output y. May be the same as y_in1 ot y_in2.
*        x_in1 - First input x. May be the same as x_in2.
*        y_in1 - First input y. May be the same as x_in2.
*        x_in2 - First input x. May be the same as x_in1.
*        y_in2 - First input y. May be the same as x_in2.
*        x1    - Temporary variable. Must not be same as any other variable.
*        y1    - Temporary variable. Must not be same as any other variable.
*        x2    - Temporary variable. Must not be same as any other variable.
*        y2    - Temporary variable. Must not be same as any other variable.
*        t     - Temporary variable. Must not be same as any other variable.
*  Rets: True if successful, false if not.
*/
static Boolean sakke_mult(BIGNUM *x_out, BIGNUM *y_out, const BIGNUM *x_in1,
	                      const BIGNUM *y_in1, const BIGNUM *x_in2,
	                      const BIGNUM *y_in2, BIGNUM *x1, BIGNUM *y1,
	                      BIGNUM *x2, BIGNUM *y2, BIGNUM *t)
{
	BN_CTX *ctx = bn_ctx();
	const BIGNUM *p = sakke_prime();

	if (ctx == NULL || p == NULL)
	{
		return false;
	}

	return BN_copy(x1, x_in1) && BN_copy(y1, y_in1)
		   && BN_copy(x2, x_in2) && BN_copy(y2, y_in2)
		   && BN_mod_mul(x_out, x1, x2, p, ctx)
		   && BN_mod_mul(t, y1, y2, p, ctx)
		   && BN_mod_sub(x_out, x_out, t, p, ctx)
		   && BN_mod_mul(y_out, x1, y2, p, ctx)
		   && BN_mod_mul(t, y1, x2, p, ctx)
		   && BN_mod_add(y_out, y_out, t, p, ctx);
}


/*
*  Power function of pair (x,y) to power r used by SAKKE.
*  Args: x_out - Output x. May be the same as x_in.
*        y_out - Output y. May be the same as y_in.
*        x_in  - Input x.
*        y_in  - Input y.
*        r     - r.
*  Rets: True if successful, false if not.
*/
static Boolean sakke_pow(BIGNUM *x_out, BIGNUM *y_out, const BIGNUM *x_in,
	                     const BIGNUM *y_in, BIGNUM *r)
{
	/*
	*  Variables used below.
	*/
	BIGNUM *x;
	BIGNUM *y;
	BIGNUM *t[5];
	Boolean success;

	/*
	*  Number of bits of power index.
	*/
	int m = BN_num_bits(r);

	if (m <= 0)
	{
		return false;
	}

	/*
	*  Temporary variables.
	*/
	x = BN_new();

	if (x == NULL)
	{
		return false;
	}

	y = BN_new();

	if (y == NULL)
	{
		BN_free(x);
		return false;
	}

	if (!big_alloc(5, t))
	{
		BN_free(x);
		BN_free(y);
		return false;
	}

	/*
	*  Copy input to temporary variables.
	*/
	if (!BN_copy(x, x_in) || !BN_copy(y, y_in))
	{
		BN_clear_free(x);
		BN_clear_free(y);
		big_free(5, t);
		return false;
	}

	/*
	*  Loop through bits of index.
	*/
	while (--m)
	{
		if (!sakke_sqr(x, y, x, y, t[0], t[1], t[2], t[3])
			|| (BN_is_bit_set(r, m - 1)
			    && !sakke_mult(x, y, x, y, x_in, y_in, t[0], t[1], t[2], t[3],
			                   t[4])))
		{
			BN_clear_free(x);
			BN_clear_free(y);
			big_free(5, t);
			return false;
		}
	}

	/*
	*  Copy result to output, clean up, and exit.
	*/
	success = BN_copy(x_out, x) && BN_copy(y_out, y);
	BN_clear_free(x);
	BN_clear_free(y);
	big_free(5, t);
	return success;
}


/*
*  Compute pairing, as used by SAKKE.
*  Args: r  - Result of pairing.
*        a  - First element to pair.
*        b  - Second element to pair.
*        ax - Temporary variable.
*        ay - Temporary variable.
*        bx - Temporary variable.
*        by - Temporary variable.
*        cx - Temporary variable.
*        cy - Temporary variable.
*        vx - Temporary variable.
*        vy - Temporary variable.
*        dx - Temporary variable.
*        dy - Temporary variable.
*        ex - Temporary variable.
*        ey - Temporary variable.
*        fx - Temporary variable.
*        fy - Temporary variable.
*        t0 - Temporary variable.
*        t1 - Temporary variable.
*        t2 - Temporary variable.
*        t3 - Temporary variable.
*  Rets: True if successful, false otherwise.
*/
static Boolean sakke_pairing(BIGNUM *r, const EC_POINT *a, const EC_POINT *b,
	                         BIGNUM *ax, BIGNUM *ay, BIGNUM *bx, BIGNUM *by,
	                         BIGNUM *vx, BIGNUM *vy, BIGNUM *cx, BIGNUM *cy,
	                         BIGNUM *dx, BIGNUM *dy, BIGNUM *ex, BIGNUM *ey,
	                         BIGNUM *fx, BIGNUM *fy, BIGNUM *t0, BIGNUM *t1,
	                         BIGNUM *t2, BIGNUM *t3)
{
	/*
	*  Variables used below.
	*/
	BN_CTX *ctx;
	const EC_GROUP *curve;
	const BIGNUM *p;
	const BIGNUM *q;
	int i;

	/*
	*  CTX object, curve and curve order.
	*/
	ctx = bn_ctx();

	if (ctx == NULL)
	{
		return false;
	}

	curve = sakke_curve();

	if (curve == NULL)
	{
		return false;
	}

	p = sakke_prime();

	if (p == NULL)
	{
		return false;
	}

	q = sakke_order();

	if (q == NULL)
	{
		return false;
	}

	/*
	*  Pairing calculation.
	*/
	if (!BN_copy(t0, q) || !BN_sub_word(t0, 1)
		|| !EC_POINT_get_affine_coordinates_GFp(curve, a, ax, ay, ctx)
		|| !EC_POINT_get_affine_coordinates_GFp(curve, b, bx, by, ctx)
		|| !BN_one(vx) || !BN_zero(vy)
		|| !BN_copy(cx, ax) || !BN_copy(cy, ay))
	{
		return false;
	}

	for (i = BN_num_bits(t0); --i; )
	{
		if (!sakke_sqr(vx, vy, vx, vy, ex, ey, fx, fy)
			|| !BN_mod_sqr(dx, cx, p, ctx)
			|| !BN_sub_word(dx, 1)
			|| !BN_mul_word(dx, 3)
			|| !BN_add(t1, bx, cx)
			|| !BN_mod_mul(dx, dx, t1, p, ctx)
			|| !BN_mod_sqr(t1, cy, p, ctx)
			|| !BN_lshift1(t1, t1)
			|| !BN_mod_sub(dx, dx, t1, p, ctx)
			|| !BN_lshift1(dy, cy)
			|| !BN_mod_mul(dy, dy, by, p, ctx)
			|| !sakke_mult(vx, vy, vx, vy, dx, dy, ex, ey, fx, fy, t1))
		{
			return false;
		}

		if (!BN_mod_sqr(t1, cx, p, ctx)
			|| !BN_sub_word(t1, 1)
			|| !BN_mul_word(t1, 3)
			|| !BN_lshift1(t3, cy)
			|| !BN_mod_inverse(t3, t3, p, ctx)
			|| !BN_mod_mul(t1, t1, t3, p, ctx)
			|| !BN_mod_sqr(t2, t1, p, ctx)
			|| !BN_lshift1(t3, cx)
			|| !BN_mod_sub(dx, t2, t3, p, ctx)
			|| !BN_sub(dy, t3, t2)
			|| !BN_add(dy, dy, cx)
			|| !BN_mod_mul(dy, dy, t1, p, ctx)
			|| !BN_mod_sub(dy, dy, cy, p, ctx)
			|| !BN_copy(cx, dx) || !BN_copy(cy, dy))
		{
			return false;
		}

		if (BN_is_bit_set(t0, i - 1))
		{
			if (!BN_add(dx, ax, bx)
				|| !BN_mod_mul(dx, dx, cy, p, ctx)
				|| !BN_add(t1, bx, cx)
				|| !BN_mod_mul(t1, ay, t1, p, ctx)
				|| !BN_mod_sub(dx, dx, t1, p, ctx)
				|| !BN_sub(dy, cx, ax)
				|| !BN_mod_mul(dy, dy, by, p, ctx)
				|| !sakke_mult(vx, vy, vx, vy, dx, dy, ex, ey, fx, fy, t1))
			{
				return false;
			}

			if (!BN_sub(t1, ay, cy)
				|| !BN_sub(t3, ax, cx)
				|| !BN_mod_inverse(t3, t3, p, ctx)
				|| !BN_mod_mul(t1, t1, t3, p, ctx)
				|| !BN_mod_sqr(t2, t1, p, ctx)
				|| !BN_sub(dx, t2, cx)
				|| !BN_mod_sub(dx, dx, ax, p, ctx)
				|| !BN_sub(dy, ax, t2)
				|| !BN_lshift1(t3, cx)
				|| !BN_add(dy, dy, t3)
				|| !BN_mod_mul(dy, dy, t1, p, ctx)
				|| !BN_mod_sub(dy, dy, cy, p, ctx)
				|| !BN_copy(cx, dx) || !BN_copy(cy, dy))
			{
				return false;
			}
		}
	}

	return sakke_sqr(vx, vy, vx, vy, ex, ey, fx, fy)
		   && sakke_sqr(vx, vy, vx, vy, ex, ey, fx, fy)
		   && BN_mod_inverse(r, vx, p, ctx)
		   && BN_mod_mul(r, r, vy, p, ctx);
}


/*
*  SAKKE-defined hash to big number.
*  Args: result   - To be set to result.
*        data     - Data to be hashed, length may not exceed SAKKE_SIZE.
*        buffer_p - Pointer to temporary buffer, with length at least
*                   2 * SHA256_DIGEST_LENGTH (assumed). May overlap data.
*        limit    - Limiting value of hash, may be same as result.
*        number   - Number of bytes to be produced from hash, the
*                   number of bytes limit - 1 requires.
*        t        - Temporary value.
*  Rets: True if successful, false otherwise.
*  Note: Memory usage in buffer is:
*        v_1 ... v_k - SHA256_DIGEST_LENGTH bytes each,
*                      <= SHA256_DIGEST_LENGTH bytes total
*        h_i         - Used for h_1 to h_k, SHA256_DIGEST_LENGTH bytes.
*        a           - SHA256_DIGEST_LENGTH bytes.
*/
static Boolean sakke_hash(BIGNUM *result, Buffer data, unsigned char *buffer_p,
						  const BIGNUM *limit, size_t number, BIGNUM *t)
{
	/*
	*  Variables used below.
	*/
	unsigned char *v_i;
	unsigned char *h_i;
	unsigned char *a;
	BN_CTX *ctx;
	int size;

	/*
	*  Verify number of hash stages and size. Verifying k ensures
	*  size does not exceed SAKKE_SIZE (*and hence is valid int).
	*/
	size_t k = (number - 1) / SHA256_DIGEST_LENGTH + 1;

	if (k > SAKKE_SIZE / SHA256_DIGEST_LENGTH)
	{
		return false;
	}

	size = (int)(k * SHA256_DIGEST_LENGTH);

	v_i = buffer_p;
	h_i = v_i + size;
	a = h_i + SHA256_DIGEST_LENGTH;

	/*
	*  CTX object.
	*/
	ctx = bn_ctx();

	/*
	*  Initial use of hash.
	*/
	if (!sha256(data, a))
	{
		return false;
	}

	/*
	*  Using hash multiple times.
	*/
	memset(h_i, 0, SHA256_DIGEST_LENGTH);

	for (; k != 0; --k)
	{
		if (!sha256(fixed_buffer(SHA256_DIGEST_LENGTH, h_i), h_i))
		{
			return false;
		}

		if (!sha256(fixed_buffer(2 * SHA256_DIGEST_LENGTH, h_i), v_i))
		{
			return false;
		}

		v_i += SHA256_DIGEST_LENGTH;
	}

	/*
	*  Convert to big number.
	*/
	return BN_bin2bn(buffer_p, size, t) != 0
		   && BN_nnmod(result, t, limit, ctx);
}


/*
*  SAKKE encrypt.
*  Args: identity       - Identity. Length must not exceed SAKKE_SIZE.
*        z              - Z (public key).
*        data           - Data to be encrypted. Length must not exceed
*                         SAKKE_SIZE.
*        encrypted_data - Buffer to which encrypted data is to be written.
*                         Must have length at least
*                         data.length + 2 * SAKKE_SIZE (assumed true).
*  Rets: Standard error code.
*/
static ScErrno sakke_encrypt(String identity, const EC_POINT *z,
	                         Buffer data, Buffer encrypted_data)
{
	/*
	*  Variables used below.
	*/
	const EC_GROUP *curve;
	const EC_POINT *g;
	const BIGNUM *gg;
	const BIGNUM *p;
	const BIGNUM *q;
	BN_CTX *ctx;
	BIGNUM *r;
	BIGNUM *b;
	BIGNUM *x;
	BIGNUM *y;
	EC_POINT *rr;
	size_t i;
	Boolean success;

	/*
	*  Verify input lengths.
	*/
	if (identity.length > SAKKE_SIZE || data.length > SAKKE_SIZE)
	{
		return GENERAL_FAILURE;
	}

	/*
	*  Get elliptic curve, generator, paired generator, order, and CTX object.
	*/
	curve = sakke_curve();

	if (curve == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	g = sakke_generator();

	if (g == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	gg = sakke_gen_pair();

	if (gg == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	if (g == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	p = sakke_prime();

	if (p == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	q = sakke_order();

	if (q == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	ctx = bn_ctx();

	if (ctx == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	/*
	*  Put data and identity into encrypted data (used as temporary buffer)
	*  and hash to r.
	*/
	r = BN_new();

	if (r == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	memcpy((unsigned char *)encrypted_data.pointer, data.pointer, data.length);
	memcpy((unsigned char *)encrypted_data.pointer + data.length,
		   identity.pointer, identity.length);

	if (!sakke_hash(r, fixed_buffer(data.length + identity.length,
		                            encrypted_data.pointer),
		                            (unsigned char *)encrypted_data.pointer
									  + data.length,
									q, SAKKE_SIZE, r))
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	/*
	*  Convert identity to b.
	*/
	b = BN_bin2bn((const unsigned char *)identity.pointer,
		          (int)identity.length, 0);

	if (b == NULL)
	{
		BN_free(r);
		return RESOURCE_NOT_ALLOCATED;
	}

	/*
	*  SAKKE encryption.
	*/
	rr = EC_POINT_new(curve);

	if (rr == NULL)
	{
		BN_free(r);
		BN_free(b);
		return RESOURCE_NOT_ALLOCATED;
	}

	if (!BN_mod_mul(b, r, b, q, ctx) || !EC_POINT_mul(curve, rr, b, z, r, ctx))
	{
		BN_clear_free(r);
		BN_clear_free(b);
		EC_POINT_clear_free(rr);
		return GENERAL_FAILURE;
	}

	x = BN_new();

	if (x == NULL)
	{
		BN_clear_free(r);
		BN_clear_free(b);
		EC_POINT_clear_free(rr);
		return RESOURCE_NOT_ALLOCATED;
	}

	y = BN_new();

	if (y == NULL)
	{
		BN_clear_free(r);
		BN_clear_free(b);
		EC_POINT_clear_free(rr);
		BN_free(x);
		return RESOURCE_NOT_ALLOCATED;
	}

	if (!BN_one(x) || !BN_copy(y, gg)
		|| !sakke_pow(x, y, x, y, r)
		|| !BN_mod_inverse(b, x, p, ctx)
		|| !BN_mod_mul(b, y, b, p, ctx)
		|| !big_binary(b, fixed_buffer(SAKKE_SIZE, encrypted_data.pointer)))
	{
		BN_clear_free(r);
		BN_clear_free(b);
		EC_POINT_clear_free(rr);
		BN_clear_free(x);
		BN_clear_free(y);
		return GENERAL_FAILURE;
	}

	BN_clear_free(y);

	if (!BN_one(b) || !BN_lshift(b, b, 8 * (int)data.length)
		|| !sakke_hash(b, fixed_buffer(SAKKE_SIZE, encrypted_data.pointer),
		               (unsigned char *)encrypted_data.pointer, b, data.length,
					   x)
		|| !big_binary(b, fixed_buffer(data.length, encrypted_data.pointer)))
	{
		BN_clear_free(r);
		BN_clear_free(b);
		EC_POINT_clear_free(rr);
		BN_clear_free(x);
		return GENERAL_FAILURE;
	}

	BN_clear_free(x);

	/*
	*  Start of encrypted data is exclusive or of data and calculated
	*  encryption.
	*/
	for (i = 0; i != data.length; ++i)
	{
		((unsigned char *)encrypted_data.pointer)[i] ^= data.pointer[i];
	}

	/*
	*  Put R at end of encrypted data (using r, b as temporary variables),
	*  clean up and exit.
	*/
	success = ec_binary(curve, ctx, rr, r, b,
		                fixed_buffer(1 + 2 * SAKKE_SIZE,
		                             encrypted_data.pointer + data.length));
	BN_clear_free(r);
	BN_clear_free(b);
	EC_POINT_clear_free(rr);
	return success ? SUCCESS : GENERAL_FAILURE;
}


/*
*  SAKKE decrypt.
*  Args: identity       - Identity. Length must not exceed SAKKE_SIZE.
*        z              - Z (public key).
*        rsk            - RSK (private key).
*        data           - Data to be decrypted. Must have length greater
*                         than 1 + 2 * SAKKE_SIZE (assumed true). Data is
*                         overwritten by this function.
*        decrypted_data - Buffer to which decrypted data is to be written.
*                         Must have length equal to
*                         data.length - (1 + 2 * SAKKE_SIZE) (assumed true)
*                         which must not exceed SAKKE_SIZE.
*  Rets: Standard error code.
*/
static ScErrno sakke_decrypt(String identity, const EC_POINT *z,
	                         const EC_POINT *rsk, Buffer data, Buffer
	                         decrypted_data)
{
	/*
	*  Variables used below.
	*/
	const EC_GROUP *curve;
	const EC_POINT *g;
	const BIGNUM *q;
	BN_CTX *ctx;
	BIGNUM *t[18];
	EC_POINT *rr;
	EC_POINT *test;
	BIGNUM *w;
	size_t length;
	unsigned char buffer[SAKKE_SIZE];
	size_t i;
	Boolean success;

	/*
	*  Verify input lengths.
	*/
	if (identity.length > SAKKE_SIZE || data.length < 1 + 2 * SAKKE_SIZE)
	{
		return SAKKE_DECRYPT_FAIL;
	}

	length = data.length - (1 + 2 * SAKKE_SIZE);

	if (length > SAKKE_SIZE || data.pointer[length] != EC_LABEL)
	{
		return SAKKE_DECRYPT_FAIL;
	}

	/*
	*  Get elliptic curve, generator, order, and CTX object.
	*/
	curve = sakke_curve();

	if (curve == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	g = sakke_generator();

	if (g == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	q = sakke_order();

	if (q == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	ctx = bn_ctx();

	if (ctx == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	/*
	*  Allocate temporary variables.
	*/
	if (!big_alloc(18, t))
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	rr = EC_POINT_new(curve);

	if (rr == NULL)
	{
		big_free(18, t);
		return RESOURCE_NOT_ALLOCATED;
	}

	w = BN_new();

	if (w == NULL)
	{
		big_free(18, t);
		EC_POINT_free(rr);
		return RESOURCE_NOT_ALLOCATED;
	}

	test = EC_POINT_new(curve);

	if (test == NULL)
	{
		big_free(18, t);
		EC_POINT_free(rr);
		BN_free(w);
		return RESOURCE_NOT_ALLOCATED;
	}

	/*
	*  Copy initial part of data to decrypted data.
	*/
	memcpy((unsigned char *)decrypted_data.pointer, data.pointer, length);

	/*
	*  Convert final part of data to R.
	*/
	if (!BN_bin2bn(data.pointer + length + 1, SAKKE_SIZE, t[0])
		|| !BN_bin2bn(data.pointer + length + 1 + SAKKE_SIZE, SAKKE_SIZE, t[1])
		|| !big_ec(rr, t[0], t[1], curve, ctx))
	{
		big_free(18, t);
		EC_POINT_clear_free(rr);
		BN_free(w);
		EC_POINT_free(test);
		return SAKKE_DECRYPT_FAIL;
	}

	if (!sakke_pairing(w, rr, rsk, t[0], t[1], t[2], t[3], t[4], t[5], t[6],
		               t[7], t[8], t[9], t[10], t[11], t[12], t[13], t[14],
					   t[15], t[16], t[17])
		|| !big_binary(w, fixed_buffer(SAKKE_SIZE, buffer)))
	{
		big_free(18, t);
		EC_POINT_clear_free(rr);
		BN_clear_free(w);
		EC_POINT_free(test);
		memset(buffer, 0, SAKKE_SIZE);
		return SAKKE_DECRYPT_FAIL;
	}

	if (!BN_one(t[0]) || !BN_lshift(t[0], t[0], 8 * (int)length)
		|| !sakke_hash(t[0], fixed_buffer(SAKKE_SIZE, buffer),
		               (unsigned char *)data.pointer, t[0], length, t[1])
		|| !big_binary(t[0], fixed_buffer(length, data.pointer)))
	{
		big_free(18, t);
		EC_POINT_clear_free(rr);
		BN_clear_free(w);
		EC_POINT_free(test);
		memset(buffer, 0, SAKKE_SIZE);
		memset((unsigned char *)data.pointer, 0, data.length);
		return SAKKE_DECRYPT_FAIL;
	}

	memset(buffer, 0, SAKKE_SIZE);

	for (i = 0; i != length; ++i)
	{
		((unsigned char *)decrypted_data.pointer)[i] ^= data.pointer[i];
	}

	memcpy((unsigned char *)data.pointer, decrypted_data.pointer, length);
	memcpy((unsigned char *)data.pointer + length, identity.pointer,
		   identity.length);

	if (!sakke_hash(t[0], fixed_buffer(length + identity.length, data.pointer),
		            (unsigned char *)data.pointer, q, SAKKE_SIZE, t[1]))
	{
		big_free(18, t);
		EC_POINT_clear_free(rr);
		BN_clear_free(w);
		EC_POINT_free(test);
		memset((unsigned char *)data.pointer, 0, data.length);
		memset((unsigned char *)decrypted_data.pointer, 0, length);
		return SAKKE_DECRYPT_FAIL;
	}

	success = BN_bin2bn((const unsigned char *)identity.pointer,
		                (int)identity.length, t[1])
		      && BN_mod_mul(t[1], t[0], t[1], q, ctx)
		      && EC_POINT_mul(curve, test, t[1], z, t[0], ctx)
		      && EC_POINT_cmp(curve, rr, test, ctx) == 0;
	big_free(18, t);
	EC_POINT_clear_free(rr);
	BN_clear_free(w);
	EC_POINT_clear_free(test);
	memset((unsigned char *)data.pointer, 0, data.length);
	return success ? SUCCESS : SAKKE_DECRYPT_FAIL;
}


/* ---- ECDSA functions ----------------------------------------------------- */

/*
*  ECDSA verify data.
*  Args: kpak      - KPAK.
*        data      - Data to be verified.
*        signature - Signature.
*  Rets: Standard error code.
*/
static ScErrno ecdsa_verify(const EC_POINT *kpak, Buffer data, Buffer signature)
{
	/*
	*  Variables used below.
	*/
	const EC_GROUP *curve;
	EC_KEY *key;
	unsigned char hash[SHA256_DIGEST_LENGTH];
	int success;

	/*
	*  Verify input formats.
	*/
	if (data.pointer == NULL || data.length == 0 || data.length > INT_MAX)
	{
		return ECDSA_NOT_VERIFIED;
	}

	if (signature.pointer == NULL || signature.length == 0 
		|| signature.length > INT_MAX)
	{
		return ECDSA_NOT_VERIFIED;
	}

	/*
	*  Curve.
	*/
	curve = eccsi_curve();

	if (curve == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	/*
	*  SHA-256 hash of message.
	*/
	if (!sha256(data, hash))
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	/*
	*  Set ECDSA key.
	*/
	key = EC_KEY_new();

	if (key == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	if (!EC_KEY_set_group(key, curve))
	{
		EC_KEY_free(key);
		return RESOURCE_NOT_ALLOCATED;
	}

	if (!EC_KEY_set_public_key(key, kpak))
	{
		EC_KEY_free(key);
		return RESOURCE_NOT_ALLOCATED;
	}

	if (!EC_KEY_check_key(key))
	{
		EC_KEY_free(key);
		return GENERAL_FAILURE;
	}

	/*
	*  ECDSA verify and cleanup.
	*/
	success = ECDSA_verify(0, hash, SHA256_DIGEST_LENGTH, signature.pointer,
		                   (int)signature.length, key);
	EC_KEY_free(key);
	return success > 0 ? SUCCESS : ECDSA_NOT_VERIFIED;
}


/* ---- Other cryptographic functions --------------------------------------- */

/*
*  Base-64 encoding.
*  Args: input    - Data to be base-64 encoded.
*        output_p - *output_p is set to result of base-64 encoding.
*  Rets: Standard error code.
*  Note: Uses result_p to allocate memory for return.
*/
static ScErrno base64(Buffer input, Buffer *output_p)
{
	/*
	*  Output base64 encoded characters.
	*/
	const char * const out
	  = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	/*
	*  Variables used below.
	*/
	const unsigned char *in_p;
	const unsigned char *end_p;
	unsigned char *out_p;
	unsigned char previous = 0;
	size_t i;

	/*
	*  Determine output size and allocate array. Fail if unable to allocate.
	*/
	size_t number = (input.length + 2) / 3;
	output_p->length = 4 * number;
	output_p->pointer = (const unsigned char *)realloc(result_p, output_p->length);

	if (output_p->pointer == 0)
	{
		output_p->length = 0;
		return RESOURCE_NOT_ALLOCATED;
	}

	result_p = (void *)output_p->pointer;

	/*
	*  Setup pointers and do some sanity checking.
	*/
	in_p = input.pointer;

	if (in_p == NULL)
	{
		return INPUT_PTR_NULL;
	}

	if (input.length > LENGTH_LIMIT)
	{
		return INPUT_OUT_OF_BOUNDS;
	}

	end_p = in_p + input.length;
	out_p = (unsigned char *)output_p->pointer;

	/*
    * Loop through input characters.
	*/

	for (i = 0; i != 3 * number; ++i)
	{
		/*
		*  Determine input character to be used, zero if beyond end of input.
		*/
		unsigned char current = 0;

		if (in_p != end_p)
		{
			current = *in_p;
			++in_p;
		}

		/*
		*  Write output character(s) using previous character (not if
		*  start of three character block) and set unused part as
		*  previous character (unless not needed).
		*/
		switch (i % 3)
		{
		case 0:
			*out_p++ = out[current >> 2];
			previous = current & 0x03;
			break;
		case 1:
			*out_p++ = out[(previous << 4) | (current >> 4)];
			previous = current & 0x0F;
			break;
		case 2:
			*out_p++ = out[(previous << 2) | (current >> 6)];
			*out_p++ = out[current & 0x3F];
			break;
		}
	}

	/*
	*  Previous loop does not pad output for missing characters, so done here.
	*  Then return successfully.
	*/
	for (; i != input.length; --i)
	{
		*--out_p = '=';
	}

	return SUCCESS;
}


/*
*  Calculate HMAC.
*  Args: key           - Key.
*        data          - Data to be authenticated.
*        length        - Length HMAC is to be truncated to, before base-64
*                        encoding. A value of zero is interpreted as no
*                        truncation.
*        hmac_base64_p - *hmac_base64_p is set to base-64 encoded HMAC.
*  Rets: Standard error code.
*  Note: Uses result_p to return HMAC result.
*/
static ScErrno hmac(Buffer key, Buffer data, size_t length,
	                Buffer *hmac_base64_p)
{
	/*
	*  Variables used below.
	*/
	unsigned char array[SHA256_DIGEST_LENGTH];
	Buffer buffer = fixed_buffer(SHA256_DIGEST_LENGTH, array);
	unsigned int buffer_length = SHA256_DIGEST_LENGTH;

	/*
	*  Check for invalid pointers.
	*/
	if (key.pointer == NULL || data.pointer == NULL || hmac_base64_p == NULL)
	{
		return INPUT_PTR_NULL;
	}

	/*
	*  Verify truncation length.
	*/
	if (length > SHA256_DIGEST_LENGTH || data.length > LENGTH_LIMIT
		|| key.length > MAX_HMAC_KEY_SIZE || buffer.length > LENGTH_LIMIT)
	{
		return INPUT_OUT_OF_BOUNDS;
	}

	/*
	*  Calculate HMAC including initialising and terminating HMAC object.
	*/
	buffer.pointer = HMAC(EVP_sha256(), key.pointer, (int)key.length,
		                  data.pointer, data.length,
		                  (unsigned char *)buffer.pointer, &buffer_length);

	if (buffer.pointer == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	/*
	*  Set possibly truncated buffer length.
	*/
	buffer.length = length ? length : SHA256_DIGEST_LENGTH;

	/*
	*  Base 64 encoding.
	*/
	return base64(buffer, hmac_base64_p);
}


/* Implements the PRF (Pseudo-Random Function) defined in Section 4.1.4 of
*  RFC 3830, except modified as described in Section 6.1 of RFC 6043, to use
*  SHA-256 rather than SHA-1.
*
*  May be used for KEMAC group communications, or SRTP key/salt calculation.
*
*  The KEMAC header described in Appendix D.3 of ETSI TS 33.303 version 12.3.0
*  Release 12, para 7, references AES-CM-128 as specified in RFC 3830. It also
*  references section 4.1.4. of the same RFC, so we may consider we are dealing
*  with a preshared key (i.e. the GMK). Section 4.1.4. of RFC 3830 also details
*  the attributes passed to the PRF. One of the attributes specified (put into
*  the label) identifies the type of key to be generated, see rfc3830_label.
*
*  Args: inKey     The key, for MS group communications the GMK (input).
*        label     Label, of the form constant || 0xff || csb-id || RAND (input).
*        outKey    The encryption, salt or authentication key (length is input,
*                  key is output).
*  Rets: Standard error code.
*/
static ScErrno rfc3830_PRF(Buffer inKey, Buffer label, Buffer outKey)
{
	/*
	*  Variables used below. Note that MAC_SIZE is that of HMAC-SHA-256, and
	*  is greater than or equal to all allowed key sizes.
	*/
	HMAC_CTX ctx;
	unsigned char hmac[MAC_SIZE];
	unsigned int length;

	/*
	*  Verify input key length.
	*/
	if (inKey.length != PRF_KEY_SIZE)
	{
		return PRF_FAIL_IN_KEY_LEN;
	}

	/*
	*  Output key length must be as for AES-128 salt or key size or AES-256
	*  key size.
	*/
	if (outKey.length != AES_SALT_SIZE && outKey.length != AES_KEY_SIZE
		&& outKey.length != AES_256_KEY_SIZE)
	{
		return PRF_FAIL_OUT_KEY_LEN;
	}

	/* As the the input key is going to be 16 bytes we do not need to do the
	*  splitting described in RFC 3830 Section 4.1.2. Effectively the algorithm
	*  now becomes an HMAC of input key and label, using SHA-256.
	*/
	HMAC_CTX_init(&ctx);
	HMAC_Init_ex(&ctx, inKey.pointer, inKey.length, EVP_sha256(), NULL);
	HMAC_Update(&ctx, label.pointer, label.length);
	HMAC_Final(&ctx, hmac, &length);
	HMAC_CTX_cleanup(&ctx);
	memcpy((unsigned char *)outKey.pointer, hmac, outKey.length);
	return SUCCESS;
}


/*
*  Create label as defined by RFC 3830 for KEMAC.
*  Args: keyAuthOrSaltType - Defines key or salt type, values as
*                            defined in RFF 3830.
*        csId              - CS-ID (crypto session ID).
*        csbId             - CSB-ID (crypto session bundle ID).
*        rand              - RAND (random value). Length assumed to
*                            be at most - should equal - KEMAC_RAND_SIZE,
*                            zeros used for unprovide bytes.
*        label             - Label to be written. Assumed to have
*                            indicated length (not verified by type
*                            system) and be all zeros on entry.
*/
static void rfc3830_label(Buffer keyAuthOrSaltType, uint8_t csId,
						  uint32_t csbId, Buffer rand,
						  unsigned char label[KEMAC_LABEL_SIZE])
{
	/*
	*  Create label, see RFC 3830 Section 4.1.4.
	*  label = Key-Type-Constant(4) + CS-ID(1) + CSB-ID(4) + RAND(16).
	*/
	memcpy(&label[KEMAC_CONSTANT_LOCATION], keyAuthOrSaltType.pointer,
		   keyAuthOrSaltType.length);
	label[KEMAC_CS_ID_LOCATION] = KEMAC_CS_ID;
	write32(&label[KEMAC_CSB_ID_LOCATION], csbId);
	memcpy(&label[KEMAC_RAND_LOCATION], rand.pointer, rand.length);
}


/*
*  Create IV as defined by RFC 3830 for KEMAC.
*  Args: csbId     - CSB-ID (crypto session bundle ID).
*        timestamp - Timestamp, assumed valid on entry.
*        salt      - Salt.
*        label     - Label to be used. Assumed to have indicated length
*                    (not verified by typex system).
*        iv        - IV to be set. Assumed to have indicated length (not
*                    verified by type system) and be all zeros on entry.
*/
static void rfc3830_iv(uint32_t csbId, uint64_t timestamp, Buffer salt,
					   unsigned char label[KEMAC_LABEL_SIZE],
					   unsigned char iv[AES_IV_SIZE])
{
	/*
	*  Variables used below.
	*/
	int i;

	/*
	*  Create IV (Initialisation Vector), see RFC 3830 Section 4.2.3.
	*  IV = (Salt XOR (0x0000 || CSB ID || Timestamp)) || 0x0000.
	*  Note IV is set to all zeros as initialised above.
	*/
	write32(&iv[KEMAC_SALT_CSB_ID_LOCATION], csbId);
	write64(&iv[KEMAC_SALT_TIMESTAMP_LOCATION], timestamp);

	for (i = 0; i < AES_SALT_SIZE; ++i)
	{
		iv[i] ^= salt.pointer[i];
	}
}


/*
*  AES key wrap decryption of ciphertext.
*  Args: key          - Key. Length (16 or 32) indicates AES option
*                       (AES-128 or AES-256). Assumed valid.
*        ciphertext   - Ciphertext to be decrypted. Must be a multiple
*                       of 64 bits (8 bytes) in length.
*        plaintext_p  - *plaintext is set to decrypted plaintext,
*                       dynamically allocated in this function.
*  Rets: Standard error code.
*  Note: The calling function must free plaintext_p->pointer after
*        a successful return. It is recommended that the calling
*        function first overwrites with zeros after use, function
*        purge_buffer may be used.
*/
static ScErrno kw_decrypt(Buffer key, Buffer ciphertext, Buffer *plaintext_p)
{
	/*
	*  Variables used below. Includes defined initialisation vector.
	*/
	AES_KEY aes_key;
	const unsigned char iv[AES_LENGTH]
	  = { 0xA6, 0xA6, 0xA6, 0xA6, 0xA6, 0xA6, 0xA6, 0xA6 };
	int length;

	/*
	*  Verify ciphertext.
	*/
	if (ciphertext.pointer == NULL || ciphertext.length == 0
		|| ciphertext.length > UINT_MAX)
	{
		return GENERAL_FAILURE;
	}

	if (ciphertext.length % AES_LENGTH != 0)
	{
		return KEY_UNWRAP_FAIL;
	}

	/*
	*  Initialise OpenSSL AES key.
	*/
	if (AES_set_decrypt_key(key.pointer, 8 * (int)key.length, &aes_key))
	{
		return KEY_UNWRAP_FAIL;
	}

	/*
	*  Allocate plaintext memory, fail if unable to allocate. Note that
	*  additional length is working length, actual plaintext length is
	*  shorter, as set below.
	*/
	plaintext_p->length = ciphertext.length + AES_LENGTH;
	plaintext_p->pointer = (const unsigned char *)malloc(plaintext_p->length);

	if (plaintext_p->pointer == NULL)
	{
		plaintext_p->length = 0;
		return RESOURCE_NOT_ALLOCATED;
	}

	/*
	*  OpenSSL implementation of AES key unwrap.
	*/
	length = AES_unwrap_key(&aes_key, iv, (unsigned char *)plaintext_p->pointer,
		                    ciphertext.pointer,
							(unsigned int)ciphertext.length);

	if (length <= 0)
	{
		purge_buffer(plaintext_p);
		return KEY_UNWRAP_FAIL;
	}

	/*
	*  Successful decryption and key unwrap. Set plaintext length to remove
	*  verified initialisation vector.
	*/
	plaintext_p->length = length;
	return SUCCESS;
}


/*
*  Verify public encryption key from certificate.
*  Args: key - Key to be verified.
*  Rets: True if key is valid, false if not.
*/
static Boolean verify_pub_enc_key(Buffer key)
{
	EC_POINT *point = sakke_point(key, true);

	if (point != NULL)
	{
		EC_POINT_clear_free(point);
		return true;
	}
	else
	{
		return false;
	}
}


/*
*  Verify public authentication key from certificate.
*  Args: key - Key to be verified.
*  Rets: True if key is valid, false if not.
*/
static Boolean verify_pub_auth_key(Buffer key)
{
	EC_POINT *point = eccsi_point(key, true);

	if (point != NULL)
	{
		EC_POINT_clear_free(point);
		return true;
	}
	else
	{
		return false;
	}
}


/* ---- Security context functions ------------------------------------------ */

/*
*  Update array of security context data references to add new unallocated
*  entry.
*  Args: extended_p - *extended_p is set to allocated array of security
*                     context data references on successful exit. Must be
*                     freed by calling function when successful.
*  Rets: Standard error code.
*/
static ScErrno new_security_context(Buffer *extended_p)
{
	/*
	*  Variables used below.
	*/
	Buffer buffer;
	ScErrno error;
	size_t i;

	/*
	*  If unable to get array of security context references then assume this
	*  is the first security context to be added, but verify this.
	*/
	if (GetData(KEYSTORE_FIRST, &buffer))
	{
		/*
		*  Variable used below.
		*/
		DataRef data_ref;

		/*
		*  Set buffer to single unallocated value to store.
		*/
		extended_p->length = sizeof(DataRef);
		extended_p->pointer = (const unsigned char *)malloc(sizeof(DataRef));

		if (extended_p->pointer == 0)
		{
			return RESOURCE_NOT_ALLOCATED;
		}

		*(DataRef *)extended_p->pointer = 0;

		/*
		*  Store buffer, which must be first stored data, otherwise fail.
		*/
		error = StoreSecureData(*extended_p, true, &data_ref);

		if (error)
		{
			free((void *)extended_p->pointer);
			return error;
		}

		if (data_ref != KEYSTORE_FIRST)
		{
			PurgeData(data_ref);
			free((void *)extended_p->pointer);
			return GENERAL_FAILURE;
		}
	}
	else
	{
		/*
		*  Create extended buffer with one extra data reference.
		*/
		extended_p->length = buffer.length + sizeof(DataRef);
		extended_p->pointer = (const unsigned char *)malloc(buffer.length);

		if (extended_p->pointer == 0)
		{
			extended_p->length = 0;
			return RESOURCE_NOT_ALLOCATED;
		}

		/*
		*  Copy existing data references and set extra data reference to
		*  zero.
		*/
		memcpy((void *)extended_p->pointer, buffer.pointer, buffer.length);
		*(DataRef *)(extended_p->pointer + buffer.length) = 0;

		/*
		*  Record extended array of data references.
		*/
		error = UpdateData(KEYSTORE_FIRST, *extended_p);

		if (error)
		{
			purge_buffer(extended_p);
			return error;
		}
	}

	/*
	*  Successful result, with stored array updated and allocated array
	*  (to update and store again) returned. Also sets so far unused
	*  context array.
	*/
	for (i = 0; i != CONTEXT_LENGTH; ++i)
	{
		context_array[i] = 0;
	}

	return SUCCESS;
}


/*
*  Update stored security contexts array.
*  Args: contexts   - Buffer containing security contexts as to be updated.
*                     (Last entry - should be zero - to be overwritten.)
*        new_context - New security context to add.
*  Rets: Standard error code.
*/
static ScErrno update_security_contexts(Buffer contexts, SecCtx new_context)
{
	((DataRef *)(contexts.pointer + contexts.length))[-1] = new_context;
	return UpdateData(KEYSTORE_FIRST, contexts);
}


/*
*  Remove security context from stored array.
*  Args: context - Context to be removed.
*  Rets: Standard error code.
*/
static ScErrno remove_security_context(SecCtx context)
{
	/*
	*  Variables used below.
	*/
	DataRef *data_ref_p;
	size_t number;
	size_t i;

	/*
	*  Get array of all security context data references. Fail if cannot.
	*/
	Buffer buffer;
	ScErrno error = GetData(KEYSTORE_FIRST, &buffer);

	if (error)
	{
		return error;
	}

	/*
	*  Find matching security context, and if found, updated
	*  stored context array for use outside this function,
	*  set security context to unallocated and resave.
	*/
	data_ref_p = (DataRef *)buffer.pointer;
	number = buffer.length / sizeof(DataRef);

	for (i = 0; i != number; ++i)
	{
		if (data_ref_p[i] == context)
		{
			Buffer context_buf;
			DataRef *context_p;
			error = GetData(context, &context_buf);

			if (error)
			{
				return error;
			}

			context_p = (DataRef *)context_buf.pointer;

			for (i = 0; i != CONTEXT_LENGTH; ++i)
			{
				context_array[i] = context_p[i];
			}

			data_ref_p[i] = 0;
			return UpdateData(KEYSTORE_FIRST, buffer);
		}
	}

	/*
	*  Security context not found, fail.
	*/
	return RESOURCE_NOT_EXIST;
}


/*
*  Get context, verify and set static context_array.
*  Args: context - Context reference.
*  Rets: Standard error code.
*/
static ScErrno get_context(SecCtx context)
{
	/*
	*  Variables used below.
	*/
	Buffer buffer;
	size_t number;
	size_t i;
	ScErrno error;

	/*
	*  Basic verification of security context.
	*/
	if (context == 0)
	{
		return RESOURCE_NOT_EXIST;
	}

	/*
	*  Get buffer of all security contexts.
	*/
	error = GetData(KEYSTORE_FIRST, &buffer);

	if (error)
	{
		return error;
	}

	/*
	*  Verify context is one previously allocated.
	*/
	number = buffer.length / sizeof(DataRef);

	for (i = 0; i != number; ++i)
	{
		if (((DataRef *)buffer.pointer)[i] == context)
		{
			break;
		}
	}

	if (i == number)
	{
		return RESOURCE_NOT_EXIST;
	}

	/*
	*  Get context data, fail if invalid context, including verification
	*  of array size.
	*/
	error = GetData(context, &buffer);

	if (error)
	{
		return error;
	}

	/*
	*  Verify if context array has correct size.
	*/
	if (buffer.length != sizeof(context_array))
	{
		return RESOURCE_NOT_EXIST;
	}

	/*
	*  Set context array and return success.
	*/
	memcpy(context_array, buffer.pointer, buffer.length);
	return SUCCESS;
}


/*
*  Return buffer referring to security context array.
*  Rets: Buffer referring to security context array.
*/
static Buffer context_buffer(void)
{
	return fixed_buffer(sizeof(context_array), context_array);
}


/*
*  Reset security context.
*  Args: context - Security context.
*  Rets: Standard error code.
*/
static ScErrno reset_context(SecCtx context)
{
	return UpdateData(context, context_buffer());
}


/* ---- Other data reference functions -------------------------------------- */

/*
*  Get array of data references, including verification.
*  Args: index    - Context array index. Assumed valid.
*        number_p - Unless null, on successful exit points to array
*                   size, and on unsuccessful exit points to zero.
*  Rets: Pointer to array of data references, or null if not
*        valid.
*  Note: Assumes static array context_array set on entry.
*/
static DataRef *get_array(size_t index, size_t *number_p)
{
	/*
	*  Get array of data references. Fail if not found, or if array has
	*  inappropriate length.
	*/
	Buffer buffer;

	if (GetData(context_array[index], &buffer))
	{
		if (number_p != NULL)
		{
			*number_p = 0;
		}

		return NULL;
	}

	if (buffer.length % sizeof(DataRef) != 0)
	{
		if (number_p != NULL)
		{
			*number_p = 0;
		}

		return NULL;
	}

	/*
	*  Set number of entries in array, if requested, and return
	*  pointer to array, indicating success.
	*/
	if (number_p != NULL)
	{
		*number_p = buffer.length / sizeof(DataRef);
	}

	return (DataRef *)buffer.pointer;
}


/*
*  Get array of data references, including verification, and make
*  possibly extended copy.
*  Args: index    - Context array index. Assumed valid.
*        extend   - Number of data references to extend copy by.
*        number_p - Unless null, on successful exit points to
*                   unextended array size, and on unsuccessful exit
*                   points to zero.
*  Rets: Buffer indicating newly allocated array of data references.
*        Pointer is null on failed exit.
*  Note: Assumes static array context_array set on entry.
*/
static Buffer get_array_copy(size_t index, size_t extend, size_t *number_p)
{
	/*
	*  Variable used below, initialised for convenience.
	*/
	Buffer buffer = fixed_buffer(0, NULL);

	/*
	*  Get pointer to array, and exit if unsuccessful.
	*/
	size_t number;
	const DataRef * const pointer = get_array(index, &number);

	if (pointer == NULL)
	{
		if (number_p != NULL)
		{
			*number_p = 0;
		}

		return buffer;
	}

	/*
	*  Allocate new array, and copy old array. Return unsuccessful
	*  if unable to allocate.
	*/
	buffer.length = (number + extend) * sizeof(DataRef);
	buffer.pointer = (const unsigned char *)malloc(buffer.length);

	if (buffer.pointer == NULL)
	{
		if (number_p != NULL)
		{
			*number_p = 0;
		}

		buffer.length = 0;
		return buffer;
	}

	/*
	*  Copy old array (leaving new part of array undefined) and
	*  return pointer to new array.
	*/
	memcpy((void *)buffer.pointer, pointer, number * sizeof(DataRef));

	if (number_p != NULL)
	{
		*number_p = number;
	}

	return buffer;
}


/*
*  Extend an array of data references held by context by storing
*  additional item of data. Array may be empty.
*  Args: context - Security context reference. Not used if zero.
*        index   - Index within security context array of array
*                  to be updated.
*        data    - Data to be added to array.
*  Rets: Standard error code.
*  Note: Assumes static array context_array set on entry.
*/
static ScErrno extend_array(SecCtx context, size_t index, Buffer data)
{
	/*
	*  Variable used below.
	*/
	ScErrno error;

	/*
	*  Separate cases for first and subsequent added references.
	*/
	if (context_array[index] == 0)
	{
		/*
		*  Use single reference as array of length one to avoid dynamic
		*  allocation.
		*/
		DataRef array_ref;
		Buffer array;
		array.length = sizeof(DataRef);
		array.pointer = (const unsigned char *)&array_ref;

		/*
		*  Store data. If fail then nothing changed.
		*/
		error = StoreSecureData(data, true, &array_ref);

		if (error)
		{
			return error;
		}

		/*
		*  Store reference array. If fail then nothing changed.
		*/
		error = StoreSecureData(array, true, &context_array[index]);

		if (error)
		{
			PurgeData(array_ref);
			return error;
		}

		/*
		*  Update context, unless zero. If fail then ensure remains
		*  consistent.
		*/
		if (context != 0)
		{
			error = reset_context(context);

			if (error)
			{
				if (PurgeData(context_array[index]) == SUCCESS)
				{
					context_array[index] = 0;
					PurgeData(array_ref);
				}

				return error;
			}
		}
	}
	else
	{
		/*
		*  Variables used below.
		*/
		DataRef *data_ref_p;
		size_t array_index;

		/*
		*  Get array of data references, lengthened by one in
		*  case needed.
		*/
		size_t length;
		Buffer buffer = get_array_copy(index, 1, &length);

		if (buffer.pointer == NULL)
		{
			return RESOURCE_NOT_ALLOCATED;
		}

		data_ref_p = (DataRef *)buffer.pointer;

		/*
		*  Determine if any unused data references, On finishing
		*  the loop, whether exited early or not, array_index will be
		*  the array position to use. In case of early exit, shorten
		*  the used size of buffer.
		*/
		for (array_index = 0; array_index != length; ++array_index)
		{
			if (data_ref_p[array_index] == 0)
			{
				buffer.length -= sizeof(DataRef);
				break;
			}
		}

		/*
		*  Store data. If fail then nothing changed.
		*  If successful then array is updated.
		*/
		error = StoreSecureData(data, true, &data_ref_p[array_index]);

		if (error)
		{
			free((void *)buffer.pointer);
			return error;
		}

		/*
		*  Store array. If fail then nothing changed. Note no need
		*  to change context.
		*/
		error = UpdateData(context_array[index], buffer);

		if (error)
		{
			PurgeData(data_ref_p[array_index]);
			free((void *)buffer.pointer);
			return error;
		}

		/*
		*  Dynamically allocated memory no longer needed.
		*/
		free((void *)buffer.pointer);
	}

	return SUCCESS;
}


/*
*  Purge subarray of context array. Updates locally stored
*  context array, but does not resave it.
*  Args: index - Context array index. Assumed valid.
*  Rets: True if purge succeeded, false if did not.
*  Note: Assumes static array context_array set on entry.
*/
static Boolean purge_array(size_t index)
{
	/*
	*  Variables used below.
	*/
	size_t number;
	size_t i;

	/*
	*  Get subarray of data references.
	*/
	Buffer buffer = get_array_copy(index, 0, &number);
	DataRef * const data_ref_p = (DataRef *)buffer.pointer;

	/*
	*  Purge array. Fail if unable to purge.
	*/
	if (!PurgeData(context_array[index]))
	{
		free((void *)buffer.pointer);
		return false;
	}

	context_array[index] = 0;

	/*
	*  Loop through data references, purging. If an individual
	*  purge fails then may leave allocated memory, but data
	*  structure will be consistent.
	*/
	for (i = 0; i != number; ++i)
	{
		PurgeData(data_ref_p[i]);
		data_ref_p[i] = 0;
	}

	/*
	*  Free memory and return success.
	*/
	free((void *)buffer.pointer);
	return true;
}


/*
*  Purge data reference from subarray of context array.
*  Args: index     - Context array index. Assumed valid.
*        subindex  - Index into subarray. Assumed valid.
*  Rets: True if succeeds, false if fails, and data is
*        not purged. With valid indices the latter should
*        not ever occur. If it does then no data is
*        removed.
*  Note: Assumes static array context_array set on entry.
*/
static Boolean purge_array_item(size_t index, size_t subindex)
{
	/*
	*  Get copy of subarray of data references.
	*/
	size_t number;
	Buffer buffer = get_array_copy(index, 0, &number);
	DataRef *data_ref_p = (DataRef *)buffer.pointer;
	const size_t data_ref = data_ref_p[subindex];
	data_ref_p[subindex] = 0;

	/*
	*  Update stored subarray.
	*/
	if (UpdateData(context_array[index], buffer))
	{
		free((void *)buffer.pointer);
		return false;
	}

	/*
	*  Purge single item. If fails then not removed, but structure
	*  remains consistent.
	*/
	PurgeData(data_ref);

	/*
	*  Free copy of subarray, and return success.
	*/
	free((void *)buffer.pointer);
	return true;
}


/* ---- String functions ---------------------------------------------------- */

/*
*  Convert number to string.
*  Args: number - Number. Assumed to not require more than length
*                 decimal digits.
*        length - Number of decimal digits in output, with leading
*                 zeros if required. Assumed to not be more than
*                 10 (sufficient for 32 bit number).
*  Rets: String, valid until this function is called again.
*/
static String string_number(uint32_t number, size_t length)
{
	/*
	*  Buffer to store string digits, persistent after function returns.
	*/
	static char string[10];

	/*
	*  Loop from least significant digit (length - 1) to most significant
	*  digit (0). number is adjusted as used.
	*/

	size_t i;

	for (i = length; i-- != 0;)
	{
		string[i] = '0' + number % 10;
		number /= 10;
	}

	/*
	*  Return string indicating used part of buffer.
	*/
	return fixed_string(length, string);
}


/*
*  Compare two strings for equality.
*  Args: string1 - First string.
*        string2 - Second string.
*  Rets: True if strings are equal, false if not. If either string is
*        invalid (has null pointer but nonzero length) then returns
*        false.
*/
static Boolean string_equal(String string1, String string2)
{
	return (string1.pointer != NULL || string1.length == 0)
		   && (string2.pointer != NULL || string2.length == 0)
		   && string1.length == string2.length
		   && (string1.length == 0
		       || memcmp(string1.pointer, string2.pointer, string1.length)
			      == 0);
}


/*
*  Create string that replaces indicated substring by replacement.
*  Args: string - String to have replacements applied.
*        remove - String to be removed (substring of string).
*        replace - Replacement string.
*  Rets: Length of string with replacements, zero if fail.
*        String with replacements is set as result_p.
*/
size_t string_replace(String string, String remove, String replace)
{
	const size_t length = string.length - remove.length + replace.length;
	const size_t prefix = remove.pointer - string.pointer;
	const size_t suffix = string.length - prefix - remove.length;
	void *pointer = realloc(result_p, length);

	if (pointer == NULL)
	{
		return 0;
	}

	result_p = pointer;
	memcpy(result_p, string.pointer, prefix);
	memcpy((unsigned char *)result_p + prefix, replace.pointer, replace.length);
	memcpy((unsigned char *)result_p + length - suffix,
		   string.pointer + string.length - suffix, suffix);
	return length;
}


/* ---- StringList functions ------------------------------------------------ */

/*
*  Initialise string list. To be followed by calls to add_list.
*  Args: number   - Number of strings to be in list.
*        list_p   - *list_p is string list to be initialised (assumed valid).
*        length_p - Required to be passed to subsequent calls of add_list.
*  Rets: Standard error code.
*  Note: Uses result_p, which must not be used again until list is complete
*        and used.
*/
static ScErrno create_list(size_t number, StringList *list_p, size_t *length_p)
{
	/*
	*  Variables used below.
	*/
	void *pointer;
	size_t i;

	/*
	*  Set length of string list, and initialise result_p which will contain
	*  both the list of String objects, and the strings that they indicate.
	*  If length is zero set pointer to null and exit successfully.
	*/
	list_p->length = number;

	if (number == 0)
	{
		list_p->pointer = 0;
		return SUCCESS;
	}

	*length_p = number * sizeof(String);
	pointer = realloc(result_p, *length_p);

	if (pointer == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	result_p = pointer;

	/*
	*  Initialise string list array. Sets all to unused, to allow for possibly
	*  not calling add_list sufficient times. Return success.
	*/
	list_p->pointer = (String *)result_p;

	for (i = 0; i != number; ++i)
	{
		((String *)list_p->pointer)[i] = fixed_string(0, NULL);
	}

	return SUCCESS;
}


/*
*  Add string to string list, used multiple times after create_list.
*  Args: index    - Index of string to be added to list (assumed valid).
*        string   - String to be added to list (assumed valid).
*        list_p   - *list_p is string list being updated (assumed valid).
*        length_p - From previous calls of create_list/add_list
*                   (assumed valid).
*  Rets: Standard error code.
*  Note: Uses result_p, which must not be used again until list is complete
*        and used.
*/
static ScErrno add_list(size_t index, String string, StringList *list_p,
	                    size_t *length_p)
{
	/*
	*  Variable used below.
	*/
	void *pointer;
	String *str_ptr;
	const char *char_ptr;
	size_t i;

	/*
	*  Update allocated memory.
	*/
	size_t length = *length_p;
	*length_p += string.length;
	pointer = realloc(result_p, *length_p);

	if (pointer == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	result_p = pointer;

	/*
	*  Set string list pointer and add string to end of allocated memory.
	*/
	list_p->pointer = (const String *)result_p;
	memcpy((unsigned char *)result_p + length, string.pointer, string.length);

	/*
	*  Set new string length and update all pointers (memory may have been
	*  reallocated). Return success.
	*/
	str_ptr = (String *)result_p;
	str_ptr[index].length = string.length;
	length = 0;
	char_ptr = (const char *)(str_ptr + list_p->length);

	for (i = 0; i <= index; ++i)
	{
		str_ptr[i].pointer = char_ptr;
		char_ptr += str_ptr[i].length;
	}

	return SUCCESS;
}


/* ---- Timestamp functions ------------------------------------------------- */

/*
*  Determine if data has correct format for timestamp.
*  Args: timestamp - Possible timestamp.
*  Rets: True if timestamp, false if not.
*/
static Boolean is_timestamp(String timestamp)
{
	/*
	*  Variable used below.
	*/
	size_t i;

	/*
	*  Data must have correct length, else fail.
	*/
	if (timestamp.length != TIMESTAMP_LENGTH)
	{
		return false;
	}

	/*
	*  All data characters must be uppercase hexadecimal, else fail.
	*/
	for (i = 0; i != TIMESTAMP_LENGTH; ++i)
	{
		if (memchr("0123456789ABCDEF", timestamp.pointer[i], 16) == 0)
		{
			return false;
		}
	}

	/*
	*  No test fails, is timestamp.
	*/
	return true;
}


/*
*  Determine if one timestamp is less than another.
*  Note that assumes that both timestamps use upper case hexadecimal,
*  and that '0' < ... < '9' < 'A' < 'B' < 'C' < 'D' < 'E' < 'F'.
*  (The digits part is guaranteed by C, the remainder is true for ASCII.)
*  Args: timestamp1 - First timestamp (assumed valid).
*        timestamp2 - Second timestamp (assumed valid).
*  Rets: true if timestamp1 < timestamp2, false otherwise.
*/
static Boolean timestamp_less(String timestamp1, String timestamp2)
{
	return strncmp(timestamp1.pointer, timestamp2.pointer, TIMESTAMP_LENGTH)
		   < 0;
}


/*
*  Convert timestamp to number of seconds. Does not apply any offset.
*  Args: timestamp - Timestamp (assumed valid).
*  Rets: Number of seconds
*/
static uint32_t timestamp_seconds(String timestamp)
{
	const char * const digits = "0123456789ABCDEF";
	uint32_t seconds = 0;
	size_t i;

	for (i = 0; i != 8; ++i)
	{
		const char * const p
		  = (const char *)strchr(digits, timestamp.pointer[i]);

		if (p == 0)
		{
			return false;
		}

		seconds <<= 4;
		seconds += (uint32_t)(p - digits);
	}

	return seconds;
}


/*
*  Convert timestamp to 64 bits (32 bit number of seconds, 32 bit fractional
*  seconds). Does not apply any offset.
*  Args: timestamp - Timestamp (assumed valid).
*  Rets: Number of seconds
*/
static uint64_t timestamp64(String timestamp)
{
	const char * const digits = "0123456789ABCDEF";
	uint64_t time = 0;
	size_t i;

	for (i = 0; i != 16; ++i)
	{
		const char *p = (const char *)strchr(digits, timestamp.pointer[i]);

		if (p == 0)
		{
			return false;
		}

		time <<= 4;
		time += (uint8_t)(p - digits);
	}

	return time;
}


/*
*  Verify timestamp by comparison with current timestamp.
*  Args: timestamp_now - Current timestamp (assumed valid).
*        timestamp     - Then timestamp to be verified (assumed valid).
*	     past_window   - Maximum time 'then' is allowed to be before 'now'
*                        (ignored if negative)
*        future_window - Maximum time 'then' is allowed to be after 'now'
*                        (ignored if negative)
*  Rets: Standard error code.
*/
static ScErrno timestamp_verify(String timestamp_now, String timestamp,
								int32_t past_window, int32_t future_window)
{
	const uint32_t now  = timestamp_seconds(timestamp_now);
	const uint32_t then = timestamp_seconds(timestamp);

	if (then < now && past_window >= 0)
	{
		if (now - then > (uint32_t)past_window)
		{
			return TIMESTAMP_OLD;
		}
	}
	else if (now < then && future_window >= 0)
	{
		if (then - now > (uint32_t)future_window)
		{
			return TIMESTAMP_FUTURE;
		}
	}

	return SUCCESS;
}


/* ---- Certificate functions ----------------------------------------------- */

/*
*  Get certificate field.
*  Args: certificate - Certificate (assumed valid).
*        field       - Field number (assumed valid).
*  Rets: Buffer describing field.
*/
static Buffer get_field(Buffer certificate, size_t field)
{
	/*
	*  Pointer to advance through certificate.
	*/
	const unsigned char *pointer = certificate.pointer;

	/*
	*  Loop through fields before required field.
	*/
	size_t i;

	for (i = 0; i != field; ++i)
	{
		pointer += 2 + 256 * pointer[0] + pointer[1];
	}

	/*
	*  Return required field.
	*/
	return fixed_buffer(256 * pointer[0] + pointer[1], pointer + 2);
}


/*
*  Get certificate domain list field.
*  Args: certificate - Certificate (assumed valid).
*        number_p    - *number_p is set to number of domains.
*  Rets: Buffer describing all domain fields.
*/
static Buffer get_domain_field(Buffer certificate, size_t *number_p)
{
	/*
	*  Pointer to advance through certificate.
	*/
	const unsigned char *pointer = certificate.pointer;

	/*
	*  Loop through fields before required field.
	*/
	size_t i;

	for (i = 0; i != KMS_DOMAIN_LIST_FIELD; ++i)
	{
		pointer += 2 + 256 * pointer[0] + pointer[1];
	}

	/*
	*  Set number of domains and return buffer covering all domains.
	*/
	*number_p = 256 * pointer[0] + pointer[1];
	pointer += 2;
	return fixed_buffer(certificate.pointer + certificate.length - pointer,
		                pointer);
}


/*
*  Verify certificate field (or subfield of final field) as length-value
*  format. Length is two bytes, most significant first.
*  Args: length_p  - *length_p is remaining length of certificate,
*                    updated on successful exit (undefined if
*                    unsuccessful).
*        pointer_p - *pointer_p is pointer to next unused byte of
*                    certificate, updated on exit (undefined if
*                    unsuccessful.
*        field_p   - *field_p indicates the verified field, if
*                    successful.
*  Rets: True if field is verified, false if not verified.
*/
static Boolean verify_field(size_t *length_p, const unsigned char **pointer_p,
	                        Buffer *field_p)
{
	/*
	*  Length bytes must both be present.
	*/
	if (*length_p < 2)
	{
		return false;
	}

	/*
	*  Read length bytes, and advance past them.
	*/
	field_p->length = 256 * (*pointer_p)[0] + (*pointer_p)[1];
	*length_p -= 2;
	*pointer_p += 2;
	field_p->pointer = *pointer_p;

	/*
	*  Field length may not exceed certificate length.
	*/
	if (field_p->length > *length_p)
	{
		return false;
	}

	/*
	*  Advance past field and return success.
	*/
	*length_p -= field_p->length;
	*pointer_p += field_p->length;
	return true;
}


/*
*  Get UID given URI, timestamp and (presumed matching) certificate.
*  Args: certificate - Certificate, assumed valid.
*        timestamp   - Timestamp.
*        uri         - User URI.
*  Rets: User ID, null pointer if not determined.
*/
static String get_uid(Buffer certificate, String timestamp, String uri)
{
	/*
	*  Get format string from certificate.
	*/
	const String format
	  = to_string(get_field(certificate, USER_ID_FORMAT_FIELD));

	/*
	*  Determine user ID. Return success or failure as appropriate.
	*/
	return user_id(timestamp, uri, format);
}


/*
*  Determine if certificate is valid.
*  Args: certificate - Certificate to be verified.
*        external    - If certificate is external
*                      (otherwise root).
*  Rets: Standard error code.
*/
static ScErrno verify_certificate(Buffer certificate, Boolean external)
{
	/*
	*  Variables used below.
	*/
	size_t number;
	size_t i;

	/*
	*  Remaining length of certificate, and pointer to where so far
	*  reached in certificate.
	*/
	size_t length = certificate.length;
	const unsigned char *pointer = certificate.pointer;

	/*
	*  Certificate sanity check.
	*/
	if (!check_buffer(certificate))
	{
		return INPUT_BUFFER_INVALID;
	}

	/*
	*  Loop through all fields except final field (which has different format).
	*/
	for (i = 0; i != KMS_DOMAIN_LIST_FIELD; ++i)
	{
		/*
		*  Verify field is of valid format.
		*/
		Buffer field;

		if (!verify_field(&length, &pointer, &field))
		{
			return CERTIFICATE_INVALID;
		}

		/*
		*  Verify specific fields.
		*/
		if (i == ROLE_FIELD)
		{
			if (external)
			{
				if (field.length != 8
					|| memcmp(field.pointer, "External", 8) != 0)
				{
					return CERTIFICATE_INVALID;
				}
			}
			else
			{
				if (field.length != 4 || memcmp(field.pointer, "Root", 4) != 0)
				{
					return CERTIFICATE_INVALID;
				}
			}
		}
		else if (i == VALID_FROM_FIELD || i == VALID_TO_FIELD)
		{
			if (!is_timestamp(to_string(field)))
			{
				return CERTIFICATE_INVALID;
			}
		}
		else if (i == REVOKED_FIELD)
		{
			if (field.length != 5 || memcmp(field.pointer, "false", 5) != 0)
			{
				return CERTIFICATE_INVALID;
			}
		}
		else if (i == PUB_ENC_KEY_FIELD)
		{
			if (!verify_pub_enc_key(field))
			{
				return CERTIFICATE_INVALID;
			}
		}
		else if (i == PUB_AUTH_KEY_FIELD)
		{
			if (!verify_pub_auth_key(field))
			{
				return CERTIFICATE_INVALID;
			}
		}
	}

	/*
	*  Final field. First verify number of sub-fields, and advance past it.
	*/
	if (length < 2)
	{
		return CERTIFICATE_INVALID;
	}

	number = 256 * pointer[0] + pointer[1];
	length -= 2;
	pointer += 2;

	/*
	*  Loop through all subfields of final field.
	*/
	for (i = 0; i != number; ++i)
	{
		Buffer field;

		if (!verify_field(&length, &pointer, &field))
		{
			return CERTIFICATE_INVALID;
		}
	}

	/*
	*  Must have used up all of certificate to be valid.
	*/
	return length != 0 ? CERTIFICATE_INVALID : SUCCESS;
}


/*
*  Determine if certificate URI matches given URI.
*  Args: certificate - Certificate, assumed to be valid.
*        cert_uri    - URI to be compared with certificate.
*  Returns true if certificate matches URI, false otherwise.
*/
static Boolean compare_certificate(Buffer certificate, String cert_uri)
{
	const String uri = to_string(get_field(certificate, CERT_URI_FIELD));
	return uri.length == cert_uri.length
		   && memcmp(uri.pointer, cert_uri.pointer, uri.length) == 0;
}


/*
*  Determine if certificate matches domain.
*  Args: certificate - Certificate, assumed to be valid.
*        domain      - Domain to be matched with certificate.
*  Returns true if certificate matches domain, false otherwise.
*/
static Boolean compare_domain(Buffer certificate, String domain)
{
	/*
	*  Variable used below.
	*/
	size_t i;

	/*
	*  Buffer used here to record KMS domain list field.
	*/
	size_t number;
	Buffer buffer = get_domain_field(certificate, &number);

	/*
	*  Loop through all domain subfields.
	*/
	for (i = 0; i != number; ++i)
	{
		/*
		*  Get domain length from buffer, and set buffer to point to
		*  domain name.
		*/
		const unsigned char length
		  = 256 * buffer.pointer[0] + buffer.pointer[1];
		buffer.length -= 2;
		buffer.pointer += 2;

		/*
		*  If domain matches input domain then return matched.
		*/
		if (string_equal(fixed_string(length, buffer.pointer), domain))
		{
			return true;
		}

		/*
		*  Advance buffer to next subfield.
		*/
		buffer.length -= length;
		buffer.pointer += length;
	}

	/*
	*  No match found.
	*/
	return false;
}


/*
*  Find certificate that matches domain.
*  Args: domain - Domain to be matched.
*  Rets: Buffer indicating matched certificate. Pointer is null
*        if no certificate found.
*  Note: Assumes static array context_array set on entry.
*/
static Buffer find_certificate_domain(String domain)
{
	/*
	*  Variables used below.
	*/
	Buffer array;
	Buffer certificate;
	DataRef *data_ref_p;
	size_t number;
	size_t i;

	/*
	*  Get root certificate. Fail if invalid, return it
	*  if it matches domain.
	*/
	if (GetData(context_array[ROOT_CERT_INDEX], &certificate))
	{
		return fixed_buffer(0, NULL);
	}

	if (compare_domain(certificate, domain))
	{
		return certificate;
	}

	/*
	*  Get data references for external certificates.
	*  Fail if none, otherwise convert to useful form.
	*/
	array = get_array_copy(EXT_CERTS_INDEX, 0, &number);

	if (array.pointer == NULL)
	{
		return fixed_buffer(0, NULL);
	}

	data_ref_p = (DataRef *)array.pointer;

	/*
	*  Loop through external certificates.
	*/
	for (i = 0; i != number; ++i)
	{
		/*
		*  Get certificate. Fail if invalid, return it
		*  if it matches domain.
		*/
		if (GetData(data_ref_p[i], &certificate))
		{
			return fixed_buffer(0, NULL);
		}

		if (compare_domain(certificate, domain))
		{
			return certificate;
		}
	}

	/*
	*  No matching certificate, so fail.
	*/
	return fixed_buffer(0, NULL);
}


/*
*  Find certificate that matches user URI.
*  Args: uri - User URI to be matched.
*  Rets: Buffer indicating matched certificate. Pointer is null
*        if no certificate found.
*  Note: Assumes static array context_array set on entry.
*/
static Buffer find_certificate_uri(String uri)
{
	/*
	*  Get domain from URI.
	*/
	const String domain = get_domain(uri);

	/*
	*  Get certificate matching domain.
	*/
	return find_certificate_domain(domain);
}


/*
*  Find certificate that matches user URI and timestamp.
*  Args: uri       - User URI to be matched.
*        timestamp - UTC timestamp (input).
*  Rets: Buffer indicating matched certificate. Pointer is null
*        if no certificate found.
*  Note: Assumes static array context_array set on entry.
*/
static Buffer find_certificate_uri_timestamp(String uri, String timestamp)
{
	/*
	*  Variables used below.
	*/
	Buffer certificate;
	String time_cert_start;
	String time_cert_end;

	/*
	*  Find certificate matching URI.
	*/
	certificate = find_certificate_uri(uri);

	if (certificate.pointer == NULL)
	{
		return fixed_buffer(0, NULL);
	}

	/*
	*  Get and verify certificate start time.
	*/
	time_cert_start = to_string(get_field(certificate, VALID_FROM_FIELD));

	if (!is_timestamp(time_cert_start))
	{
		return fixed_buffer(0, NULL);
	}

	/*
	*  Check timestamp is after start of certificate.
	*  Do not check if time_cert_start is before timestamp,
	*  check it is not after by more than TIMESTAMP_CERT_JITTER.
	*/
	if (timestamp_verify(timestamp, time_cert_start, -1,
		                 TIMESTAMP_CERT_PAST_JITTER) != SUCCESS)
	{
		return fixed_buffer(0, NULL);
	}

	/*
	*  Get and verify certificate end time.
	*/
	time_cert_end = to_string(get_field(certificate, VALID_TO_FIELD));

	if (!is_timestamp(time_cert_end))
	{
		return fixed_buffer(0, NULL);
	}

	/*
	*  Check timestamp is before end of certificate.
	*  Do not check if time_cert_end is after timestamp,
	*  check it is not before by more than TIMESTAMP_CERT_JITTER.
	*/
	if (timestamp_verify(timestamp, time_cert_end,
		                 TIMESTAMP_CERT_FUTURE_JITTER, -1) != SUCCESS)
	{
		return fixed_buffer(0, NULL);
	}

	/*
	*  Return successfully found and matching certificate.
	*/
	return certificate;
}

/* ---- URI and UID functions ----------------------------------------------- */

/*
*  Get domain from URI. If no @ then all of URI. Otherwise all between first @
*  and first ? (if any).
*  Args: uri - URI.
*  Rets: String representing domain (as substring of URI).
*/
static String get_domain(String uri)
{
	/*
	*  Variables used below.
	*/
	size_t domain_length;
	const char *query_pointer;
	
	/*
	*  Find first @ (if any) in URI.
	*/
	const char *domain_pointer
	  = (const char *)memchr(uri.pointer, '@', uri.length);

	/*
	*  If no @ then return URI, otherwise continue to process the part of the
	*  URI after the @.
	*/
	if (domain_pointer == NULL)
	{
		return uri;
	}

	/*
	*  Advance past @.
	*/
	++domain_pointer;

	/*
	*  Initially assume there is no query string and the length of the domain
	*  is the length of the whole string after the @.
	*/
	domain_length = uri.length - (domain_pointer - uri.pointer);
		
	/*
	*  Search for the start of the query string, if any.
	*/
	query_pointer = (const char *)memchr(domain_pointer, '?', domain_length);
		
	/*
	*  If there is a query string, set the domain length so the query string
	*  is excluded.
	*/
	if (query_pointer != NULL)
	{
		domain_length = query_pointer - domain_pointer;
	}
		
	/*
	*  Return String indicating domain.
	*/
	return fixed_string(domain_length, domain_pointer);
}


/*
*  Return NAI part (user@host) of URI.
*  Args: uri - URI.
*  Rets: String of NAI part, as substring of URI. Pointer is null if
*        cannot be found.
*/
static String uri_nai(String uri)
{
	/*
	*  Find @ sign in string. Convenient to set start and (one after)
	*  finish pointers initial values here. Fail if no @ sign.
	*/
	const char * const pointer
	  = (const char *)memchr(uri.pointer, '@', uri.length);
	const char *start = pointer;
	const char *finish = pointer;

	if (pointer == NULL)
	{
		return fixed_string(0, NULL);
	}

	/*
	*  Loop as long as not about to step before start of string.
	*/
	while (start != uri.pointer)
	{
		/*
		*  Pointer now to next character to be checked. If not a
		*  valid character (from RFC 2396) then return to last
		*  valid character (or back to pointer) and exit loop.
		*/
		--start;

		if (!isalnum(*start) && strchr("-_.!~*'()%", *start) == NULL)
		{
			++start;
			break;
		}
	}

	/*
	*  Fail if no user.
	*/
	if (start == pointer)
	{
		return fixed_string(0, NULL);
	}

	/*
	*  Loop only until stepped one beyond end of string.
	*/
	while (++finish != uri.pointer + uri.length)
	{
		/*
		*  Pointer now to next character to be checked. If not a
		*  valid character (from RFC 2396) then return to last
		*  valid character (or back to pointer) and exit loop.
		*/
		if (!isalnum(*finish) && strchr("-_.!~*'()%", *finish) == 0)
		{
			break;
		}
	}

	/*
	*  Fail if no host.
	*/
	if (finish == pointer)
	{
		return fixed_string(0, NULL);
	}

	/*
	*  Return identified part of string.
	*/
	return fixed_string(finish - start, start);
}


/*
*  Return user part of URI.
*  Args: uri - URI.
*  Rets: String of user part, as substring of URI. Pointer is null if
*        cannot be found.
*/
static String uri_user(String uri)
{
	/*
	*  Find @ sign in string. Convenient to set start pointer
	*  initial value here. Fail if no @ sign.
	*/
	const char * const pointer
		= (const char *)memchr(uri.pointer, '@', uri.length);
	const char *start = pointer;

	if (pointer == NULL)
	{
		return fixed_string(0, NULL);
	}

	/*
	*  Loop as long as not about to step before start of string.
	*/
	while (start != uri.pointer)
	{
		/*
		*  Pointer now to next character to be checked. If not a
		*  valid character from RFC 2396) then return to last
		*  valid character (or back to pointer) and exit loop.
		*/
		--start;

		if (!isalnum(*start) && strchr("-_.!~*'()%", *start) == NULL)
		{
			++start;
			break;
		}
	}

	/*
	*  Fail if no user.
	*/
	if (start == pointer)
	{
		return fixed_string(0, NULL);
	}

	/*
	*  Return identified part of string.
	*/
	return fixed_string(pointer - start, start);
}


/*
*  Return host part of URI.
*  Args: uri - URI.
*  Rets: String of host part, as substring of URI. Pointer is null if
*        cannot be found.
*/
static String uri_host(String uri)
{
	/*
	*  Find @ sign in string. Convenient to set (one after)
	*  finish pointer initial value here. Fail if no @ sign.
	*/
	const char *pointer
	  = (const char *)memchr(uri.pointer, '@', uri.length);
	const char *finish = pointer;

	if (pointer == NULL)
	{
		return fixed_string(0, NULL);
	}

	/*
	*  Loop only until stepped one beyond end of string.
	*/
	while (++finish != uri.pointer + uri.length)
	{
		/*
		*  Pointer now to next character to be checked. If not a
		*  valid character (0-9 A-Z a-z - .) then return to last
		*  valid character (or back to pointer) and exit loop.
		*/
		if (!isalnum(*finish) && *finish != '-' && *finish != '.')
		{
			break;
		}
	}

	/*
	*  Fail if no host.
	*/
	if (finish == pointer)
	{
		return fixed_string(0, NULL);
	}

	/*
	*  Return identified part of string.
	*/
	++pointer;
	return fixed_string(finish - pointer, pointer);
}


/*
*  Return parameters part of URI.
*  Args: uri - URI.
*  Rets: String of parameter part, as substring of URI. Pointer is null if
*        cannot be found.
*/
static String uri_parameters(String uri)
{
	/*
	*  Find ? sign in string. If no ? then return empty (not error) string.
	*/
	const char *pointer = (const char *)memchr(uri.pointer, '?', uri.length);

	if (pointer == 0)
	{
		return fixed_string(0, uri.pointer);
	}

	/*
	*  Return identified part of string.
	*/
	++pointer;
	return fixed_string(uri.pointer + uri.length - pointer, pointer);
}


/*
*  Determine year from timestamp.
*  Args: timestamp - Timestamp.
*  Rets: 4 character string, or string with null pointer if fail.
*/
static String uri_year(String timestamp)
{
	/*
	*  Get year, month and day of year from timestamp. Fail if
	*  timestamp is invalid.
	*/
	static int year;
	int month, yday;

	if (GetDateFromTimestamp(timestamp, &year, &month, &yday))
	{
		return fixed_string(0, NULL);
	}

	/*
	*  Convert year to 4 character string.
	*/
	return string_number((uint32_t)year, 4);
}


/*
*  Determine month from timestamp.
*  Args: timestamp - Timestamp.
*  Rets: 2 character string, or string with null pointer if fail.
*/
static String uri_month(String timestamp)
{
	/*
	*  Get year, month and day of year from timestamp. Fail if
	*  timestamp is invalid.
	*/
	static int month;
	int year, yday;

	if (GetDateFromTimestamp(timestamp, &year, &month, &yday))
	{
		return fixed_string(0, NULL);
	}

	/*
	*  Convert month to 2 character string.
	*/
	return string_number((uint32_t)month, 2);
}


/*
*  Determine day of year from timestamp.
*  Args: timestamp - Timestamp.
*  Rets: 3 character string, or string with null pointer if fail.
*/
static String uri_yday(String timestamp)
{
	/*
	*  Get year, month and day of year from timestamp. Fail if
	*  timestamp is invalid.
	*/
	static int yday;
	int year, month;

	if (GetDateFromTimestamp(timestamp, &year, &month, &yday))
	{
		return fixed_string(0, NULL);
	}

	/*
	*  Convert month to 3 character string.
	*/
	return string_number((uint32_t)yday, 3);
}


/*
*  Convert URI to UID.
*  Args: timestamp - Timestamp.
*        uri       - URI.
*        format    - String defining UID format.
*  Rets: String defining UID. Pointer is null indicates invalid.
*  Note: Assumes static array context_array set on entry.
*/
static String user_id(String timestamp, String uri, String format)
{
	/*
	*  If format records dynamic memory to be restored to result_p
	*  on exit.
	*/
	Boolean restore = false;

	/*
	*  Make substitutions in current string that is indicated by
	*  format until no more substitutions possible.
	*/
	while (true)
	{
		/*
		*  Variable used below.
		*/
		size_t length;

		/*
		*  Find # in format. If none then current string is result.
		*  This may be original format string or using result_p.
		*/
		const char * const pointer
			= (const char *)memchr(format.pointer, '#', format.length);

		if (pointer == NULL)
		{
			return format;
		}

		/*
		*  Find length of string including and after #, to allow safe
		*  verification of presence of #uri etc.
		*/
		length = format.length - (pointer - format.pointer);

		/*
		*  Check for various # codes. Does not include #week as that is
		*  neither defined in RFC 3339 (Section 5.6) not supported by
		*  struct tm. In each case replace # code (with indicated length)
		*  by specified replacement. using functions uri_nai etc.
		*/
		if (length >= 4 && memcmp(pointer, "#uri", 4) == 0)
		{
			const String replace = uri_nai(uri);

			if (replace.pointer == NULL)
			{
				break;
			}

			format.length
			  = string_replace(format, fixed_string(4, pointer), replace);
		}
		else if (length >= 5 && memcmp(pointer, "#user", 5) == 0)
		{
			const String replace = uri_user(uri);

			if (replace.pointer == NULL)
			{
				break;
			}

			format.length
			  = string_replace(format, fixed_string(5, pointer), replace);
		}
		else if (length >= 5 && memcmp(pointer, "#host", 5) == 0)
		{
			const String replace = uri_host(uri);

			if (replace.pointer == NULL)
			{
				break;
			}

			format.length
			  = string_replace(format, fixed_string(5, pointer), replace);
		}
		else if (length >= 11 && memcmp(pointer, "#parameters", 11) == 0)
		{
			const String replace = uri_parameters(uri);

			if (replace.pointer == NULL)
			{
				break;
			}

			format.length
			  = string_replace(format, fixed_string(11, pointer), replace);
		}
		else if (length >= 4 && memcmp(pointer, "#kms", 4) == 0)
		{
			/*
			 *  Variable used below.
			 */
			String cert_uri;

			/*
			*  Get certificate.
			*/
			const Buffer certificate = find_certificate_uri(uri);

			if (certificate.pointer == NULL)
			{
				break;
			}

			/*
			*  Extract certificate URI.
			*/
			cert_uri = to_string(get_field(certificate, CERT_URI_FIELD));

			if (cert_uri.pointer == NULL)
			{
				break;
			}

			format.length
			  = string_replace(format, fixed_string(4, pointer), cert_uri);
		}
		else if (length >= 5 && memcmp(pointer, "#year", 5) == 0)
		{
			const String replace = uri_year(timestamp);

			if (replace.pointer == NULL)
			{
				break;
			}

			format.length
			  = string_replace(format, fixed_string(5, pointer), replace);
		}
		else if (length >= 6 && memcmp(pointer, "#month", 6) == 0)
		{
			const String replace = uri_month(timestamp);

			if (replace.pointer == NULL)
			{
				break;
			}

			format.length
			  = string_replace(format, fixed_string(6, pointer), replace);
		}
		else if (length >= 5 && memcmp(pointer, "#yday", 5) == 0)
		{
			const String replace = uri_yday(timestamp);

			if (replace.pointer == NULL)
			{
				break;
			}

			format.length
			  = string_replace(format, fixed_string(5, pointer), replace);
		}
		else
		{
			break;
		}

		if (restore)
		{
			free((void *)format.pointer);
		}

		format.pointer = (const char *)result_p;
		result_p = 0;
		restore = true;
	}

	/*
	*  Failure.
	*/
	if (restore)
	{
		result_p = (void *)format.pointer;
	}

	return fixed_string(0, NULL);
}


/* ---- ECCSI, SAKKE and ECDSA specific certificate functions --------------- */

/*
*  Get ECCSI certificate information.
*  Args: uri        - URI.
*        time_from  - Valid from timestamp.
*        time_to    - Valid to timestamp.
*        identity_p - *identity_p is set to identity.
*        kpak_p     - *kpak_p is set to KPAK, which is allocated by this
*                     function, and must be freed by calling function.
*  Rets: Standard error code.
*  Note: Assumes static array context_array set on entry.
*        Lifetime of information is until next access to
*        stored data.
*/
static ScErrno eccsi_certificate(String uri, String time_from, String time_to,
	                             String *identity_p, EC_POINT **kpak_p)
{
	/*
	*  Variables used below.
	*/
	Buffer certificate;
	Buffer key;
	String valid_to;

	/*
	*  Verify timestamps properly ordered (allow to be equal).
	*/
	if (timestamp_less(time_to, time_from))
	{
		return CERTIFICATE_URI_NOT_MATCH;
	}

	/*
	*  Find certificate matching URI and verify matches from timestamp.
	*/
	certificate = find_certificate_uri_timestamp(uri, time_from);

	if (certificate.pointer == NULL)
	{
		return CERTIFICATE_URI_NOT_MATCH;
	}

	/*
	*  Verify to timestamp.
	*/
	valid_to = to_string(get_field(certificate, VALID_TO_FIELD));

	if (valid_to.pointer == NULL)
	{
		return CERTIFICATE_URI_NOT_MATCH;
	}

	if (timestamp_verify(time_to, valid_to, TIMESTAMP_CERT_FUTURE_JITTER, -1))
	{
		return CERTIFICATE_URI_NOT_MATCH;
	}

	/*
	*  Get identity of signer.
	*/
	*identity_p = get_uid(certificate, time_from, uri);

	if (identity_p->pointer == NULL)
	{
		return CERTIFICATE_INVALID;
	}

	/*
	*  Get authorisation key field.
	*/
	key = get_field(certificate, PUB_AUTH_KEY_FIELD);

	if (key.pointer == NULL)
	{
		return CERTIFICATE_INVALID;
	}

	/*
	*  Get ECCSI KPAK and indicate if success.
	*/
	*kpak_p = eccsi_point(key, true);
	return *kpak_p == 0 ? CERTIFICATE_INVALID : SUCCESS;
}


/*
*  Get SAKKE certificate information.
*  Args: uri        - URI.
*        timestamp  - Timestamp for certificate.
*        identity_p - *identity_p is set to identity.
*        z_p        - *z_p is set to Z, which is allocated by this
*                     function, and must be freed by calling function.
*  Rets: Standard error code.
*  Note: Assumes static array context_array set on entry.
*        Lifetime of information is until next access to
*        stored data.
*/
static ScErrno sakke_certificate(String uri, String timestamp,
								 String *identity_p, EC_POINT **z_p)
{
	/*
	*  Variables used below.
	*/
	Buffer certificate;
	Buffer key;

	/*
	*  Find certificate matching URI and verify matches from timestamp.
	*/
	certificate = find_certificate_uri_timestamp(uri, timestamp);

	if (certificate.pointer == NULL)
	{
		return CERTIFICATE_URI_NOT_MATCH;
	}

	/*
	*  Get identity of signer.
	*/
	*identity_p = get_uid(certificate, timestamp, uri);

	if (identity_p->pointer == NULL)
	{
		return CERTIFICATE_INVALID;
	}

	/*
	*  Get encryption key field.
	*/
	key = get_field(certificate, PUB_ENC_KEY_FIELD);

	if (key.pointer == NULL)
	{
		return CERTIFICATE_INVALID;
	}

	/*
	*  Get SAKKE Z and indicate if success.
	*/
	*z_p = sakke_point(key, true);
	return *z_p == NULL ? CERTIFICATE_INVALID : SUCCESS;
}


/*
*  Get ECDSA certificate information, using root certificate.
*  Args: context - Security context.
*        kpak_p  - *kpak_p is set to public authentication key, which is
*                  allocated by this function, and must be freed by calling
*                  function.
*/
static ScErrno ecdsa_certificate(SecCtx context, EC_POINT **kpak_p)
{
	/*
	*  Variable used below.
	*/
	Buffer key;

	/*
	*  Get root certificate. Note that this will get and validate
	*  context array.
	*/
	Buffer certificate;

	if (GetRootCert(context, &certificate))
	{
		return ECDSA_NOT_VERIFIED;
	}

	/*
	*  Get heaxdecimal form of KPAK from certificate.
	*/
	key = get_field(certificate, PUB_AUTH_KEY_FIELD);

	/*
	*  Convert to elliptic curve point.
	*/
	*kpak_p = eccsi_point(key, true);
	return *kpak_p == NULL ? ECDSA_NOT_VERIFIED : SUCCESS;
}


/* ---- Transport key functions --------------------------------------------- */

/*
*  Find transport key given identity.
*  Args: tk_id - Transport key ID.
*  Rets: Index of transport key ID (and hence also of transport key)
*        in corresponding array, plus one, or zero if not found.
*  Note: Assumes static array context_array set on entry.
*/
static size_t find_transport_key(String tk_id)
{
	/*
	*  Variables used below.
	*/
	Buffer buffer;
	size_t i;

	/*
	*  Get array of transport key ID data references.
	*/
	size_t number;
	const DataRef * const tk_id_p = get_array(TK_ID_INDEX, &number);

	if (tk_id_p == NULL)
	{
		return 0;
	}

	/*
	*  Loop through transport key IDs looking for match.
	*/
	for (i = 0; i != number; ++i)
	{
		if (GetData(tk_id_p[i], &buffer) == SUCCESS)
		{
			if (string_equal(tk_id, to_string(buffer)))
			{
				return i + 1;
			}
		}
	}

	/*
	*  Transport key ID not found.
	*/
	return 0;
}


/*
*  Get transport key given transport key ID.
*  Args: tk_id - Transport key ID.
*        tk_p  - Set to transport key ID on exit. Note that this
*                is as returned by data store, cannot be used
*                beyond next call to data store.
*  Rets: Standard error code.
*/
static ScErrno get_transport_key(String tk_id, Buffer *tk_p)
{
	/*
	*  Variables used below.
	*/
	Buffer buffer;
	DataRef *tk_refs;
	ScErrno error;

	/*
	*  Find transport key data reference.
	*/
	size_t index = find_transport_key(tk_id);

	if (index == 0)
	{
		return TRK_NOT_EXIST;
	}

	--index;

	/*
	*  Get array of transport key data references.
	*/
	error = GetData(context_array[TK_INDEX], &buffer);

	if (error)
	{
		return error;
	}

	tk_refs = (DataRef *)buffer.pointer;

	/*
	*  Get transport key, returning success indication.
	*/
	return GetData(tk_refs[index], tk_p);
}


/* ---- Key pack functions -------------------------------------------------- */

/*
*  Determine if key pack matches URI and timestamp.
*  Args: key_pack  - Key pack data reference.
*        uri       - URI to be matched. Ignore if empty.
*        timestamp - Timestamp to be matched. Ignore if empty.
*  Rets: Time key pack is valid to, or zero if key pack does not match.
*        (May be used as logical value for match.)
*/
static uint32_t match_key_pack(DataRef key_pack, String uri, String timestamp)
{
	/*
	*  Variables used below.
	*/
	DataRef *kp_refs;
	DataRef uri_ref, from_ref, to_ref;

	/*
	*  Get data pack, fail if not found.
	*/
	Buffer buffer;

	if (GetData(key_pack, &buffer))
	{
		return 0;
	}

	/*
	*  Get required data from buffer, as buffer is about to be
	*  overwritten (and would be invalidated even if not reused).
	*/
	kp_refs = (DataRef *)buffer.pointer;
	from_ref = kp_refs[KP_FROM_INDEX];
	to_ref = kp_refs[KP_TO_INDEX];

	/*
	*  Get URI data, and check if matches parameter, if not
	*  then fail. Only check if URI is not empty.
	*/
	if (uri.pointer != NULL)
	{
		uri_ref = kp_refs[KP_URI_INDEX];

		if (GetData(uri_ref, &buffer))
		{
			return 0;
		}

		if (!string_equal(uri, to_string(buffer)))
		{
			return 0;
		}
	}

	/*
	*  Get from and to timestamp data, and check if match timestamp
	*  If not then fail. Only check if timestamp is not empty, but
	*  ensure buffer contains to timestamp for return.
	*/
	if (timestamp.pointer != NULL)
	{
		if (GetData(from_ref, &buffer))
		{
			return 0;
		}

		if (timestamp_verify(timestamp, to_string(buffer), -1,
			                TIMESTAMP_KEYPACK_PAST_JITTER))
		{
			return 0;
		}

		if (GetData(to_ref, &buffer))
		{
			return 0;
		}

		if (timestamp_verify(timestamp, to_string(buffer),
			                 TIMESTAMP_KEYPACK_FUTURE_JITTER, -1))
		{
			return 0;
		}
	}
	else if (GetData(to_ref, &buffer))
	{
		return 0;
	}

	/*
	*  Successful match, return to timestamp converted to time in seconds.
	*  (Must be non-zero for a usable key pack.)
	*/
	return timestamp_seconds(to_string(buffer));
}


/*
*  Determine if key pack matches UID and timestamp.
*  Args: key_pack  - Key pack data reference.
*        uri       - URI to be matched. Ignore if empty.
*        timestamp - Timestamp to be matched. Ignore if empty.
*  Rets: Time key pack is valid to, or zero if key pack does not match.
*        (May be used as logical value for match.)
*/
static uint32_t match_key_pack_uid(DataRef key_pack, String uid,
								   String timestamp)
{
	/*
	*  Variables used below.
	*/
	const DataRef *kp_refs;
	DataRef uid_ref, from_ref, to_ref;

	/*
	*  Get data pack, fail if not found.
	*/
	Buffer buffer;

	if (GetData(key_pack, &buffer))
	{
		return 0;
	}

	/*
	*  Get required data from buffer, as buffer is about to be
	*  overwritten (and would be invalidated even if not reused).
	*/
	kp_refs = (DataRef *)buffer.pointer;
	uid_ref = kp_refs[KP_UID_INDEX];
	from_ref = kp_refs[KP_FROM_INDEX];
	to_ref = kp_refs[KP_TO_INDEX];

	/*
	*  Get UID data, and check if matches parameter, if not then fail.
	*/
	if (GetData(uid_ref, &buffer))
	{
		return 0;
	}

	if (!string_equal(uid, to_string(buffer)))
	{
		return 0;
	}

	/*
	*  Get from and to timestamp data, and check if match timestamp
	*  If not then fail. Only check if timestamp is not empty, but
	*  ensure buffer contains to timestamp for return.
	*/
	if (timestamp.pointer != NULL)
	{
		if (GetData(from_ref, &buffer))
		{
			return 0;
		}

		if (timestamp_verify(timestamp, to_string(buffer), -1,
			                TIMESTAMP_KEYPACK_PAST_JITTER))
		{
			return 0;
		}

		if (GetData(to_ref, &buffer))
		{
			return 0;
		}

		if (timestamp_verify(timestamp, to_string(buffer),
			                 TIMESTAMP_KEYPACK_FUTURE_JITTER, -1))
		{
			return 0;
		}
	}
	else if (GetData(to_ref, &buffer))
	{
		return 0;
	}

	/*
	*  Successful match, return to timestamp converted to time in seconds.
	*  (Must be non-zero for a usable key pack.)
	*/
	return timestamp_seconds(to_string(buffer));
}


/*
*  Find data reference of key pack matching URI and timestamp.
*  Args: uri       - URI to be matched.
*        timestamp - Timestamp to be matched.
*  Rets: Key pack reference, zero if none matching.
*  Note: Assumes static array context_array set on entry.
*/
static DataRef find_key_pack(String uri, String timestamp)
{
	/*
	*  Variables used below.
	*/
	Boolean found = false;
	uint32_t found_timestamp = 0;
	uint32_t to_timestamp;
	DataRef data_ref = 0;
	size_t number;
	size_t i;

	/*
	*  Get copy of array of key pack data references, or return failure.
	*/
	const Buffer buffer = get_array_copy(KEY_PACKS_INDEX, 0, &number);
	const DataRef * const key_packs = (DataRef *)buffer.pointer;

	if (key_packs == NULL)
	{
		return 0;
	}

	/*
	*  Loop through data packs. Look for newest (latest to time) that matches
	*  (must therefore also have from time that is not in future).
	*/
	for (i = 0; i != number; ++i)
	{
		to_timestamp = match_key_pack(key_packs[i], uri, timestamp);

		if (to_timestamp && (!found || to_timestamp > found_timestamp))
		{
			data_ref = key_packs[i];
			found_timestamp = to_timestamp;
			found = true;
		}
	}

	/*
	*  Free allocated memory and return found key pack or 0.
	*/
	free((void *)buffer.pointer);
	return data_ref;
}


/*
*  Find key pack matching UID.
*  Find data reference of key pack matching URI and timestamp.
*  Args: uri       - URI to be matched.
*        timestamp - Timestamp to be matched.
*  Rets: Key pack reference, zero if none matching.
*  Note: Assumes static array context_array set on entry.
*/
static size_t find_key_pack_uid(String uid, String timestamp)
{
	/*
	*  Variables used below.
	*/
	Boolean found = false;
	uint32_t found_timestamp = 0;
	uint32_t to_timestamp;
	DataRef data_ref = 0;
	size_t number;
	size_t i;

	/*
	*  Get copy of array of key pack data references, or return failure.
	*/
	const Buffer buffer = get_array_copy(KEY_PACKS_INDEX, 0, &number);
	const DataRef * const key_packs = (DataRef *)buffer.pointer;

	if (key_packs == NULL)
	{
		return 0;
	}

	/*
	*  Loop through data packs. Look for newest (latest to time) that matches
	*  (must therefore also have from time that is not in future).
	*/
	for (i = 0; i != number; ++i)
	{
		to_timestamp = match_key_pack_uid(key_packs[i], uid, timestamp);

		if (to_timestamp && (!found || to_timestamp > found_timestamp))
		{
			data_ref = key_packs[i];
			found_timestamp = to_timestamp;
			found = true;
		}
	}

	/*
	*  Free allocated memory and return found key pack or 0.
	*/
	free((void *)buffer.pointer);
	return data_ref;
}


/*
*  Find key pack matching UID with oldest to time.
*  Args: uid       - UID to be matched.
*        key_packs - *key_packs is set to buffer indicating allocated
*                    array of pack data references that must be freed
*                    by calling function if successful.
*  Rets: Key pack index plus one, or zero if no key pack found.
*  Note: Assumes static array context_array set on entry.
*/
static size_t find_key_packs_uid(String uid, Buffer *key_packs)
{
	/*
	*  Variables used below.
	*/
	const DataRef *kp_p;
	size_t kp = 0;
	uint32_t found_timestamp = 0;
	size_t number;
	size_t i;

	/*
	*  Get copy of array of key pack data references, or return
	*  failure.
	*/
	*key_packs = get_array_copy(KEY_PACKS_INDEX, 0, &number);

	if (key_packs->pointer == NULL)
	{
		*key_packs = fixed_buffer(0, NULL);
		return 0;
	}

	kp_p = (DataRef *)key_packs->pointer;

	/*
	*  Loop through data packs.
	*/
	for (i = 0; i != number; ++i)
	{
		/*
		*  If match key pack then return successfully. Note that memory
		*  has been allocated but not freed.
		*/
		const uint32_t timestamp
		  = match_key_pack_uid(kp_p[i], uid, fixed_string(0, NULL));

		if (kp == 0 || timestamp < found_timestamp)
		{
			found_timestamp = timestamp;
			kp = i + 1;
		}
	}

	/*
	*  Free allocated memory if failure. Return key pack found, or failure.
	*/
	if (kp == 0)
	{
	    free((void *)key_packs->pointer);
	    *key_packs = fixed_buffer(0, NULL);
	}

	return kp;
}


/*
*  Verify and decrypt key pack.
*  Args: aes_length - AES key length (16 for AES-128, 32 for AES-256).
*        tk_id      - Transport key ID to unwrap UDK, SSK and PVT.
*        enc_udk    - Encrypted SAKKE UDK.
*        enc_ssk    - Encrypted ECCSI SSK.
*        enc_pvt    - Encrypted ECCSI PVT.
*        udk_p      - *udk_p is used to return dynamically allocated
*                     decrypted SAKKE UDK.
*        ssk_p      - *ssk_p is used to return dynamically allocated
*                     decrypted ECCSI SSK.
*        pvt_p      - *pvt_p is used to return dynamically allocated
*                     decrypted ECCSI PVT.
*  Rets: Standard error code.
*  Note: Assumes static array context_array set on entry.
*        Calling function must free udk_p->pointer, ssk_p->pointer
*        and pvt_p->pointer if function succeeds. Recommend to first
*        overwrite with zeros, function purge_buffer may be used.
*/
static ScErrno decrypt_key_pack(size_t aes_length, String tk_id,
	                            Buffer enc_udk, Buffer enc_ssk, Buffer enc_pvt,
	                            Buffer *udk_p, Buffer *ssk_p, Buffer *pvt_p)
{
	/*
	*  Variables used below.
	*/
	Buffer buffer;
	const DataRef *tk_refs;
	size_t index;
	ScErrno error;

	/*
	*  Ensure that pointers can always be freed.
	*/
	*udk_p = *ssk_p = *pvt_p = fixed_buffer(0, NULL);

	/*
	*  Find transport key data reference.
	*/
	index = find_transport_key(tk_id);

	if (index == 0)
	{
		return TRK_NOT_EXIST;
	}

	--index;

	/*
	*  Get array of transport key data references.
	*/
	error = GetData(context_array[TK_INDEX], &buffer);

	if (error)
	{
		return error;
	}

	tk_refs = (DataRef *)buffer.pointer;

	/*
	*  Get transport key and verify length.
	*/
	error = GetData(tk_refs[index], &buffer);

	if (error)
	{
		return error;
	}

	if (buffer.length != aes_length)
	{
		return TRK_INVALID;
	}

	/*
	*  Decrypt UDK, SSK and PVT.
	*/
	error = kw_decrypt(buffer, enc_udk, udk_p);

	if (error)
	{
		return error;
	}

	error = kw_decrypt(buffer, enc_ssk, ssk_p);

	if (error)
	{
		purge_buffer(udk_p);
		return error;
	}

	error = kw_decrypt(buffer, enc_pvt, pvt_p);

	if (error)
	{
		purge_buffer(udk_p);
		purge_buffer(ssk_p);
		return error;
	}

	/*
	*  Successfully decrypted data.
	*/
	return SUCCESS;
}


/*
*  Purge key pack.
*  Args: key_pack - Key pack data reference.
*/
static void purge_key_pack(DataRef key_pack)
{
	/*
	*  Variable used below.
	*/
	size_t i;

	/*
	*  Get data references to all components of key pack.
	*/
	Buffer buffer;

	if (GetData(key_pack, &buffer) == SUCCESS)
	{
		const DataRef * const pointer = (DataRef *)buffer.pointer;
		const size_t number = buffer.length / sizeof(DataRef);

		for (i = 0; i != number; ++i)
		{
			PurgeData(pointer[i]);
		}
	}

	PurgeData(key_pack);
}


/*
*  Purge all key packs. Updates locally stored context array,
*  but does not resave it.
*  Rets: True if successfully purged, false otherwise.
*  Note: Assumes static array context_array set on entry.
*/
static Boolean purge_key_packs(void)
{
	/*
	*  Variables used below.
	*/
	size_t number;
	size_t i;

	/*
	*  Get copy of subarray of key pack references.
	*/
	const Buffer buffer = get_array_copy(KEY_PACKS_INDEX, 0, &number);
	const DataRef * const data_ref_p = (DataRef *)buffer.pointer;

	/*
	*  Purge array.
	*/
	if (!PurgeData(context_array[KEY_PACKS_INDEX]))
	{
		free((void *)buffer.pointer);
		return false;
	}

	context_array[KEY_PACKS_INDEX] = 0;

	/*
	*  Loop through data references, purging. Retains
	*  consistency of structure even if any purge fails.
	*/
	for (i = 0; i != number; ++i)
	{
		purge_key_pack(data_ref_p[i]);
	}

	/*
	*  Free allocated memory and return success.
	*/
	free((void *)buffer.pointer);
	return false;
}


/* ==== Library functions =================================================== */

/*
*  Creates a new security context for KMS URI.
*  Args: kmsUri         - KMS URI (input).
*        transportKeyId - Transport key identity (input).
*        transportKey   - Transport key (input).
*        secCtx_p       - *secCtx_p is set to new security context
*                         matching input parameters.
*  Rets: Standard error code.
*/
ScErrno CreateKmsSecurityContext(String kmsUri, String transportKeyId,
	                             Buffer transportKey, SecCtx *secCtx_p)
{
	/*
	*  Variables used below.
	*/
	Buffer contexts;
	ScErrno error;

	/*
	*  Sanity checks on inputs.
	*/
	if (!check_buffer(transportKey))
	{
		return INPUT_BUFFER_INVALID;
	}

	if (!check_string(transportKeyId) || !check_string(kmsUri))
	{
		return INPUT_STRING_INVALID;
	}

	/*
	*  Fail if unable to return new security context.
	*/
	if (secCtx_p == 0)
	{
		return GENERAL_FAILURE;
	}

	/*
	*  Add new security context to array, with unused entry.
	*/
	error = new_security_context(&contexts);

	if (error)
	{
		return error;
	}

	/*
	*  Store KMS URI.
	*/
	error = StoreSecureData(to_buffer(kmsUri), false,
		context_array + KMS_URI_INDEX);

	if (error)
	{
		return error;
	}

	/*
	*  Store Transport Key ID.
	*/
	error = extend_array(0, TK_ID_INDEX, to_buffer(transportKeyId));

	if (error)
	{
		PurgeData(context_array[KMS_URI_INDEX]);
		return error;
	}

	/*
	*  Store Transport Key.
	*/
	error = extend_array(0, TK_INDEX, transportKey);

	if (error)
	{
		PurgeData(context_array[KMS_URI_INDEX]);
		PurgeData(context_array[TK_ID_INDEX]);
		return error;
	}

	/*
	*  Set unallocated root and external certificates and key packs.
	*/
	context_array[ROOT_CERT_INDEX] = 0;
	context_array[EXT_CERTS_INDEX] = 0;
	context_array[KEY_PACKS_INDEX] = 0;

	/*
	*  Store security context.
	*/
	error = StoreSecureData(context_buffer(), true, secCtx_p);

	if (error)
	{
		PurgeData(context_array[KMS_URI_INDEX]);
		PurgeData(context_array[TK_ID_INDEX]);
		PurgeData(context_array[TK_INDEX]);
		return error;
	}

	/*
	*  Update stored security context array.
	*/
	error = update_security_contexts(contexts, *secCtx_p);

	if (error)
	{
		PurgeData(context_array[KMS_URI_INDEX]);
		PurgeData(context_array[TK_ID_INDEX]);
		PurgeData(context_array[TK_INDEX]);
		return error;
	}

	/*
	*  Return successfully stored.
	*/
	return SUCCESS;
}


/*
*  Get KMS URI.
*  Args: secCtx   - Security context (input).
*        kmsUri_p - *kmsUri_p is set to KMS URI.
*  Rets: Standard error code.
*/
ScErrno GetKmsUri(SecCtx secCtx, String *kmsUri_p)
{
	/*
	*  Get and validate context array.
	*/
	ScErrno error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Get KMS URI, returning success or failure as appropriate.
	*/
	return GetData(context_array[KMS_URI_INDEX], (Buffer *)kmsUri_p);
}


/*
*  Stores root KMS certificate.
*  Args: secCtx      - Security context (input).
*        rootKmsCert - Root KMS certificate (input).
*  Rets: Standard error code.
*/
ScErrno StoreRootCert(SecCtx secCtx, Buffer rootKmsCert)
{
	/*
	*  Variable used below.
	*/
	ScErrno error;

	/*
	*  Sanity check on inputs.
	*/
	if (!check_buffer(rootKmsCert))
	{
		return INPUT_BUFFER_INVALID;
	}

	/*
	*  Get and validate context array.
	*/
	error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Verify certificate.
	*/
	error = verify_certificate(rootKmsCert, false);

	if (error)
	{
		return error;
	}

	/*
	*  Separate cases for first storage and update.
	*/
	if (context_array[ROOT_CERT_INDEX] == 0)
	{
		/*
		*  Store root certificate. If fail then nothing changed.
		*/
		error = StoreSecureData(rootKmsCert, true,
			                    &context_array[ROOT_CERT_INDEX]);

		if (error)
		{
			return error;
		}

		/*
		*  Store changed context. If fail then purge stored root certificate.
		*  Context is unchanged (no root certificate).
		*/
		error = reset_context(secCtx);

		if (error)
		{
			PurgeData(context_array[ROOT_CERT_INDEX]);
			return error;
		}
	}
	else
	{
		/*
		*  Update stored root certificate. No need to change context.
		*  Old root certificate stored if fails.
		*/
		error = UpdateData(context_array[ROOT_CERT_INDEX], rootKmsCert);

		if (error)
		{
			return error;
		}
	}

	/*
	*  Successfully stored root certificate and updated context.
	*/
	return SUCCESS;
}


/*
*  Stores certificate for an external domain.
*  Args: secCtx          - Security context (input).
*        externalKmsCert - External domain certificate (input).
*  Rets: Standard error code.
*/
ScErrno StoreExternalCert(SecCtx secCtx, Buffer externalKmsCert)
{
	/*
	*  Variable used below.
	*/
	ScErrno error;

	/*
	*  Sanity check on input.
	*/
	if (!check_buffer(externalKmsCert))
	{
		return INPUT_BUFFER_INVALID;
	}

	/*
	*  Get and validate context array.
	*/
	error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Verify certificate.
	*/
	error = verify_certificate(externalKmsCert, true);

	if (error)
	{
		return error;
	}

	/*
	*  Store certificate and update all storage as required.
	*/
	return extend_array(secCtx, EXT_CERTS_INDEX, externalKmsCert);
}


/*
*  Get root KMS certificate.
*  Args: secCtx        - Security context (input).
*        rootKmsCert_p - *rootKmsCert_p is set to root KMS certificate.
*  Rets: Standard error code.
*/
ScErrno GetRootCert(SecCtx secCtx, Buffer *rootKmsCert_p)
{
	/*
	*  Get and validate context array.
	*/
	const ScErrno error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Get root certificate, returning success or failure as appropriate.
	*/
	return GetData(context_array[ROOT_CERT_INDEX], rootKmsCert_p);
}


/*
*  Get KMS certificate.
*  Args: secCtx    - Security context (input).
*        certUri   - URI for required certificate.
*        kmsCert_p - *kmsCert_p is set to KMS certificate.
*  Rets: Standard error code.
*/
ScErrno GetCert(SecCtx secCtx, String certUri, Buffer *kmsCert_p)
{
	/*
	*  Variables used below.
	*/
	Buffer certificate;
	const DataRef *array_p;
	size_t number;
	ScErrno error;
	size_t i;

	/*
	*  Sanity check on input.
	*/
	if (!check_string(certUri))
	{
		return INPUT_STRING_INVALID;
	}

	/*
	*  Get and validate context array.
	*/
	error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Get root certificate from data store. Consider no root certificate
	*  as a failure.
	*/
	error = GetData(context_array[ROOT_CERT_INDEX], &certificate);

	if (error)
	{
		return error;
	}

	/*
	*  Return root certificate if matches URI.
	*/
	if (compare_certificate(certificate, certUri))
	{
		*kmsCert_p = certificate;
		return SUCCESS;
	}

	/*
	*  Get array of external certificates, verified as valid,
	*  and number of certificates.
	*/
	array_p = get_array(EXT_CERTS_INDEX, &number);

	/*
	*  Consider all external certificates until one matches.
	*/
	for (i = 0; i != number; ++i)
	{
		/*
		*  Get external certificate. If none then continue with
		*  next certificate.
		*/
		if (GetData(array_p[i], &certificate))
		{
			continue;
		}

		/*
		*  Return certificate if matches URI.
		*/
		if (compare_certificate(certificate, certUri))
		{
			*kmsCert_p = certificate;
			return SUCCESS;
		}
	}

	/*
	*  No certificate matches.
	*/
	return RESOURCE_NOT_EXIST;
}


/*
*  Get list of KMS certificate URIs.
*  Args: secCtx              - Security context (input).
*        listofKmsCertUris_p - *listofKmsCertUris_p is set to indicate
*                              list of KMS certificate URIs, starting
*                              with the root KMS certificate.
*  Rets: Standard error code.
*/
ScErrno ListKmsCertUris(SecCtx secCtx, StringList *listofKmsCertUris_p)
{
	/*
	*  Variables used below.
	*/
	Buffer buffer;
	Buffer certificate;
	const DataRef *array_p;
	size_t number;
	size_t number_ext;
	size_t length;
	size_t index;
	size_t i;

	/*
	*  Get and validate context array.
	*/
	ScErrno error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Get array of external certificate data references, and number
	*  of external certificates. Result may be null, in which case
	*  number will be zero, which can be handled by following code.
	*/
	buffer = get_array_copy(EXT_CERTS_INDEX, 0, &number);
	array_p = (DataRef *)buffer.pointer;

	/*
	*  Loop through external certificates to count how many are valid.
	*/
	number_ext = 0;

	for (i = 0; i != number; ++i)
	{
		if (array_p[i] != 0)
		{
			++number_ext;
		}
	}

	/*
	*  Get root certificate from data store. Consider no root certificate
	*  as a failure.
	*/
	error = GetData(context_array[ROOT_CERT_INDEX], &certificate);

	if (error)
	{
		free((void *)buffer.pointer);
		return error;
	}

	/*
	*  Initialise list.
	*/
	error = create_list(number_ext + 1, listofKmsCertUris_p, &length);

	if (error)
	{
		free((void *)buffer.pointer);
		return error;
	}

	/*
	*  Add root certificate.
	*/
	error = add_list(0, to_string(get_field(certificate, CERT_URI_FIELD)),
		             listofKmsCertUris_p, &length);

	if (error)
	{
		free((void *)buffer.pointer);
		return error;
	}

	/*
	*  Loop through external certificates.
	*/
	for (i = 0, index = 1; i != number; ++i)
	{
		/*
		*  Get external certificate. Add to list if valid. Note that this
		*  will skip all those certificates skipped by previous loop.
		*/
		if (GetData(array_p[i], &certificate) == SUCCESS)
		{
			error = add_list(index, to_string(get_field(certificate,
				                                        CERT_URI_FIELD)),
				             listofKmsCertUris_p, &length);

			if (error)
			{
				free((void *)buffer.pointer);
				return error;
			}

			++index;
		}
	}

	/*
	*  Return successfully.
	*/
	free((void *)buffer.pointer);
	return SUCCESS;
}


/*
*  Converts a user URI into a Mikey-Sakke UID.
*  Args: secCtx    - Security context (input).
*        userUri   - User URI (input).
*        timestamp - UTC timestamp (input). If string is empty then uses
*                    current time.
*        userId_p  - *userId_p is set to Mikey-Sakke UID.
*  Rets: Standard error code.
*/
ScErrno GetMikeySakkeUid(SecCtx secCtx, String userUri, String timestamp,
	                     String *userId_p)
{
	/*
	*  Variables used below.
	*/
	Buffer certificate;
	ScErrno error;

	/*
	*  Sanity check on input.
	*/
	if (!check_string(userUri))
	{
		return INPUT_STRING_INVALID;
	}

	/*
	*  Get and validate context array.
	*/
	error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Set timestamp if required.
	*/
	if (timestamp.length == 0 || timestamp.pointer == NULL)
	{
		int year, month, yday;
		error = GetDateTime(&timestamp, &year, &month, &yday);

		if (error)
		{
			return error;
		}
	}
	else if (!is_timestamp(timestamp))
	{
		return TIMESTAMP_FORMAT_INVALID;
	}

	/*
	*  Get certificate matching user URI.
	*/
	certificate = find_certificate_uri_timestamp(userUri, timestamp);

	if (certificate.pointer == NULL)
	{
		return MS_URI_NOT_MATCH;
	}

	/*
	*  Determine user ID. Return success or failure as appropriate.
	*/
	*userId_p = get_uid(certificate, timestamp, userUri);
	return userId_p->pointer == 0 ? CERTIFICATE_INVALID : SUCCESS;
}


/*
*  Returns if timestamp and URI combination is provisioned for use
*  (decryption and signing).
*  Args: secCtx    - Security context (input).
*        userUri   - User URI (input).
*        timestamp - UTC timestamp (input).
*  Rets: True if provisioned, false if not.
*/
Boolean IsMyUriReady(SecCtx secCtx, String userUri, String timestamp)
{
	/*
	*  Variables used below.
	*/
	String uid;
	ScErrno error;

	/*
	*  Sanity checks on input.
	*/
	if (!check_string(userUri) || !check_string(timestamp))
	{
		return INPUT_STRING_INVALID;
	}

	/*
	*  Get and validate context array.
	*/
	error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Get UID from URI.
	*/
	error = GetMikeySakkeUid(secCtx, userUri, timestamp, &uid);

	if (error)
	{
		return error;
	}

	/*
	*  Return if can find matching certificate and key pack.
	*/
	return find_certificate_uri_timestamp(userUri, timestamp).pointer != NULL
		   && find_key_pack_uid(uid, timestamp);
}


/*
*  Returns if timestamp and URI combination is provisioned for use
*  (encryption and verification).
*  Args: secCtx    - Security context (input).
*        userUri   - User URI (input).
*        timestamp - UTC timestamp (input).
*  Returns true if provisioned, false if not.
*/
Boolean IsTheirUriReady(SecCtx secCtx, String userUri, String timestamp)
{
	/*
	*  Variables used below.
	*/
	Buffer certificate;
	ScErrno error;

	/*
	*  Sanity checks on input.
	*/
	if (!check_string(userUri) || !check_string(timestamp))
	{
		return INPUT_STRING_INVALID;
	}

	/*
	*  Get and validate context array.
	*/
	error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Find certificate matching URI and verify matches timestamp.
	*/
	certificate = find_certificate_uri_timestamp(userUri, timestamp);

	/*
	*  Return if certificate found.
	*/
	return certificate.pointer != NULL;
}


/*
*  Get list of user URIs with stored keys.
*  Args: secCtx           - Security context (input).
*        timestamp        - UTC timestamp (input).
*        listofUserUris_p - *listofUserUris_p is set to indicate
*                           list of user URIs with valid keys at
*                           indicated time (unless timestamp is
*                           empty).
*  Rets: Standard error code.
*/
ScErrno ListKeyedUserUris(SecCtx secCtx, String timestamp,
	                      StringList *listofUserUris_p)
{
	/*
	*  Variables used below.
	*/
	Buffer buffer;
	DataRef *kp_refs;
	Buffer kp_buffer;
	size_t number;
	size_t length;
	size_t i, j;

	/*
	*  Get and validate context array.
	*/
	ScErrno error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Get copy of array of key pack data references.
	*/
	buffer = get_array_copy(KEY_PACKS_INDEX, 0, &number);
	kp_refs = (DataRef *)buffer.pointer;

	/*
	*  Replace array of key pack data references by only
	*  those that are allocated and which match timestamp
	*  (if not empty). To do this, loop through key packs,
	*  using i to indicate key pack being considered, j for
	*  that being written. Store data references of URIs.
	*/
	for (i = 0, j = 0; i != number; ++i)
	{
		/*
		*  Skip key pack if not allocated.
		*/
		if (kp_refs[i] == 0)
		{
			continue;
		}

		/*
		*  Only skip if timestamp is not empty, but does not match.
		*/
		if (timestamp.pointer != NULL)
		{
			if (!match_key_pack(kp_refs[i], fixed_string(0, NULL), timestamp))
			{
				continue;
			}
		}

		/*
		*  Key pack matches, so update array, recording URI data reference
		*  and incrementing j. Skip if unable to get key pack information.
		*/
		if (GetData(kp_refs[i], &kp_buffer))
		{
			continue;
		}

		kp_refs[j++] = ((DataRef *)kp_buffer.pointer)[KP_URI_INDEX];
	}

	/*
	*  Create empty list of URIs
	*/
	number = j;
	error = create_list(number, listofUserUris_p, &length);

	if (error)
	{
		free((void *)buffer.pointer);
		return error;
	}

	/*
	*  Add URIs to list.
	*/
	for (i = 0; i != number; ++i)
	{
		Buffer uri;
		error = GetData(kp_refs[i], &uri);

		if (error)
		{
			free((void *)buffer.pointer);
			return error;
		}

		error = add_list(i, to_string(uri), listofUserUris_p, &length);

		if (error)
		{
			free((void *)buffer.pointer);
			return error;
		}
	}

	/*
	*  Free memory and return success.
	*/
	free((void *)buffer.pointer);
	return SUCCESS;
}


/*
*  Get list of user UIDs with stored keys.
*  Args: secCtx                 - Security context (input).
*        userUri                - User URI (input). If empty then consider
*                                 all user UIDs.
*        timestamp              - UTC timestamp (input). If empty then do
*                                 not validate timestamp
*        listofMikeySakkeUids_p - *listofMikeySakkeUids_p is set to indicate
*                                 list of user UIDs with valid keys matching
*                                 parameters as indicated.
*  Rets: Standard error code.
*/
ScErrno ListKeyedMikeySakkeUids(SecCtx secCtx, String userUri, String timestamp,
	                            StringList *listofMikeySakkeUids_p)
{
	/*
	*  Variables used below.
	*/
	Buffer buffer;
	DataRef *kp_refs;
	Buffer kp_buffer;
	size_t number;
	size_t length;
	size_t i, j;

	/*
	*  Get and validate context array.
	*/
	ScErrno error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Get copy of array of key pack data references.
	*/
	buffer = get_array_copy(KEY_PACKS_INDEX, 0, &number);
	kp_refs = (DataRef *)buffer.pointer;

	/*
	*  Replace array of key pack data references by only considering
	*  those that are allocated and which match URI and timestamp
	*  (each if not empty). To do this, loop through key packs,
	*  using i to indicate key pack being considered, j for
	*  that being written. Store data references of UIDs.
	*/
	for (i = 0, j = 0; i != number; ++i)
	{
		/*
		*  Skip key pack if not allocated.
		*/
		if (kp_refs[i] == 0)
		{
			continue;
		}

		/*
		*  Only skip if URI or timestamp is not empty, but do not match.
		*/
		if (userUri.pointer != NULL || timestamp.pointer != NULL)
		{
			if (!match_key_pack(kp_refs[i], userUri, timestamp))
			{
				continue;
			}
		}

		/*
		*  Key pack matches, so update array, recording URI data reference
		*  and incrementing j. Skip if unable to get key pack information.
		*/
		if (GetData(kp_refs[i], &kp_buffer))
		{
			continue;
		}

		kp_refs[j++] = ((DataRef *)kp_buffer.pointer)[KP_UID_INDEX];
	}

	/*
	*  Create empty list of UIDs
	*/
	number = j;
	error = create_list(number, listofMikeySakkeUids_p, &length);

	if (error)
	{
		free((void *)buffer.pointer);
		return error;
	}

	/*
	*  Add UIDs to list.
	*/
	for (i = 0; i != number; ++i)
	{
		Buffer uid;
		error = GetData(kp_refs[i], &uid);

		if (error)
		{
			free((void *)buffer.pointer);
			return error;
		}

		error = add_list(i, to_string(uid), listofMikeySakkeUids_p, &length);

		if (error)
		{
			free((void *)buffer.pointer);
			return error;
		}
	}

	/*
	*  Free memory and return success.
	*/
	free((void *)buffer.pointer);
	return SUCCESS;
}


/*
*  Store UDK, SSK and PVT for given user ID. These are provided encrypted
*  using indicated transport key using indicated transport algorithm.
*  Args: secCtx                       - Security context (input).
*        userUri                      - User URI (input).
*        userId                       - User ID (input).
*        transportAlg                 - Indicates algorithm used to encrypt
*                                       UDK, SSK and PVT, either "kw-aes128"
*                                       or "kw-aes-256" (input).
*        transportKeyId               - Identity of key used to encrypt UDK,
*                                       SSK and PVT (input).
*        validFrom                    - Timestamp from which key is valid
*                                       (input).
*        validTo                      - Timestamp from which key is invalid
*                                       (input).
*        encryptedSakkeUserDecryptKey - Encrypted SAKKE UDK (input).
*        encryptedEccsiSsk            - Encrypted ECCSI SSK (input).
*        encryptedEccsiPvt            - Encrypted ECCSI PVT (input).
*  Rets: Standard error code.
*/
ScErrno StoreKeyPack(SecCtx secCtx, String userUri, String userId,
	                 const char *transportAlg, String transportKeyId,
	                 String validFrom, String validTo,
	                 Buffer encryptedSakkeUserDecryptKey,
	                 Buffer encryptedEccsiSsk, Buffer encryptedEccsiPvt)
{
	/*
	*  Variables used below.
	*/
	Buffer udk_buf;
	Buffer ssk_buf;
	Buffer pvt_buf;
	DataRef data_refs[KEY_PACK_LENGTH];
	String identity;
	EC_POINT *kpak;
	EC_POINT *udk;
	BIGNUM *ssk;
	EC_POINT *pvt;
	ScErrno error;
	Boolean success;
	size_t length;

	/*
	*  Sanity checks on input.
	*/
	if (transportAlg == NULL)
	{
		return INPUT_PTR_NULL;
	}

	if (!check_buffer(encryptedSakkeUserDecryptKey)
		|| !check_buffer(encryptedEccsiSsk) || !check_buffer(encryptedEccsiPvt))
	{
		return INPUT_BUFFER_INVALID;
	}

	if (!check_string(userUri) || !check_string(userId)
		|| !check_string(transportKeyId) || !check_string(validFrom)
		|| !check_string(validTo))
	{
		return INPUT_STRING_INVALID;
	}

	/*
	*  Verify algorithm and set AES key length.
	*/
	if (strcmp(transportAlg, "kw-aes128") == 0)
	{
		length = 16;
	}
	else if (strcmp(transportAlg, "kw-aes256") == 0)
	{
		length = 32;
	}
	else
	{
		return ALGORITHM_INVALID;
	}

	/*
	*  Get and validate context array.
	*/
	error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Verify and decrypt key pack.
	*/
	error = decrypt_key_pack(length, transportKeyId,
		                     encryptedSakkeUserDecryptKey, encryptedEccsiSsk,
							 encryptedEccsiPvt, &udk_buf, &ssk_buf, &pvt_buf);

	if (error)
	{
		return error;
	}

	if (udk_buf.length != 2 * SAKKE_SIZE || ssk_buf.length != ECCSI_SIZE
		|| pvt_buf.length != 2 * ECCSI_SIZE)
	{
		return KEY_UNWRAP_FAIL;
	}

	/*
	*  Get information from certificate matching valid from and to
	*  timestamps. Note that KPAK is now owned by this function.
	*/
	error = eccsi_certificate(userUri, validFrom, validTo, &identity, &kpak);

	if (error)
	{
		return error;
	}

	/*
	*  Verify UDK, SSK and PVT.
	*/
	udk = sakke_point(udk_buf, false);

	if (udk == 0)
	{
		EC_POINT_clear_free(kpak);
		purge_buffer(&udk_buf);
		purge_buffer(&ssk_buf);
		purge_buffer(&pvt_buf);
		return MS_UID_KEYS_NOT_MATCH;
	}

	EC_POINT_clear_free(udk);
	ssk = BN_bin2bn(ssk_buf.pointer, (int)ssk_buf.length, 0);

	if (ssk == 0)
	{
		EC_POINT_clear_free(kpak);
		purge_buffer(&udk_buf);
		purge_buffer(&ssk_buf);
		purge_buffer(&pvt_buf);
		return MS_UID_KEYS_NOT_MATCH;
	}

	pvt = eccsi_point(pvt_buf, false);

	if (pvt == 0)
	{
		EC_POINT_clear_free(kpak);
		BN_clear_free(ssk);
		purge_buffer(&udk_buf);
		purge_buffer(&ssk_buf);
		purge_buffer(&pvt_buf);
		return MS_UID_KEYS_NOT_MATCH;
	}

	success = eccsi_verify_key(identity, kpak, ssk, pvt);
	EC_POINT_clear_free(kpak);
	BN_clear_free(ssk);
	EC_POINT_clear_free(pvt);

	if (!success)
	{
		purge_buffer(&udk_buf);
		purge_buffer(&ssk_buf);
		purge_buffer(&pvt_buf);
		return ECCSI_NOT_VERIFIED;
	}

	/*
	*  Store key pack data.
	*/
	error = StoreSecureData(to_buffer(userUri), true, &data_refs[KP_URI_INDEX]);

	if (error)
	{
		purge_buffer(&udk_buf);
		purge_buffer(&ssk_buf);
		purge_buffer(&pvt_buf);
		return error;
	}

	error = StoreSecureData(to_buffer(userId), true, &data_refs[KP_UID_INDEX]);

	if (error)
	{
		PurgeData(data_refs[KP_URI_INDEX]);
		purge_buffer(&udk_buf);
		purge_buffer(&ssk_buf);
		purge_buffer(&pvt_buf);
		return error;
	}

	error = StoreSecureData(to_buffer(transportKeyId), true,
		                    &data_refs[KP_TK_ID_INDEX]);

	if (error)
	{
		PurgeData(data_refs[KP_URI_INDEX]);
		PurgeData(data_refs[KP_UID_INDEX]);
		purge_buffer(&udk_buf);
		purge_buffer(&ssk_buf);
		purge_buffer(&pvt_buf);
		return error;
	}

	error = StoreSecureData(to_buffer(validFrom), true,
		                    &data_refs[KP_FROM_INDEX]);

	if (error)
	{
		PurgeData(data_refs[KP_URI_INDEX]);
		PurgeData(data_refs[KP_UID_INDEX]);
		PurgeData(data_refs[KP_TK_ID_INDEX]);
		purge_buffer(&udk_buf);
		purge_buffer(&ssk_buf);
		purge_buffer(&pvt_buf);
		return error;
	}

	error = StoreSecureData(to_buffer(validTo), true, &data_refs[KP_TO_INDEX]);

	if (error)
	{
		PurgeData(data_refs[KP_URI_INDEX]);
		PurgeData(data_refs[KP_UID_INDEX]);
		PurgeData(data_refs[KP_TK_ID_INDEX]);
		PurgeData(data_refs[KP_FROM_INDEX]);
		purge_buffer(&udk_buf);
		purge_buffer(&ssk_buf);
		purge_buffer(&pvt_buf);
		return error;
	}

	error = StoreSecureData(udk_buf, true, &data_refs[KP_SAKKE_UDK_INDEX]);
	purge_buffer(&udk_buf);

	if (error)
	{
		PurgeData(data_refs[KP_URI_INDEX]);
		PurgeData(data_refs[KP_UID_INDEX]);
		PurgeData(data_refs[KP_TK_ID_INDEX]);
		PurgeData(data_refs[KP_FROM_INDEX]);
		PurgeData(data_refs[KP_TO_INDEX]);
		purge_buffer(&ssk_buf);
		purge_buffer(&pvt_buf);
		return error;
	}

	error = StoreSecureData(ssk_buf, true, &data_refs[KP_ECCSI_SSK_INDEX]);
	purge_buffer(&ssk_buf);

	if (error)
	{
		PurgeData(data_refs[KP_URI_INDEX]);
		PurgeData(data_refs[KP_UID_INDEX]);
		PurgeData(data_refs[KP_TK_ID_INDEX]);
		PurgeData(data_refs[KP_FROM_INDEX]);
		PurgeData(data_refs[KP_TO_INDEX]);
		PurgeData(data_refs[KP_SAKKE_UDK_INDEX]);
		purge_buffer(&pvt_buf);
		return error;
	}

	error = StoreSecureData(pvt_buf, true, &data_refs[KP_ECCSI_PVT_INDEX]);
	purge_buffer(&pvt_buf);

	if (error)
	{
		PurgeData(data_refs[KP_URI_INDEX]);
		PurgeData(data_refs[KP_UID_INDEX]);
		PurgeData(data_refs[KP_TK_ID_INDEX]);
		PurgeData(data_refs[KP_FROM_INDEX]);
		PurgeData(data_refs[KP_TO_INDEX]);
		PurgeData(data_refs[KP_SAKKE_UDK_INDEX]);
		PurgeData(data_refs[KP_ECCSI_SSK_INDEX]);
		return error;
	}

	/*
	*  Store key pack data references.
	*/
	error = extend_array(secCtx, KEY_PACKS_INDEX,
		                 fixed_buffer(sizeof(data_refs), data_refs));

	if (error)
	{
		PurgeData(data_refs[KP_URI_INDEX]);
		PurgeData(data_refs[KP_UID_INDEX]);
		PurgeData(data_refs[KP_TK_ID_INDEX]);
		PurgeData(data_refs[KP_FROM_INDEX]);
		PurgeData(data_refs[KP_TO_INDEX]);
		PurgeData(data_refs[KP_SAKKE_UDK_INDEX]);
		PurgeData(data_refs[KP_ECCSI_SSK_INDEX]);
		PurgeData(data_refs[KP_ECCSI_PVT_INDEX]);
		return error;
	}

	/*
	*  Successful exit after purging keys.
	*/
	purge_buffer(&udk_buf);
	purge_buffer(&ssk_buf);
	purge_buffer(&pvt_buf);
	return SUCCESS;
}


/*
*  Remove user (all keys).
*  Args: secCtx  - Security context (input).
*        userUri - User URI (input).
*  Rets: Standard error code.
*/
ScErrno RemoveUser(SecCtx secCtx, String userUri)
{
	/*
	*  Variables used below.
	*/
	Buffer key_packs;
	DataRef *kp_p1;
	DataRef *kp_p2;
	size_t number;
	size_t i;

	/*
	*  Get and validate context array.
	*/
	ScErrno error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Get copy of array of key pack data references, or return failure.
	*/
	key_packs = get_array_copy(KEY_PACKS_INDEX, 0, &number);
	kp_p1 = (DataRef *)key_packs.pointer;

	if (kp_p1 == 0)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	/*
	*  Make second copy of key pack data references, or return failure.
	*/
	kp_p2 = (DataRef *)malloc(number * sizeof(DataRef));

	if (kp_p2 == 0)
	{
		free(kp_p1);
		return RESOURCE_NOT_ALLOCATED;
	}

	memcpy(kp_p2, kp_p1, key_packs.length);

	/*
	*  Loop through key packs. Set kp_p2 entries to zero if not to be
	*  purged, left as data reference to be purged otherwise. Set kp_p1
	*  entries to values to be used for update (i.e set to zero if to
	*  be purged).
	*/
	for (i = 0; i != number; ++i)
	{
		/*
		*  Variables used below.
		*/
		Buffer buffer;
		DataRef *data_refs;

		/*
		*  Get array of data references for this key pack.
		*  If none then this is not to be purged.
		*/
		if (GetData(kp_p1[i], &buffer))
		{
			kp_p2[i] = 0;
			continue;
		}

		data_refs = (DataRef *)buffer.pointer;

		/*
		*  Get URI for this key pack. If none then this key pack is not
		*  to be purged (though should not occur).
		*/
		if (GetData(data_refs[KP_URI_INDEX], &buffer))
		{
			kp_p2[i] = 0;
			continue;
		}

		/*
		*  If not matching URI then this key pack is not to be purged.
		*/
		if (!string_equal(userUri, to_string(buffer)))
		{
			kp_p2[i] = 0;
			continue;
		}

		/*
		*  To be purged.
		*/
		kp_p1[i] = 0;
	}

	/*
	*  Update key pack data references. If fail then maintain
	*  consistency of data structure.
	*/
	error = UpdateData(context_array[KEY_PACKS_INDEX], key_packs);

	if (error)
	{
		free(kp_p1);
		free(kp_p2);
		return error;
	}

	/*
	*  Purge key packs. Zero values will be ignored.
	*/
	for (i = 0; i != number; ++i)
	{
		purge_key_pack(kp_p2[i]);
	}

	/*
	*  Free allocated memory and return success.
	*/
	free(kp_p1);
	free(kp_p2);
	return SUCCESS;
}


/*
*  Remove UID (single key set).
*  Args: secCtx  - Security context (input).
*        userUid - User identity (input).
*  Rets: Standard error code.
*/
ScErrno RemoveUid(SecCtx secCtx, String userId)
{
	/*
	*  Variables used below.
	*/
	Buffer key_packs;
	DataRef *kp_p;
	DataRef kp_ref;
	size_t index;

	/*
	*  Get and validate context array.
	*/
	ScErrno error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Find key pack data reference matching UID, if any, oldest if more than
	*  one. Note remove offset in value returned from find_key_packs_uid.
	*/
	index = find_key_packs_uid(userId, &key_packs);

	if (index == 0)
	{
		return RESOURCE_NOT_EXIST;
	}

	--index;
	kp_p = (DataRef *)key_packs.pointer;

	/*
	*  Remove key pack.
	*/
	kp_ref = kp_p[index];
	kp_p[index] = 0;

	error = UpdateData(context_array[KEY_PACKS_INDEX], key_packs);

	if (error)
	{
		free((void *)key_packs.pointer);
		return error;
	}

	/*
	*  Remove data, and exit successfully.
	*/
	PurgeData(kp_ref);
	free((void *)key_packs.pointer);
	return SUCCESS;
}


/*
*  Store new transport key.
*  Args: secCtx                   - Security context (input).
*        transportAlg             - Indicates algorithm used, either
*                                   "kw-aes128" or "kw-aes-256" (input).
*        transportKeyId           - Transport key identity (input).
*        newTransportKeyId        - New transport key identity (input).
*        encryptedNewTransportKey - Encrypted new transport key.
*  Rets: Standard error code.
*/
ScErrno StoreTk(SecCtx secCtx, String userUri, const char *transportAlg,
	            String transportKeyId, String newTransportKeyId,
	            Buffer encryptedNewTransportKey)
{
	/*
	*  Variables used below.
	*/
	Buffer old_key;
	Buffer new_key;
	size_t length;
	ScErrno error;

	/*
	*  Verify algorithm, recording key length.
	*/
	if (strcmp(transportAlg, "kw-aes128") == 0)
	{
		length = TRK_AES_128_SIZE;
	}
	else if (strcmp(transportAlg, "kw-aes256") == 0)
	{
		length = TRK_AES_256_SIZE;
	}
	else
	{
		return ALGORITHM_INVALID;
	}

	/*
	*  Get and validate context array.
	*/
	error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Find matching transport key and verify length.
	*/
	error = get_transport_key(transportKeyId, &old_key);

	if (error)
	{
		return error;
	}

	if (old_key.length != length)
	{
		return TRK_INVALID;
	}

	/*
	*  Decrypt new key and validate length is one allowed.
	*/
	error = kw_decrypt(old_key, encryptedNewTransportKey, &new_key);

	if (error)
	{
		return error;
	}

	if (new_key.length != TRK_AES_128_SIZE
		&& new_key.length != TRK_AES_256_SIZE)
	{
		purge_buffer(&new_key);
		return TRK_INVALID;
	}

	/*
	*  Save new key and key ID, clearing former storage, and return
	*  success, unless unable to save new information.
	*/
	error = extend_array(secCtx, TK_INDEX, new_key);

	if (error)
	{
		purge_buffer(&new_key);
		return error;
	}

	purge_buffer(&new_key);
	error = extend_array(secCtx, TK_ID_INDEX, to_buffer(newTransportKeyId));

	if (error)
	{
		PurgeData(context_array[TK_INDEX]);
		return error;
	}

	return SUCCESS;
}


/*
*  Get list of transport key identities.
*  Args: secCtx        - Security context (input).
*        listofTkIds_p - *listofTkIds_p is set to indicate
*                        list of transport key identities.
*  Rets: Standard error code.
*/
ScErrno ListTkIds(SecCtx secCtx, StringList *listofTkIds_p)
{
	/*
	*  Variables used below.
	*/
	Buffer buffer;
	Buffer identity;
	const DataRef *data_refs;
	size_t number;
	size_t number_ids;
	size_t length;
	size_t index;
	size_t i;

	/*
	*  Get and validate context array.
	*/
	ScErrno error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Get array of transport key ID data references, and number.
	*  Result may be null, in which case number will be zero, which
	*  can be handled by following code.
	*/
	buffer = get_array_copy(TK_ID_INDEX, 0, &number);
	data_refs = (DataRef *)buffer.pointer;

	/*
	*  Loop through transport key IDs to count how many are allocated.
	*/
	number_ids = 0;

	for (i = 0; i != number; ++i)
	{
		if (data_refs[i] != 0)
		{
			++number_ids;
		}
	}

	/*
	*  Initialise list.
	*/
	error = create_list(number_ids, listofTkIds_p, &length);

	if (error)
	{
		free((void *)buffer.pointer);
		return error;
	}

	/*
	*  Loop through external certificates.
	*/
	for (i = 0, index = 0; i != number; ++i)
	{
		/*
		*  Get transport key ID. Add to list if valid. Note that this
		*  will skip all those identities skipped by previous loop.
		*/
		if (GetData(data_refs[i], &identity) == SUCCESS)
		{
			error = add_list(index, to_string(identity), listofTkIds_p,
				             &length);

			if (error)
			{
				free((void *)buffer.pointer);
				return error;
			}

			++index;
		}
	}

	/*
	*  Return successfully.
	*/
	free((void *)buffer.pointer);
	return SUCCESS;
}


/*
*  Remove a stored transport key.
*  Args: secCtx         - Security context (input).
*        transportKeyId - Transport key identity (input).
*  Rets: Standard error code.
*/
ScErrno PurgeTk(SecCtx secCtx, String transportKeyId)
{
	/*
	*  Variable used below.
	*/
	size_t index;

	/*
	*  Get and validate context array.
	*/
	const ScErrno error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Find matching transport key ID. If none then fail.
	*/

	index = find_transport_key(transportKeyId);

	if (index == 0)
	{
		return RESOURCE_NOT_EXIST;
	}

	/*
	*  Purge transport key ID and transport key and reset context.
	*/
	--index;

	purge_array_item(TK_ID_INDEX, index);
	purge_array_item(TK_INDEX, index);
	return reset_context(secCtx);
}


/*
*  Remove all keys and all certificates for a context.
*  Args: secCtx - Security context (input).
*  Rets: True if successful, false if not successful.
*/
Boolean PurgeKeys(SecCtx secCtx)
{
	/*
	*  Renove this context, also verifying it.
	*/
	if (remove_security_context(secCtx))
	{
		return false;
	}

	/*
	*  Purge external certificates.
	*/
	purge_array(EXT_CERTS_INDEX);

	/*
	*  Purge root certificate.
	*/
	PurgeData(context_array[ROOT_CERT_INDEX]);

	/*
	*  Purge key material.
	*/
	purge_array(TK_INDEX);
	purge_array(TK_ID_INDEX);
	PurgeData(context_array[KMS_URI_INDEX]);
	purge_key_packs();

	/*
	*  Purge context and return success.
	*/
	PurgeData(secCtx);
	return true;
}


/*
*  Generates SSV and encrypts using SAKKE. (Used at start of
*  communications.)
*  Args: secCtx       - Security context (input).
*        recipientUri - URI of intended recipient (input).
*        timestamp    - UTC timestamp (input).
*        sed_p        - *sed_p is set to encrypted SSV.
*        ssv_p        - *ssv_p is set to SSV.
*  Rets: Standard error code.
*/
ScErrno GenerateSharedSecretAndSakkeEncrypt(SecCtx secCtx, String recipientUri,
	                                        String timestamp, Buffer *sed_p,
	                                        Buffer *ssv_p)
{
	/*
	*  Variables used below.
	*/
	String identity;
	EC_POINT *z;
	void *pointer;
	ScErrno error;

	/*
	*  Sanity checks on inputs.
	*/
	if (!check_string(recipientUri) || !check_string(timestamp))
	{
		return INPUT_STRING_INVALID;
	}

	if (sed_p == NULL || ssv_p == NULL)
	{
		return INPUT_PTR_NULL;
	}

	/*
	*  Get and validate context array.
	*/
	error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Find certificate matching URI and timestamp, and extract identity
	*  and KPAK.
	*/
	error = sakke_certificate(recipientUri, timestamp, &identity, &z);

	if (error)
	{
		return error;
	}

	/*
	*  Allocate data for both SSV and SED.
	*/
	pointer = realloc(result_p, 2 * SAKKE_KEY_SIZE + 1 + 2 * SAKKE_SIZE);

	if (pointer == 0)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	result_p = pointer;

	/*
	*  Generate SSV.
	*/
	*ssv_p = fixed_buffer(SAKKE_KEY_SIZE, result_p);
	error = GetSecureRandom(*ssv_p);

	if (error)
	{
		return error;
	}

	/*
	*  Encrypt SSV.
	*/
	*sed_p = fixed_buffer(SAKKE_KEY_SIZE + 1 + 2 * SAKKE_SIZE,
		                  (unsigned char *)result_p + SAKKE_KEY_SIZE);
	error = sakke_encrypt(identity, z, *ssv_p, *sed_p);
	EC_POINT_clear_free(z);
	return error;
}


/*
*  Encrypts data using SAKKE.
*  Args: secCtx          - Security context (input).
*        recipientUri    - URI of intended recipient (input).
*        timestamp       - UTC timestamp (input).
*        data            - Data to be encrypted (input).
*        encryptedData_p - *encryptedData_p is set to
*                          encrypted data.
*  Rets: Standard error code.
*/
ScErrno SakkeEncrypt(SecCtx secCtx, String recipientUri, String timestamp,
	                 Buffer data, Buffer *encryptedData_p)
{
	/*
	*  Variables used below.
	*/
	String identity;
	EC_POINT *z;
	void *pointer;

	/*
	*  Get and validate context array.
	*/
	ScErrno error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Sanity checks on inputs.
	*/
	if (!check_string(recipientUri) || !check_string(timestamp))
	{
		return INPUT_STRING_INVALID;
	}

	if (!check_buffer(data))
	{
		return INPUT_BUFFER_INVALID;
	}

	/*
	*  Find certificate matching URI and timestamp, and extract identity
	*  and KPAK.
	*/
	error = sakke_certificate(recipientUri, timestamp, &identity, &z);

	if (error)
	{
		return error;
	}

	/*
	*  Allocate memory for encrypted data.
	*/
	encryptedData_p->length = data.length + 1 + 2 * SAKKE_SIZE;
	pointer = realloc(result_p, encryptedData_p->length);
	encryptedData_p->pointer = (const unsigned char *)pointer;

	if (encryptedData_p->pointer == 0)
	{
		encryptedData_p->length = 0;
		EC_POINT_clear_free(z);
		return RESOURCE_NOT_ALLOCATED;
	}

	result_p = pointer;

	/*
	*  Encrypt data, clean up, and exit.
	*/
	error = sakke_encrypt(identity, z, data, *encryptedData_p);
	EC_POINT_clear_free(z);
	return error;
}


/*
*  Decrypts data using SAKKE.
*  Args: secCtx          - Security context (input).
*        recipientUri    - URI of sender (input).
*        timestamp       - UTC timestamp (input).
*        data            - Data to be decrypted (input).
*        decryptedData_p - *decryptedData_p is set to
*                          decrypted data.
*  Rets: Standard error code.
*/
ScErrno SakkeDecrypt(SecCtx secCtx, String recipientUri, String timestamp,
	                 Buffer data, Buffer *decryptedData_p)
{
	/*
	*  Variables used below.
	*/
	String identity;
	const EC_GROUP *curve;
	BN_CTX *ctx;
	EC_POINT *z;
	EC_POINT *rsk;
	BIGNUM *x;
	BIGNUM *y;
	void *pointer;
	DataRef data_ref;
	Buffer buffer;
	String uid;
	ScErrno error;

	/*
	*  Sanity checks on inputs.
	*/
	if (!check_string(recipientUri) || !check_string(timestamp))
	{
		return INPUT_STRING_INVALID;
	}

	if (!check_buffer(data))
	{
		return INPUT_BUFFER_INVALID;
	}

	/*
	*  Verify encrypted data length.
	*/
	if (data.length <= 1 + 2 * SAKKE_SIZE)
	{
		return SAKKE_DECRYPT_FAIL;
	}

	/*
	*  Get and validate context array.
	*/
	error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Find certificate matching URI and timetamp, and extract identity
	*  and Z.
	*/
	error = sakke_certificate(recipientUri, timestamp, &identity, &z);

	if (error)
	{
		return error;
	}

	/*
	*  Get data reference of matching key pack, if any.
	*/
	error = GetMikeySakkeUid(secCtx, recipientUri, timestamp, &uid);

	if (error)
	{
		return error;
	}

	data_ref = find_key_pack_uid(uid, timestamp);

	if (data_ref == 0)
	{
		EC_POINT_clear_free(z);
		return MS_URI_NOT_MATCH;
	}

	/*
	*  Get key pack.
	*/
	error = GetData(data_ref, &buffer);

	if (error)
	{
		EC_POINT_clear_free(z);
		return error;
	}

	/*
	*  Get elliptic curve and object required.
	*/
	curve = sakke_curve();

	if (curve == 0)
	{
		return RESOURCE_NOT_EXIST;
	}

	ctx = bn_ctx();

	if (ctx == 0)
	{
		return RESOURCE_NOT_EXIST;
	}

	/*
	*  Get UDK (RSK).
	*/
	error = GetData(((DataRef *)buffer.pointer)[KP_SAKKE_UDK_INDEX], &buffer);

	if (error)
	{
		EC_POINT_clear_free(z);
		return error;
	}

	x = BN_new();

	if (x == 0)
	{
		EC_POINT_clear_free(z);
		return RESOURCE_NOT_EXIST;
	}

	if (BN_bin2bn(buffer.pointer, SAKKE_SIZE, x) == 0)
	{
		EC_POINT_clear_free(z);
		BN_clear_free(x);
		return RESOURCE_NOT_EXIST;
	}

	y = BN_new();

	if (y == 0)
	{
		EC_POINT_clear_free(z);
		BN_clear_free(x);
		return RESOURCE_NOT_EXIST;
	}

	if (BN_bin2bn(buffer.pointer + SAKKE_SIZE, SAKKE_SIZE, y) == 0)
	{
		EC_POINT_clear_free(z);
		BN_clear_free(x);
		BN_clear_free(y);
		return RESOURCE_NOT_EXIST;
	}

	rsk = EC_POINT_new(curve);

	if (rsk == 0)
	{
		EC_POINT_clear_free(z);
		BN_clear_free(x);
		BN_clear_free(y);
		return RESOURCE_NOT_EXIST;
	}

	if (!big_ec(rsk, x, y, curve, ctx))
	{
		EC_POINT_clear_free(z);
		BN_clear_free(x);
		BN_clear_free(y);
		EC_POINT_clear_free(rsk);
		return RESOURCE_NOT_EXIST;
	}

	/*
	*  Free temporary variables.
	*/
	BN_clear_free(x);
	BN_clear_free(y);

	/*
	*  Allocate memory for decrypted data.
	*/
	decryptedData_p->length = data.length - (1 + 2 * SAKKE_SIZE);
	pointer = realloc(result_p, decryptedData_p->length);
	decryptedData_p->pointer = (const unsigned char *)pointer;

	if (decryptedData_p->pointer == NULL)
	{
		EC_POINT_clear_free(z);
		EC_POINT_clear_free(rsk);
		decryptedData_p->length = 0;
		return RESOURCE_NOT_ALLOCATED;
	}

	result_p = pointer;

	/*
	*  Decrypt data, clean up, and exit.
	*/
	error = sakke_decrypt(identity, z, rsk, data, *decryptedData_p);
	EC_POINT_clear_free(z);
	EC_POINT_clear_free(rsk);
	return error;
}


/*
*  Creates a key and salt from a master key and session information as
*  defined in RFC 3830 (MIKEY), but using SHA-256 rather than SHA-1 for
*  larger key and salt values.
*  Args: masterKey    - Master key (input).
*        csbId        - Crypto session bundle ID (input).
*        csId         - Crypto session ID (input).
*        rndNum       - Random data (RAND), must be of length 16 bytes (input).
*        key          - Key, must provide length of 16 bytes (output).
*        salt         - Salt, must provide length of 14, 16 or 32 bytes
*                       (output).
*  Rets: Standard error code.
*/
ScErrno CreateKeySalt(Buffer masterKey, uint32_t csbId, uint8_t csId,
					  Buffer rndNum, Buffer key, Buffer salt)
{
	/*
	*  Variables used below.
	*/
	unsigned char label[KEMAC_LABEL_SIZE] = { 0 };
    ScErrno error;

	/*
	*  Sanity checks on input. Note that masterKey size is verified by
	*  rfc3830_label and salt size is verified by rfc3830_PRF.
	*/
	if (!check_buffer(masterKey) || !check_buffer(rndNum)
		|| !check_buffer(key) || !check_buffer(salt))
	{
		return INPUT_BUFFER_INVALID;
	}

	if (rndNum.length != KEMAC_RAND_SIZE || key.length != AES_KEY_SIZE)
	{
		return INPUT_BUFFER_INVALID;
	}

	/*
	*  Create label, see RFC 3830 Section 4.1.4.
	*  label = Key-Type-Constant(4) + CS-ID(1) + CSB-ID(4) + RAND(16).
	*  Assumes label initialised correctly above, including to zeros.
	*/
	rfc3830_label(fixed_buffer(sizeof(TGK_TEK), TGK_TEK), csId, csbId, rndNum,
		          label);

	/*
	*  Create and set AES encryption key.
    */
	error = rfc3830_PRF(masterKey, fixed_buffer(KEMAC_LABEL_SIZE, label), key);

	if (error != SUCCESS)
	{
		return error;
	}

	/*
	*  Create the salt. Assumes label already initialised but needs constant
	*  changed.
	*/
	memcpy(&label[KEMAC_CONSTANT_LOCATION], TGK_SALTING, sizeof(TGK_SALTING));
	return rfc3830_PRF(masterKey, fixed_buffer(KEMAC_LABEL_SIZE, label), salt);
}


/*
*  Encrypts data using KEMAC, as defined in RFC 3830.
*  Args: csbId        - CSB-ID needed for AES calculation (input).
*        timestamp    - Timestamp needed for AES calculation (input).
*        rndNum         RAND needed for AES calculation (input).
*        gmk            Group Master Key held by all group members (input).
*        gsk            Group Session Key to be encrypted and distributed
*                       (input).
*        kemacEncData - Encrypted data containing the group session key
*                       (output).
*        salt         - Salt used in calculations, to be passed to peers
*                       (output).
*  Rets: Standard error code.
*/
ScErrno KemacEncrypt(uint32_t csbId, String timestamp, Buffer rndNum,
					 Buffer gmk, Buffer gsk, Buffer kemacEncData, Buffer salt)
{
	/*
	*  Variables used below.
	*  Key length is doubled to avoid unexplained problem on Windows.
	*/
	unsigned char label[KEMAC_LABEL_SIZE] = { 0 };
	unsigned char iv[AES_IV_SIZE] = { 0 };
	unsigned char ecount[AES_IV_SIZE] = { 0 };
	unsigned char key[AES_KEY_SIZE * 2] = { 0 };
	AES_KEY aes_key;
	uint64_t time;
	unsigned int number = 0;
	ScErrno error;
	int i;

	/*
	*  Sanity checks on input. Note that gmk length is verified by
	*  rfc3830_label and salt size is verified by rfc3830_PRF.
	*/
	if (!check_buffer(rndNum) || !check_buffer(gmk) || !check_buffer(gsk)
		|| !check_buffer(kemacEncData) || !check_buffer(salt))
	{
		return INPUT_BUFFER_INVALID;
	}

	if (rndNum.length != KEMAC_RAND_SIZE || gsk.length != AES_KEY_SIZE
		|| kemacEncData.length != AES_KEY_SIZE)
	{
		return INPUT_BUFFER_INVALID;
	}

	/*
	*  Verify and convert timestamo.
	*/
	if (!check_string(timestamp))
	{
		return INPUT_STRING_INVALID;
	}

	if (!is_timestamp(timestamp))
	{
		return TIMESTAMP_FORMAT_INVALID;
	}

	time = timestamp64(timestamp);


	/*
	*  Create label, see RFC 3830 Section 4.1.4.
	*  label = Key-Type-Constant(4) + CS-ID(1) + CSB-ID(4) + RAND(16).
	*  Assumes label initialised correctly above, including to zeros.
	*/
	rfc3830_label(fixed_buffer(sizeof(ENV_PSK_ENCRYPTION), ENV_PSK_ENCRYPTION),
		          KEMAC_CS_ID, csbId, rndNum, label);

	/*
	*  Create and set AES encryption key.
    */
	error = rfc3830_PRF(gmk, fixed_buffer(KEMAC_LABEL_SIZE, label),
		                fixed_buffer(AES_KEY_SIZE, key));

	if (error != SUCCESS)
	{
		return error;
	}

	if (AES_set_encrypt_key(key, 8 * AES_KEY_SIZE, &aes_key))
	{
		return AES_SET_ENCRYPT_KEY_FAIL;
	}

	/*
	*  Create the salt. Assumes label already initialised (for different key
	*  or salt type).
	*/
	memcpy(&label[KEMAC_CONSTANT_LOCATION], ENV_PSK_SALTING,
		   sizeof(ENV_PSK_SALTING));
	error = rfc3830_PRF(gmk, fixed_buffer(KEMAC_LABEL_SIZE, label), salt);

	if (error != SUCCESS)
	{
		return error;
	}

	/*
	*  Create IV (Initialisation Vector), see RFC 3830 Section 4.2.3.
	*  IV = (Salt XOR (0x0000 || CSB ID || Timestamp)) || 0x0000.
	*  Note IV is set to all zeros as initialised above.
	*/
	write32(&iv[KEMAC_SALT_CSB_ID_LOCATION], csbId);
	write64(&iv[KEMAC_SALT_TIMESTAMP_LOCATION], time);

	for (i = 0; i < AES_SALT_SIZE; ++i)
	{
		iv[i] ^= salt.pointer[i];
	}

	/*
	*  Encrypt the GSK.
	*/
	AES_ctr128_encrypt(gsk.pointer, (unsigned char *)kemacEncData.pointer,
		               AES_KEY_SIZE, &aes_key, iv, ecount, &number);
	return SUCCESS;
}


/*
*  Decrypts data for KEMAC, as defined in RFC 3830.
*  Args: csbId        - CSB-ID needed for AES calculation (input).
*        timestamp    - Timestamp needed for AES calculation (input).
*        rndNum         RAND needed for AES calculation (input).
*        salt         - Salt used in calculations (input).
*        kemacEncData - Encrypted data containing the group session key
*                       (input).
*        gmk            Group Master Key previously distributed (input).
*        gsk            Group Session Key (output).
*  Rets: Standard error code.
*/
ScErrno KemacDecrypt(uint32_t csbId, String timestamp, Buffer rndNum,
	                 Buffer salt, Buffer kemacEncData, Buffer gmk, Buffer gsk)
{
	/*
	*  Variables used below.
	*  Key length is doubled to avoid unexplained problem on Windows.
	*/
	unsigned char label[KEMAC_LABEL_SIZE] = { 0 };
	unsigned char iv[AES_IV_SIZE] = { 0 };
	unsigned char ecount[AES_IV_SIZE] = { 0 };
	unsigned char key[AES_KEY_SIZE * 2] = { 0 };
	AES_KEY aes_key;
	uint64_t time;
	unsigned int number;
	ScErrno error;

	/*
	*  Sanity checks on input. Note that gmk length is verified by
	*  rfc3830_label.
	*/
	if (!check_buffer(rndNum) || !check_buffer(salt)
		|| !check_buffer(kemacEncData) || !check_buffer(gmk)
		|| !check_buffer(gsk))
	{
		return INPUT_BUFFER_INVALID;
	}

	if (rndNum.length != KEMAC_RAND_SIZE || gsk.length != AES_KEY_SIZE
		|| salt.length < AES_SALT_SIZE || kemacEncData.length != AES_KEY_SIZE)
	{
		return INPUT_BUFFER_INVALID;
	}

	/*
	*  Must have allocated sufficient memeory for AES key.
	*/
	if (gsk.length < AES_KEY_SIZE)
	{
		return INPUT_BUFFER_INVALID;
	}

	/*
	*  Verify and convert timestamo.
	*/
	if (!check_string(timestamp))
	{
		return INPUT_STRING_INVALID;
	}

	if (!is_timestamp(timestamp))
	{
		return TIMESTAMP_FORMAT_INVALID;
	}

	time = timestamp64(timestamp);

	/*
	*  Create label, see RFC 3830 Section 4.1.4.
	*  label = Key-Type-Constant(4) + CS-ID(1) + CSB-ID(4) + RAND(16).
	*  Note label is set to all zeros as initialised above.
	*/
	rfc3830_label(fixed_buffer(sizeof(ENV_PSK_ENCRYPTION), ENV_PSK_ENCRYPTION),
		          KEMAC_CS_ID, csbId, rndNum, label);

	/*
	*  Create and set AES decryption key.
	*/
	error = rfc3830_PRF(gmk, fixed_buffer(KEMAC_LABEL_SIZE, label),
		                fixed_buffer(AES_KEY_SIZE, key));

	if (error != SUCCESS)
	{
		return error;
	}

	if (AES_set_encrypt_key(key, 8 * AES_KEY_SIZE, &aes_key))
	{
		return AES_SET_ENCRYPT_KEY_FAIL;
	}

	/*
	*  Create IV (Initialisation Vector), see RFC 3830 Section 4.2.3.
	*  IV = (Salt XOR (0x0000 || CSB ID || Timestamp)) || 0x0000.
	*  Note IV is set to all zeros as initialised above.
	*/
	rfc3830_iv(csbId, time, salt, label, iv);

	/*
	*  Decrypt the GSK.
	*/
	AES_ctr128_encrypt(kemacEncData.pointer, (unsigned char *)gsk.pointer,
		               AES_KEY_SIZE, &aes_key, iv, ecount, &number);

	/*
	*  Clean up.
	*/
	memset(label, 0, sizeof(label));
	memset(iv, 0, sizeof(iv));
	memset(ecount, 0, sizeof(ecount));
	memset(key, 0, sizeof(key));
	return SUCCESS;
}


/*
*  Calculates the MAC part of a message header, as defined in RFC 3830.
*  Args: iMessage - The message we are creating the MAC for (input).
*        csbId    - CSB-ID needed for AES calculation (input).
*        rndNum   - RAND needed for AES calculation (input).
*        gmk      - Group Master Key, previously distributed (input).
*        kemacMacData The result MAC (output).
*  Rets: Standard error code.
*/
ScErrno calculateKemacHMAC(Buffer iMessage, uint32_t csbId, Buffer rndNum,
						   Buffer gmk, Buffer kemacMacData)
{
	/*
	*  Variables used below.
	*  Key length is doubled to avoid unexplained problem on Windows.
	*/
	unsigned char label[KEMAC_LABEL_SIZE] = { 0 };
	unsigned char key[AES_KEY_SIZE * 2] = { 0 };
	ScErrno error;
	unsigned int length;

	/*
	*  Sanity checks on input. Note that gmk length is verified by
	*  rfc3830_label.
	*/
	if (!check_buffer(iMessage) || !check_buffer(rndNum) || !check_buffer(gmk)
		|| !check_buffer(kemacMacData))
	{
		return INPUT_BUFFER_INVALID;
	}

	if (rndNum.length != KEMAC_RAND_SIZE || kemacMacData.length != MAC_SIZE)
	{
		return INPUT_BUFFER_INVALID;
	}

	/*
	*  Create label, see RFC 3830 Section 4.1.4.
	*  label = Key-Type-Constant(4) + CS-ID(1) + CSB-ID(4) + RAND(16).
	*/
	rfc3830_label(fixed_buffer(sizeof(ENV_PSK_AUTHENTICATION),
		                       ENV_PSK_AUTHENTICATION),
				  KEMAC_CS_ID, csbId, rndNum, label);

	/*
	*  Create and set authentication key.
	*/
	error = rfc3830_PRF(gmk, fixed_buffer(KEMAC_LABEL_SIZE, label),
		               fixed_buffer(AES_KEY_SIZE, key));

	if (error != SUCCESS)
	{
		return error;
	}

	/*
	*  Calculate HMAC.
	*/
	HMAC(EVP_sha256(), key, sizeof(key), iMessage.pointer, iMessage.length,
		 (unsigned char *)kemacMacData.pointer, &length);
	kemacMacData.length = length;

	/*
	*  Clean up.
	*/
	memset(label, 0, sizeof(label));
	memset(key, 0, sizeof(key));
	return SUCCESS;
} 


/*
*  Verifies the MAC part of a message header, as defined in RFC 3830.
*  Args: iMessage       - The message verifying the MAC for (input).
*        iMsgLenForHash - The length of the part of the message over
*                         which the hash must be calculated. A received
*                         message also contains the hash, which must not
*                         be included (input).
*        csbId          - CSB-ID, from received message (input).
*        rndNum         - RAND, from received message (input).
*        gmk            - Group Master Key, previously distributed (input).
*        kemacMacData   - The received MAC against which the calculated
*                         MAC will be verified (input).
*  Rets: Standard error code.
*/
ScErrno verifyKemacHMAC(Buffer iMessage, size_t iMsgLenForHash, uint32_t csbId,
	                    Buffer rndNum, Buffer gmk, Buffer kemacMacData)
{
	/*
	*  Variables used below.
	*  Key length is doubled to avoid unexplained problem on Windows.
	*/
	unsigned char label[KEMAC_LABEL_SIZE] = { 0 };
	unsigned char key[AES_KEY_SIZE * 2] = { 0 };
	unsigned char mac[MAC_SIZE] = { 0 };
	ScErrno error;
	unsigned int length;

	/*
	*  Sanity checks on input. Note that gmk length is verified by
	*  rfc3830_label.
	*/
	if (!check_buffer(iMessage) || !check_buffer(rndNum) || !check_buffer(gmk)
		|| !check_buffer(kemacMacData))
	{
		return INPUT_BUFFER_INVALID;
	}

	if (rndNum.length != KEMAC_RAND_SIZE || iMsgLenForHash > iMessage.length)
	{
		return INPUT_BUFFER_INVALID;
	}

	/*
	*  Invalid MAC length treated as failure to verify.
	*/
	if (kemacMacData.length != MAC_SIZE)
	{
		return KEMAC_NOT_VERIFIED;
	}

	/*
	*  Create label, see RFC 3830 Section 4.1.4.
	*  label = Key-Type-Constant(4) + CS-ID(1) + CSB-ID(4) + RAND(16).
	*/
	rfc3830_label(fixed_buffer(sizeof(ENV_PSK_AUTHENTICATION),
		                        ENV_PSK_AUTHENTICATION),
				  KEMAC_CS_ID, csbId, rndNum, label);

	/*
	*  Create and set authentication key.
	*/
	error = rfc3830_PRF(gmk, fixed_buffer(KEMAC_LABEL_SIZE, label),
		                fixed_buffer(AES_KEY_SIZE, key));

	if (error != SUCCESS)
	{
	    memset(label, 0, sizeof(label));
	    memset(key, 0, sizeof(key));
		return error;
	}

	/*
	*  Calculate HMAC.
	*/
	HMAC(EVP_sha256(), key, sizeof(key), iMessage.pointer, iMsgLenForHash,
		 mac, &length);

	if (memcmp(kemacMacData.pointer, mac, length) != 0)
	{
		error = KEMAC_NOT_VERIFIED;
	}

	/*
	*  Clean up.
	*/
	memset(label, 0, sizeof(label));
	memset(key, 0, sizeof(key));
	memset(mac, 0, sizeof(mac));
	return error;
}


/*
*  ECCSI verify.
*  Args: secCtx     - Security context (input).
*        signingUri - URI of signer (input).
*        timestamp  - UTC timestamp (input).
*        data       - Data to be verified (input).
*        signature  - Signature (input).
*  Rets: Standard error code.
*/
ScErrno EccsiVerify(SecCtx secCtx, String signingUri, String timestamp,
	                Buffer data, Buffer signature)
{
	/*
	*  Variables used below.
	*/
	String identity;
	EC_POINT *kpak;
	ScErrno error;
	
	if (!check_string(signingUri) || !check_string(timestamp))
	{
		return INPUT_STRING_INVALID;
	}
	if (!check_buffer(data) || !check_buffer(signature))
	{
		return INPUT_BUFFER_INVALID;
	}
	
	/*
	*  Get and validate context array.
	*/
	error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Find certificate matching URI and timetamp, and extract identity
	*  and KPAK.
	*/
	error = eccsi_certificate(signingUri, timestamp, timestamp, &identity, &kpak);

	if (error)
	{
		return error;
	}

	/*
	*  ECCSI verification. Must free KPAK before exit.
	*/
	error = eccsi_verify(kpak, identity, data, signature);
	EC_POINT_clear_free(kpak);
	return error;
}


/*
*  ECCSI sign.
*  Args: secCtx      - Security context (input).
*        signingUri  - URI of intended recipient (input).
*        timestamp   - UTC timestamp (input).
*        data        - Data to be signed (input).
*        signature_p - *signature_p is set to signature.
*  Rets: Standard error code.
*/

ScErrno EccsiSign(SecCtx secCtx, String signingUri, String timestamp,
	              Buffer data, Buffer *signature_p)
{
	/*
	*  Variables used below.
	*/
	String timestamp_now;
	DataRef data_ref;
	Buffer buffer;
	String identity;
	const EC_GROUP *curve;
	BN_CTX *ctx;
	EC_POINT *kpak = 0;
	BIGNUM *ssk;
	EC_POINT *pvt;
	BIGNUM *x;
	BIGNUM *y;
	String uid;
	int year, month, day_year;

	/*
	*  Get and validate context array.
	*/
	ScErrno error = get_context(secCtx);

	if (error)
	{
		return error;
	}

	/*
	*  Sanity checks on input.
	*/
	if (signature_p == NULL)
	{
		return INPUT_PTR_NULL;
	}

	if (!check_string(signingUri) || !check_string(timestamp))
	{
		return INPUT_STRING_INVALID;
	}

	if (!check_buffer(data))
	{
		return INPUT_BUFFER_INVALID;
	}

	if (!is_timestamp(timestamp))
	{
		return TIMESTAMP_FORMAT_INVALID;
	}

	/*
	*  Only want to do a real-time check if all messages are expected to arrive
	*  instantly. Otherwise check elsewhere.
	*/
	if (REAL_TIME_CHECK)
	{
		/*
		*  Get current timestamp and verify received timestamp is close to it.
		*/
		GetDateTime(&timestamp_now, &year, &month, &day_year);
		error = timestamp_verify(timestamp_now, timestamp, TIMESTAMP_RT_LESS,
			                     TIMESTAMP_RT_MORE);

		if (error)
		{
			return error;
		}
	}

	/*
	*  Find certificate matching URI and verify matches timestamp.
	*/
	buffer = find_certificate_uri_timestamp(signingUri, timestamp);

	if (buffer.pointer == NULL)
	{
		return MS_URI_NOT_MATCH;
	}

	/*
	*  Get data reference of matching key pack, if any.
	*/
	error = GetMikeySakkeUid(secCtx, signingUri, timestamp, &uid);
	
	if (error)
	{
		return error;
	}
	
	data_ref = find_key_pack_uid(uid, timestamp);

	if (data_ref == 0)
	{
		return MS_URI_NOT_MATCH;
	}

	/*
	*  Get key pack.
	*/
	error = GetData(data_ref, &buffer);

	if (error)
	{
		return error;
	}

	/*
	*  Get SSK.
	*/
	error = GetData(((DataRef *)buffer.pointer)[KP_ECCSI_SSK_INDEX], &buffer);

	if (error)
	{
		return error;
	}

	ssk = BN_new();

	if (ssk == NULL)
	{
		return RESOURCE_NOT_ALLOCATED;
	}

	if (BN_bin2bn(buffer.pointer, ECCSI_SIZE, ssk) == NULL)
	{
		BN_clear_free(ssk);
		return RESOURCE_NOT_EXIST;
	}

	/*
	*  Get key pack.
	*/
	error = GetData(data_ref, &buffer);

	if (error)
	{
		BN_clear_free(ssk);
		return error;
	}

	/*
	*  Get elliptic curve and object required.
	*/
	curve = eccsi_curve();

	if (curve == 0)
	{
		BN_clear_free(ssk);
		return RESOURCE_NOT_EXIST;
	}

	ctx = bn_ctx();

	if (ctx == 0)
	{
		BN_clear_free(ssk);
		return RESOURCE_NOT_EXIST;
	}

	/*
	*  Get PVT.
	*/
	error = GetData(((DataRef *)buffer.pointer)[KP_ECCSI_PVT_INDEX], &buffer);

	if (error)
	{
		BN_clear_free(ssk);
		return error;
	}

	x = BN_new();

	if (x == 0)
	{
		BN_clear_free(ssk);
		return RESOURCE_NOT_EXIST;
	}

	if (BN_bin2bn(buffer.pointer, ECCSI_SIZE, x) == 0)
	{
		BN_clear_free(ssk);
		BN_clear_free(x);
		return RESOURCE_NOT_EXIST;
	}

	y = BN_new();

	if (y == 0)
	{
		BN_clear_free(ssk);
		BN_clear_free(x);
		return RESOURCE_NOT_EXIST;
	}

	if (BN_bin2bn(buffer.pointer + ECCSI_SIZE, ECCSI_SIZE, y) == 0)
	{
		BN_clear_free(ssk);
		BN_clear_free(x);
		BN_clear_free(y);
		return RESOURCE_NOT_EXIST;
	}

	pvt = EC_POINT_new(curve);

	if (pvt == 0)
	{
		BN_clear_free(ssk);
		BN_clear_free(x);
		BN_clear_free(y);
		return RESOURCE_NOT_EXIST;
	}

	if (!big_ec(pvt, x, y, curve, ctx))
	{
		BN_clear_free(ssk);
		BN_clear_free(x);
		BN_clear_free(y);
		EC_POINT_clear_free(pvt);
		return RESOURCE_NOT_EXIST;
	}

	/*
	*  Find certificate matching URI and timetamp, and extract identity
	*  and KPAK.
	*/
	error = eccsi_certificate(signingUri, timestamp, timestamp, &identity,
		                      &kpak);

	if (error)
	{
		EC_POINT_clear_free(kpak);
		BN_clear_free(ssk);
		BN_clear_free(x);
		BN_clear_free(y);
		EC_POINT_clear_free(pvt);
		return error;
	}

	/*
	*  ECCSI signature.
	*/
	error = eccsi_sign(curve, ctx, kpak, ssk, pvt, x, y, identity, data,
		               signature_p);

	/*
	*  Cleanup and report signature success.
	*/
	EC_POINT_clear_free(kpak);
	BN_clear_free(ssk);
	BN_clear_free(x);
	BN_clear_free(y);
	EC_POINT_clear_free(pvt);
	return error;
}


/*
*  ECDSA verify.
*  Args: secCtx    - Security context (input).
*        data      - Data to be verified (input).
*        signature - Signature (input).
*  Rets: Standard error code.
*/
ScErrno EcdsaVerify(SecCtx secCtx, Buffer data, Buffer signature)
{
	/*
	*  Variables used below.
	*/
	EC_POINT *kpak = NULL;
	ScErrno error;

	/*
	*  Sanity checks on input.
	*/
	if (!check_buffer(data) || !check_buffer(signature))
	{
		return INPUT_BUFFER_INVALID;
	}

	/*
	*  Get public authentication key (KPAK).
	*/
	error = ecdsa_certificate(secCtx, &kpak);

	if (error)
	{
		return error;
	}

	/*
	*  ECDSA verification. Must free KPAK before exit.
	*/
	error = ecdsa_verify(kpak, data, signature);
	EC_POINT_clear_free(kpak);
	return error;
}


/*
*  Calculate HMAC using stored transport key.
*  Args: secCtx         - Security context (input).
*        transportAlg   - Indicates algorithm used, must be
*                         "hmac-sha256" (input).
*        transportKeyId - Transport key identity (input).
*        data           - Data to be signed (input).
*        length         - HMAC will be truncated to this length
*                         (before base 64 encoding). A value of
*                         zero is interpreted as no truncation.
*        hmac_p         - *hmac_p is set to HMAC (base64 encoded).
*  Rets: Standard error code.
*/
ScErrno HmacTk(SecCtx secCtx, const char *transportAlg, String transportKeyId,
			   Buffer data, size_t length, Buffer *hmac_p)
{
	/*
	*  Variables used below.
	*/
	Buffer key;
	ScErrno error;

	/*
	* Sanity checks on input.
	*/
	if (transportAlg == NULL || hmac_p == NULL)
	{
		return INPUT_PTR_NULL;
	}

	if (!check_buffer(data))
	{
		return INPUT_BUFFER_INVALID;
	}

	if (!check_string(transportKeyId))
	{
		return INPUT_STRING_INVALID;
	}

	/*
	*  Verify algorithm.
	*/
	if (strcmp(transportAlg, "hmac-sha256") != 0)
	{
		return ALGORITHM_INVALID;
	}

	error = get_transport_key(transportKeyId, &key);

	if (error)
	{
		return error;
	}

	/*
	*  HMAC calculation.
	*/
	return hmac(key, data, length, hmac_p);
}


/*
*  Calculate hash.
*  Args: hashAlg - Indicates algorithm used, must be "sha256" (input).
*        data    - Data to be hashed (input).
*        length  - Hash will be truncated to this length
*                  (before base 64 encoding). A value of
*                  zero is interpreted as no truncation.
*        hash_p  - *hash_p is set to hash (base64 encoded).
*  Rets: Standard error code.
*/
ScErrno HashData(const char *hashAlg, Buffer data, size_t length,
	             Buffer *hash_p)
{
	/*
	*  Variables used below.
	*/
	Buffer buffer;
	unsigned char hash[SHA256_DIGEST_LENGTH];

	/*
	*  Sanity checks on input.
	*/
	if (hashAlg == NULL || hash_p == NULL)
	{
		return INPUT_PTR_NULL;
	}

	if (!check_buffer(data))
	{
		return INPUT_BUFFER_INVALID;
	}

	/*
	*  Verify algorithm.
	*/
	if (strcmp(hashAlg, "sha256") != 0)
	{
		return ALGORITHM_INVALID;
	}

	/*
	*  Verify truncation length.
	*/
	if (length > SHA256_DIGEST_LENGTH)
	{
		return INPUT_OUT_OF_BOUNDS;
	}

	/*
	*  Calculate hash.
	*/
	if (!sha256(data, hash))
	{
		return GENERAL_FAILURE;
	}

	/*
	*  Set buffer to (possibly truncated) hash.
	*/
	buffer.length = length ? length : SHA256_DIGEST_LENGTH;
	buffer.pointer = hash;

	/*
	*  Base 64 encoding.
	*/
	return base64(buffer, hash_p);
}

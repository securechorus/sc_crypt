# Android makefile for SecureChorus libraries

LOCAL_PATH:= $(call my-dir)

common_CFLAGS := -Wall
common_INCLUDES := $(LOCAL_PATH)/include

# SC Keystore lib
include $(CLEAR_VARS)

LOCAL_MODULE := libsc_keystore
LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := \
	libkeystore/sc_keystore.c \
	libkeystore/sc_misc.c

LOCAL_CFLAGS += $(common_CFLAGS)

LOCAL_C_INCLUDES += $(common_INCLUDES) \
	$(LOCAL_PATH)/../openssl-legacy/include \
	$(LOCAL_PATH)/../xerces-c/include \
	$(LOCAL_PATH)/../xml-security-c

LOCAL_SHARED_LIBRARIES := \
	libxerces-c \
	libxml-security \
	libcrypto_legacy

include $(BUILD_SHARED_LIBRARY)

# SC Crypto lib
include $(CLEAR_VARS)

LOCAL_MODULE := libsc_crypto
LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := libmscrypto/sc_crypto.c

LOCAL_CFLAGS += $(common_CFLAGS)
LOCAL_CPPFLAGS += -fexceptions -frtti

LOCAL_C_INCLUDES += $(common_INCLUDES) \
	$(LOCAL_PATH)/../openssl-legacy/include \
	$(LOCAL_PATH)/../xerces-c/include \
	$(LOCAL_PATH)/../xml-security-c

LOCAL_SHARED_LIBRARIES := libcrypto_legacy libsc_keystore

include $(BUILD_SHARED_LIBRARY)

# SC MS lib
include $(CLEAR_VARS)

LOCAL_MODULE := libsc_ms
LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := \
	libms/libms.cpp \
	libms/helper.cpp \
	libms/payload.cpp

LOCAL_CFLAGS += $(common_CFLAGS) -fexceptions -frtti

LOCAL_C_INCLUDES += $(common_INCLUDES)

LOCAL_SHARED_LIBRARIES := libsc_crypto libsc_keystore

include $(BUILD_SHARED_LIBRARY)

# SC KMS lib
include $(CLEAR_VARS)

LOCAL_MODULE := libsc_kms
LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := \
	libmskms/libmskms.cpp

LOCAL_CFLAGS += $(common_CFLAGS)
LOCAL_CFLAGS += -Wno-narrowing
LOCAL_CPPFLAGS += -fexceptions -frtti
LOCAL_CPPFLAGS += -Wno-conversion-null

LOCAL_C_INCLUDES += $(common_INCLUDES) \
	$(LOCAL_PATH)/../openssl-legacy/include \
	$(LOCAL_PATH)/../xerces-c/include \
	$(LOCAL_PATH)/../xml-security-c

LOCAL_SHARED_LIBRARIES := \
	libxerces-c \
	libxml-security \
	libcrypto_legacy \
	libsc_crypto \
	libsc_keystore

include $(BUILD_SHARED_LIBRARY)

# SC JNI interface
include $(CLEAR_VARS)

LOCAL_MODULE := libsc_jni

LOCAL_CPPFLAGS += -Wno-unused-parameter
LOCAL_CPPFLAGS += -Wno-non-virtual-dtor

LOCAL_SRC_FILES := \
	jni/cpp/com_securechorus_SecureChorusLibrary.cpp \
	jni/cpp/SCKMSParameterBlock.cpp \
	jni/cpp/SCEccsiParameterBlock.cpp \
	jni/cpp/SCParameterBlock.cpp \
	jni/cpp/SCIMessageParameterBlock.cpp \
	jni/cpp/SCWrapperTools.cpp

LOCAL_SHARED_LIBRARIES := \
	libnativehelper \
	libsc_crypto \
	libsc_kms \
	libsc_ms \
	libsc_keystore

LOCAL_C_INCLUDES += \
	$(common_INCLUDES) \
	$(JNI_H_INCLUDE) \
	$(LOCAL_PATH)/jni/cpp

include $(BUILD_SHARED_LIBRARY)

# building Java part of JNI
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-java-files-under, jni/java)

LOCAL_JNI_SHARED_LIBRARIES := libsc_jni
LOCAL_REQUIRED_MODULES := libsc_jni
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := sc_jni

include $(BUILD_JAVA_LIBRARY)

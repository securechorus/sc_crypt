/*
 *  Implementation file for Secure Chorus miscellaneous functions.
 */

#include <limits.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include <openssl/rand.h>

#include "sc_misc.h"

#include "sc_errno.h"
#include "sc_types.h"


/*
 *  POSIX time zero offset from NTP time zero (from RFC 5905 Figure 4).
 */
#define NTP_OFFSET 2208988800ul


/*
 *  Initialises random number generator.
 *  Args: rand - Used to indicate bytes that are to be used to initialise
 *               the random number generator, in combination with its own
 *               internal processes. These bytes should have maximum
 *               entropy, and may be reported to the random number
 *               generator as having such.
 *  Rets: Standard error code. Failure may indicate either an unsuitable
 *        input (no bytes, or too many bytes) or that the random number
 *        generator reports itself as not properly seeded.
 */
ScErrno InitSecureRandom(Buffer rand)
{
  /*
   *  Verify able to seed generator.
   */
  if (rand.pointer == 0 || rand.length == 0 || rand.length > INT_MAX)
  {
    return GENERAL_FAILURE;
  }

  /*
   *  Seed generator.
   */
  RAND_seed((unsigned char *)rand.pointer, (int)rand.length);

  /*
   *  Return generator status.
   */
  return RAND_status() ? SUCCESS : GENERAL_FAILURE;
}


/*
 *  Returns string random bytes.
 *  Args: rand - Used to indicate the random bytes.
 *               On entry must have rand.length set to
 *               indicate required number of bytes, and
 *               rand.pointer set to be able to store
 *               random bytes.
 *  Rets: Standard error code.
 */
ScErrno GetSecureRandom(Buffer rand)
{
  /*
   *  Verify able to return data.
   */
  if (rand.pointer == 0 || rand.length == 0 || rand.length > INT_MAX)
  {
    return GENERAL_FAILURE;
  }

  /*
   *  Generate random data.
   */
  return RAND_bytes((unsigned char *)rand.pointer, (int)rand.length)
	       ? SUCCESS : GENERAL_FAILURE;
}


/*
 *  Returns current time UTC timestamp, using numbering as defined by
 *  RFC 3339.
 *  Args: utcTimestamp_p - *utcTimestamp_p is set to UTC timestamp.
 *                         Ignored if null.
 *        year_p         - *year_p is set to year. Ignored if null.
 *        month_p        - *month_p is set to month. Ignored if null.
 *        dayYear_p      - *dayYear_p is set to day of year.
 *                         Ignored if null.
 *  Rets: Standard error code.
 */
ScErrno GetDateTime(String *utcTimestamp_p, int *year_p, int *month_p,
                    int *dayYear_p)
{
  /*
   *  Timestamp is string representation of 64 bit value (RFC 5905)
   *  hence 16 characters (not null terminated). Last 8 characters
   *  are fractional values not used here. Initialisation of first
   *  8 bytes is irrelevant.
   */
  static char timestamp[16] = { 0, 0, 0, 0, 0, 0, 0, 0,
		                        '0', '0', '0', '0', '0', '0', '0', '0' };

  /*
   *  Current time is assumed to be POSIX time (also used by MS Visual Studio).
   */
  const time_t now = time(0);
  uint32_t ntp = NTP_OFFSET + (uint32_t)now;

  /*
   *  UTC time structure.
   */
  const struct tm * const utc_p = gmtime(&now);

  /*
   *  Return values: year, month, day of year, if required.
   *  Note that struct tm defines month and yday indexed
   *  from 0, RFC 3339 defines indexed from 1.
   */
  if (year_p != 0)
  {
    *year_p = 1900 + utc_p->tm_year;
  }

  if (month_p != 0)
  {
    *month_p = utc_p->tm_mon + 1;
  }

  if (dayYear_p != 0)
  {
    *dayYear_p = utc_p->tm_yday + 1;
  }

  /*
   *  Timestamp is big endian, convenient to start from little end
   *  of seconds value. Ignored if not required.
   */
  if (utcTimestamp_p != 0)
  {
	int i;

    for (i = 7; i >= 0; --i)
    {
	  timestamp[i] = "0123456789ABCDEF"[ntp & 0x0F];
	  ntp >>= 4;
    }

    /*
     *  Set return timestamp string length and pointer.
     */
    utcTimestamp_p->length = 16;
    utcTimestamp_p->pointer = timestamp;
  }

  /*
   *  Return completed successfully.
   */
  return SUCCESS;
}


/*
 *  Converts UTC timestamp to date, using numbering as defined by RFC 3339.
 *  Args: utcTimestamp - UTC timestamp (input).
 *        year_p       - *year_p is set to year. Ignored if null.
 *        month_p      - *month_p is set to month. Ignored if null.
 *        dayYear_p    - *dayYear_p is set to day of year.
 *                       Ignored if null.
 *  Rets: Standard error code.
 */
ScErrno GetDateFromTimestamp(String utcTimestamp, int *year_p, int *month_p,
	                         int *dayYear_p)
{
  /*
   *  Variables used below.
   */
  const char * const digits = "0123456789ABCDEF";
  const struct tm *utc_p;
  time_t time;
  size_t i;

  /*
   *  Variables it is convenient to set required initial values of.
   */
  uint32_t ntp = 0;
  const char *pointer = utcTimestamp.pointer;

  /*
   *  Verify timestamp (use pointer rather than utcTimestamp.pointer
   *  as convenient).
   */
  if (utcTimestamp.length != 16 || pointer == 0)
  {
	return TIMESTAMP_FORMAT_INVALID;
  }

  /*
   *  Loop through non-fractional seconds part of timestamp.
   */
  for (i = 0; i != 8; ++i)
  {
	/*
	 *  Verify hexadecimal digit and use to add to NTP value.
	 *  Fail if not hexadecimal digit (uppercase as used here).
	 */
	const char * const ptr
	  = (const char *)memchr((const void *)digits, pointer[i], 16);

	if (ptr == 0)
	{
	  return TIMESTAMP_FORMAT_INVALID;
	}

	ntp <<= 4;
	ntp += ptr - digits;
  }

  /*
   *  Convert NTP time to Unix/Windows time, and determine date
   *  information. Note that struct tm defines month and yday
   *  indexed from 0, RFC 3339 defines indexed from 1. Return
   *  success, even if not all information requested.
   */
  time = (time_t)(ntp - NTP_OFFSET);
  utc_p = gmtime(&time);

  if (year_p != 0)
  {
    *year_p = 1900 + utc_p->tm_year;
  }

  if (month_p != 0)
  {
    *month_p = utc_p->tm_mon + 1;
  }

  if (dayYear_p != 0)
  {
    *dayYear_p = utc_p->tm_yday + 1;
  }

  return SUCCESS;
}

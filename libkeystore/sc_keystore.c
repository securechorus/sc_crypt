/*
 *  Implementation file for Secure Chorus key store.
 */


/* ==== Header file inclusions ============================================== */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sc_keystore.h"

#include "sc_dataref.h"
#include "sc_errno.h"
#include "sc_types.h"


/* ==== Type definitions ==================================================== */

/*
 *  Structure used to store an allocation. May be unallocated,
 *  indicated by buffer pointer null and length zero.
 */
struct Store
{
  Boolean updateable;
  Buffer  buffer;
};

typedef struct Store Store;


/* ==== Static data storage ================================================= */

/*
 *  Recorded store number of allocations and pointer to array of
 *  allocations.
 */
static size_t store_size = 0;
static Store *store_p    = 0;


/*
 *  External store filename. This may be changed to any name.
 *  Use of a null pointer or empty string means no external file.
 */
static const char * filename = "sc_keystore.txt";


/* ==== Local (static) functions ============================================ */

/*
 *  Free allocated storage, up to point set.
 *  Args: number - Number of items set and to be cleared and freed.
 */
static void free_store(size_t number)
{
  size_t i;

  for (i = 0; i != number; ++i)
  {
	if (store_p[i].buffer.pointer != 0)
	{
	  memset((void *)store_p[i].buffer.pointer, 0, store_p[i].buffer.length);
	}

	free((void *)store_p[i].buffer.pointer);
  }

  free(store_p);
  store_p = 0;
  store_size = 0;
}


/*
 *  Find first unused item, reallocating memory if necessary. Returns index
 *  plus one, or zero if failed to reallocate.
 */
static size_t find_unused()
{
  /*
   *  Variables used below.
   */
  Store *new_store_p;
  size_t i;

  /*
   *  Find first unused item, if any.
   */
  for (i = 0; i != store_size; ++i)
  {
	if (store_p[i].buffer.pointer == 0)
	{
      return i + 1;
	}
  }

  /*
   *  Increase number of stored data items, fail if unable.
   */
  new_store_p = (Store *)realloc(store_p, (store_size + 1) * sizeof(Store));

  if (new_store_p == 0)
  {
	return 0;											  
  }

  /*
   *  Update stored data items, including setting new item unused.
   */
  store_p = new_store_p;
  store_p[store_size].updateable = true;
  store_p[store_size].buffer.length = 0;
  store_p[store_size].buffer.pointer = 0;
  ++store_size;

  /*
   *  Return is one more than new data item number, as required.
   */
  return store_size;
}


/*
 *  Read external file, only if no allocations.
 */
static void read()
{
  /*
   *  Variables required later. Convenient to set size and shift to zero.
   */
  FILE *file;
  size_t size = 0;
  size_t shift = 0;
  size_t i, j;

  /*
   *  Exclude cases where file is not read.
   */
  if (store_size != 0 || filename == 0 || *filename == 0)
  {
	return;
  }

  /*
   *  Open file. Return if cannot be opened.
   */
  file = fopen(filename, "rb");

  if (file == 0)
  {
    return;
  }

  /*
   *  Read number of stored items, return if not successfully read.
   */
  for (j = 0; j != sizeof(DataRef); ++j)
  {
    int byte = fgetc(file);

	if (byte < 0)
	{
	  fclose(file);
	  return;
	}

	size += (size_t)byte << shift;
	shift += 8;
  }

  if (size == 0)
  {
    fclose(file);
	return;
  }

  /*
   *  Allocate stored items, return if not successfully read.
   */
  store_p = (Store *)malloc(size * sizeof(Store));

  if (store_p == 0)
  {
    fclose(file);
	return;
  }

  /*
   *  Set number of stored items.
   */
  store_size = size;

  /*
   *  Read each stored item.
   */
  for (i = 0; i != store_size; ++i)
  {
    size = 0;
	shift = 0;

    for (j = 0; j != sizeof(DataRef); ++j)
    {
      int byte = fgetc(file);

	  if (byte < 0)
	  {
	    free_store(i);
	    fclose(file);
	    return;
	  }

	  size += (size_t)byte << shift;
	  shift += 8;
    }

	/*
	 *  If size is zero (unused item) then set to unused, otherwise
	 *  read further data.
	 */
	if (size == 0)
	{
	  store_p[i].updateable = true;
	  store_p[i].buffer.length = 0;
	  store_p[i].buffer.pointer = 0;
	}
	else
	{
	  /*
	   *  Read if item is updateable. Return if not read successfully.
	   */
      int update = fgetc(file);

	  if (update < 0)
	  {
	    free_store(i);
	    fclose(file);
	    return;
	  }

	  /*
	   *  Allocate memory for item data. Return if unable to allocate.
	   */
	  store_p[i].buffer.pointer = (const unsigned char *)malloc(size);

	  if (store_p[i].buffer.pointer == 0)
	  {
		free_store(i);
		fclose(file);
		return;
	  }

	  store_p[i].buffer.length = size;
	  store_p[i].updateable = update;
	}

	/*
	 *  Read item data. Return if not read successfully.
	 */
	if (size != 0
		&& fread((void *)store_p[i].buffer.pointer, 1, size, file) != size)
	{
	  free_store(i + 1);
	  fclose(file);
	  return;
	}
  }

  /*
   *  Close successfully read file.
   */
  fclose(file);
}


/*
 *  Write external file, only if some allocations.
 */
static void write()
{
  /*
   *  Variables required later.
   */
  FILE *file;
  size_t number;
  size_t i, j;

  /*
   *  Exclude cases where file is not written.
   */
  if (filename == 0 || *filename == 0)
  {
	return;
  }

  /*
   *  Open file. Return if cannot be opened.
   */
  file = fopen(filename, "wb");

  if (file == 0)
  {
    return;
  }

  /*
   *  Write number of stored items, return if not successfully written.
   */
  number = store_size;

  for (j = 0; j != sizeof(DataRef); ++j)
  {
    if (fputc(number % 256, file) < 0)
    {
	  fclose(file);
	  remove(filename);
	  return;
    }

	number >>= 8;
  }

  /*
   *  Write each stored item.
   */
  for (i = 0; i != store_size; ++i)
  {
	/*
	 *  Size of this data item.
	 */
	const size_t size = store_p[i].buffer.length;

	/*
	 *  Write size of stored item. Return if not successfully written.
	 */
    number = size;

    for (j = 0; j != sizeof(DataRef); ++j)
    {
      if (fputc(number % 256, file) < 0)
      {
	    fclose(file);
	    remove(filename);
	    return;
      }

	  number >>= 8;
    }

	/*
	 *  Additional output only if size is nonzero (item is used).
	 */
	if (size != 0)
	{
	  /*
	   * Write if item is updateable. Return if not successfully written.
	   */
      if (fputc(store_p[i].updateable, file) < 0)
	  {
		fclose(file);
		remove(filename);
		return;
	  }

	  /*
	   *  Write item data. Return if not successfully written.
	   */
	  if (fwrite((void *)store_p[i].buffer.pointer, 1, size, file) != size)
	  {
		fclose(file);
		remove(filename);
		return;
	  }
	}
  }

  /*
   *  Close successfully read file.
   */
  fclose(file);
}


/* ==== Library functions =================================================== */

/*
 *  Sets the file path of the keystore file which defaults to sc_keystore.txt
 *  Args: newFilePath - The new file path
 *  Rets : Standard error code.
 */
ScErrno SetFilePath(const char * newFilePath)
{
	filename = newFilePath;
	return SUCCESS;
}

/*
 *  Stores data securely in data store, returning reference.
 *  Args: data          - Data to be stored (input).
 *        updateAllowed - If a subsequent update of this data
 *                        is allowed (input).
 *        dataRef_p     - *dataRef_p is set to a copyable
 *                        "handle" to the stored data. It is
 *                        unchanged in the event of an error.
 *  Rets: Standard error code.
 */
ScErrno StoreSecureData(Buffer data, Boolean updateAllowed,
                        DataRef *dataRef_p)
{
  /*
   *  Variables required later.
   */
  unsigned char *pointer;
  size_t index;

  /*
   *  Return failure if unable to return reference to store.
   */
  if (dataRef_p == 0)
  {
	return GENERAL_FAILURE;
  }

  /*
   *  Return failure if no data to store.
   */
  if (data.pointer == 0 || data.length == 0)
  {
 	return RESOURCE_NOT_ALLOCATED;
  }

  /*
   *  Ensure stored data is set in memory.
   */
  read();

  /*
   *  Find item to use as store. Fail if none.
   */
  index = find_unused();

  if (index == 0)
  {
	return RESOURCE_NOT_ALLOCATED;
  }

  /*
   *  This data allocation, return failure if unable to allocate.
   */
  pointer = (unsigned char *)malloc(data.length);

  if (pointer == 0)
  {
	return RESOURCE_NOT_ALLOCATED;
  }

  /*
   *  Past point of failure, so set return data reference.
   *  Convert data reference to array index.
   */
  *dataRef_p = index;
  --index;

  /*
   *  Copy data into new store.
   */
  memcpy(pointer, data.pointer, data.length);

  /*
   *  Set new structure.
   */
  store_p[index].updateable = updateAllowed;
  store_p[index].buffer.length = data.length;
  store_p[index].buffer.pointer = pointer;

  /*
   *  Ensure stored data is set in file. Return success.
   */
  write();
  return SUCCESS;
}


/*
 *  Updates data in data store.
 *  Args: dataRef - Handle to the stored data to be updated (input).
 *        data    - Data to be stored (input).
 *  Rets: Standard error code.
 */
ScErrno UpdateData(DataRef dataRef, Buffer data)
{
  /*
   *  Variable required later.
   */
  const unsigned char *pointer;

  /*
   *  Return failure if no data to store.
   */
  if (data.pointer == 0 || data.length == 0)
  {
	return RESOURCE_NOT_ALLOCATED;
  }

  /*
   *  Ensure stored data is set in memory.
   */
  read();

  /*
   *  Ensure reference is valid.
   */
  if (dataRef == 0 || dataRef > store_size
	  || store_p[dataRef - 1].buffer.pointer == 0)
  {
    return RESOURCE_NOT_EXIST;
  }

  /*
   *  Ensure data is updateable.
   */
  if (!store_p[dataRef - 1].updateable)
  {
    return RESOURCE_DENIED;
  }

  /*
   *  Case that new data can overwrite old data.
   */
  if (data.length <= store_p[dataRef - 1].buffer.length)
  {
	memcpy((void *)store_p[dataRef - 1].buffer.pointer,
		   data.pointer, data.length);
	store_p[dataRef - 1].buffer.length = data.length;
    write();
    return SUCCESS;
  }

  /*
   *  Allocate memory for item.
   */
  pointer = (const unsigned char *)malloc(data.length);

  if (pointer == 0)
  {
	return RESOURCE_NOT_ALLOCATED;
  }

  /*
   *  Free old memory, after overwriting.
   */
  memset((void *)store_p[dataRef - 1].buffer.pointer, 0,
	     store_p[dataRef - 1].buffer.length);
  free((void *)store_p[dataRef - 1].buffer.pointer);

  /*
   *  Copy item data and update other information. Remains
   *  updateable.
   */
  memcpy((unsigned char *)pointer, data.pointer, data.length);
  store_p[dataRef - 1].buffer.length = data.length;
  store_p[dataRef - 1].buffer.pointer = pointer;

  /*
   *  Ensure stored data is set in file. Return success.
   */
  write();
  return SUCCESS;
}


/*
 *  Gets data from data store.
 *  Args: dataRef - Handle to the stored data to be returned (input).
 *        data_p  - *data_p is set to indicate the stored data.
 *                  Unspecified value if error.
 *  Rets: Standard error code.
 */
ScErrno GetData(DataRef dataRef, Buffer *data_p)
{
  /*
   *  Ensure return buffer is valid.
   */
  if (data_p == 0)
  {
    return GENERAL_FAILURE;
  }

  /*
   *  Ensure stored data is set in memory.
   */
  read();

  /*
   *  Ensure reference is valid.
   */
  if (dataRef == 0 || dataRef > store_size)
  {
    return RESOURCE_NOT_EXIST;
  }

  /*
   *  Set return buffer, fail if empty.
   */
  *data_p = store_p[dataRef - 1].buffer;

  if (data_p->pointer == 0)
  {
	return RESOURCE_NOT_EXIST;
  }

  /*
   *  Successful return.
   */
  return SUCCESS;
}


/*
 *  Purge single item of data in data store.
 *  Args: dataRef - Handle to the stored data to be purged (input).
 *  Rets: Standard error code. Note that an error can occur only
 *        if the function is passed a data reference that was not
 *        returned by this library, or is outdated.
 */
ScErrno PurgeData(DataRef dataRef)
{
  /*
   *  Ensure stored data is set in memory.
   */
  read();

  /*
   *  Ensure reference is valid.
   */
  if (dataRef == 0 || dataRef > store_size
	  || store_p[dataRef - 1].buffer.pointer == 0)
  {
	return RESOURCE_NOT_EXIST;
  }

  /*
   *  Purge data.
   */
  memset((void *)store_p[dataRef - 1].buffer.pointer, 0,
	     store_p[dataRef - 1].buffer.length);
  free((void *)store_p[dataRef - 1].buffer.pointer);
  store_p[dataRef - 1].updateable = true;
  store_p[dataRef - 1].buffer.pointer = 0;
  store_p[dataRef - 1].buffer.length = 0;

  /*
   *  Ensure stored data is set in file. Return success.
   */
  write();
  return SUCCESS;
}


/*
 *  Purges all data in data store.
 *  Rets: Standard error code.
 */
ScErrno PurgeAllData()
{
	free_store(store_size);

	if (filename != 0 && *filename != 0)
	{
	  remove(filename);
	}

	return SUCCESS;
}

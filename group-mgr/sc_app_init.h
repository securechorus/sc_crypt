#ifndef SC_APP_INIT_H
#define SC_APP_INIT_H

#include <string>

#include "sc_context.h"
#include "sc_errno.h"

//#define DEBUG

//  Initialise cryptographic functions, using KMS client and server.
//  Args: kms_uri  - KMS URI.
//        kms_url  - KMS URL.
//        my_uri - User URI.
//        filename - Bootstrap file name, containing bootstrap key.
//                   Opened and closed by this function.
//  Rets: Security context, tests false (equal to zero) if
//        initialisation fails.
SecCtx init(const std::string &kms_uri, const std::string &kms_url,
            const std::string &my_uri, const char *filename);

//  Provisions keys from the KMS, using KMS client and KMS server.
//  Args: secCtx   - the security context.
//        kms_auth - KMS URL authority (optional user, host, optional
//                   port).
//        user_uri - User URI.
//  Rets: Standard return codes
ScErrno ProvisionKeys(const SecCtx &ctx, const std::string &kms_auth,
					  const std::string &user_uri);

//  Updates the certificate cache, using KMS client and KMS server.
//  Args: secCtx   - the security context.
//        kms_auth - KMS URL authority (optional user, host, optional
//                   port).
//        user_uri - User URI.
//  Rets: Standard return codes
ScErrno updateCertCache(const SecCtx &ctx, const std::string &kms_auth,
                        const std::string &user_uri);

#endif


#include <iostream>
#include <fstream>
#include "sc_socket.h"
#include <errno.h>
#include <string.h>

//Logging
#include "log4cpp/Category.hh"
#include "log4cpp/Appender.hh"
#include "log4cpp/FileAppender.hh"
#include "log4cpp/OstreamAppender.hh"
#include "log4cpp/Layout.hh"
#include "log4cpp/BasicLayout.hh"
#include "log4cpp/Priority.hh"

#include <stdio.h>
#if defined (_WIN32) || defined (_WIN64)
#include <conio.h>
#endif

// Secure Chorus includes
#include <sc_libms.h>
#include <sc_libmskms.h>

// Conditional include for message testing
#ifdef MESSAGE_TEST
#include <sc_misc.h>
#endif // MESSAGE_TEST

// Local utility includes
#include "sc_app_init.h"
#include "sc_app_crypto.h"
#include "sc_errno.h"

#include <iostream>
#include <fstream>

#include <sys/stat.h>

#define GROUP_CONFIG_DIRECTORY "./config"

#define MAX_PATH_LEN    1024
#define MAX_GROUP_SIZE   100
#define MAX_URI_LEN      164
#define MAX_URL_LEN      164
#define MAX_IP_ADDR_LEN   16
#define MAX_PORT_LEN       8

using namespace std;

enum KeyType {
    GSK,
    GMK,
    MCPTT
};

typedef struct
{
    std::string ip_addr;
    int port;
    std::string user_name;
} Ue;


const string configfile = "sc.cfg";
const string groupfile = "group.cfg";

#define DEFAULT_SEND_PORT 8301

#if !defined(_WIN32) && !defined(_WIN64)
/*******************************************************************************
 * Get keyboard input.
 ******************************************************************************/
int _kbhit(void) {
    struct timeval tv;
    fd_set read_fd;

    /* Do not wait */
    tv.tv_sec=0;
    tv.tv_usec=0;

    /* Must be done first to initialize read_fd */
    FD_ZERO(&read_fd);

    /* Makes select() ask if input is ready:
     *   - 0 is the file descriptor for stdin 
     */
    FD_SET(0, &read_fd);

    /* The first parameter is the number of the
    * largest file descriptor to check + 1. */
    if (select(1, &read_fd, NULL, /*No writes*/NULL, /*No exceptions*/&tv) == -1) {
        printf("Error occurred\n");
        return 0; 
    }

    if (FD_ISSET(0, &read_fd)) {
        /* Character pending on stdin */
        return 1;
    }

    /* no characters were pending */
    return 0;
} /* _kbhit */
#endif

/*******************************************************************************
 * Close a socket
 *
 * Params:
 *     The socket to close
 ******************************************************************************/
void closeSocket(SOCKET socket) {
    closesocket(socket);
    WSACleanup();
} /* closeSocket */

/*******************************************************************************
 * Create a Sender socket for sending Goup key material to group members.
 *
 * Returns:
 *     The socket or error.
 ******************************************************************************/
SOCKET createSenderSocket() {
    SOCKET sending_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sending_socket == INVALID_SOCKET) {
        printf("    Error: unable to create sender socket: %d\n", 
               WSAGetLastError());
        WSACleanup();
        return -1;
    }
    int         result = 0;
    sockaddr_in sender_address;

    /* Create listener socket */
    sender_address.sin_family      = AF_INET;
    sender_address.sin_port        = htons(DEFAULT_SEND_PORT);
    sender_address.sin_addr.s_addr = htonl(INADDR_ANY);
    result = bind(sending_socket, (SOCKADDR *) &sender_address, sizeof(sender_address));
    if (result != 0) {
        printf("    Error: failed to bind sending socket. result = %d\n", result);
        closesocket(sending_socket);
        WSACleanup();
        return -1;
    }
    return sending_socket;
} /* createSenderSocket */

/*******************************************************************************
 * Saves configuration information to a config file
 *
 * Params:
 *     ctx  - the secure context to save
 *     user - the users uri to save
 * Returns:
 *     true if successful otherwise false
 ******************************************************************************/
bool saveConfig(
    SecCtx const &ctx, 
    string const &user)
{
    ofstream config(configfile);

    /* check the file is open */
    if (!config.is_open()) {
        return false;
    }

    /* Save the configuration */
    config << ctx << endl;
    config << user << endl;

    /* Close the file */
    config.close();

    return true;
} /* saveConfig */

/*******************************************************************************
 * Saves configuration information to a config file
 *
 * Params:
 *     ctx  - the secure context to put read configuration into
 *     user - the users uri to put read configuration into
 * Returns:
 *     true if successful otherwise false
 ******************************************************************************/
bool loadConfig(
    SecCtx &ctx,
    string &user)
{
    ifstream config(configfile);

    /* check the file is open */
    if (!config.is_open()) {
		return false;
    }

    /* Read the configuration */
    config >> ctx;
    config >> user;

    /* close the file */
    config.close();

    return true;
} /* loadConfig */

/*******************************************************************************
 * Loads specified key data (GMK, GSK or MCPTT) from file.
 *
 * Params:
 *     KType - GMK, GSK or MCPTT
 *     key   - The result
 * Returns:
 *     true if successful otherwise false
 ******************************************************************************/
bool loadKey(KeyType KType, vector<uint8_t> &key)
{
    string key_filename;
    string key_str;

    switch (KType) {
        case GMK :
            key_filename = "GMK";
            break;
        case GSK :
            key_filename = "GSK";
            break;
        case MCPTT :
            key_filename = "MCPTT";
            break;
        default :
            cerr << "Unknown keyfile type <" << KType << ">" << endl;
            return false;
            break;
    }

    ifstream key_file(key_filename);
            
    if (!key_file.is_open())
    {
        cerr << "Failed to read key file <" << key_filename << ">" << endl;
        return false;
    }

    key_file >> key_str;

    /* Close the file */
    key_file.close();

    if (key_str.length() != 32) {
        cerr << "Key is incorrect length" << endl;
        key_file.close();
        exit(1);
    }
    else {
        for (int c = 0; c < 32; c += 2)
        {
            key.push_back(strtoul(key_str.substr(c, 2).c_str(), NULL, 16));
        }
    }

    return true;

} /* loadKey */

/*******************************************************************************
 * Gets a UDP socket address for the supplied domain name and service.
 *
 * Params:
 *     domainname - the name or ip address
 *     service    - the UDP port
 *     sockaddr   - the socker address to populate.
 *
 * Return  Success/ Failure indication
 *
 * Lifted from original sc_app_main.cpp
 ******************************************************************************/
bool getInetUdpSocketAddress(
    const char *domainname, 
    const char *service, 
    sockaddr_in &sockaddr)
{
    int result;
    bool success = false;
    struct addrinfo hints;
    struct addrinfo *infolist = NULL;
    struct addrinfo *info_p = NULL;

    /* Set up the hints for an IPV4 UDP socket */
    memset( &hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_UDP;

    result = getaddrinfo(domainname, service, &hints, &infolist);
    if (result == 0) {
        /* Search through the results for a suitable socket */
        for( info_p = infolist; info_p != NULL; info_p = info_p->ai_next) {
            if( info_p->ai_family == AF_INET) {
                /* Copy the sockaddr which we know is IPV4 sockaddr */
                memcpy(&sockaddr, info_p->ai_addr, sizeof(sockaddr_in));
                success = true;
                break;
            }
        }
    }

    /* Cleanup and exit */
    freeaddrinfo( infolist);

    return success;
} /* getInetUdpSocketAddress */

int userList(std::vector<Ue> &group_ues);

/*******************************************************************************
 * Pass GMK, GSK MCPTT Key material to all members of groups.
 ******************************************************************************/
int sendGroupKeyMatToGroup(
    const ScLibMs::IMsgType &msgType, 
    const SecCtx &secCtx, 
    const string &group_uri)
{
    log4cpp::Category& log = log4cpp::Category::getRoot();

    vector<uint8_t> iMessage;

    string remote_uri;
    string recipient_uri;
    ScErrno sc_result;
    sockaddr_in remote_address;

    char tmpPath[MAX_PATH_LEN];
    char line[MAX_PATH_LEN];

    vector<Ue> group_ues;

    vector<uint8_t> gmk;
    vector<uint8_t> gsk;
    vector<uint8_t> mcptt;

    int            result     = 0;

    SOCKET sending_socket = -1;
    log.debug("Entered sendGroupKeyMatToGroup");

    /**************************************************************************/
    /* Initialise WinSock if MSoft.                                           */
    /**************************************************************************/

    /* Create a sender socket */
    WSADATA wsaData;
    result = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (result != NO_ERROR) {
         log.error("Failed to initialise Winsock. Result = %d", result);
         return -1;
    }

    /**************************************************************************/
    /* Create Sender Socket                                                   */
    /**************************************************************************/
    sending_socket = createSenderSocket();
    if( sending_socket == INVALID_SOCKET) {
        log.error("Unable to create sender socket: %d\n", WSAGetLastError());
        WSACleanup();
        return -1;
    }
    log.debug("Socket created.");

    /***************************************************************************
     * Read from the group file to get details of members -
     *     o the filename is the recipient/member URI.
     *     o the file itself contains the IP address and Port for the member.
     **************************************************************************/
    userList(group_ues);

    if (group_ues.size() == 0)
    {
        log.errorStream() << "No UEs in group file: " << groupfile;
        return -1;
    }

    if (!loadKey(GMK, gmk)) {
        log.errorStream() << "Could not retrieve GMK to send";
    }

    if (!loadKey(GSK, gsk)) {
        log.errorStream() << "Could not retrieve GSK to send";
    }

    if (!loadKey(MCPTT, mcptt)) {
        log.errorStream() << "Could not retrieve MCPTT to send";
    }

    for (auto & ue : group_ues) {
        log.debug("    -------------------------------------------");

        /* Create socket addresses */
        log.debugStream() << "    Sending to " << ue.user_name << " " << ue.ip_addr << ":" << ue.port;
        if (!getInetUdpSocketAddress(ue.ip_addr.c_str(), std::to_string(ue.port).c_str(), remote_address)) {
            log.errorStream() << "     Error: Unable to get socket address for "
                 << ue.user_name << " " << ue.ip_addr << ":" << ue.port;
            closeSocket(sending_socket);
            return -1;
        }

        switch (msgType) {
        case (ScLibMs::GMKMsgType) : 
            log.debugStream() << "    Create GMK message type iMessage";
            sc_result = ScLibMs::CreateMikeySakkeGMKIMessageWithGmk(iMessage,
                secCtx, group_uri, /* sender_uri*/
                ue.user_name,/* recipient_uri*/
                gmk);
            if (sc_result != ScErrno::SUCCESS) 
            {
                log.errorStream() << "Call to Create MS GMK failed. Error code is " << sc_result;
                exit(1);
            }
            break;
        case (ScLibMs::GSKMsgType) :
            log.debugStream() << "    Create GSK message type iMessage";
            sc_result = ScLibMs::CreateMikeySakkeGSKIMessage(iMessage,
                gmk, /* GMK needed to encrypt GSK */
                gsk, secCtx,
                ue.user_name, /* sender_uri  IDRi */
                group_uri,    /* recipient_uri IDRr  */
                /* v */ false);
            if (sc_result != ScErrno::SUCCESS)
            {
                log.errorStream() << "Call to Create MS GSK failed. Error code is " << sc_result;
                exit(1);
            }
            break;
        case (ScLibMs::MCPTTMsgType) :
            log.debugStream() << "    Create MC-PTT message type iMessage";
            sc_result = ScLibMs::CreateMikeySakkeMCPTTIMessageWithGmk(iMessage,
                secCtx,
                group_uri,    /* sender_uri*/
                ue.user_name,/* recipient_uri*/
                mcptt, /* v */ false);
            if (sc_result != ScErrno::SUCCESS) 
            {
                log.errorStream() << "Call to Create MS MCPTT failed. Error code is " << sc_result;
                exit(1);
            }
            break;
        default:
            log.errorStream() << "Unknown messagetype, not GMK, GSK or MC-PTT";
            break;
        }

        log.debug("iMessage created");

        result = sendto(sending_socket, (const char *)&iMessage.front(), iMessage.size(), 0,
            (SOCKADDR *)&remote_address, sizeof(remote_address));

/* Output message sent */
printf("iMessage sent: ");
for (int r = 0; r < iMessage.size(); r++) {
    if ((r%16)==0) { printf("\n            "); }
    else if ((r%4)==0) { printf(" "); }
    printf("%02x", iMessage[r]);
}
printf("\n");

        if (result == SOCKET_ERROR) {
            log.errorStream() << " Failed to send MIKEY-SAKKE I_MESSAGE.";
            WSACleanup();
        }
        log.debug("    iMessage sent");
        log.debug("    -------------------------------------------");
        /* If we sent a GMK to the group participants, this is a specific action
        * like init, or prov. Therefore we should end for now. Subsequentally,
        * we can use GSK to start a dialog.
        */
    }

    log.debug("    All iMessage sends complete!");
    /* Clean up */
    closeSocket(sending_socket);

    return 0;
} /* sendGroupKeyMatToGroup */


/*******************************************************************************
 * Read all the users from the group file.
 *
 * Returns:
 *     Number of users in group
 ******************************************************************************/
int userList(std::vector<Ue> &group_ues)
{
    log4cpp::Category& log = log4cpp::Category::getRoot();
    group_ues.clear();
    std::ifstream group_file_stream;
    group_file_stream.open(groupfile);
    if (!group_file_stream.is_open())
    {
        log.errorStream() << "Error opening group file: " << groupfile;
        return -1;
    }

    std::string file_stream_line;
    while (std::getline(group_file_stream, file_stream_line))
    {
        Ue newUe;
        std::stringstream ss(file_stream_line);
        ss >> newUe.user_name;
        ss >> newUe.ip_addr;
        std::string port_string;
        ss >> port_string;
        newUe.port = atoi(port_string.c_str());
        group_ues.push_back(newUe);
    }

    group_file_stream.close();

    return 0;
} /* userList */

/*******************************************************************************
 * Simple menu for Group Manager Configuration.
 ******************************************************************************/
int groupManagerConfigMenu() {
    unsigned int cont = 1;
    char         selection[10];
    ScErrno      sc_result;
    SecCtx       secCtx;

    struct stat  fileInfo;
    FILE        *fp = NULL;
    char         bootstrapFilename[MAX_PATH_LEN];
    char         kmsUrl[MAX_URL_LEN];
    char         kmsUri[MAX_URI_LEN];
    char         groupUri[MAX_URI_LEN];

    while (cont) {
        memset(bootstrapFilename, 0, sizeof(bootstrapFilename));
        memset(kmsUrl,            0, sizeof(kmsUrl));
        memset(kmsUri,            0, sizeof(kmsUri));
        memset(groupUri,          0, sizeof(groupUri));
        if (NULL == (fp = fopen(GROUP_CONFIG_DIRECTORY, "r"))) {
            printf("    Group manager NOT configured yet\n");
        }
        else {
            if (!fscanf(fp, "%s%s%s%s", bootstrapFilename, kmsUrl, kmsUri, groupUri)) {
                printf("    Error: Failed to read config file\n");
                return -1;
            }
            fclose(fp);
        }

        printf("\n    Group Manager Config Menu");
        printf("\n    =========================\n");

        if (strlen(bootstrapFilename)) {
            printf("        Current configuration - \n");
            printf("            bootstrap file: %s\n", bootstrapFilename);
        }
        if (strlen(kmsUrl)) {
            printf("            KMS URL       : %s\n", kmsUrl);
        }
        if (strlen((char *)&kmsUri)) {
            printf("            KMS URI       : %s\n", kmsUri);
        }
        if (strlen((char *)&groupUri)) {
            printf("            Group URI     : %s\n", groupUri);
        }
        printf("\n        0 - Return to previous menu\n");
        printf("        1 - Set details\n");
        printf("        2 - init\n");
        printf("        3 - prov\n");
        printf("        4 - cert\n");
        printf("    Selection: ");
        scanf("%1s", &selection);

        switch (atoi((const char *)selection)) {
            case 0 : /* Return to previous menu */
                cont = 0;
                break;
            case 1 : /* Set Details */
                getchar();
                printf("    Bootstrap filename (default boot.key)      : ");
                fgets(bootstrapFilename, sizeof(bootstrapFilename), stdin);
                *(strchr(bootstrapFilename, '\n')) = 0;
                if (!strlen(bootstrapFilename)) {
                    strcpy(bootstrapFilename, "boot.key");
                }

                printf("    KMS URL (default demo.securechous.com)     : ");
                fgets(kmsUrl, sizeof(kmsUrl), stdin);
                *(strchr(kmsUrl, '\n')) = 0;
                if (!strlen(kmsUrl)) {
                    strcpy(kmsUrl, "demo.securechorus.com");
                }

                printf("    KMS URI (default kms.demo.securechous.com) : ");
                fgets(kmsUri, sizeof(kmsUri), stdin);
                *(strchr(kmsUri, '\n')) = 0;
                if (!strlen(kmsUri)) {
                    strcpy(kmsUri, "kms.demo.securechorus.com");
                }
                while (!strlen(groupUri)) { /* No default value */
                    printf("    Group URI (e.g. id@demo.securechorus.com)  :");
                    fgets(groupUri, sizeof(groupUri), stdin);
                    *(strchr(groupUri, '\n')) = 0;
                }
                if (NULL == (fp = fopen(GROUP_CONFIG_DIRECTORY, "w"))) {
                    printf("    Error: Failed to add group config file <%s>\n", 
                    GROUP_CONFIG_DIRECTORY);
                }
                else {
                    fprintf(fp, "%s %s %s %s", bootstrapFilename, kmsUrl, kmsUri, groupUri);
                    fclose(fp);
                }
                break;
            case 2 : /* Init -  Set up parameters */
                { 
                    struct stat buf;
                    if ((stat("./sc.cfg", &buf)==0) || (stat("./sc_keystore.txt", &buf)==0)) {
                        printf("    Error: You cannot run 'init' more than once!\n");
                        printf("           You have existing sc.cfg or sc_keystfile files\n");
                    }
                    else {
                        /* Check initial details have been set (option 1). */
                        if ((!strlen(bootstrapFilename)) || (!strlen(kmsUrl)) ||
                            (!strlen(kmsUri)) || (!strlen(groupUri))) {
                            printf("    Error: Can't preform 'init' don't have all the info needed\n");
                            printf("        - Have you run Option 1 yet?\n");
                            return -1;
                        }

                        /* Perform the intitialisation */
                        string kms_uri(kmsUri);
                        string kms_url(kmsUrl);
                        string group_uri(groupUri);
                        secCtx = init(kms_uri, kms_url, group_uri, bootstrapFilename);

                        /* Check if it suceeded */
                        if (secCtx == 0) {
                             /* Failed to initialise */
                             printf("    Error: init operation failed\n");
                             return -1;
                        }
 
                        /* Write out config */
                        if (!saveConfig( secCtx, group_uri)) {
                            /* Failed to save configuration */
                            printf("    Error: failed to write configuration file - %s\n", configfile.c_str());
                            return -1;
                        }
                        printf("Initialisation successful\n"); 
                    }
                }
                break;
            case 3 : /* Prov */
                {   /* check and decode command line arguments */

                    /* Load the configuration */
                    string my_uri(groupUri);
                    if( !loadConfig( secCtx, my_uri)) {
                        /* Failed to save configuration */
                        printf("    Error: failed to read configuration file - %s\n", configfile.c_str());
                        return -1;
                    }

                    /* Request keys from KMS */
                    cout << "Provisioning keys from KMS." << endl;
                    if ((sc_result = ProvisionKeys(secCtx, kmsUrl, my_uri)) != ScErrno::SUCCESS) {
                        printf("    Error: Failed to provision keys. SC Error = %d (%s)\n", sc_result, ScErrName(sc_result));
                        return -1;
                    }

                    /* Success */
                    printf("Key provisioning succeeded.\n");

                }
                break;
            case 4 : /* Cert */
                {
                    string my_uri(groupUri);

                    /* Load the configuration */
                    if( !loadConfig( secCtx, my_uri)) {
                        /* Failed to save configuration */
                        printf("    Error: failed to read configuration file - %s\n", configfile.c_str());
                        return -1;
                    }

                    /* Request certificate update from KMS server */
                    printf("Updating certificate cache from KMS.\n");
                    if( (sc_result = updateCertCache( secCtx, kmsUrl, my_uri)) != ScErrno::SUCCESS) {
                        printf("    Error: Failed to update certificate cache. SC Error = %d (%s)", sc_result,  ScErrName(sc_result));
                        return -1;
                    }

                    /* Success */
                    printf("Certificate cache update succeeded.\n");
                }
                break;
            default : 
                printf("    Error: Unknown option - try again!\n");
                break;
        }
    }
} /* groupManagerConfigMenu */

/*******************************************************************************
 * manageGroupMenu
 *
 * List, Add and Delete group members (UE - User Entities)
 *
 * IP Address and Port are stored in a file named the UE. These UE files are 
 * stored in the UE directory (GROUP_MEMBERS_DIRECTORY).
 ******************************************************************************/
void manageGroupMenu() 
{
    log4cpp::Category& log = log4cpp::Category::getRoot();

    unsigned int  cont       = 1;
    unsigned int  selection  = 1;
    FILE         *fp         = NULL;;
    uint8_t       c          = 0;
    uint8_t       user_count = 0;
    char         *user       = NULL;
    struct stat   fileInfo;

    while (cont) {
        printf("\n    Manage Group Menu");
        printf("\n    =================\n");
        printf("        0 - Return to previous menu\n");
        printf("        1 - List Group members\n");
        printf("        2 - Add group member\n");
        printf("        3 - Delete group member\n");
        printf("    Selection: ");
        scanf("%u", &selection);
        std::string userId;
        std::string ipAddress;
        std::string port;
        std::vector<Ue> uelist;
        userList(uelist);
        bool match = false;
        switch (selection) {
            case 0 : /* Return to previous menu */
                cont = 0;
                break;
            case 1 : /* List users in group */
                printf("\n    User List:\n");
                for (Ue & ue : uelist)
                {
                    cout << "        " <<ue.user_name << " "
                        << ue.ip_addr << ":" << ue.port << std::endl;
                }
                break;
            case 2 : /* Add new user to group */
                cin.ignore();
                cout << "    New user  :";
                std::getline(std::cin, userId);

                cout << "    IP Address :";
                std::getline(std::cin, ipAddress);

                cout << "    Port       :";
                std::getline(std::cin, port);

                for (Ue & ue : uelist)
                {
                    if (ue.port == atoi(port.c_str()) &&
                        ue.ip_addr.compare(ipAddress) == 0)
                    {
                        match = true;
                        break;
                    }
                }

                if (match) 
                {
                     printf("    Error: User <%s> already exists. Delete first to re-add\n", userId.c_str());
                }
                else 
                {
                    std::ofstream group_file_stream;
                    group_file_stream.open(groupfile, ios::app);
                    if (!group_file_stream.is_open())
                    {
                        log.errorStream() << "Error opening group file: " << groupfile;
                    }
                    group_file_stream << userId << " " << ipAddress << " " << port << endl;
                    group_file_stream.close();
                }
                break;
            case 3 : /* Delete user from group */
                printf("\n    Delete user\n");
                printf("        0 - Return to previous menu\n");
                for (int count = 0; count < uelist.size(); count++)
                {
                    auto ue = uelist[count];
                    cout << "        " << count + 1 << " - " << ue.user_name << " "
                        << ue.ip_addr << ":" << ue.port << std::endl;
                }
                printf("    Selection: ");
                selection = 0;
                scanf(" %u", &selection);
                if ((selection > 0) && (selection <= uelist.size()))
                {
                    std::ifstream group_file_stream;
                    std::ofstream temp_stream;
                    group_file_stream.open(groupfile);
                    temp_stream.open("temp");
                    if (!group_file_stream.is_open())
                    {
                        log.errorStream() << "Error opening group file: " << groupfile;
                    }

                    std::string line;
                    int count = 0;
                    while (std::getline(group_file_stream, line))
                    {
                        if (count != selection - 1)
                        {
                            temp_stream << line << endl;
                        }
                        count++;
                    }
                    
                    group_file_stream.close();
                    temp_stream.close();
                    std::remove(groupfile.c_str());
                    std::rename("temp", groupfile.c_str());
                }
                break;
            default : 
                printf("    Error: Unknown option - try again!\n");
                break;
        }
    }
} /* manageGroupMenu */

/*******************************************************************************
 * distributeGroupKeysMenu
 *
 * List, Add and Delete group members (UE - User Entities)
 ******************************************************************************/
void distributeGroupKeysMenu() {
	log4cpp::Category& log = log4cpp::Category::getRoot();

    unsigned int cont      = 1;
    unsigned int selection = 1;
    SecCtx secCtx;

    string my_uri;

    /* Load the configuration */
    if (!loadConfig(secCtx, my_uri)) {
        /* Failed to save configuration */
        cerr << "Failed to read configuration file - " << configfile;
        return;
    }

    while (cont) {
        printf("\n    Distribute Group Keys Menu");
        printf("\n    ==========================\n");
        printf("        0 - Return to previous menu\n");
        printf("        1 - Send GMK to group members\n");
        printf("        2 - Send GSK to group members\n");
        printf("        3 - Send MC-PTT to group members\n");
        printf("    Selection: ");

        scanf("%u", &selection);
        switch (selection) {
            case 0 : /* Return to previous menu */
                cont = 0;
                break;
            case 1 : /* Send GMK to Group Members*/
                sendGroupKeyMatToGroup(ScLibMs::GMKMsgType, secCtx, my_uri);
                break;
            case 2 : /* Send GSK to Group Members*/
                sendGroupKeyMatToGroup(ScLibMs::GSKMsgType, secCtx, my_uri);
                break;
            case 3 : /* Send MC-PTT to Group Members*/
                sendGroupKeyMatToGroup(ScLibMs::MCPTTMsgType, secCtx, my_uri);
                break;
            default : 
                printf("    Unknown option - try again!\n");
                break;
        }
    }
} /* distributeGroupKeysMenu */


int main(int argc, char* argv[])
{
    /* Setup logging. To log to just the log file do the following:
    log.debugStream() << "Log message to log file";
    To log to the file and console do the following:
    log.infoStream() << "Log message to log file and console";*/
    log4cpp::OstreamAppender console_appender("console", &std::cout);
    console_appender.setThreshold(log4cpp::Priority::INFO);
    log4cpp::FileAppender file_appender("default", "sc_group_mgr.log", false);
    file_appender.setThreshold(log4cpp::Priority::DEBUG);
    log4cpp::Category& log = log4cpp::Category::getRoot();
    log.setPriority(log4cpp::Priority::DEBUG);
    log.addAppender(console_appender);
    log.addAppender(file_appender);

    SecCtx       secCtx;
    string       my_uri;
    ScErrno      sc_result;
    string       remote_addr = "127.0.0.1";
    string       remote_uri;
    string       sender_uri;
    string       recipient_uri;
    unsigned int cont = 1;
    unsigned int selection = 1;
    char         userIncPath[MAX_PATH_LEN];
    int          result = 0;

    cout << "Secure Chorus Group Manager..." << endl;

    string app(argv[0]);

    /* Load the configuration */
    /* Loads ctx and user from sc.cfg */
    if (!loadConfig( secCtx, my_uri)) {
        /* Failed to save configuration */
        log.errorStream() << " failed to read configuration file - " << configfile;
        cout << "    You probably haven't initiated the keymat yet";
        cout << "        - create boot.key file and run 'init'!";
    }
    else {
        sender_uri = my_uri;
    }

    while (cont) {
        /* Main Menu */
        printf("\n    Group Manager Main Menu");
        printf("\n    =======================\n");
        printf("        0 - Exit\n");
        printf("        1 - Group manager config\n");
        printf("        2 - Manage group\n");
        printf("        3 - Distribute group keys (gmk, gsk, mc-ptt)\n");
        printf("    Selection: ");
        scanf("%u", &selection);
        switch (selection) {
            case 0 : /* Exit */
                printf("    Exiting!\n");
                exit(0);
                break;
            case 1 : /* Group Manager Config */
                groupManagerConfigMenu();
                break;
            case 2 : /* Manage group */
                manageGroupMenu();
                break;
            case 3 : /* Distribute group keys*/
                distributeGroupKeysMenu();
                break;
            default : 
                printf("    Unknown option - try again!\n");
                break;
        }
    }
    return 0;
} 

/******************************************************************************/
/*                                End Of File                                 */
/******************************************************************************/

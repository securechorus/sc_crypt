// Derived class for KMS parameter blocks

#ifndef SCKMSPARAMETERBLOCK_H
#define SCKMSPARAMETERBLOCK_H

#include "SCParameterBlock.h"

#include <vector>
#include <string>
#include <stdint.h>
#include <jni.h>
#include "sc_context.h"
#include "sc_errno.h"

class SCKMSParameterBlock : SCParameterBlock
{
public:
	// construct (resets all parameters)
	SCKMSParameterBlock(JNIEnv * jniEnvironment);
	// import from a jobject into the relevant fields
	ScErrno importFromJObject(const jobject javaParamsObject) override;
	// import from a jobject into the relevant fields
	ScErrno exportToJObject(jobject& javaParamsObject) override;

	// getter methods for the member variables
	SecCtx getSecCtx(void) { return secCtx; }
	std::vector<uint8_t> getGenericBuffer(void) { return genericBytes; }
	std::string getUserUri(void) { return userUri; }
	std::string getKmsUrl(void) { return kmsUrl; }
	std::string getKmsUri(void) { return kmsUri; }
	std::string getTransportKeyId(void) { return transportKeyId; }
	std::string getSchemaPath(void) { return schemaPath; }

	// setter methods for the member variables
	void setSecCtx(SecCtx secCtx) { this->secCtx = secCtx; }
	void setGenericBuffer(std::vector<uint8_t> genericBytes) { this->genericBytes = genericBytes; }
	void setUserUri(std::string userUri) { this->userUri = userUri; }
	void setKmsUrl(std::string kmsUrl) { this->kmsUrl = kmsUrl; }
	void setKmsUri(std::string kmsUri) { this->kmsUri = kmsUri; }
	void setTransportKeyId(std::string transportKeyId) { this->transportKeyId = transportKeyId; }
	void setSchemaPath(std::string schemaPath) { this->schemaPath = schemaPath; }

protected:
	// set up the Java JNI methods (initialise class and methods)
	ScErrno configureJNI(void) override;

private:
	// directly maps to the expected jobject
	SecCtx secCtx;
	std::vector<uint8_t> genericBytes;
	std::string userUri;
	std::string kmsUrl;
	std::string kmsUri;
	std::string transportKeyId;
	std::string schemaPath;

	// Java class for message block
	static const char* const SCKMSParameterBlock_ClassName;
	jclass SCKMSParameterBlock_Class;

	// Java method: public void getSecCtx(long);
	static const char* const SCKMSParameterBlock_setSecCtx_MethodName;
	static const char* const SCKMSParameterBlock_setSecCtx_MethodSignature;
	jmethodID SCKMSParameterBlock_setSecCtx_Method;

	// Java method: public long getSecCtx();
	static const char* const SCKMSParameterBlock_getSecCtx_MethodName;
	static const char* const SCKMSParameterBlock_getSecCtx_MethodSignature;
	jmethodID SCKMSParameterBlock_getSecCtx_Method;

	// Java method: public java.lang.String getUserUri();
	static const char* const SCKMSParameterBlock_getUserUri_MethodName;
	static const char* const SCKMSParameterBlock_getUserUri_MethodSignature;
	jmethodID SCKMSParameterBlock_getUserUri_Method;

	// Java method: public void setUserUri(java.lang.String);
	static const char* const SCKMSParameterBlock_setUserUri_MethodName;
	static const char* const SCKMSParameterBlock_setUserUri_MethodSignature;
	jmethodID SCKMSParameterBlock_setUserUri_Method;

	// Java method: public java.lang.String getKmsUrl();
	static const char* const SCKMSParameterBlock_getKmsUrl_MethodName;
	static const char* const SCKMSParameterBlock_getKmsUrl_MethodSignature;
	jmethodID SCKMSParameterBlock_getKmsUrl_Method;

	// Java method: public void setKmsUrl(java.lang.String);
	static const char* const SCKMSParameterBlock_setKmsUrl_MethodName;
	static const char* const SCKMSParameterBlock_setKmsUrl_MethodSignature;
	jmethodID SCKMSParameterBlock_setKmsUrl_Method;

	// Java method: public java.lang.String getKmsUri();
	static const char* const SCKMSParameterBlock_getKmsUri_MethodName;
	static const char* const SCKMSParameterBlock_getKmsUri_MethodSignature;
	jmethodID SCKMSParameterBlock_getKmsUri_Method;

	// Java method: public void setKmsUrl(java.lang.String);
	static const char* const SCKMSParameterBlock_setKmsUri_MethodName;
	static const char* const SCKMSParameterBlock_setKmsUri_MethodSignature;
	jmethodID SCKMSParameterBlock_setKmsUri_Method;

	// Java method: public java.lang.String getTransportKeyId();
	static const char* const SCKMSParameterBlock_getTransportKeyId_MethodName;
	static const char* const SCKMSParameterBlock_getTransportKeyId_MethodSignature;
	jmethodID SCKMSParameterBlock_getTransportKeyId_Method;

	// Java method: public void setKmsUrl(java.lang.String);
	static const char* const SCKMSParameterBlock_setTransportKeyId_MethodName;
	static const char* const SCKMSParameterBlock_setTransportKeyId_MethodSignature;
	jmethodID SCKMSParameterBlock_setTransportKeyId_Method;

	// Java method: public byte[] getGenericBytes();
	static const char* const SCKMSParameterBlock_getGenericBytes_MethodName;
	static const char* const SCKMSParameterBlock_getGenericBytes_MethodSignature;
	jmethodID SCKMSParameterBlock_getGenericBytes_Method;

	// Java method: public void setGenericBytes(byte[]);
	static const char* const SCKMSParameterBlock_setGenericBytes_MethodName;
	static const char* const SCKMSParameterBlock_setGenericBytes_MethodSignature;
	jmethodID SCKMSParameterBlock_setGenericBytes_Method;

	// Java method: public int getLengthGenericBytes();
	static const char* const SCKMSParameterBlock_getLengthGenericBytes_MethodName;
	static const char* const SCKMSParameterBlock_getLengthGenericBytes_MethodSignature;
	jmethodID SCKMSParameterBlock_getLengthGenericBytes_Method;

	// Java method: public void setSchemaPath(java.lang.String);
	static const char* const SCKMSParameterBlock_setSchemaPath_MethodName;
	static const char* const SCKMSParameterBlock_setSchemaPath_MethodSignature;
	jmethodID SCKMSParameterBlock_setSchemaPath_Method;

	// Java method: public java.lang.String getSchemaPath();
	static const char* const SCKMSParameterBlock_getSchemaPath_MethodName;
	static const char* const SCKMSParameterBlock_getSchemaPath_MethodSignature;
	jmethodID SCKMSParameterBlock_getSchemaPath_Method;

};

#endif

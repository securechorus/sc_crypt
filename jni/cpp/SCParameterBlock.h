// Base class for parameter blocks

#ifndef SCPARAMETERBLOCK_H
#define SCPARAMETERBLOCK_H

#include <vector>
#include <stdint.h>
#include <jni.h>

#include "sc_context.h"
#include "sc_errno.h"

class SCParameterBlock
{
public:
	// construct (resets all parameters)
	SCParameterBlock(JNIEnv * jniEnvironment);
	// import from a jobject into the relevant fields
	virtual ScErrno importFromJObject(const jobject javaParamsObject) = 0;
	// import from a jobject into the relevant fields
	virtual ScErrno exportToJObject(jobject& javaParamsObject) = 0;

protected:
	// set up the Java JNI methods (initialise class and methods)
	virtual ScErrno configureJNI(void) = 0;

	// byte array extraction
	ScErrno importBytes(const jobject javaParamsObject, jmethodID methodID, std::vector<uint8_t>& outputVec);
	ScErrno exportBytes(jobject javaParamsObject, jmethodID methodID, const std::vector<uint8_t> outputVec);

	// the Java VM
	JNIEnv * env;
	bool jniInitialised;

};

#endif
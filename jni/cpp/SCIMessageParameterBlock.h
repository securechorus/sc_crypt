// Derived class for IMessage parameter blocks

#ifndef SCIMESSAGEPARAMETERBLOCK_H
#define SCIMESSAGEPARAMETERBLOCK_H

#include <vector>
#include <string>
#include <stdint.h>
#include <jni.h>

#include "sc_context.h"
#include "sc_errno.h"
#include "SCParameterBlock.h"

class SCIMessageParameterBlock : SCParameterBlock
{
public:
	// construct (resets all parameters)
	SCIMessageParameterBlock(JNIEnv * jniEnvironment);
	// import from a jobject into the relevant fields
	ScErrno importFromJObject(const jobject javaParamsObject) override;
	// import from a jobject into the relevant fields
	ScErrno exportToJObject(jobject& javaParamsObject) override;

	// getter methods for the member variables
	SecCtx getSecCtx(void) { return secCtx; }
	std::vector<uint8_t> getIMessage(void) { return iMessage; }
	std::vector<uint8_t> getSsv(void) { return ssv; }
	std::string getSenderUri(void) { return senderUri; }
	std::string getRecipientUri(void) { return recipientUri; }
	bool getV(void) { return v; }
	bool getCheckSignature(void) { return checkSignature; }
	std::vector<uint8_t> getGenericBytes1(void) { return genericBytes1; }
	std::vector<uint8_t> getGenericBytes2(void) { return genericBytes2; }

	// setter methods for the member variables
	void setSecCtx(SecCtx secCtx) { this->secCtx = secCtx; }
	void setIMessage(std::vector<uint8_t> iMessage) { this->iMessage = iMessage; }
	void setSsv(std::vector<uint8_t> ssv) { this->ssv = ssv; }
	void setSenderUri(std::string senderUri) { this->senderUri = senderUri; }
	void setRecipientUri(std::string recipientUri) { this->recipientUri = recipientUri; }
	void setV(bool v) { this->v = v; }
	void setCheckSignature(bool checkSignature) { this->checkSignature = checkSignature; }
	void setGenericBytes1(std::vector<uint8_t> genericBytes1) { this->genericBytes1 = genericBytes1; }
	void setGenericBytes2(std::vector<uint8_t> genericBytes2) { this->genericBytes2 = genericBytes2; }

protected:
	// set up the Java JNI methods (initialise class and methods)
	ScErrno configureJNI(void) override;

private:
	// directly maps to the expected jobject
	SecCtx secCtx;
	std::vector<uint8_t> iMessage;
	std::vector<uint8_t> ssv;
	std::vector<uint8_t> genericBytes1;
	std::vector<uint8_t> genericBytes2;
	std::string senderUri;
	std::string recipientUri;
	bool v;
	bool checkSignature;

	// Java class for message block
	static const char* const SCIMessageParameterBlock_ClassName;
	jclass SCIMessageParameterBlock_Class;

	// Java method: public void getSecCtx(long);
	static const char* const SCIMessageParameterBlock_setSecCtx_MethodName;
	static const char* const SCIMessageParameterBlock_setSecCtx_MethodSignature;
	jmethodID SCIMessageParameterBlock_setSecCtx_Method;

	// Java method: public long getSecCtx();
	static const char* const SCIMessageParameterBlock_getSecCtx_MethodName;
	static const char* const SCIMessageParameterBlock_getSecCtx_MethodSignature;
	jmethodID SCIMessageParameterBlock_getSecCtx_Method;

	// Java method: public java.lang.String getSenderUri();
	static const char* const SCIMessageParameterBlock_getSenderUri_MethodName;
	static const char* const SCIMessageParameterBlock_getSenderUri_MethodSignature;
	jmethodID SCIMessageParameterBlock_getSenderUri_Method;

	// Java method: public void setSenderUri(java.lang.String);
	static const char* const SCIMessageParameterBlock_setSenderUri_MethodName;
	static const char* const SCIMessageParameterBlock_setSenderUri_MethodSignature;
	jmethodID SCIMessageParameterBlock_setSenderUri_Method;

	// Java method: public java.lang.String getRecipientUri();
	static const char* const SCIMessageParameterBlock_getRecipientUri_MethodName;
	static const char* const SCIMessageParameterBlock_getRecipientUri_MethodSignature;
	jmethodID SCIMessageParameterBlock_getRecipientUri_Method;

	// Java method: public void setRecipientUri(java.lang.String);
	static const char* const SCIMessageParameterBlock_setRecipientUri_MethodName;
	static const char* const SCIMessageParameterBlock_setRecipientUri_MethodSignature;
	jmethodID SCIMessageParameterBlock_setRecipientUri_Method;

	// Java method: public byte[] getIMessage();
	static const char* const SCIMessageParameterBlock_getIMessage_MethodName;
	static const char* const SCIMessageParameterBlock_getIMessage_MethodSignature;
	jmethodID SCIMessageParameterBlock_getIMessage_Method;

	// Java method: public void setIMessage(byte[]);
	static const char* const SCIMessageParameterBlock_setIMessage_MethodName;
	static const char* const SCIMessageParameterBlock_setIMessage_MethodSignature;
	jmethodID SCIMessageParameterBlock_setIMessage_Method;

	// Java method: public int getLengthIMessage();
	static const char* const SCIMessageParameterBlock_getLengthIMessage_MethodName;
	static const char* const SCIMessageParameterBlock_getLengthIMessage_MethodSignature;
	jmethodID SCIMessageParameterBlock_getLengthIMessage_Method;

	// Java method: public byte[] getSsv();
	static const char* const SCIMessageParameterBlock_getSsv_MethodName;
	static const char* const SCIMessageParameterBlock_getSsv_MethodSignature;
	jmethodID SCIMessageParameterBlock_getSsv_Method;

	// Java method: public void setSsv(byte[]);
	static const char* const SCIMessageParameterBlock_setSsv_MethodName;
	static const char* const SCIMessageParameterBlock_setSsv_MethodSignature;
	jmethodID SCIMessageParameterBlock_setSsv_Method;

	// Java method: public int getLengthSsv();
	static const char* const SCIMessageParameterBlock_getLengthSsv_MethodName;
	static const char* const SCIMessageParameterBlock_getLengthSsv_MethodSignature;
	jmethodID SCIMessageParameterBlock_getLengthSsv_Method;

	// Java method: public byte[] getGenericBytes1();
	static const char* const SCIMessageParameterBlock_getGenericBytes1_MethodName;
	static const char* const SCIMessageParameterBlock_getGenericBytes1_MethodSignature;
	jmethodID SCIMessageParameterBlock_getGenericBytes1_Method;

	// Java method: public void setGenericBytes1(byte[]);
	static const char* const SCIMessageParameterBlock_setGenericBytes1_MethodName;
	static const char* const SCIMessageParameterBlock_setGenericBytes1_MethodSignature;
	jmethodID SCIMessageParameterBlock_setGenericBytes1_Method;

	// Java method: public int getLengthGenericBytes1();
	static const char* const SCIMessageParameterBlock_getLengthGenericBytes1_MethodName;
	static const char* const SCIMessageParameterBlock_getLengthGenericBytes1_MethodSignature;
	jmethodID SCIMessageParameterBlock_getLengthGenericBytes1_Method;

	// Java method: public byte[] getGenericBytes2();
	static const char* const SCIMessageParameterBlock_getGenericBytes2_MethodName;
	static const char* const SCIMessageParameterBlock_getGenericBytes2_MethodSignature;
	jmethodID SCIMessageParameterBlock_getGenericBytes2_Method;

	// Java method: public void setGenericBytes2(byte[]);
	static const char* const SCIMessageParameterBlock_setGenericBytes2_MethodName;
	static const char* const SCIMessageParameterBlock_setGenericBytes2_MethodSignature;
	jmethodID SCIMessageParameterBlock_setGenericBytes2_Method;

	// Java method: public int getLengthGenericBytes2();
	static const char* const SCIMessageParameterBlock_getLengthGenericBytes2_MethodName;
	static const char* const SCIMessageParameterBlock_getLengthGenericBytes2_MethodSignature;
	jmethodID SCIMessageParameterBlock_getLengthGenericBytes2_Method;

};

#endif

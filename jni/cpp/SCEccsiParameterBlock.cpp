// Derived class for Eccsi parameter blocks

#include "SCEccsiParameterBlock.h"

// Java class for message block
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_ClassName = "com/securechorus/SCEccsiParameterBlock";

// Java method: public void getSecCtx(long);
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_setSecCtx_MethodName = "setSecCtx";
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_setSecCtx_MethodSignature = "(J)V";

// Java method: public long getSecCtx();
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_getSecCtx_MethodName = "getSecCtx";
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_getSecCtx_MethodSignature = "()J";

// Java method: public java.lang.String getSigningUri();
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_getSigningUri_MethodName = "getSigningUri";
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_getSigningUri_MethodSignature = "()Ljava/lang/String;";

// Java method: public void setSigningUri(java.lang.String);
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_setSigningUri_MethodName = "setSigningUri";
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_setSigningUri_MethodSignature = "(Ljava/lang/String;)V";

// Java method: public java.lang.String getMsgTimestamp();
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_getMsgTimestamp_MethodName = "getMsgTimestamp";
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_getMsgTimestamp_MethodSignature = "()Ljava/lang/String;";

// Java method: public void setMsgTimestamp(java.lang.String);
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_setMsgTimestamp_MethodName = "setMsgTimestamp";
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_setMsgTimestamp_MethodSignature = "(Ljava/lang/String;)V";

// Java method: public byte[] getDataBuffer();
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_getDataBuffer_MethodName = "getDataBuffer";
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_getDataBuffer_MethodSignature = "()[B";

// Java method: public void setDataBuffer(byte[]);
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_setDataBuffer_MethodName = "setDataBuffer";
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_setDataBuffer_MethodSignature = "([B)V";

// Java method: public int getLengthDataBuffer();
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_getLengthDataBuffer_MethodName = "getLengthDataBuffer";
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_getLengthDataBuffer_MethodSignature = "()I";

// Java method: public byte[] getSigningBuffer();
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_getSigningBuffer_MethodName = "getSigningBuffer";
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_getSigningBuffer_MethodSignature = "()[B";

// Java method: public void setSigningBuffer(byte[]);
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_setSigningBuffer_MethodName = "setSigningBuffer";
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_setSigningBuffer_MethodSignature = "([B)V";

// Java method: public int getLengthSigningBuffer();
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_getLengthSigningBuffer_MethodName = "getLengthSigningBuffer";
const char* const SCEccsiParameterBlock::SCEccsiParameterBlock_getLengthSigningBuffer_MethodSignature = "()I";

SCEccsiParameterBlock::SCEccsiParameterBlock(JNIEnv * jniEnvironment) :
SCParameterBlock(jniEnvironment),
secCtx(0),
dataBuffer(),
signingBuffer(),
signingUri(""),
msgTimestamp(""),
SCEccsiParameterBlock_Class(nullptr),
SCEccsiParameterBlock_setSecCtx_Method(nullptr),
SCEccsiParameterBlock_getSecCtx_Method(nullptr),
SCEccsiParameterBlock_getSigningUri_Method(nullptr),
SCEccsiParameterBlock_setSigningUri_Method(nullptr),
SCEccsiParameterBlock_getMsgTimestamp_Method(nullptr),
SCEccsiParameterBlock_setMsgTimestamp_Method(nullptr),
SCEccsiParameterBlock_getDataBuffer_Method(nullptr),
SCEccsiParameterBlock_setDataBuffer_Method(nullptr),
SCEccsiParameterBlock_getLengthDataBuffer_Method(nullptr),
SCEccsiParameterBlock_getSigningBuffer_Method(nullptr),
SCEccsiParameterBlock_setSigningBuffer_Method(nullptr),
SCEccsiParameterBlock_getLengthSigningBuffer_Method(nullptr)
{
	ScErrno err = configureJNI();
	if (err == SUCCESS)
		jniInitialised = true;
}

ScErrno SCEccsiParameterBlock::configureJNI(void)
{
	// initialise class
	SCEccsiParameterBlock_Class = env->FindClass(SCEccsiParameterBlock_ClassName);
	if (!SCEccsiParameterBlock_Class)
		return GENERAL_FAILURE;

	// context methods
	SCEccsiParameterBlock_setSecCtx_Method = env->GetMethodID(
		SCEccsiParameterBlock_Class,
		SCEccsiParameterBlock_setSecCtx_MethodName,
		SCEccsiParameterBlock_setSecCtx_MethodSignature);
	if (!SCEccsiParameterBlock_setSecCtx_Method)
		return GENERAL_FAILURE;

	SCEccsiParameterBlock_getSecCtx_Method = env->GetMethodID(
		SCEccsiParameterBlock_Class,
		SCEccsiParameterBlock_getSecCtx_MethodName,
		SCEccsiParameterBlock_getSecCtx_MethodSignature);
	if (!SCEccsiParameterBlock_getSecCtx_Method)
		return GENERAL_FAILURE;

	// sender URI methods
	SCEccsiParameterBlock_getSigningUri_Method = env->GetMethodID(
		SCEccsiParameterBlock_Class,
		SCEccsiParameterBlock_getSigningUri_MethodName,
		SCEccsiParameterBlock_getSigningUri_MethodSignature);
	if (!SCEccsiParameterBlock_getSigningUri_Method)
		return GENERAL_FAILURE;

	SCEccsiParameterBlock_setSigningUri_Method = env->GetMethodID(
		SCEccsiParameterBlock_Class,
		SCEccsiParameterBlock_setSigningUri_MethodName,
		SCEccsiParameterBlock_setSigningUri_MethodSignature);
	if (!SCEccsiParameterBlock_setSigningUri_Method)
		return GENERAL_FAILURE;

	// recipient URI methods
	SCEccsiParameterBlock_getMsgTimestamp_Method = env->GetMethodID(
		SCEccsiParameterBlock_Class,
		SCEccsiParameterBlock_getMsgTimestamp_MethodName,
		SCEccsiParameterBlock_getMsgTimestamp_MethodSignature);
	if (!SCEccsiParameterBlock_getMsgTimestamp_Method)
		return GENERAL_FAILURE;

	SCEccsiParameterBlock_setMsgTimestamp_Method = env->GetMethodID(
		SCEccsiParameterBlock_Class,
		SCEccsiParameterBlock_setMsgTimestamp_MethodName,
		SCEccsiParameterBlock_setMsgTimestamp_MethodSignature);
	if (!SCEccsiParameterBlock_setMsgTimestamp_Method)
		return GENERAL_FAILURE;

	// generic buffer methods
	SCEccsiParameterBlock_getDataBuffer_Method = env->GetMethodID(
		SCEccsiParameterBlock_Class,
		SCEccsiParameterBlock_getDataBuffer_MethodName,
		SCEccsiParameterBlock_getDataBuffer_MethodSignature);
	if (!SCEccsiParameterBlock_getDataBuffer_Method)
		return GENERAL_FAILURE;

	SCEccsiParameterBlock_setDataBuffer_Method = env->GetMethodID(
		SCEccsiParameterBlock_Class,
		SCEccsiParameterBlock_setDataBuffer_MethodName,
		SCEccsiParameterBlock_setDataBuffer_MethodSignature);
	if (!SCEccsiParameterBlock_setDataBuffer_Method)
		return GENERAL_FAILURE;

	SCEccsiParameterBlock_getLengthDataBuffer_Method = env->GetMethodID(
		SCEccsiParameterBlock_Class,
		SCEccsiParameterBlock_getLengthDataBuffer_MethodName,
		SCEccsiParameterBlock_getLengthDataBuffer_MethodSignature);
	if (!SCEccsiParameterBlock_getLengthDataBuffer_Method)
		return GENERAL_FAILURE;

	// signing buffer methods
	SCEccsiParameterBlock_getSigningBuffer_Method = env->GetMethodID(
		SCEccsiParameterBlock_Class,
		SCEccsiParameterBlock_getSigningBuffer_MethodName,
		SCEccsiParameterBlock_getSigningBuffer_MethodSignature);
	if (!SCEccsiParameterBlock_getSigningBuffer_Method)
		return GENERAL_FAILURE;

	SCEccsiParameterBlock_setSigningBuffer_Method = env->GetMethodID(
		SCEccsiParameterBlock_Class,
		SCEccsiParameterBlock_setSigningBuffer_MethodName,
		SCEccsiParameterBlock_setSigningBuffer_MethodSignature);
	if (!SCEccsiParameterBlock_setSigningBuffer_Method)
		return GENERAL_FAILURE;

	SCEccsiParameterBlock_getLengthSigningBuffer_Method = env->GetMethodID(
		SCEccsiParameterBlock_Class,
		SCEccsiParameterBlock_getLengthSigningBuffer_MethodName,
		SCEccsiParameterBlock_getLengthSigningBuffer_MethodSignature);
	if (!SCEccsiParameterBlock_getLengthSigningBuffer_Method)
		return GENERAL_FAILURE;

	return SUCCESS;
}

ScErrno SCEccsiParameterBlock::importFromJObject(const jobject javaParamsObject)
{
	if (!jniInitialised)
		return GENERAL_FAILURE;
	// call the method which returns a long
	jlong methodReturn = env->CallLongMethod(javaParamsObject, SCEccsiParameterBlock_getSecCtx_Method);
	// set the context to the returned long
	secCtx = (SecCtx)methodReturn;

	// get the strings
	jstring JavaSigningUri = (jstring)(env->CallObjectMethod(javaParamsObject, SCEccsiParameterBlock_getSigningUri_Method));
	signingUri = env->GetStringUTFChars(JavaSigningUri, 0);
	jstring JavaMsgTimestamp = (jstring)(env->CallObjectMethod(javaParamsObject, SCEccsiParameterBlock_getMsgTimestamp_Method));
	msgTimestamp = env->GetStringUTFChars(JavaMsgTimestamp, 0);

	// get input bytes
	ScErrno err;
	err = importBytes(javaParamsObject, SCEccsiParameterBlock_getDataBuffer_Method, dataBuffer);
	if (err != SUCCESS) return GENERAL_FAILURE;
	err = importBytes(javaParamsObject, SCEccsiParameterBlock_getSigningBuffer_Method, signingBuffer);
	if (err != SUCCESS) return GENERAL_FAILURE;

	return SUCCESS;
}

ScErrno SCEccsiParameterBlock::exportToJObject(jobject& javaParamsObject)
{
	if (!jniInitialised)
		return GENERAL_FAILURE;

	// write the secCtx into object
	jlong newCtx = secCtx;
	env->CallVoidMethod(javaParamsObject, SCEccsiParameterBlock_setSecCtx_Method, newCtx);

	// convert std::string into a new Java string object
	jstring JavaSigningUri = env->NewStringUTF((const char *)signingUri.c_str());
	jstring JavaMsgTimestamp = env->NewStringUTF((const char *)msgTimestamp.c_str());
	// set new string in object
	env->CallVoidMethod(javaParamsObject, SCEccsiParameterBlock_setSigningUri_Method, JavaSigningUri);
	env->CallVoidMethod(javaParamsObject, SCEccsiParameterBlock_setMsgTimestamp_Method, JavaMsgTimestamp);

	// export from vector to byte[] in Java object
	ScErrno err;
	err = exportBytes(javaParamsObject, SCEccsiParameterBlock_setDataBuffer_Method, dataBuffer);
	if (err != SUCCESS) return GENERAL_FAILURE;
	err = exportBytes(javaParamsObject, SCEccsiParameterBlock_setSigningBuffer_Method, signingBuffer);
	if (err != SUCCESS) return GENERAL_FAILURE;

	return SUCCESS;
}

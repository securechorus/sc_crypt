// Derived class for Eccsi parameter blocks

#ifndef SCECCSIPARAMETERBLOCK_H
#define SCECCSIPARAMETERBLOCK_H

#include <vector>
#include <string>
#include <stdint.h>
#include <jni.h>

#include "sc_context.h"
#include "sc_errno.h"
#include "SCParameterBlock.h"

class SCEccsiParameterBlock : SCParameterBlock
{
public:
	// construct (resets all parameters)
	SCEccsiParameterBlock(JNIEnv * jniEnvironment);
	// import from a jobject into the relevant fields
	ScErrno importFromJObject(const jobject javaParamsObject) override;
	// import from a jobject into the relevant fields
	ScErrno exportToJObject(jobject& javaParamsObject) override;
	// do intersting work
	ScErrno temporaryInterestingWork(void);

	// getter methods for the member variables
	SecCtx getSecCtx(void) { return secCtx; }
	std::vector<uint8_t> getDataBuffer(void) { return dataBuffer; }
	std::vector<uint8_t> getSigningBuffer(void) { return signingBuffer; }
	std::string getSigningUri(void) { return signingUri; }
	std::string getMsgTimestamp(void) { return msgTimestamp; }

	// setter methods for the member variables
	void setSecCtx(SecCtx secCtx) { this->secCtx = secCtx; }
	void setDataBuffer(std::vector<uint8_t> genericBytes) { this->dataBuffer = dataBuffer; }
	void setSigningBuffer(std::vector<uint8_t> signingBuffer) { this->dataBuffer = signingBuffer; }
	void setSigningUri(std::string signingUri) { this->signingUri = signingUri; }
	void setMsgTimestamp(std::string msgTimestamp) { this->msgTimestamp = msgTimestamp; }

protected:
	// set up the Java JNI methods (initialise class and methods)
	ScErrno configureJNI(void) override;

private:
	// directly maps to the expected jobject
	SecCtx secCtx;
	std::vector<uint8_t> dataBuffer;
	std::vector<uint8_t> signingBuffer;
	std::string signingUri;
	std::string msgTimestamp;

	// Java class for message block
	static const char* const SCEccsiParameterBlock_ClassName;
	jclass SCEccsiParameterBlock_Class;

	// Java method: public void getSecCtx(long);
	static const char* const SCEccsiParameterBlock_setSecCtx_MethodName;
	static const char* const SCEccsiParameterBlock_setSecCtx_MethodSignature;
	jmethodID SCEccsiParameterBlock_setSecCtx_Method;
	// Java method: public long getSecCtx();
	static const char* const SCEccsiParameterBlock_getSecCtx_MethodName;
	static const char* const SCEccsiParameterBlock_getSecCtx_MethodSignature;
	jmethodID SCEccsiParameterBlock_getSecCtx_Method;

	// Java method: public java.lang.String getSigningUri();
	static const char* const SCEccsiParameterBlock_getSigningUri_MethodName;
	static const char* const SCEccsiParameterBlock_getSigningUri_MethodSignature;
	jmethodID SCEccsiParameterBlock_getSigningUri_Method;
	// Java method: public void setSigningUri(java.lang.String);
	static const char* const SCEccsiParameterBlock_setSigningUri_MethodName;
	static const char* const SCEccsiParameterBlock_setSigningUri_MethodSignature;
	jmethodID SCEccsiParameterBlock_setSigningUri_Method;

	// Java method: public java.lang.String getMsgTimestamp();
	static const char* const SCEccsiParameterBlock_getMsgTimestamp_MethodName;
	static const char* const SCEccsiParameterBlock_getMsgTimestamp_MethodSignature;
	jmethodID SCEccsiParameterBlock_getMsgTimestamp_Method;

	// Java method: public void setMsgTimestamp(java.lang.String);
	static const char* const SCEccsiParameterBlock_setMsgTimestamp_MethodName;
	static const char* const SCEccsiParameterBlock_setMsgTimestamp_MethodSignature;
	jmethodID SCEccsiParameterBlock_setMsgTimestamp_Method;

	// Java method: public byte[] getDataBuffer();
	static const char* const SCEccsiParameterBlock_getDataBuffer_MethodName;
	static const char* const SCEccsiParameterBlock_getDataBuffer_MethodSignature;
	jmethodID SCEccsiParameterBlock_getDataBuffer_Method;

	// Java method: public void setDataBuffer(byte[]);
	static const char* const SCEccsiParameterBlock_setDataBuffer_MethodName;
	static const char* const SCEccsiParameterBlock_setDataBuffer_MethodSignature;
	jmethodID SCEccsiParameterBlock_setDataBuffer_Method;

  // Java method: public int getLengthDataBuffer();
	static const char* const SCEccsiParameterBlock_getLengthDataBuffer_MethodName;
	static const char* const SCEccsiParameterBlock_getLengthDataBuffer_MethodSignature;
	jmethodID SCEccsiParameterBlock_getLengthDataBuffer_Method;

	// Java method: public byte[] getSigningBuffer();
	static const char* const SCEccsiParameterBlock_getSigningBuffer_MethodName;
	static const char* const SCEccsiParameterBlock_getSigningBuffer_MethodSignature;
	jmethodID SCEccsiParameterBlock_getSigningBuffer_Method;

  // Java method: public void setSigningBuffer(byte[]);
	static const char* const SCEccsiParameterBlock_setSigningBuffer_MethodName;
	static const char* const SCEccsiParameterBlock_setSigningBuffer_MethodSignature;
	jmethodID SCEccsiParameterBlock_setSigningBuffer_Method;

	// Java method: public int getLengthSigningBuffer();
	static const char* const SCEccsiParameterBlock_getLengthSigningBuffer_MethodName;
	static const char* const SCEccsiParameterBlock_getLengthSigningBuffer_MethodSignature;
	jmethodID SCEccsiParameterBlock_getLengthSigningBuffer_Method;

};

#endif

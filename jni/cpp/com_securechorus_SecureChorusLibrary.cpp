// Defines the exported functions for the MIKEY-SAKKE wrapper

// system headers

#include <string>
#include <vector>

// wrapper headers
#include "com_securechorus_SecureChorusLibrary.h"
#include "SCIMessageParameterBlock.h"
#include "SCKMSParameterBlock.h"
#include "SCEccsiParameterBlock.h"
#include "SCWrapperTools.h"

// crypto lib headers
#include "sc_context.h"
#include "sc_errno.h"
#include "sc_libmskms.h"
#include "sc_libms.h"
#include "sc_types.h"
#include "sc_keystore.h"

// include the items in SCWrapperTools for use
using namespace SCWrapperTools;

/*
* Class:     com_securechorus_JNIWrapper
* Method:    SetFilePath
* Signature: (Ljava/lang/String;)Lcom/com.securechorus/SCErrno;
*/
JNIEXPORT jobject JNICALL Java_com_securechorus_SecureChorusLibrary_SetFilePath
        (JNIEnv * env, jclass, jstring path)
{
    static std::string filePath; // needs to persist across the lifetime of the program
    const char * buf = env->GetStringUTFChars(path, 0); // modified UTF-8
    filePath = buf;
    env->ReleaseStringUTFChars(path, buf);
    ScErrno err = SetFilePath(filePath.c_str());
    return getSCErrnoStaticObject(env, err);
}

/*
* Class:     com_securechorus_JNIWrapper
* Method:    CreateMikeySakkeIMessage
* Signature: (Lcom/securechorus/SCIMessageParameterBlock;)Lcom/securechorus/SCErrno;
*/
JNIEXPORT jobject JNICALL Java_com_securechorus_SecureChorusLibrary_CreateMikeySakkeIMessage
(JNIEnv * env, jclass, jobject SCIMessageParameterBlockJavaInstance)
{
	// create instance of parameter block helper class
	SCIMessageParameterBlock SCIMessageParameterBlockInstance(env);

	// import the jobject input/output parameters into a C++ class containing the input/output parameters
	ScErrno err;
	err = SCIMessageParameterBlockInstance.importFromJObject(SCIMessageParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// run function
	std::vector<uint8_t> iMessage;
	std::vector<uint8_t> ssv;
	err = ScLibMs::CreateMikeySakkeIMessage(
		iMessage,
		ssv,
		SCIMessageParameterBlockInstance.getSecCtx(),
		SCIMessageParameterBlockInstance.getSenderUri(),
		SCIMessageParameterBlockInstance.getRecipientUri(),
		SCIMessageParameterBlockInstance.getV());
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// copy changed params back into C++ parameters object
	SCIMessageParameterBlockInstance.setIMessage(iMessage);
	SCIMessageParameterBlockInstance.setSsv(ssv);

	// copy back parameters into the jobject to update the outputs
	err = SCIMessageParameterBlockInstance.exportToJObject(SCIMessageParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// all ok so return success
	return getSCErrnoStaticObject(env, SUCCESS);
}

/*
* Class:     com_securechorus_JNIWrapper
* Method:    CreateMikeySakkeIMessageForSRTP
* Signature: (Lcom/securechorus/SCIMessageParameterBlock;)Lcom/securechorus/SCErrno;
*/
JNIEXPORT jobject JNICALL Java_com_securechorus_SecureChorusLibrary_CreateMikeySakkeIMessageForSRTP
(JNIEnv * env, jclass, jobject SCIMessageParameterBlockJavaInstance)
{
	// create instance of parameter block helper class
	SCIMessageParameterBlock SCIMessageParameterBlockInstance(env);

	// import the jobject input/output parameters into a C++ class containing the input/output parameters
	ScErrno err;
	err = SCIMessageParameterBlockInstance.importFromJObject(SCIMessageParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// run function
	std::vector<uint8_t> iMessage;
	std::vector<uint8_t> masterKey;
	std::vector<uint8_t> masterSalt(14, 0); // 112 Bit Master Salt
	err = ScLibMs::CreateMikeySakkeIMessageForSRTP(
		iMessage,
		masterKey,
		masterSalt,
		SCIMessageParameterBlockInstance.getSecCtx(),
		SCIMessageParameterBlockInstance.getSenderUri(),
		SCIMessageParameterBlockInstance.getRecipientUri(),
		SCIMessageParameterBlockInstance.getV());
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// copy changed params back into C++ parameters object
	SCIMessageParameterBlockInstance.setIMessage(iMessage);
	SCIMessageParameterBlockInstance.setGenericBytes1(masterKey);
	SCIMessageParameterBlockInstance.setGenericBytes2(masterSalt);

	// copy back parameters into the jobject to update the outputs
	err = SCIMessageParameterBlockInstance.exportToJObject(SCIMessageParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// all ok so return success
	return getSCErrnoStaticObject(env, SUCCESS);
}

/*
* Class:     com_securechorus_JNIWrapper
* Method:    CreateMikeySakkeIMessageWithSsv
* Signature: (Lcom/securechorus/SCIMessageParameterBlock;)Lcom/securechorus/SCErrno;
*/
JNIEXPORT jobject JNICALL Java_com_securechorus_SecureChorusLibrary_CreateMikeySakkeIMessageWithSsv
(JNIEnv * env, jclass, jobject SCIMessageParameterBlockJavaInstance)
{
	// create instance of parameter block helper class
	SCIMessageParameterBlock SCIMessageParameterBlockInstance(env);

	// import the jobject input/output parameters into a C++ class containing the input/output parameters
	ScErrno err;
	err = SCIMessageParameterBlockInstance.importFromJObject(SCIMessageParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// run function
	std::vector<uint8_t> iMessage;
	err = ScLibMs::CreateMikeySakkeIMessageWithSsv(
		iMessage,
		SCIMessageParameterBlockInstance.getSecCtx(),
		SCIMessageParameterBlockInstance.getSenderUri(),
		SCIMessageParameterBlockInstance.getRecipientUri(),
		SCIMessageParameterBlockInstance.getSsv(),
		SCIMessageParameterBlockInstance.getV());
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// copy changed params back into C++ parameters object
	SCIMessageParameterBlockInstance.setIMessage(iMessage);

	// copy back parameters into the jobject to update the outputs
	err = SCIMessageParameterBlockInstance.exportToJObject(SCIMessageParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// all ok so return success
	return getSCErrnoStaticObject(env, SUCCESS);
}

/*
* Class:     com_securechorus_JNIWrapper
* Method:    ProcessMikeySakkeIMessage
* Signature: (Lcom/securechorus/SCIMessageParameterBlock;)Lcom/securechorus/SCErrno;
*/
JNIEXPORT jobject JNICALL Java_com_securechorus_SecureChorusLibrary_ProcessMikeySakkeIMessage
(JNIEnv * env, jclass, jobject SCIMessageParameterBlockJavaInstance)
{
	// create instance of parameter block helper class
	SCIMessageParameterBlock SCIMessageParameterBlockInstance(env);

	// import the jobject input/output parameters into a C++ class containing the input/output parameters
	ScErrno err;
	err = SCIMessageParameterBlockInstance.importFromJObject(SCIMessageParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// run function
	std::string recipientUri;
	std::string senderUri;
	std::vector<uint8_t> ssv;
	bool v;
	err =  ScLibMs::ProcessMikeySakkeIMessage(
		recipientUri,
		senderUri,
		ssv,
		v,
		SCIMessageParameterBlockInstance.getSecCtx(),
		SCIMessageParameterBlockInstance.getIMessage(),
		SCIMessageParameterBlockInstance.getCheckSignature());
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);
	// copy changed params back into C++ parameters object
	SCIMessageParameterBlockInstance.setRecipientUri(recipientUri);
	SCIMessageParameterBlockInstance.setSenderUri(senderUri);
	SCIMessageParameterBlockInstance.setSsv(ssv);
	SCIMessageParameterBlockInstance.setV(v);

	// copy back parameters into the jobject to update the outputs
	err = SCIMessageParameterBlockInstance.exportToJObject(SCIMessageParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// all ok so return success
	return getSCErrnoStaticObject(env, SUCCESS);
}

/*
* Class:     com_securechorus_JNIWrapper
* Method:    ProcessMikeySakkeIMessageForSRTP
* Signature: (Lcom/securechorus/SCIMessageParameterBlock;)Lcom/securechorus/SCErrno;
*/
JNIEXPORT jobject JNICALL Java_com_securechorus_SecureChorusLibrary_ProcessMikeySakkeIMessageForSRTP
(JNIEnv * env, jclass, jobject SCIMessageParameterBlockJavaInstance)
{
	// create instance of parameter block helper class
	SCIMessageParameterBlock SCIMessageParameterBlockInstance(env);

	// import the jobject input/output parameters into a C++ class containing the input/output parameters
	ScErrno err;
	err = SCIMessageParameterBlockInstance.importFromJObject(SCIMessageParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	std::string recipientUri;
	std::string senderUri;
	std::vector<uint8_t> masterKey;
	std::vector<uint8_t> masterSalt(14, 0); // 112 Bit Master Salt
	bool v;
	err =  ScLibMs::ProcessMikeySakkeIMessageForSRTP(
		recipientUri,
		senderUri,
		masterKey,
		masterSalt,
		v,
		SCIMessageParameterBlockInstance.getSecCtx(),
		SCIMessageParameterBlockInstance.getIMessage(),
		SCIMessageParameterBlockInstance.getCheckSignature());
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// copy changed params back into C++ parameters object
	SCIMessageParameterBlockInstance.setRecipientUri(recipientUri);
	SCIMessageParameterBlockInstance.setSenderUri(senderUri);
	SCIMessageParameterBlockInstance.setGenericBytes1(masterKey);
	SCIMessageParameterBlockInstance.setGenericBytes2(masterSalt);
	SCIMessageParameterBlockInstance.setV(v);

	// copy back parameters into the jobject to update the outputs
	err = SCIMessageParameterBlockInstance.exportToJObject(SCIMessageParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// all ok so return success
	return getSCErrnoStaticObject(env, SUCCESS);
}

/*
* Class:     com_securechorus_JNIWrapper
* Method:    CreateKmsSecurityContext
* Signature: (Lcom/securechorus/SCKMSParameterBlock;)Lcom/securechorus/SCErrno;
*/
JNIEXPORT jobject JNICALL Java_com_securechorus_SecureChorusLibrary_CreateKmsSecurityContext
(JNIEnv * env, jclass, jobject SCKMSParameterBlockJavaInstance)
{
	// create instance of parameter block helper class
	SCKMSParameterBlock SCKMSParameterBlockInstance(env);

	// import the jobject input/output parameters into a C++ class containing the input/output parameters
	ScErrno err;
	err = SCKMSParameterBlockInstance.importFromJObject(SCKMSParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// call function with a new secctx (passed in by ref) and elements from parameter input block
	SecCtx secCtx = 0;
	err = ScLibMsKMS::CreateKmsSecurityContext(secCtx,
		SCKMSParameterBlockInstance.getKmsUri(),
		SCKMSParameterBlockInstance.getTransportKeyId(),
		SCKMSParameterBlockInstance.getGenericBuffer());
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// copy changed params back into C++ parameters object
	SCKMSParameterBlockInstance.setSecCtx(secCtx);

	// copy back parameters into the jobject to update the outputs
	err = SCKMSParameterBlockInstance.exportToJObject(SCKMSParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// all ok so return success
	return getSCErrnoStaticObject(env, SUCCESS);

}


/*
* Class:     com_securechorus_JNIWrapper
* Method:    RequestKmsInitXML
* Signature: (Lcom/securechorus/SCKMSParameterBlock;)Lcom/securechorus/SCErrno;
*/
JNIEXPORT jobject JNICALL Java_com_securechorus_SecureChorusLibrary_RequestKmsInitXML
(JNIEnv * env, jclass, jobject SCKMSParameterBlockJavaInstance)
{
	// create instance of parameter block helper class
	SCKMSParameterBlock SCKMSParameterBlockInstance(env);

	// import the jobject input/output parameters into a C++ class containing the input/output parameters
	ScErrno err;
	err = SCKMSParameterBlockInstance.importFromJObject(SCKMSParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// call function with a new secctx (passed in by ref) and elements from parameter input block
	std::vector<unsigned char> KmsInitReq;
	err = ScLibMsKMS::RequestKmsInitXML(
		SCKMSParameterBlockInstance.getSecCtx(),
		SCKMSParameterBlockInstance.getKmsUrl(),
		SCKMSParameterBlockInstance.getUserUri(),
		KmsInitReq);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// copy changed params back into C++ parameters object
	SCKMSParameterBlockInstance.setGenericBuffer(KmsInitReq);

	// copy back parameters into the jobject to update the outputs
	err = SCKMSParameterBlockInstance.exportToJObject(SCKMSParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// all ok so return success
	return getSCErrnoStaticObject(env, SUCCESS);
}

/*
* Class:     com_securechorus_JNIWrapper
* Method:    ProcessKmsRespXML
* Signature: (Lcom/securechorus/SCKMSParameterBlock;)Lcom/securechorus/SCErrno;
*/
JNIEXPORT jobject JNICALL Java_com_securechorus_SecureChorusLibrary_ProcessKmsRespXML
(JNIEnv * env, jclass, jobject SCKMSParameterBlockJavaInstance)
{
	// create instance of parameter block helper class
	SCKMSParameterBlock SCKMSParameterBlockInstance(env);

	// import the jobject input/output parameters into a C++ class containing the input/output parameters
	ScErrno err;
	err = SCKMSParameterBlockInstance.importFromJObject(SCKMSParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// call function with a new secctx (passed in by ref) and elements from parameter input block
	err = ScLibMsKMS::ProcessKmsRespXML(
		SCKMSParameterBlockInstance.getSecCtx(),
		SCKMSParameterBlockInstance.getGenericBuffer(),
		SCKMSParameterBlockInstance.getSchemaPath());

	// return the status of the called function (no param updated to copy back out to caller)
	return getSCErrnoStaticObject(env, err);
}

/*
* Class:     com_securechorus_JNIWrapper
* Method:    RequestKmsKeyProvXML
* Signature: (Lcom/securechorus/SCKMSParameterBlock;)Lcom/securechorus/SCErrno;
*/
JNIEXPORT jobject JNICALL Java_com_securechorus_SecureChorusLibrary_RequestKmsKeyProvXML
(JNIEnv * env, jclass, jobject SCKMSParameterBlockJavaInstance)
{
	// create instance of parameter block helper class
	SCKMSParameterBlock SCKMSParameterBlockInstance(env);

	// import the jobject input/output parameters into a C++ class containing the input/output parameters
	ScErrno err;
	err = SCKMSParameterBlockInstance.importFromJObject(SCKMSParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// call function with a new secctx (passed in by ref) and elements from parameter input block
	std::vector<unsigned char> KmsKeyProvReq;
	err = ScLibMsKMS::RequestKmsInitXML(
		SCKMSParameterBlockInstance.getSecCtx(),
		SCKMSParameterBlockInstance.getKmsUrl(),
		SCKMSParameterBlockInstance.getUserUri(),
		KmsKeyProvReq);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// copy changed params back into C++ parameters object
	SCKMSParameterBlockInstance.setGenericBuffer(KmsKeyProvReq);

	// copy back parameters into the jobject to update the outputs
	err = SCKMSParameterBlockInstance.exportToJObject(SCKMSParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// all ok so return success
	return getSCErrnoStaticObject(env, SUCCESS);
}

/*
* Class:     com_securechorus_JNIWrapper
* Method:    RequestKmsCertCacheXML
* Signature: (Lcom/securechorus/SCKMSParameterBlock;)Lcom/securechorus/SCErrno;
*/
JNIEXPORT jobject JNICALL Java_com_securechorus_SecureChorusLibrary_RequestKmsCertCacheXML
(JNIEnv * env, jclass, jobject SCKMSParameterBlockJavaInstance)
{
	// create instance of parameter block helper class
	SCKMSParameterBlock SCKMSParameterBlockInstance(env);

	// import the jobject input/output parameters into a C++ class containing the input/output parameters
	ScErrno err;
	err = SCKMSParameterBlockInstance.importFromJObject(SCKMSParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// call function with a new secctx (passed in by ref) and elements from parameter input block
	std::vector<unsigned char> KmsCertCacheReq;
	err = ScLibMsKMS::RequestKmsCertCacheXML(
		SCKMSParameterBlockInstance.getSecCtx(),
		SCKMSParameterBlockInstance.getKmsUrl(),
		SCKMSParameterBlockInstance.getUserUri(),
		KmsCertCacheReq);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// copy changed params back into C++ parameters object
	SCKMSParameterBlockInstance.setGenericBuffer(KmsCertCacheReq);

	// copy back parameters into the jobject to update the outputs
	err = SCKMSParameterBlockInstance.exportToJObject(SCKMSParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// all ok so return success
	return getSCErrnoStaticObject(env, SUCCESS);
}



/*
* Class:     com_securechorus_JNIWrapper
* Method:    CreateMikeyEccsiSig
* Signature: (Lcom/securechorus/SCEccsiParameterBlock;)Lcom/securechorus/SCErrno;
*/
JNIEXPORT jobject JNICALL Java_com_securechorus_SecureChorusLibrary_CreateMikeyEccsiSig
(JNIEnv * env, jclass, jobject SCEccsiParameterBlockJavaInstance)
{
	// create instance of parameter block helper class
	SCEccsiParameterBlock SCEccsiParameterBlockInstance(env);

	// import the jobject input/output parameters into a C++ class containing the input/output parameters
	ScErrno err;
	err = SCEccsiParameterBlockInstance.importFromJObject(SCEccsiParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// run function
	std::vector<uint8_t> mikeyEccsi;
	err = ScLibMs::CreateMikeyEccsiSig(
		mikeyEccsi,
		SCEccsiParameterBlockInstance.getSecCtx(),
		SCEccsiParameterBlockInstance.getSigningUri(),
		SCEccsiParameterBlockInstance.getMsgTimestamp(),
		SCEccsiParameterBlockInstance.getDataBuffer());
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);
	// copy changed params back into C++ parameters object
	SCEccsiParameterBlockInstance.setSigningBuffer(mikeyEccsi);

	// copy back parameters into the jobject to update the outputs
	err = SCEccsiParameterBlockInstance.exportToJObject(SCEccsiParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// all ok so return success
	return getSCErrnoStaticObject(env, SUCCESS);
}


/*
* Class:     com_securechorus_JNIWrapper
* Method:    VerifyMikeyEccsiSig
* Signature: (Lcom/securechorus/SCEccsiParameterBlock;)Lcom/securechorus/SCErrno;
*/
JNIEXPORT jobject JNICALL Java_com_securechorus_SecureChorusLibrary_VerifyMikeyEccsiSig
(JNIEnv * env, jclass, jobject SCEccsiParameterBlockJavaInstance)
{
	// create instance of parameter block helper class
	SCEccsiParameterBlock SCEccsiParameterBlockInstance(env);

	// import the jobject input/output parameters into a C++ class containing the input/output parameters
	ScErrno err;
	err = SCEccsiParameterBlockInstance.importFromJObject(SCEccsiParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// run function
	err = ScLibMs::VerifyMikeyEccsiSig(
		SCEccsiParameterBlockInstance.getSecCtx(),
		SCEccsiParameterBlockInstance.getSigningUri(),
		SCEccsiParameterBlockInstance.getMsgTimestamp(),
		SCEccsiParameterBlockInstance.getDataBuffer(),
    SCEccsiParameterBlockInstance.getSigningBuffer());
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// copy back parameters into the jobject to update the outputs
	err = SCEccsiParameterBlockInstance.exportToJObject(SCEccsiParameterBlockJavaInstance);
	if (err != SUCCESS)
		return getSCErrnoStaticObject(env, err);

	// all ok so return success
	return getSCErrnoStaticObject(env, SUCCESS);
}




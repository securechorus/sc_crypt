// Base class for parameter blocks

#include "SCParameterBlock.h"

SCParameterBlock::SCParameterBlock(JNIEnv * jniEnvironment) :
env(jniEnvironment),
jniInitialised(false)
{
}

ScErrno SCParameterBlock::importBytes(const jobject javaParamsObject, jmethodID methodID, std::vector<uint8_t>& outputVec)
{
	// call the method which returns an object
	jobject byteArrayBytes = env->CallObjectMethod(javaParamsObject, methodID);
	if (!byteArrayBytes) return GENERAL_FAILURE;

	// the object is in fact a Java byte array so cast this to a jbytearray
	jbyteArray* arr = reinterpret_cast<jbyteArray*>(&byteArrayBytes);
	if (!arr) return GENERAL_FAILURE;

	// get the bytes in a pointer and also the length (which the object is aware of the size of)
	int byteLength = env->GetArrayLength(*arr);

	// clear the vector and resize to required size
	outputVec.clear();
	outputVec.resize(byteLength);

	// process if data present
	if (byteLength > 0)
	{
		jbyte* data = env->GetByteArrayElements(*arr, NULL);
		if (!data) return GENERAL_FAILURE;

		// cast the jbytes to an unsigned char
		uint8_t* data_uint8 = reinterpret_cast<uint8_t*>(data);

		// copy the data into the vector (we know a vector is guaranteed to be contiguous data so memcpy is safe)
		memcpy(&outputVec[0], data_uint8, byteLength * sizeof(uint8_t));

		// release the Java allocated memory as it is now in a C++ vector
		env->ReleaseByteArrayElements(*arr, data, 0);
	}

	return SUCCESS;

}
ScErrno SCParameterBlock::exportBytes(jobject javaParamsObject, jmethodID methodID, const std::vector<uint8_t> sourceVec)
{
	if (sourceVec.size() > 0)
	{
		// ByteArray requires data to be provided as a byte[] object, allocate and fill
		jbyteArray javaByteArrayMemory = env->NewByteArray(sourceVec.size());
		if (!javaByteArrayMemory) return GENERAL_FAILURE;
		env->SetByteArrayRegion(javaByteArrayMemory, 0, sourceVec.size(), (const jbyte*)&sourceVec[0]);

		// provide the data to the byte array
		env->CallVoidMethod(javaParamsObject, methodID, javaByteArrayMemory);
	}
	return SUCCESS;
}
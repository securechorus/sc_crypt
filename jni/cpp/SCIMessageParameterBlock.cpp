// Derived class for IMessage parameter blocks

#include "SCIMessageParameterBlock.h"

// Java class for message block
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_ClassName = "com/securechorus/SCIMessageParameterBlock";

// Java method: public void getSecCtx(long);
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_setSecCtx_MethodName = "setSecCtx";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_setSecCtx_MethodSignature = "(J)V";

// Java method: public long getSecCtx();
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getSecCtx_MethodName = "getSecCtx";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getSecCtx_MethodSignature = "()J";

// Java method: public java.lang.String getSenderUri();
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getSenderUri_MethodName = "getSenderUri";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getSenderUri_MethodSignature = "()Ljava/lang/String;";

// Java method: public void setSenderUri(java.lang.String);
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_setSenderUri_MethodName = "setSenderUri";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_setSenderUri_MethodSignature = "(Ljava/lang/String;)V";

// Java method: public java.lang.String getRecipientUri();
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getRecipientUri_MethodName = "getRecipientUri";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getRecipientUri_MethodSignature = "()Ljava/lang/String;";

// Java method: public void setRecipientUri(java.lang.String);
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_setRecipientUri_MethodName = "setRecipientUri";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_setRecipientUri_MethodSignature = "(Ljava/lang/String;)V";

// Java method: public byte[] getIMessage();
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getIMessage_MethodName = "getIMessage";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getIMessage_MethodSignature = "()[B";

// Java method: public void setIMessage(byte[]);
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_setIMessage_MethodName = "setIMessage";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_setIMessage_MethodSignature = "([B)V";

// Java method: public int getLengthIMessage();
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getLengthIMessage_MethodName = "getLengthIMessage";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getLengthIMessage_MethodSignature = "()I";

// Java method: public byte[] getSsv();
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getSsv_MethodName = "getSsv";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getSsv_MethodSignature = "()[B";

// Java method: public void setSsv(byte[]);
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_setSsv_MethodName = "setSsv";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_setSsv_MethodSignature = "([B)V";

// Java method: public int getLengthSsv();
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getLengthSsv_MethodName = "getLengthSsv";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getLengthSsv_MethodSignature = "()I";

// Java method: public byte[] getGenericBytes1();
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getGenericBytes1_MethodName = "getGenericBytes1";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getGenericBytes1_MethodSignature = "()[B";

// Java method: public void setGenericBytes1(byte[]);
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_setGenericBytes1_MethodName = "setGenericBytes1";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_setGenericBytes1_MethodSignature = "([B)V";

// Java method: public int getLengthGenericBytes1();
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getLengthGenericBytes1_MethodName = "getLengthGenericBytes1";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getLengthGenericBytes1_MethodSignature = "()I";

// Java method: public byte[] getGenericBytes1();
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getGenericBytes2_MethodName = "getGenericBytes2";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getGenericBytes2_MethodSignature = "()[B";

// Java method: public void setGenericBytes1(byte[]);
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_setGenericBytes2_MethodName = "setGenericBytes2";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_setGenericBytes2_MethodSignature = "([B)V";

// Java method: public int getLengthGenericBytes1();
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getLengthGenericBytes2_MethodName = "getLengthGenericBytes2";
const char* const SCIMessageParameterBlock::SCIMessageParameterBlock_getLengthGenericBytes2_MethodSignature = "()I";

SCIMessageParameterBlock::SCIMessageParameterBlock(JNIEnv * jniEnvironment) :
SCParameterBlock(jniEnvironment),
secCtx(0),
iMessage(),
ssv(),
senderUri(""),
recipientUri(""),
v(false),
checkSignature(false),
SCIMessageParameterBlock_Class(nullptr),
SCIMessageParameterBlock_setSecCtx_Method(nullptr),
SCIMessageParameterBlock_getSecCtx_Method(nullptr),
SCIMessageParameterBlock_getSenderUri_Method(nullptr),
SCIMessageParameterBlock_setSenderUri_Method(nullptr),
SCIMessageParameterBlock_getRecipientUri_Method(nullptr),
SCIMessageParameterBlock_setRecipientUri_Method(nullptr),
SCIMessageParameterBlock_getIMessage_Method(nullptr),
SCIMessageParameterBlock_setIMessage_Method(nullptr),
SCIMessageParameterBlock_getLengthIMessage_Method(nullptr),
SCIMessageParameterBlock_getSsv_Method(nullptr),
SCIMessageParameterBlock_setSsv_Method(nullptr),
SCIMessageParameterBlock_getLengthSsv_Method(nullptr),
SCIMessageParameterBlock_getGenericBytes1_Method(nullptr),
SCIMessageParameterBlock_setGenericBytes1_Method(nullptr),
SCIMessageParameterBlock_getLengthGenericBytes1_Method(nullptr),
SCIMessageParameterBlock_getGenericBytes2_Method(nullptr),
SCIMessageParameterBlock_setGenericBytes2_Method(nullptr),
SCIMessageParameterBlock_getLengthGenericBytes2_Method(nullptr)
{
	ScErrno err = configureJNI();
	if (err == SUCCESS)
		jniInitialised = true;
}

ScErrno SCIMessageParameterBlock::configureJNI(void)
{
	// initialise class
	SCIMessageParameterBlock_Class = env->FindClass(SCIMessageParameterBlock_ClassName);
	if (!SCIMessageParameterBlock_Class)
		return GENERAL_FAILURE;

	// context methods
	SCIMessageParameterBlock_setSecCtx_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_setSecCtx_MethodName,
		SCIMessageParameterBlock_setSecCtx_MethodSignature);
	if (!SCIMessageParameterBlock_setSecCtx_Method)
		return GENERAL_FAILURE;

	SCIMessageParameterBlock_getSecCtx_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_getSecCtx_MethodName,
		SCIMessageParameterBlock_getSecCtx_MethodSignature);
	if (!SCIMessageParameterBlock_getSecCtx_Method)
		return GENERAL_FAILURE;

	// sender URI methods
	SCIMessageParameterBlock_getSenderUri_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_getSenderUri_MethodName,
		SCIMessageParameterBlock_getSenderUri_MethodSignature);
	if (!SCIMessageParameterBlock_getSenderUri_Method)
		return GENERAL_FAILURE;

	SCIMessageParameterBlock_setSenderUri_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_setSenderUri_MethodName,
		SCIMessageParameterBlock_setSenderUri_MethodSignature);
	if (!SCIMessageParameterBlock_setSenderUri_Method)
		return GENERAL_FAILURE;

	// recipient URI methods
	SCIMessageParameterBlock_getRecipientUri_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_getRecipientUri_MethodName,
		SCIMessageParameterBlock_getRecipientUri_MethodSignature);
	if (!SCIMessageParameterBlock_getRecipientUri_Method)
		return GENERAL_FAILURE;

	SCIMessageParameterBlock_setRecipientUri_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_setRecipientUri_MethodName,
		SCIMessageParameterBlock_setRecipientUri_MethodSignature);
	if (!SCIMessageParameterBlock_setRecipientUri_Method)
		return GENERAL_FAILURE;

	// IMessage methods
	SCIMessageParameterBlock_getIMessage_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_getIMessage_MethodName,
		SCIMessageParameterBlock_getIMessage_MethodSignature);
	if (!SCIMessageParameterBlock_getIMessage_Method)
		return GENERAL_FAILURE;

	SCIMessageParameterBlock_setIMessage_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_setIMessage_MethodName,
		SCIMessageParameterBlock_setIMessage_MethodSignature);
	if (!SCIMessageParameterBlock_setIMessage_Method)
		return GENERAL_FAILURE;

	SCIMessageParameterBlock_getLengthIMessage_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_getLengthIMessage_MethodName,
		SCIMessageParameterBlock_getLengthIMessage_MethodSignature);
	if (!SCIMessageParameterBlock_getLengthIMessage_Method)
		return GENERAL_FAILURE;

	// SSV methods
	SCIMessageParameterBlock_getSsv_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_getSsv_MethodName,
		SCIMessageParameterBlock_getSsv_MethodSignature);
	if (!SCIMessageParameterBlock_getSsv_Method)
		return GENERAL_FAILURE;

	SCIMessageParameterBlock_setSsv_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_setSsv_MethodName,
		SCIMessageParameterBlock_setSsv_MethodSignature);
	if (!SCIMessageParameterBlock_setSsv_Method)
		return GENERAL_FAILURE;

	SCIMessageParameterBlock_getLengthSsv_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_getLengthSsv_MethodName,
		SCIMessageParameterBlock_getLengthSsv_MethodSignature);
	if (!SCIMessageParameterBlock_getLengthSsv_Method)
		return GENERAL_FAILURE;

	// Generic data buffer used for SRTP Master Key
	SCIMessageParameterBlock_getGenericBytes1_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_getGenericBytes1_MethodName,
		SCIMessageParameterBlock_getGenericBytes1_MethodSignature);
	if (!SCIMessageParameterBlock_getGenericBytes1_Method)
		return GENERAL_FAILURE;

	SCIMessageParameterBlock_setGenericBytes1_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_setGenericBytes1_MethodName,
		SCIMessageParameterBlock_setGenericBytes1_MethodSignature);
	if (!SCIMessageParameterBlock_setGenericBytes1_Method)
		return GENERAL_FAILURE;

	SCIMessageParameterBlock_getLengthGenericBytes1_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_getLengthGenericBytes1_MethodName,
		SCIMessageParameterBlock_getLengthGenericBytes1_MethodSignature);
	if (!SCIMessageParameterBlock_getLengthGenericBytes1_Method)
		return GENERAL_FAILURE;

	// Generic data buffer used for SRTP Master Salt
	SCIMessageParameterBlock_getGenericBytes2_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_getGenericBytes2_MethodName,
		SCIMessageParameterBlock_getGenericBytes2_MethodSignature);
	if (!SCIMessageParameterBlock_getGenericBytes2_Method)
		return GENERAL_FAILURE;

	SCIMessageParameterBlock_setGenericBytes2_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_setGenericBytes2_MethodName,
		SCIMessageParameterBlock_setGenericBytes2_MethodSignature);
	if (!SCIMessageParameterBlock_setGenericBytes2_Method)
		return GENERAL_FAILURE;

	SCIMessageParameterBlock_getLengthGenericBytes2_Method = env->GetMethodID(
		SCIMessageParameterBlock_Class,
		SCIMessageParameterBlock_getLengthGenericBytes2_MethodName,
		SCIMessageParameterBlock_getLengthGenericBytes2_MethodSignature);
	if (!SCIMessageParameterBlock_getLengthGenericBytes2_Method)
		return GENERAL_FAILURE;

	return SUCCESS;
}

ScErrno SCIMessageParameterBlock::importFromJObject(const jobject javaParamsObject)
{
	if (!jniInitialised)
		return GENERAL_FAILURE;
	// call the method which returns a long
	jlong methodReturn = env->CallLongMethod(javaParamsObject, SCIMessageParameterBlock_getSecCtx_Method);
	// set the context to the returned long
	secCtx = (SecCtx)methodReturn;

	// get the strings
	jstring JavaSenderUri = (jstring)(env->CallObjectMethod(javaParamsObject, SCIMessageParameterBlock_getSenderUri_Method));
	senderUri = env->GetStringUTFChars(JavaSenderUri, 0);
	jstring JavaRecipientUri = (jstring)(env->CallObjectMethod(javaParamsObject, SCIMessageParameterBlock_getRecipientUri_Method));
	recipientUri = env->GetStringUTFChars(JavaRecipientUri, 0);

	// get input bytes
	ScErrno err;
	err = importBytes(javaParamsObject, SCIMessageParameterBlock_getIMessage_Method, iMessage);
	if (err != SUCCESS) return GENERAL_FAILURE;
	err = importBytes(javaParamsObject, SCIMessageParameterBlock_getSsv_Method, ssv);
	if (err != SUCCESS) return GENERAL_FAILURE;
	err = importBytes(javaParamsObject, SCIMessageParameterBlock_getGenericBytes1_Method, genericBytes1);
	if (err != SUCCESS) return GENERAL_FAILURE;
	err = importBytes(javaParamsObject, SCIMessageParameterBlock_getGenericBytes2_Method, genericBytes2);
	if (err != SUCCESS) return GENERAL_FAILURE;

	return SUCCESS;
}

ScErrno SCIMessageParameterBlock::exportToJObject(jobject& javaParamsObject)
{
	if (!jniInitialised)
		return GENERAL_FAILURE;

	// write the secCtx into object
	jlong newCtx = secCtx;
	env->CallVoidMethod(javaParamsObject, SCIMessageParameterBlock_setSecCtx_Method, newCtx);

	// convert std::string into a new Java string object
	jstring JavaSenderUri = env->NewStringUTF((const char *)senderUri.c_str());
	jstring JavaRecipientUri = env->NewStringUTF((const char *)recipientUri.c_str());
	// set new string in object
	env->CallVoidMethod(javaParamsObject, SCIMessageParameterBlock_setSenderUri_Method, JavaSenderUri);
	env->CallVoidMethod(javaParamsObject, SCIMessageParameterBlock_setRecipientUri_Method, JavaRecipientUri);

	// export from vector to byte[] in Java object
	ScErrno err;
	err = exportBytes(javaParamsObject, SCIMessageParameterBlock_setIMessage_Method, iMessage);
	if (err != SUCCESS) return GENERAL_FAILURE;
	err = exportBytes(javaParamsObject, SCIMessageParameterBlock_setSsv_Method, ssv);
	if (err != SUCCESS) return GENERAL_FAILURE;
	err = exportBytes(javaParamsObject, SCIMessageParameterBlock_setGenericBytes1_Method, genericBytes1);
	if (err != SUCCESS) return GENERAL_FAILURE;
	err = exportBytes(javaParamsObject, SCIMessageParameterBlock_setGenericBytes2_Method, genericBytes2);
	if (err != SUCCESS) return GENERAL_FAILURE;

	return SUCCESS;
}

// A namespace for tools that are useful during MIKEY-SAKKE wrapping

#ifndef SCWRAPPERTOOLS_H
#define SCWRAPPERTOOLS_H

#include <vector>
#include <string>
#include <stdint.h>
#include <jni.h>

#include "sc_context.h"
#include "sc_errno.h"

namespace SCWrapperTools
{
	jobject getSCErrnoStaticObject(JNIEnv * env, ScErrno result);
};

#endif

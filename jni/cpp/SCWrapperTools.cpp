// A namespace for tools that are useful during MIKEY-SAKKE wrapping

#include "SCWrapperTools.h"

namespace SCWrapperTools
{
	// returns a Java SCErrno object when provided with the Java JNI env and a C library ScErrno value for encoding
	jobject getSCErrnoStaticObject(JNIEnv * env, ScErrno result)
	{
		// mutable context object
		const char* SCErrno_ClassName = "com/securechorus/SCErrno";
		const char* SCErrno_getError_MethodName = "getError";
		const char* SCErrno_getError_MethodSignature = "(I)Lcom/securechorus/SCErrno;";

		// initialise the SCErrno class
		jclass SCErrno_Class = 0;
		SCErrno_Class = env->FindClass(SCErrno_ClassName);
		if (!SCErrno_Class)
			return 0;
		// initialise the method we need to use
		jmethodID SCErrno_getError_Method = 0;
		SCErrno_getError_Method = env->GetStaticMethodID(SCErrno_Class, SCErrno_getError_MethodName, SCErrno_getError_MethodSignature);
		if (!SCErrno_getError_Method)
			return 0;

		// create the new object with an int
		jint returnValueInt = result;

		// create the oject which will be a Java SCErrno
		jobject returnObject = env->CallStaticObjectMethod(SCErrno_Class, SCErrno_getError_Method, returnValueInt);

		return returnObject;
	}
};
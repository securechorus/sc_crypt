// Derived class for KMS parameter blocks

#include "SCKMSParameterBlock.h"

const char* const SCKMSParameterBlock::SCKMSParameterBlock_ClassName = "com/securechorus/SCKMSParameterBlock";

// Java method: public void getSecCtx(long);
const char* const SCKMSParameterBlock::SCKMSParameterBlock_setSecCtx_MethodName = "setSecCtx";
const char* const SCKMSParameterBlock::SCKMSParameterBlock_setSecCtx_MethodSignature = "(J)V";

// Java method: public long getSecCtx();
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getSecCtx_MethodName = "getSecCtx";
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getSecCtx_MethodSignature = "()J";

// Java method: public java.lang.String getUserUri();
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getUserUri_MethodName = "getUserUri";
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getUserUri_MethodSignature = "()Ljava/lang/String;";

// Java method: public void setUserUri(java.lang.String);
const char* const SCKMSParameterBlock::SCKMSParameterBlock_setUserUri_MethodName = "setUserUri";
const char* const SCKMSParameterBlock::SCKMSParameterBlock_setUserUri_MethodSignature = "(Ljava/lang/String;)V";

// Java method: public java.lang.String getKmsUrl();
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getKmsUrl_MethodName = "getKmsUrl";
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getKmsUrl_MethodSignature = "()Ljava/lang/String;";

// Java method: public void setKmsUrl(java.lang.String);
const char* const SCKMSParameterBlock::SCKMSParameterBlock_setKmsUrl_MethodName = "setKmsUrl";
const char* const SCKMSParameterBlock::SCKMSParameterBlock_setKmsUrl_MethodSignature = "(Ljava/lang/String;)V";

// Java method: public java.lang.String getKmsUri();
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getKmsUri_MethodName = "getKmsUri";
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getKmsUri_MethodSignature = "()Ljava/lang/String;";

// Java method: public void setKmsUrl(java.lang.String);
const char* const SCKMSParameterBlock::SCKMSParameterBlock_setKmsUri_MethodName = "setKmsUri";
const char* const SCKMSParameterBlock::SCKMSParameterBlock_setKmsUri_MethodSignature = "(Ljava/lang/String;)V";

// Java method: public java.lang.String getTransportKeyId();
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getTransportKeyId_MethodName = "getTransportKeyId";
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getTransportKeyId_MethodSignature = "()Ljava/lang/String;";

// Java method: public void setKmsUrl(java.lang.String);
const char* const SCKMSParameterBlock::SCKMSParameterBlock_setTransportKeyId_MethodName = "setTransportKeyId";
const char* const SCKMSParameterBlock::SCKMSParameterBlock_setTransportKeyId_MethodSignature = "(Ljava/lang/String;)V";

// Java method: public byte[] getGenericBytes();
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getGenericBytes_MethodName = "getGenericBytes";
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getGenericBytes_MethodSignature = "()[B";

// Java method: public void setGenericBytes(byte[]);
const char* const SCKMSParameterBlock::SCKMSParameterBlock_setGenericBytes_MethodName = "setGenericBytes";
const char* const SCKMSParameterBlock::SCKMSParameterBlock_setGenericBytes_MethodSignature = "([B)V";

// Java method: public int getLengthGenericBytes();
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getLengthGenericBytes_MethodName = "getLengthGenericBytes";
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getLengthGenericBytes_MethodSignature = "()I";

// Java method: public void setSchemaPath(java.lang.String);
const char* const SCKMSParameterBlock::SCKMSParameterBlock_setSchemaPath_MethodName = "setSchemaPath";
const char* const SCKMSParameterBlock::SCKMSParameterBlock_setSchemaPath_MethodSignature = "(Ljava/lang/String;)V";

// Java method: public java.lang.String getSchemaPath();
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getSchemaPath_MethodName = "getSchemaPath";
const char* const SCKMSParameterBlock::SCKMSParameterBlock_getSchemaPath_MethodSignature = "()Ljava/lang/String;";

SCKMSParameterBlock::SCKMSParameterBlock(JNIEnv * jniEnvironment) :
SCParameterBlock(jniEnvironment),
secCtx(0),
genericBytes(),
userUri(""),
kmsUrl(""),
kmsUri(""),
schemaPath(""),
SCKMSParameterBlock_Class(nullptr),
SCKMSParameterBlock_setSecCtx_Method(nullptr),
SCKMSParameterBlock_getSecCtx_Method(nullptr),
SCKMSParameterBlock_getUserUri_Method(nullptr),
SCKMSParameterBlock_setUserUri_Method(nullptr),
SCKMSParameterBlock_getKmsUrl_Method(nullptr),
SCKMSParameterBlock_setKmsUrl_Method(nullptr),
SCKMSParameterBlock_getKmsUri_Method(nullptr),
SCKMSParameterBlock_setKmsUri_Method(nullptr),
SCKMSParameterBlock_getTransportKeyId_Method(nullptr),
SCKMSParameterBlock_setTransportKeyId_Method(nullptr),
SCKMSParameterBlock_getGenericBytes_Method(nullptr),
SCKMSParameterBlock_setGenericBytes_Method(nullptr),
SCKMSParameterBlock_getLengthGenericBytes_Method(nullptr),
SCKMSParameterBlock_setSchemaPath_Method(nullptr),
SCKMSParameterBlock_getSchemaPath_Method(nullptr)
{
	ScErrno err = configureJNI();
	if (err == SUCCESS)
		jniInitialised = true;
}

ScErrno SCKMSParameterBlock::configureJNI(void)
{
	// initialise class
	SCKMSParameterBlock_Class = env->FindClass(SCKMSParameterBlock_ClassName);
	if (!SCKMSParameterBlock_Class)
		return GENERAL_FAILURE;

	// context methods
	SCKMSParameterBlock_setSecCtx_Method = env->GetMethodID(
		SCKMSParameterBlock_Class,
		SCKMSParameterBlock_setSecCtx_MethodName,
		SCKMSParameterBlock_setSecCtx_MethodSignature);
	if (!SCKMSParameterBlock_setSecCtx_Method)
		return GENERAL_FAILURE;

	SCKMSParameterBlock_getSecCtx_Method = env->GetMethodID(
		SCKMSParameterBlock_Class,
		SCKMSParameterBlock_getSecCtx_MethodName,
		SCKMSParameterBlock_getSecCtx_MethodSignature);
	if (!SCKMSParameterBlock_getSecCtx_Method)
		return GENERAL_FAILURE;

	// sender URI methods
	SCKMSParameterBlock_getUserUri_Method = env->GetMethodID(
		SCKMSParameterBlock_Class,
		SCKMSParameterBlock_getUserUri_MethodName,
		SCKMSParameterBlock_getUserUri_MethodSignature);
	if (!SCKMSParameterBlock_getUserUri_Method)
		return GENERAL_FAILURE;

	SCKMSParameterBlock_setUserUri_Method = env->GetMethodID(
		SCKMSParameterBlock_Class,
		SCKMSParameterBlock_setUserUri_MethodName,
		SCKMSParameterBlock_setUserUri_MethodSignature);
	if (!SCKMSParameterBlock_setUserUri_Method)
		return GENERAL_FAILURE;

	// recipient URI methods
	SCKMSParameterBlock_getKmsUrl_Method = env->GetMethodID(
		SCKMSParameterBlock_Class,
		SCKMSParameterBlock_getKmsUrl_MethodName,
		SCKMSParameterBlock_getKmsUrl_MethodSignature);
	if (!SCKMSParameterBlock_getKmsUrl_Method)
		return GENERAL_FAILURE;

	SCKMSParameterBlock_setKmsUrl_Method = env->GetMethodID(
		SCKMSParameterBlock_Class,
		SCKMSParameterBlock_setKmsUrl_MethodName,
		SCKMSParameterBlock_setKmsUrl_MethodSignature);
	if (!SCKMSParameterBlock_setKmsUrl_Method)
		return GENERAL_FAILURE;

	// Kms URI methods
	SCKMSParameterBlock_getKmsUri_Method = env->GetMethodID(
		SCKMSParameterBlock_Class,
		SCKMSParameterBlock_getKmsUri_MethodName,
		SCKMSParameterBlock_getKmsUri_MethodSignature);
	if (!SCKMSParameterBlock_getKmsUri_Method)
		return GENERAL_FAILURE;

	SCKMSParameterBlock_setKmsUri_Method = env->GetMethodID(
		SCKMSParameterBlock_Class,
		SCKMSParameterBlock_setKmsUri_MethodName,
		SCKMSParameterBlock_setKmsUri_MethodSignature);
	if (!SCKMSParameterBlock_setKmsUri_Method)
		return GENERAL_FAILURE;

	// Transport key id methods
	SCKMSParameterBlock_getTransportKeyId_Method = env->GetMethodID(
		SCKMSParameterBlock_Class,
		SCKMSParameterBlock_getTransportKeyId_MethodName,
		SCKMSParameterBlock_getTransportKeyId_MethodSignature);
	if (!SCKMSParameterBlock_getTransportKeyId_Method)
		return GENERAL_FAILURE;

	SCKMSParameterBlock_setTransportKeyId_Method = env->GetMethodID(
		SCKMSParameterBlock_Class,
		SCKMSParameterBlock_setTransportKeyId_MethodName,
		SCKMSParameterBlock_setTransportKeyId_MethodSignature);
	if (!SCKMSParameterBlock_setTransportKeyId_Method)
		return GENERAL_FAILURE;

	// generic bytes methods
	SCKMSParameterBlock_getGenericBytes_Method = env->GetMethodID(
		SCKMSParameterBlock_Class,
		SCKMSParameterBlock_getGenericBytes_MethodName,
		SCKMSParameterBlock_getGenericBytes_MethodSignature);
	if (!SCKMSParameterBlock_getGenericBytes_Method)
		return GENERAL_FAILURE;

	SCKMSParameterBlock_setGenericBytes_Method = env->GetMethodID(
		SCKMSParameterBlock_Class,
		SCKMSParameterBlock_setGenericBytes_MethodName,
		SCKMSParameterBlock_setGenericBytes_MethodSignature);
	if (!SCKMSParameterBlock_setGenericBytes_Method)
		return GENERAL_FAILURE;

	SCKMSParameterBlock_getLengthGenericBytes_Method = env->GetMethodID(
		SCKMSParameterBlock_Class,
		SCKMSParameterBlock_getLengthGenericBytes_MethodName,
		SCKMSParameterBlock_getLengthGenericBytes_MethodSignature);
	if (!SCKMSParameterBlock_getLengthGenericBytes_Method)
		return GENERAL_FAILURE;

	// Schema Path methods
	SCKMSParameterBlock_getSchemaPath_Method = env->GetMethodID(
		SCKMSParameterBlock_Class,
		SCKMSParameterBlock_getSchemaPath_MethodName,
		SCKMSParameterBlock_getSchemaPath_MethodSignature);
	if (!SCKMSParameterBlock_getSchemaPath_Method)
		return GENERAL_FAILURE;

	SCKMSParameterBlock_setSchemaPath_Method = env->GetMethodID(
		SCKMSParameterBlock_Class,
		SCKMSParameterBlock_setSchemaPath_MethodName,
		SCKMSParameterBlock_setSchemaPath_MethodSignature);
	if (!SCKMSParameterBlock_setSchemaPath_Method)
		return GENERAL_FAILURE;

	return SUCCESS;
}

ScErrno SCKMSParameterBlock::importFromJObject(const jobject javaParamsObject)
{
	if (!jniInitialised)
		return GENERAL_FAILURE;
	// call the method which returns a long
	jlong methodReturn = env->CallLongMethod(javaParamsObject, SCKMSParameterBlock_getSecCtx_Method);
	// set the context to the returned long
	secCtx = (SecCtx)methodReturn;

	// get the strings
	jstring JavaUserUri = (jstring)(env->CallObjectMethod(javaParamsObject, SCKMSParameterBlock_getUserUri_Method));
	userUri = env->GetStringUTFChars(JavaUserUri, 0);
	jstring JavaKmsUrl = (jstring)(env->CallObjectMethod(javaParamsObject, SCKMSParameterBlock_getKmsUrl_Method));
	kmsUrl = env->GetStringUTFChars(JavaKmsUrl, 0);
	jstring JavaKmsUri = (jstring)(env->CallObjectMethod(javaParamsObject, SCKMSParameterBlock_getKmsUri_Method));
	kmsUri = env->GetStringUTFChars(JavaKmsUri, 0);
	jstring JavaTransportKeyId = (jstring)(env->CallObjectMethod(javaParamsObject, SCKMSParameterBlock_getTransportKeyId_Method));
	transportKeyId = env->GetStringUTFChars(JavaTransportKeyId, 0);
	jstring JavaSchemaPath = (jstring)(env->CallObjectMethod(javaParamsObject, SCKMSParameterBlock_getSchemaPath_Method));
	schemaPath = env->GetStringUTFChars(JavaSchemaPath, 0);

	// get input bytes
	ScErrno err;
	err = importBytes(javaParamsObject, SCKMSParameterBlock_getGenericBytes_Method, genericBytes);
	if (err != SUCCESS) return GENERAL_FAILURE;

	return SUCCESS;
}

ScErrno SCKMSParameterBlock::exportToJObject(jobject& javaParamsObject)
{
	if (!jniInitialised)
		return GENERAL_FAILURE;

	// write the secCtx into object
	jlong newCtx = secCtx;
	env->CallVoidMethod(javaParamsObject, SCKMSParameterBlock_setSecCtx_Method, newCtx);

	// convert std::string into a new Java string object
	jstring JavaUserUri = env->NewStringUTF((const char *)userUri.c_str());
	jstring JavaKmsUrl = env->NewStringUTF((const char *)kmsUrl.c_str());
	jstring JavaKmsUri = env->NewStringUTF((const char *)kmsUri.c_str());
	jstring JavaTransportKeyId = env->NewStringUTF((const char *)kmsUri.c_str());
	jstring JavaSchemaPath = env->NewStringUTF((const char *)schemaPath.c_str());
	// set new string in object
	env->CallVoidMethod(javaParamsObject, SCKMSParameterBlock_setUserUri_Method, JavaUserUri);
	env->CallVoidMethod(javaParamsObject, SCKMSParameterBlock_setKmsUrl_Method, JavaKmsUrl);
	env->CallVoidMethod(javaParamsObject, SCKMSParameterBlock_setKmsUri_Method, JavaKmsUri);
	env->CallVoidMethod(javaParamsObject, SCKMSParameterBlock_setTransportKeyId_Method, JavaTransportKeyId);
	env->CallVoidMethod(javaParamsObject, SCKMSParameterBlock_setSchemaPath_Method, JavaSchemaPath);

	// export from vector to byte[] in Java object
	ScErrno err;
	err = exportBytes(javaParamsObject, SCKMSParameterBlock_setGenericBytes_Method, genericBytes);
	if (err != SUCCESS) return GENERAL_FAILURE;

	return SUCCESS;
}

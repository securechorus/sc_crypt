package com.securechorus;

/**
 * Enum containing Secure Chorus library error codes with the enumeration numbers corresponding to those in the C/C++ library.
 * e.g. In the C/C++ code, library functions return a 0 if they are successful,
 * so enumerating a 0 here gives the "error code" {@code SCErrno.SUCCESS}.
 */

/**
 * Possible enumerations of SCErrno
 */
public enum SCErrno
{
    SUCCESS(0),

    GENERAL_FAILURE(1),
    RESOURCE_NOT_EXIST(2),
    RESOURCE_DENIED(3),
    RESOURCE_NOT_ALLOCATED(4),
    ALGORITHM_INVALID(5),

    SENDING_URI_NOT_PROVISIONED(16),
    RECIPIENT_URI_NOT_PROVISIONED(17),
    URI_DOMAIN_NOT_RECOGNISED(18),

    IMESSAGE_DECRYPT_FAIL(32),
    IMESSAGE_NOT_SIGNED(33),
    ECDSA_NOT_VERIFIED(34),
    HMAC_NOT_VERIFIED(35),
    ECCSI_NOT_VERIFIED(36),
    TRK_NOT_AVAILABLE(37),
    SAKKE_DECRYPT_FAIL(38),

    TIMESTAMP_FORMAT_INVALID(48),
    TIMESTAMP_OLD(49),
    TIMESTAMP_FUTURE(50),

    TRK_NOT_EXIST(64),
    KEY_UNWRAP_FAIL(65),

    KMS_RESPONSE_FORMAT_INVALID(67),
    TRK_INVALID(68),

    CERTIFICATE_INVALID(80),
    ROOT_CERT_CTX_NOT_MATCH(81),
    ROOT_CERT_CTX_FAIL(82),
    CERTIFICATE_URI_NOT_MATCH(83),

    MS_UID_FORMAT_INVALID(96),
    MS_UID_KEYS_NOT_MATCH(97),
    MS_URI_NOT_MATCH_UID(98),
    MS_URI_NOT_MATCH(99),
    MS_UID_NOT_MATCH(100),

    MS_INPUT_STRING_INVALID(120),
    MS_INPUT_OUT_OF_BOUNDS(121),
    MS_INPUT_PTR_NULL(122),
    MS_INPUT_BUFFER_INVALID(123),

    UNDEFINED(124);

    /**
     * Translate integer, as received from the C/C++ interface, into the corresponding enumeration error code
     * and return that error code.
     *
     * @param val The integer received from the C/C++ library indicating
     *            whether or not the library function call was successful.
     * @return The enumerated error code from the list of possible response.
     */
    public static SCErrno getError(int val)
    {
        if ((val >= 0) && (val < allCodes.length))
            return allCodes[val];
        return UNDEFINED;
    }

    /**
     * Get integer value from an SCErrno for serialisation
     * NB this is not the same as ordinal(), which returns its position in the list above
     * @return The corresponding integer value
     */
    public int getCode()
    {
        return code;
    }

    /**
     * Constructor.
     * @param codeValue Numerical value to  initialise the code
     */
    SCErrno(int codeValue)
    {
        code = codeValue;
    }
    private final int code;
    private static SCErrno[] allCodes = new SCErrno[125];

    /**
     * Initialise the reverse translation process from int to SCErrno
     * to mimic the initialisation in the C header.
     */
    static
    {
        for (int i = 0; i < allCodes.length; i++)
            allCodes[i] = UNDEFINED;

        for (SCErrno e : SCErrno.values())
        {
            int i = e.code;
            allCodes[i] = e;
        }
    }
}

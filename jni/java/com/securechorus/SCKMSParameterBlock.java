package com.securechorus;

/**
 * Class for gathering parameters required for using the C/C++ Secure Chorus library
 * to initialise with and process keys from the KMS.
 * <p>
 * The KMS client configuration is created/loaded by passing this parameter block in the
 * {@link SecureChorusLibrary#RequestKmsInitXML(SCKMSParameterBlock)} and
 * {@link SecureChorusLibrary#RequestKmsKeyProvXML(SCKMSParameterBlock)} methods.
 */

public class SCKMSParameterBlock {

    /**
     * Constructor for creating an empty instance of {@link SCKMSParameterBlock}.
     * Instance attributes can be populated after by invoking the instance's set methods.
     */
    public SCKMSParameterBlock()
    {
        setSecCtx(0);
        setGenericBytes(new byte[]{});
        setUserUri("");
        setKmsUri("");
        setKmsUrl("");
        setTransportKeyId("");
        setSchemaPath("");
    }

    /**
     * Constructor for creating an instance of {@link SCKMSParameterBlock} ready to be used with
     * the appropriate Secure Chorus library methods (see class header).
     *
     * @param secCtx The KMS security context
     * @param userUri The URI of the user wishing to communicate with the KMS
     * @param kmsUrl The URL of the KMS
     */
    public SCKMSParameterBlock(long secCtx, String userUri, String kmsUrl)
    {
        this();
        setSecCtx(secCtx);
        setUserUri(userUri);
        setKmsUrl(kmsUrl);
        setGenericBytes(new byte[]{});
    }

    /**
     * Constructor for creating an instance of {@link SCKMSParameterBlock} ready to be used with
     * {@link SecureChorusLibrary#CreateKmsSecurityContext(SCKMSParameterBlock)}.
     * This should only be used for transport key management.
     *
     * @param secCtx The KMS security context
     * @param kmsUrl The URL of the KMS
     * @param transportKeyId The identifier for the transport key to be managed
     * @param genericBuffer The buffer containing the transport key to be managed
     */
    public SCKMSParameterBlock(long secCtx, String kmsUrl, String transportKeyId, byte[] genericBuffer)
    {
        this();
        setSecCtx(secCtx);
        setKmsUrl(kmsUrl);
        setGenericBytes(genericBuffer);
        setTransportKeyId(transportKeyId);
     }

    /**
     * Constructor for creating an instance of {@link SCKMSParameterBlock} ready to be used with
     * {@link SecureChorusLibrary#ProcessKmsRespXML(SCKMSParameterBlock)}.
     * This is used to ask the Secure Chorus library to fill the provided buffer with
     * information from the KMS.
     *
     * @param secCtx The KMS security context
     * @param genericBuffer The buffer where the KMS information will be stored
     */
    public SCKMSParameterBlock(long secCtx, byte[] genericBuffer)
    {
        this();
        setSecCtx(secCtx);
        setGenericBytes(genericBuffer);
    }

    /**
     * Alternative constructor for creating an instance of {@link SCKMSParameterBlock} ready
     * to be used with {@link SecureChorusLibrary#ProcessKmsRespXML(SCKMSParameterBlock)}.
     * This is used to ask the Secure Chorus library to fill the provided buffer with
     * information from the KMS. This alternative constructor allows the schema path to be
     * specified.
     *
     * @param secCtx The KMS security context
     * @param schemaPath The location of the schemas
     * @param genericBuffer The buffer where the KMS information will be stored
     */
    public SCKMSParameterBlock(long secCtx, String schemaPath, byte[] genericBuffer)
    {
        this();
        setSecCtx(secCtx);
        setSchemaPath(schemaPath);
        setGenericBytes(genericBuffer);
    }

    /**
     * Sets the KMS security context locally.
     *
     * @param secCtx The KMS security context
     */
    public void setSecCtx(long secCtx)
    {
        this.secCtx = secCtx;
    }

    /**
     * Retrieves the KMS security context.
     *
     * @return The local value of the KMS security context
     */
    public long getSecCtx()
    {
        return secCtx;
    }

    /**
     * Sets the URI of the user wishing to communicate with the KMS.
     *
     * @param userUri The user's URI
     */
    public void setUserUri(String userUri)
    {
        this.userUri = userUri;
    }

    /**
     * Retrieves the URI of the user that will be communicating with the KMS.
     *
     * @return The user's URI
     */
    public String getUserUri()
    {
        return userUri;
    }

    /**
     * Sets the URI of the KMS that the user is intending on communicating with.
     *
     * @param kmsUri The KMS' URI
     */
    public void setKmsUri(String kmsUri)
    {
        this.kmsUri = kmsUri;
    }

    /**
     * Retrieves the URI of the KMS that will be communicated with.
     *
     * @return The KMS' URI
     */
    public String getKmsUri()
    {
        return kmsUri;
    }

    /**
     * Sets the identifier for the transport key to be used for communications with the KMS.
     *
     * @param transportKeyId The transport key identifier
     */
    public void setTransportKeyId(String transportKeyId)
    {
        this.transportKeyId = transportKeyId;
    }

    /**
     * Retrieves the identifier for the transport key that will be used for communications with the KMS.
     *
     * @return The transport key identifier
     */
    public String getTransportKeyId()
    {
        return transportKeyId;
    }

    /**
     * Sets the URL of the KMS that the user is intending on communicating with.
     *
     * @param kmsUrl The KMS' URL
     */
    public void setKmsUrl(String kmsUrl)
    {
        this.kmsUrl = kmsUrl;
    }

    /**
     * Retrieves the URL of the KMS that will be communicated with.
     *
     * @return The KMS' URL
     */
    public String getKmsUrl()
    {
        return kmsUrl;
    }

    /**
     * Sets the schema path for processing KMS XML.
     *
     * @param schemaPath The schema path.
     */
    public void setSchemaPath(String schemaPath)
    {
        this.schemaPath = schemaPath;
    }

    /**
     * Retrieves the schema path for processing KMS XML.
     *
     * @return The schema path.
     */
    public String getSchemaPath()
    {
        return schemaPath;
    }

    /**
     * Sets the contents of the generic data buffer {@link this#genericBuffer}.
     *
     * @param data The new contents of the buffer
     */
    public void setGenericBytes(byte[] data) { this.genericBuffer = data; }

    /**
     * Retrieves the generic data buffer {@link this#genericBuffer}.
     *
     * @return A byte array representing the generic data buffer.
     */
    public byte[] getGenericBytes() { return genericBuffer; }

    /**
     * Retrieves the length of the generic data buffer {@link this#genericBuffer}.
     *
     * @return The length of the generic data buffer
     */
    public int getLengthGenericBytes(){ return genericBuffer.length; }

    private long secCtx;
    private String userUri;
    private String kmsUri;
    private String kmsUrl;
    private String transportKeyId;
    private String schemaPath;

    /**
     * The generic buffer is a single data buffer that can be used for:
     * KmsInitRes, KMSResponse, KmsKeyProvReq, and KmsCertCacheReq.
     */
    private byte[] genericBuffer;
}

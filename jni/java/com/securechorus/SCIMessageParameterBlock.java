package com.securechorus;
/**
 * Class for gathering parameters required for using the C/C++ Secure Chorus library to create a MIKEY-SAKKE I_Message.
 *
 * An I_Message is created by passing this parameter block in the
 * {@link SecureChorusLibrary#CreateMikeySakkeIMessage(SCIMessageParameterBlock)} method.
 * The I_Message is required for obtaining the Secure Chorus shared secret for communications.
 */

public class SCIMessageParameterBlock {
    /**
     * Constructor for creating an empty instance of {@link SCIMessageParameterBlock}.
     * Instance attributes can be populated after by invoking the instance's set methods.
     */
    public SCIMessageParameterBlock()
    {
        setSecCtx(0);
        setIMessage(new byte[]{});
        setSsv(new byte[]{});
        setSenderUri("");
        setRecipientUri("");
        setV(false);
        setCheckSignature(false);
        setGenericBytes1(new byte[]{});
        setGenericBytes2(new byte[]{});
    }

    /**
     * Constructor for creating a instance of {@link SCIMessageParameterBlock} ready to be used with
     * {@link SecureChorusLibrary#CreateMikeySakkeIMessage(SCIMessageParameterBlock)}
     *
     * @param secCtx The security context of the KMS, this would have been retrieved from a local KMS configuration file
     * @param senderUri The URI of the user initiating the call to be encrypted
     * @param recipientUri The URI of the user receiving the call to be encrypted
     * @param v A boolean to determine whether or not the I_Message's signature should be checked
     */
    public SCIMessageParameterBlock(long secCtx, String senderUri, String recipientUri, boolean v)
    {
        this();   // clear all members - may not need this
        setSecCtx(secCtx);
        setSenderUri(senderUri);
        setRecipientUri(recipientUri);
        setV(v);
    }


    /**
     * Constructor for creating an instance of {@link SCIMessageParameterBlock} ready to be used with
     * {@link SecureChorusLibrary#CreateMikeySakkeIMessageWithSsv(SCIMessageParameterBlock)}.
     * N.B. This constructor includes the setup necessary for calling the {@code ...WithSsv} library method,
     * where a externally stored SSV is used for the encryption.
     *
     * @param secCtx The security context of the KMS, this would have been retrieved from a local KMS configuration file
     * @param senderUri The URI of the user initiating the call to be encrypted
     * @param recipientUri The URI of the user receiving the call to be encrypted
     * @param ssv The externally provided SSV to use for encryption
     * @param v A boolean to determine whether or not the I_Message's signature should be checked
     */
    public SCIMessageParameterBlock(long secCtx, String senderUri, String recipientUri, byte[] ssv, boolean v)
    {
        this();   // clear all members - may not need this
        setSecCtx(secCtx);
        setSenderUri(senderUri);
        setRecipientUri(recipientUri);
        setSsv(ssv);
        setV(v);
    }

    /**
     * Constructor for creating an instance of {@link SCIMessageParameterBlock} ready to be used with
     * {@link SecureChorusLibrary#ProcessMikeySakkeIMessage(SCIMessageParameterBlock)}.
     * This will be used once the I_Message has been created by the C/C++ library to extract the SSV.
     *
     * @param secCtx The security context of the KMS, this would have been retrieved from a local KMS configuration file
     * @param iMessage The I_Message received from the C/C++ library
     * @param checkSignature A boolean to determine whether or not the I_Message's signature should be checked
     */
    public SCIMessageParameterBlock(long secCtx, byte[] iMessage, boolean checkSignature)
    {
        this();   // clear all members - may not need this
        setSecCtx(secCtx);
        setIMessage(iMessage);
        setCheckSignature(checkSignature);
    }

    /**
     * Sets the value of the locally known KMS security context.
     *
     * @param secCtx The new value for the security context
     */
    public void setSecCtx(long secCtx)
    {
        this.secCtx = secCtx;
    }

    /**
     * Retrieves the currently set KMS security context.
     *
     * @return Currently set KMS security context
     */
    public long getSecCtx()
    {
        return secCtx;
    }

    /**
     * Sets the I_Message to be used for processing and SSV extraction
     *
     * @param iMessage The new I_Message to be used
     */
    public void setIMessage(byte[] iMessage)
    {
        this.iMessage = iMessage;
    }

    /**
     * Sets the I_Message to be used for processing and SSV extraction,
     * by copying bytes to the appropriate length.
     *
     * @param iMessage The array containing the new I_Message to be used
     * @param length The length of the I_Message in the array
     */
    public void setIMessage(byte[] iMessage, int length)
    {
        this.iMessage = new byte[length];
        for (int i = 0; i < length; i++)
        this.iMessage[i] = iMessage[i];
    }

    /**
     * Retrieves the current I_Message to be used for processing and SSV extraction.
     *
     * @return Byte array containing I_Message
     */
    public byte[] getIMessage()
    {
        return iMessage;
    }

    /**
     * Retrieves the length of the I_Message array
     *
     * @return Length of I_Message array
     */
    public int getLengthIMessage()
    {
        return iMessage.length;
    }

    /**
     * Sets the SSV to be used for I_Message creation using an external SSV.
     *
     * @param ssv The new SSV to be used
     */
    public void setSsv(byte[] ssv)
    {
        this.ssv = ssv;
    }

    /**
     * Sets the SSV to be used for I_Message creation using an external SSV,
     * by copying bytes up to an appropriate length.
     *
     * @param ssv The array containing the SSV to be used
     * @param length The length of the SSV in the array
     */
    public void setSsv(byte[] ssv, int length)
    {
      this.ssv = new byte[length];
      for (int i = 0; i < length; i++)
        this.ssv[i] = ssv[i];
    }

    /**
     * Retrieves the SSV either to be used for I_Message creation with external SSV,
     * or as provided by the C/C++ library methods.
     *
     * @return Byte array containing SSV
     */
    public byte[] getSsv()
    {
        return ssv;
    }

    /**
     * Retrieves the length of the SSV array.
     *
     * @return The length of the SSV array
     */
    public int getLengthSsv()
    {
        return ssv.length;
    }

    /**
     * Sets the URI of the user initiating the call to be encrypted.
     *
     * @param senderUri URI of call initiator
     */
    public void setSenderUri(String senderUri)
    {
        this.senderUri = senderUri;
    }

    /**
     * Retrieves the URI of the user initiating the call to be encrypted.
     *
     * @return URI of call initiator
     */
    public String getSenderUri()
    {
        return senderUri;
    }

    /**
     * Sets the URI of the user receiving the call to be encrypted.
     *
     * @param recipientUri URI of call receiver
     */
    public void setRecipientUri(String recipientUri)
    {
        this.recipientUri = recipientUri;
    }

    /**
     * Retrieves the URI of the user receiving the call to be encrypted.
     *
     * @return URI of call receiver
     */
    public String getRecipientUri()
    {
        return recipientUri;
    }

    /**
     * Sets the boolean field for determining whether or not the I_Message should be signed.
     *
     * @param v Boolean value representing I_Message signing state
     */
    public void setV(boolean v)
    {
        this.v = v;
    }

    /**
     * Retrieves the boolean field for seeing whether or not the I_Message will be signed.
     *
     * @return Boolean value representing I_Message signing state
     */
    public boolean getV()
    {
        return v;
    }

    /**
     * Sets the boolean field for declaring whether or not the I_Message is signed before processing.
     *
     * @param checkSignature Boolean value representing I_Message signing state
     */
    public void setCheckSignature(boolean checkSignature)
    {
        this.checkSignature = checkSignature;
    }

    /**
     * Retrieves the boolean field for seeing if the I_Message is signed before processing.
     *
     * @return Boolean value representing I_Message signing state
     */
    public boolean getCheckSignature()
    {
        return checkSignature;
    }

   /**
     * Sets the contents of the first generic data buffer.
     *
     * @param data The new contents of the first buffer
     */
    public void setGenericBytes1(byte[] data) { this.genericBytes1 = data; }

    /**
     * Retrieves the first generic data buffer.
     *
     * @return A byte array representing the first generic data buffer.
     */
    public byte[] getGenericBytes1() { return genericBytes1; }

    /**
     * Retrieves the length of the first generic data buffer.
     *
     * @return The length of the first generic data buffer
     */
    public int getLengthGenericBytes1(){ return genericBytes1.length; }

    /**
     * Sets the contents of the second generic data buffer.
     *
     * @param data The new contents of the second buffer
     */
    public void setGenericBytes2(byte[] data) { this.genericBytes2 = data; }

    /**
     * Retrieves the second generic data buffer.
     *
     * @return A byte array representing the second generic data buffer.
     */
    public byte[] getGenericBytes2() { return genericBytes2; }

    /**
     * Retrieves the length of the second generic data buffer.
     *
     * @return The length of the second generic data buffer
     */
    public int getLengthGenericBytes2(){ return genericBytes2.length; }

    private long secCtx;
    private byte[] iMessage;
    private byte[] ssv;
    private byte[] genericBytes1;
    private byte[] genericBytes2;
    private String senderUri;
    private String recipientUri;
    private boolean v;
    private boolean checkSignature;
}

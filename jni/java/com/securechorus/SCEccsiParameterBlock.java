package com.securechorus;
/**
 * Parameter block class for use with ECCSI signing and verification with the KMS.
 * Verification checks the {@code signatureBuffer} on the {@code dataBuffer} using the given {@code signingUri} and {@msgTimestamp}.
 */

public class SCEccsiParameterBlock {

    /**
     * Creates an empty SCEccsiParameterBlock which can be initialised later with various set...()
     */
    public SCEccsiParameterBlock()
    {
        setSecCtx(0);
        setDataBuffer(new byte[]{});
        setSigningBuffer(new byte[]{});
        setSigningUri("");
        setMsgTimestamp("");
    }

    /**
     * Creates an empty SCEccsiParameterBlock which can be initialised later with various set...()
     *
     * @param secCtx Security context of KMS
     * @param signingUri URI of location for ECCSI signing
     * @param msgTimestamp Timestamp of the message being sent
     * @param dataBuffer Buffer containing data to be sent
     * @param signingBuffer Buffer containing information for signing ECCSI
     */
    public SCEccsiParameterBlock(long secCtx, String signingUri, String msgTimestamp, byte[] dataBuffer, byte[] signingBuffer)
    {
        this();
        setSecCtx(secCtx);
        setDataBuffer(dataBuffer);
        setSigningBuffer(signingBuffer);
        setSigningUri(signingUri);
        setMsgTimestamp(msgTimestamp);
    }

    /**
     * Sets the KMS security context using the supplied long parameter
     *
     * @param secCtx New value for {@code secCtx}
     */
    public void setSecCtx(long secCtx)
    {
        this.secCtx = secCtx;
    }

    /**
     * Retrieves the value of the KMS security context
     *
     * @return Currently specified KMS security context
     */
    public long getSecCtx()
    {
        return secCtx;
    }

    /**
     * Sets the signingUri using the supplied String
     *
     * @param signingUri New value for {@code signingUri}
     */
    public void setSigningUri(String signingUri)
    {
        this.signingUri = signingUri;
    }

    /**
     * Retrieves the value of the signing URI
     *
     * @return Currently specified signing URI
     */
    public String getSigningUri()
    {
        return signingUri;
    }

    /**
     * Sets the timestamp for ECCSI verification using the supplied String
     *
     * @param msgTimestamp New value for {@code timestamp}
     */
    public void setMsgTimestamp(String msgTimestamp)
    {
        this.msgTimestamp = msgTimestamp;
    }

    /**
     * Retrieves the value of the timestamp to be used for ECCSI verification
     *
     * @return Currently specified timestamp
     */
    public String getMsgTimestamp()
    {
        return msgTimestamp;
    }

    /**
     * Sets the bytes in the signing buffer for ECCSI verification using the byte array parameter
     *
     * @param data New array of bytes to use as the signing buffer
     */
    public void setSigningBuffer(byte[] data)
    {
        this.signingBuffer = data;
    }

    /**
     * Retrieves the contents of the signing buffer to be used for ECCSI verification
     *
     * @return Current contents of {@code signingBuffer}
     */
    public byte[] getSigningBuffer()
    {
        return signingBuffer;
    }

    /**
     * Retrieves the length of the array holding the signing buffer
     *
     * @return Length of {@code singingBuffer}
     */
    public int getLengthSigningBuffer()
    {
        return signingBuffer.length;
    }

    /**
     * Sets the value of dataBuffer to the bytes that require ECCSI verification.
     *
     * @param data New array of bytes to use as data to be verified
     */
    public void setDataBuffer(byte[] data)
    {
        this.dataBuffer = data;
    }

    /**
     * Retrieves the contents of the data buffer
     *
     * @return Current contents of {@code dataBuffer}
     */
    public byte[] getDataBuffer()
    {
        return dataBuffer;
    }

    /**
     * Retrieves the length of the array holding the data buffer
     *
     * @return Length of {@code dataBuffer}
     */
    public int getLengthDataBuffer()
    {
        return dataBuffer.length;
    }

    // private members
    private long secCtx;
    private String signingUri;
    private String msgTimestamp;
    private byte[] dataBuffer;
    private byte[] signingBuffer;
}

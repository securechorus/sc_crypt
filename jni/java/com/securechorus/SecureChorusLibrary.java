package com.securechorus;

/**
 * JNI Wrapper for Secure Chorus C/C++ library.
 * Excluding {@link this#LoadDLLLibrary()}, the methods here link to ones in the C/C++ code for Secure Chorus,
 * for which the full documentation can be found in the Secure Chorus library resources.
 *
 * N.B. Get the C header file by running the following from the top-level {@code src} directory:
 * {@code javah -classpath . com.securechorus.JNIWrapper}
 *
 * Get the method signatures by running the following from the directory in which this file's outputted .class file resides:
 * {@code javap -s com.securechorus.JNIWrapper}
 */

public class SecureChorusLibrary {
    // the name of the compiled Windows DLL (minus the extension)
    // this must be visible to the system via the Windows PATH
    // if this isn't run before the other static methods they won't work
    private static final String DLL_FILE_WITHOUT_EXTENSION = "sc_jni";

    /**
     * Private constructor so we can't make an instance of this class (it only has static methods).
     */
    private SecureChorusLibrary(){}

    /**
     * Load the DLL, needs to be called before any static wrapper method.
     */
    public static void LoadDLLLibrary() {
        System.loadLibrary(DLL_FILE_WITHOUT_EXTENSION);
    }

    /* C++ method
    SCErrno SetFilePath(const char * newFilePath)
    */

    /**
     * Sets the file path of the keystore file.
     *
     * @param path The file path to set.
     * @return The Secure Chorus response code for the operation.
     */
    public static native SCErrno SetFilePath(String path);

    /* C++ method
    ScErrno CreateMikeySakkeIMessage(
              std::vector<uint8_t> &iMessage,
              std::vector<uint8_t> &ssv,
              SecCtx const &secCtx,
              std::string const &senderUri,
              std::string const &recipientUri,
              bool const &v = false
              );
     */
    /**
     * Creates a MIKEY-SAKKE I_Message containing a returned SSV, for the specified URI using a stored external KMS cert.
     * If the sender URI is NULL, the I_Message is not signed using ECCSI.
     * Function creates the I_Message using the current time.
     *
     * The I_Message is added to the {@code paramBlock}.
     *
     * @param paramBlock An appropriately populated {@link SCIMessageParameterBlock}.
     *                   For this method, it should be created using {@link SCIMessageParameterBlock#SCIMessageParameterBlock(long, String, String, boolean)}.
     * @return The Secure Chorus response code for the operation.
     */
    public static native SCErrno CreateMikeySakkeIMessage(SCIMessageParameterBlock paramBlock);

    /* C++ method
    ScErrno CreateMikeySakkeIMessageForSRTP(
              std::vector<uint8_t> &iMessage,
              std::vector<uint8_t> &masterKey,
              std::vector<uint8_t> &masterSalt,
              SecCtx const &secCtx,
              std::string const &senderUri,
              std::string const &recipientUri,
              bool const &v = false
              );
     */
    /**
     * Creates a MIKEY-SAKKE I_Message containing a returned SRTP master key and salt, for the specified URI using a stored external KMS cert.
     * If the sender URI is NULL, the I_Message is not signed using ECCSI.
     * Function creates the I_Message using the current time.
     *
     * The I_Message is added to the {@code paramBlock}.
     *
     * @param paramBlock An appropriately populated {@link SCIMessageParameterBlock}.
     *                   For this method, it should be created using {@link SCIMessageParameterBlock#SCIMessageParameterBlock(long, String, String, boolean)}.
     * @return The Secure Chorus response code for the operation.
     */
    public static native SCErrno CreateMikeySakkeIMessageForSRTP(SCIMessageParameterBlock paramBlock);

    /* C++ method
    ScErrno ProcessMikeySakkeIMessageForSRTP(
              std::string &recipientUri,
              std::string &senderUri,
              std::vector<uint8_t> &masterKey,
              std::vector<uint8_t> &masterSalt,
              bool &v,
              SecCtx const &secCtx,
              std::vector<uint8_t> const &iMessage,
              bool checkSignature=true
              );
     */
    /**
     * Processes a MIKEY SAKKE I_MESSAGE with optional signature verification
     * and return the SRTP Master Key and Master Salt.
     *
     * NOTE: For most use cases the signature should be checked.
     * However, for users for whom availability of communication is imperative, the signature may be ignored.
     * There is a risk of both fraudulent communication and denial-of-service attacks in this case.
     *
     * The {@code paramBlock} holds the recipient's URI, the sender's URI, the SRTP Master Key (in genericBytes1),
     * the SRTP Master Salt (in genericBytes2), and the 'v' boolean.
     *
     * @param paramBlock An appropriately populated {@link SCIMessageParameterBlock}.
     *                   For this method, it should be created using {@link SCIMessageParameterBlock#SCIMessageParameterBlock(long, byte[], boolean)}.
     * @return The Secure Chorus response code for the operation.
     */
    public static native SCErrno ProcessMikeySakkeIMessageForSRTP(SCIMessageParameterBlock paramBlock);

    /* C++ method
    ScErrno ProcessMikeySakkeIMessage(
              std::string &recipientUri,
              std::string &senderUri,
              std::vector<uint8_t> &ssv,
              bool &v,
              SecCtx const &secCtx,
              std::vector<uint8_t> const &iMessage,
              bool checkSignature=true
              );
     */
    /**
     * Processes a MIKEY-SAKKE I_Message and extracts the SSV within.
     *
     * NOTE: For most use cases the signature should be checked.
     * However, for users for whom availability of communication is imperative, the signature may be ignored.
     * There is a risk of both fraudulent communication and denial-of-service attacks in this case.
     *
     * The {@code paramBlock} holds the recipient's URI, the sender's URI, the SSV, and the 'v' boolean.
     *
     * @param paramBlock An appropriately populated {@link SCIMessageParameterBlock}.
     *                   For this method, it should be created using {@link SCIMessageParameterBlock#SCIMessageParameterBlock(long, byte[], boolean)}.
     * @return The Secure Chorus response code for the operation.
     */
    public static native SCErrno ProcessMikeySakkeIMessage(SCIMessageParameterBlock paramBlock);


    /* C++ method
    ScErrno CreateMikeySakkeIMessageWithSsv(
              std::vector<uint8_t> &iMessage,
              SecCtx const &secCtx,
              std::string const &senderUri,
              std::string const &recipientUri,
              std::vector<uint8_t> const &ssv,
              bool const &v = false
              );
     */
    /**
     * Creates a MIKEY-SAKKE I_Message using an SSV provided to the function, for the specified URI using a stored external KMS cert.
     * An external SSV is useful to support conference/group calls.
     * If the sender URI is NULL, the I_Message is not signed using ECCSI.
     * Function creates the I_Message using the current time.
     *
     * The I_Message is added to the {@code paramBlock}.
     *
     * @param paramBlock An appropriately populated {@link SCIMessageParameterBlock}.
     *                   For this method, it should be created using {@link SCIMessageParameterBlock#SCIMessageParameterBlock(long, String, String, byte[], boolean)}.
     * @return The Secure Chorus response code for the operation.
     */
    public static native SCErrno CreateMikeySakkeIMessageWithSsv(SCIMessageParameterBlock paramBlock);

    /* C++ method
    ScErrno CreateKmsSecurityContext(SecCtx & secCtx, std::string KMSUri,
                                 const std::string &TransportKeyId,
                                 const std::vector<unsigned char> &
                                 transportKey)
    */
    /**
     * Pass through function (to libmscrypto in C/C++ code) to allow management of transport keys.
     *
     * The KMS security context is added to the {@code paramBlock}.
     *
     * @param paramBlock An appropriately populated {@link SCKMSParameterBlock}.
     *                   For this method, it should be created using {@link SCKMSParameterBlock#SCKMSParameterBlock(long, String, String, byte[])}.
     * @return The Secure Chorus response code for the operation.
     */
    public static native SCErrno CreateKmsSecurityContext(SCKMSParameterBlock paramBlock);

    /* C++ method
    ScErrno RequestKmsInitXML(SecCtx secCtx, std::string KMSUrl,
                                    const std::string &UserUri,
                                    std::vector<unsigned char> &KmsInitReq)
     */
    /**
     * Returns XML containing an initialisation request.
     *
     * The XML response from the KMS for the {@code init} request is added to the {@code paramBlock}.
     *
     * @param paramBlock An appropriately populated {@link SCKMSParameterBlock}.
     *                   For this method, it should be created using {@link SCKMSParameterBlock#SCKMSParameterBlock(long, String, String)}.
     * @return The Secure Chorus response code for the operation.
     */
    public static native SCErrno RequestKmsInitXML(SCKMSParameterBlock paramBlock);

    /* C++ method
    ScErrno ProcessKmsRespXML(SecCtx secCtx,
                              const std::vector<unsigned char> &KMSResponse,
                              std::string schemaDirectory)
     */
    /**
     * Processes information received from the KMS.
     *
     * @param paramBlock An appropriately populated {@link SCKMSParameterBlock}.
     *                   For this method, it should be created using {@link SCKMSParameterBlock#SCKMSParameterBlock(long, byte[])}.
     * @return The Secure Chorus response code for the operation.
     */
    public static native SCErrno ProcessKmsRespXML(SCKMSParameterBlock paramBlock);

    /* C++ method
    ScErrno RequestKmsKeyProvXML(SecCtx secCtx, std::string KMSUrl,
                                 const std::string & UserUri,
                                 std::vector<unsigned char> &KmsKeyProvReq)
    */
    /**
     * Returns XML request for key provisioning by KMS.
     *
     * The XML response from the KMS for the {@code prov} request is added to the {@code paramBlock}.
     *
     * @param paramBlock An appropriately populated {@link SCKMSParameterBlock}.
     *                   For this method, it should be created using {@link SCKMSParameterBlock#SCKMSParameterBlock(long, String, String)}.
     * @return The Secure Chorus response code for the operation.
     */
    public static native SCErrno RequestKmsKeyProvXML(SCKMSParameterBlock paramBlock);

    /* C++ method
    ScErrno RequestKmsCertCacheXML(SecCtx secCtx, std::string KMSUrl,
                                         const std::string & UserUri,
                                         std::vector<unsigned char> &KmsCertCacheReq)
     */
    /**
     * Returns XML containing a certificate cache request.
     * The library shall store the last received Cache Number and include it in the request.
     *
     * The XML response from the KMS for the {@code cert} request is added to the {@code paramBlock}.
     *
     * @param paramBlock An appropriately populated {@link SCKMSParameterBlock}.
     *                   For this method, it should be created using {@link SCKMSParameterBlock#SCKMSParameterBlock(long, String, String)}.
     * @return The Secure Chorus response code for the operation.
     */
    public static native SCErrno RequestKmsCertCacheXML(SCKMSParameterBlock paramBlock);

    /* C++ method
    ScErrno CreateMikeyEccsiSig(
              std::vector<uint8_t> &mikeyEccsi,
              SecCtx const &secCtx,
              std::string const &signingUri,
              std::string const &msgTimestamp,
              std::vector<uint8_t> const &data
              );
     */
    /**
     * Creates a MIKEY ECCSI signature for attaching to other types of MIKEY message e.g. MIKEY-PSK.
     *
     * The MIKEY ECCSI signature is added to the {@code paramBlock}.
     *
     * @param paramBlock An appropriately populated {@link SCEccsiParameterBlock}.
     *                   For this method, it should be created using {@link SCEccsiParameterBlock#SCEccsiParameterBlock(long, String, String, byte[], byte[])}
     *                   with the signature data buffer being null or empty.
     * @return The Secure Chorus response code for the operation.
     */
    public static native SCErrno CreateMikeyEccsiSig(SCEccsiParameterBlock paramBlock);

    /* C++ method
    ScErrno VerifyMikeyEccsiSig(
              SecCtx const &secCtx,
              std::string const &signingUri,
              std::string const &msgTimestamp,
              std::vector<uint8_t> const &signedData,
              std::vector<uint8_t> const &signature
              );
     */
    /**
     * Verifies a MIKEY ECCSI signature on arbitrary data.
     *
     * @param paramBlock An appropriately populated {@link SCEccsiParameterBlock}.
     *                   For this method, it should be created using {@link SCEccsiParameterBlock#SCEccsiParameterBlock(long, String, String, byte[], byte[])}
     * @return The Secure Chorus response code for the operation.
     */
    public static native SCErrno VerifyMikeyEccsiSig(SCEccsiParameterBlock paramBlock);

}

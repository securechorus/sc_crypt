#ifndef SC_LIBMS_HELPER_H
#define SC_LIBMS_HELPER_H

// Header file inclusions.
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <string>
#include <vector>

#include "sc_errno.h"

// Forward declarations.
struct Buffer;
struct String;

/**
 *  Secure Chorus MIKEY-SAKKE Library namespace.
 */
namespace ScLibMs
{
    // Convert unsigned integer value to network byte order. Assumes sufficient
    // memory present. Platform endianness independent, at cost of not as
    // efficient as possible.
    template<typename T> void ToNetworkBytes(T value, std::uint8_t *ptr)
    {
        std::uint8_t *p = ptr + sizeof(T);

        do
        {
            *--p = std::uint8_t(value);
            value >>= 8;
        }
        while (p != ptr);
    }


    // Convert unsigned integer value from network byte order. Assumes
    // sufficient memory present. Platform endianness independent, at cost of
    // not as efficient as possible.
    template<typename T> void FromNetworkBytes(const std::uint8_t *ptr, T &value)
    {
        const std::uint8_t * const end = ptr + sizeof(T);
        value = *ptr;

        while (++ptr != end)
        {
            value <<= 8;
            value |= *ptr;
        }
    }


    // Appends the value to an unsigned char vector.
    template<typename T>
      void VectorAppend(std::vector<std::uint8_t> &v, const T &value)
    {
        v.resize(v.size() + sizeof(T));
        ToNetworkBytes(value, &*(v.end() - sizeof(T)));
    };


    // Configures a String descriptor to use data from a string.
    void SetStringDescriptor(String &descriptor, const std::string &s);


    // Configures a Buffer descriptor to use data from a char vector.
    void SetBufferDescriptor(Buffer &descriptor,
                             const std::vector<std::uint8_t> &v);


    // Initialises a vector with the content of a Buffer.
    void VectorFromBuffer(std::vector<std::uint8_t> &v, const Buffer &b);


    // Appends Buffer contents to an unsigned char vector.
    void VectorAppendBuffer(std::vector<std::uint8_t> &v, const Buffer &b);


    // Appends the content of a string to an unsigned char vector.
    void VectorAppendString(std::vector<std::uint8_t> &v, const std::string &s);


    // Appends the content of an unsigned char vector to an unsigned char
    // vector.
    void VectorAppendVector(std::vector<std::uint8_t> &v1,
                            const std::vector<std::uint8_t> &v2);


    // Converts a 16 character hex string into a 64 bit unsigned value.
    uint64_t HexStringToU64(const String &string);


    // Converts an unsigned 64bit value into a 16 character hex string.
    void U64ToHexString(String &string, std::uint64_t value);


    // Reads a value from an unsigned char vector in network byte order.
    template<typename T>
      bool VectorRead(const std::vector<std::uint8_t> &v,
                      const std::vector<std::uint8_t>::const_iterator &index,
                      T &value)
    {
        // Check there is enough data in the vector.
        if (std::size_t(v.end() - index) < sizeof(T))
        {
            return false;
        }

        FromNetworkBytes(&*index, value);
        return true;
    };


    // Reads len bytes from an unsigned char vector into a string.
    bool VectorReadString(const std::vector<std::uint8_t> &v,
                          const std::vector<std::uint8_t>::const_iterator
                            &index,
                          std::uint32_t len, std::string &s);


    // Reads len bytes from an unsigned char vector into an unsigned char
    // vector.
    bool VectorReadVector(const std::vector<std::uint8_t> &v,
                          const std::vector<std::uint8_t>::const_iterator
                            &index,
                          std::uint32_t len, std::vector<std::uint8_t> &d);


    // Checks that the unsigned char vector contains at least n entries from
    // the index to the end.
    bool IsDataAvailable(const std::vector<std::uint8_t> &data,
                         std::vector<std::uint8_t>::const_iterator &index,
                         std::int32_t n);
}

#endif

// Helper functions that manipulate MIKEY payloads and I_MESSAGES.

#ifndef SC_LIBMS_PAYLOAD_H
#define SC_LIBMS_PAYLOAD_H

// Header file inclusions.
#include <cstddef>
#include <cstdint>
#include <string>
#include <vector>

#include "sc_errno.h"
#include "sc_libms.h"


// Forward declarations.
struct Buffer;
struct String;


/**
 *  Secure Chorus MIKEY-SAKKE Library namespace.
 */
namespace ScLibMs
{
    /**
    *  Define useful constants.
    */

    // Bitfield for checking which payloads are included in an I_MESSAGE.
    const uint8_t MIKEY_PAYLOAD_HAS_RAND  = 1;
    const uint8_t MIKEY_PAYLOAD_HAS_T     = 2;
    const uint8_t MIKEY_PAYLOAD_HAS_IDRI  = 4;
    const uint8_t MIKEY_PAYLOAD_HAS_IDRR  = 8;
    const uint8_t MIKEY_PAYLOAD_HAS_SAKKE = 16;
    const uint8_t MIKEY_PAYLOAD_HAS_SIGN  = 32;

    const uint8_t MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST  = 0;
    const uint8_t MIKEY_PAYLOAD_NEXT_PAYLOAD_KEMAC = 1;
    const uint8_t MIKEY_PAYLOAD_NEXT_PAYLOAD_SIGN  = 4;
    const uint8_t MIKEY_PAYLOAD_NEXT_PAYLOAD_T     = 5;
    const uint8_t MIKEY_PAYLOAD_NEXT_PAYLOAD_RAND  = 11;
    const uint8_t MIKEY_PAYLOAD_NEXT_PAYLOAD_IDR   = 14;
    const uint8_t MIKEY_PAYLOAD_NEXT_PAYLOAD_SAKKE = 26;

    const uint8_t MIKEY_PAYLOAD_HDR_VERSION          = 1;     // RFC 3830
    const uint8_t MIKEY_PAYLOAD_HDR_TYPE_SAKKE       = 26;    // RFC 3830
    const uint8_t MIKEY_PAYLOAD_HDR_PRF_HMAC_SHA_256 = 1;     // RFC 6043
    const uint8_t MIKEY_PAYLOAD_HDR_CS_ID_MAP_TYPE   = 0;     // RFC 3830

    const uint8_t MIKEY_PAYLOAD_T_TYPE_NTP_UTC = 0;
    const uint8_t MIKEY_PAYLOAD_T_TYPE_NTP = 1;
    const uint8_t MIKEY_PAYLOAD_T_TYPE_COUNTER = 2;

    const uint8_t MIKEY_PAYLOAD_IDR_TYPE_NAI = 0;
    const uint8_t MIKEY_PAYLOAD_IDR_TYPE_URI = 1;

    const uint8_t MIKEY_PAYLOAD_IDR_ROLE_IDRI    = 1;    // RFC 6043 Table 6.9
    const uint8_t MIKEY_PAYLOAD_IDR_ROLE_IDRR    = 2;    // RFC 6043 Table 6.9
    const uint8_t MIKEY_PAYLOAD_IDR_ROLE_IDRKMS  = 3;    // RFC 6043 Table 6.9
    const uint8_t MIKEY_PAYLOAD_IDR_ROLE_IDRPSK  = 4;    // RFC 6043 Table 6.9
    const uint8_t MIKEY_PAYLOAD_IDR_ROLE_IDRAPP  = 5;    // RFC 6043 Table 6.9
    const uint8_t MIKEY_PAYLOAD_IDR_ROLE_IDRKMSI = 6;    // RFC 6509 Section 4.4
    const uint8_t MIKEY_PAYLOAD_IDR_ROLE_IDRKMSR = 7;    // RFC 6509 Section 4.4

    const uint8_t MIKEY_PAYLOAD_SIGN_TYPE_RSA_PKCS = 0;
    const uint8_t MIKEY_PAYLOAD_SIGN_TYPE_RSASSA_PSS = 1;
    const uint8_t MIKEY_PAYLOAD_SIGN_TYPE_ECCSI = 2;
    const uint64_t MIKEY_MESSAGE_MAX_TIME_ERROR = (5 * 60LL) << 32;  // 5 mins.


    /**
    *  Creates a SAKKE I_MESSAGE from supplied data. The message is not signed.
    *  An IDRi payload is only present in the I_MESSAGE if sender URI is not
    *  empty.
    *  Args: iMessage             - Will contain the created I_MESSAGE on exit.
    *        nextPayloadTypeIndex - Will contain the index for the next payload
    *                               type on exit.
    *        csbId                - Will contain the CSB_ID on exit.
    *        rand                 - Will contain the random data in the message
    *                               on exit.
    *        v                    - Specifies if the V bit is to be set in the
    *                               HDR payload.
    *        timestamp            - Byte string representation of a UTC-NTP
    *                               timestamp to be included in message.
    *        senderUri            - Sender URI to be included in message.
    *        recipientUri         - Receiver URI to be included in message.
    *        sakkeData            - SAKKE encrypted data to be included in
    *                               message.
    *  Rets: None. There is no error/failure report.
    */
    void CreateIMessage(std::vector<std::uint8_t> &iMessage,
                        std::size_t &nextPayloadTypeIndex,
                        std::uint32_t &csbId, std::vector<std::uint8_t> &rand,
                        bool v, String &timestamp, const std::string &senderUri,
                        const std::string &recipientUri, Buffer sakkeData);


    /**
    *  Creates a GROUP GMK SAKKE I_MESSAGE from supplied data. The message is not
    *  signed.
    *  Args: iMessage             - Will contain the created I_MESSAGE on exit.
    *        nextPayloadTypeIndex - Will contain the index for the next payload
    *                               type on exit.
    *        v                    - Specifies if the V bit is to be set in the HDR
    *                               payload.
    *        timestamp            - Byte string representation of a UTC-NTP
    *                               timestamp to be included in message.
    *        senderUri            - Sender URI to be included in message.
    *        recipientUri         - Receiver URI to be included in message.
    *        sakkeData            - SAKKE encrypted data to be included in message.
    *  Rets: None. There is no error/failure report.
    */
    void CreateGmkIMessage(std::vector<std::uint8_t> &iMessage,
                           std::size_t &nextPayloadTypeIndex, bool v,
                           String &timestamp, const std::string &senderUri,
                           const std::string &recipientUri, Buffer sakkeData);


    /**
    *  Creates a GROUP GSK SAKKE I_MESSAGE from supplied data. The message is
    *  not signed. KEMAC is not added at this stage, the caller will add it.
    *  This is because the AES encryption needs the CSB-ID and timestamp
    *  from this part of the message construction.
    *  Args: iMessage             - Will contain the created I_MESSAGE on exit.
    *        nextPayloadTypeIndex - Will contain the index for the next payload
    *                               type on exit.
    *        v                    - Specifies if the V bit is to be set in the HDR
    *                               payload.
    *        timestamp            - Byte string representation of a UTC-NTP
    *                               timestamp to be included in message.
    *        senderUri            - Sender URI to be included in message.
    *        recipientUri         - Receiver URI to be included in message.
    *  Rets: None. There is no error/failure report.
    */
    void CreateGskIMessage(std::vector<std::uint8_t> &iMessage,
                           std::size_t &nextPayloadTypeIndex, bool v,
                           String &timestamp, const std::string &senderUri,
                           const std::string &recipientUri);


    /**
    *  Creates a GROUP MC PTT SAKKE I_MESSAGE from supplied data. The message
    *  is not signed.
    *  Args: iMessage             - Will contain the created I_MESSAGE on exit.
    *        nextPayloadTypeIndex - Will contain the index for the next payload
    *                               type on exit.
    *        v                    - Specifies if the V bit is to be set in the HDR
    *                               payload.
    *        timestamp            - Byte string representation of a UTC-NTP
    *                               timestamp to be included in message.
    *        senderUri            - Sender URI to be included in message.
    *        recipientUri         - Receiver URI to be included in message.
    *       sakkeData            Contains the SAKKE encrypted data.
    *  Rets: None. There is no error/failure report.
    */
    void CreateMcPttIMessage(std::vector<uint8_t> &iMessage,
                             std::size_t &nextPayloadTypeIndex, bool v,
                             String &timestamp,  const std::string &senderUri,
                             const std::string &recipientUri, Buffer sakkeData);


    /**
    *  Creates an I_MESSAGE with HDR payload but no crypto sessions.
    *  Args: iMessage             - Will contain the created I_MESSAGE on exit.
    *        nextPayloadTypeIndex - Will contain the index for the next payload
    *                               type on exit.
    *        csbId                - Will be set to CSB ID on exit.
    *        v                    - Specifies if the V bit is to be set in the HDR
    *                               payload.
    *        hdrMsgType           - Header message type.
    *  Rets: None. There is no error/failure report.
    */
    void CreateIMessageWithHdrPayload(std::vector<std::uint8_t> &iMessage,
                                      std::size_t &nextPayloadTypeIndex,
                                      uint32_t &csbId, bool v,
                                      std::uint8_t hdrMsgType);


    /**
    *  Creates and appends a MIKEY T payload to an I_MESSAGE.
    *  Args: iMessage             - Will contain the created I_MESSAGE on exit.
    *        nextPayloadTypeIndex - Will contain the index for the next payload
    *                               type on exit.
    *        timestampString      - Hex string representation of a UTC-NTP
    *                               timestamp.
    *  Rets: None. There is no error/failure report.
    */
    void AppendTPayloadToIMessage(std::vector<std::uint8_t> &iMessage,
                                  std::size_t &nextPayloadTypeIndex,
                                  String &timestampString);


    /**
    *  Creates and appends a MIKEY RAND payload to an I_MESSAGE.
    *  Args: iMessage             - Message to be updated.
    *        nextPayloadTypeIndex - Will contain the index for the next payload
    *                               type on exit.
    *        rand                 - Will contain the random data in the message
    *                               on exit.
    *  Rets: None. There is no error/failure report.
    */
    void AppendRANDPayloadToIMessage(std::vector<std::uint8_t> &iMessage,
                                     std::size_t &nextPayloadTypeIndex,
                                     std::vector<std::uint8_t> & rand);

    /**
    * Creates and appends a MIKEY IDR payload to an I_MESSAGE.
    *  Args: iMessage             - Message to be updated.
    *        nextPayloadTypeIndex - Will contain the index for the next payload
    *                               type on exit.
    *        type                 - ID type.
    *        role                 - ID role.
    *        identity             - Identity.
    *  Rets: None. There is no error/failure report.
    */
    void AppendIDRPayloadToIMessage(std::vector<std::uint8_t> &iMessage,
                                    std::size_t &nextPayloadTypeIndex,
                                    const std::uint8_t type,
                                    const std::uint8_t role,
                                    const std::string &identity);


    /**
    *  Creates and appends a KEMAC payload to an I_MESSAGE.
    *  Args: iMessage             - Message to be updated.
    *        nextPayloadTypeIndex - Will contain the index for the next payload
    *                               type on exit.
    *        encryptionAlgorithm  - Algorithm used to encrypt the data.
    *        encryptedData        - Encrypted data (encrypted GSK).
    *        salt                 - Salt.
    *  Rets: None. There is no error/failure report.
    */
    void AppendKEMACPayloadToIMessage(std::vector<std::uint8_t> &iMessage,
                                      std::size_t &nextPayloadTypeIndex,
                                      std::uint8_t encryptionAlgorithm,
                                      Buffer encryptedData, Buffer salt);


    /**
    *  Creates and appends a MAC payload to an I_MESSAGE.
    *  Args: iMessage             - Message to be updated.
    *        nextPayloadTypeIndex - Will contain the index for the next payload
    *                               type on exit.
    *        macAlgorithm         - Algorithm used to create the MAC.
    *        mac                  - MAC.
    *  Rets: None. There is no error/failure report.
    *
    *  Note: This function appears to be in error, it does not add header
    *  information, and the parameter nextPayloadTypeIndex is unused.
    */
    void AppendMACPayloadToIMessage(std::vector<std::uint8_t> &iMessage,
                                    std::size_t &nextPayloadTypeIndex,
                                    std::uint8_t macAlgorithm, Buffer mac);


    /**
    *  Creates and appends a SAKKE payload to an I_MESSAGE.
    *  Args: iMessage             - Message to be updated.
    *        nextPayloadTypeIndex - Will contain the index for the next payload
    *                               type on exit.
    *        sakke                - SAKKE data.
    *  Rets: None. There is no error/failure report.
    */
    void AppendSAKKEPayloadToIMessage(std::vector<uint8_t> &iMessage,
                                      std::size_t &nextPayloadTypeIndex,
                                      Buffer sakke);


    /**
    *  Creates and appends a SIGN (signature) payload to an I_MESSAGE.
    *  Args: iMessage             - Message to be updated.
    *        nextPayloadTypeIndex - Will contain the index for the next payload
    *                               type on exit.
    *        type                 - Lower 4 bits are signature type.
    *        signature            - Signature data.
    *  Rets: None. There is no error/failure report.
    */
    void AppendSIGNPayloadToIMessage(std::vector<std::uint8_t> &iMessage,
                                     std::size_t nextPayloadTypeIndex,
                                     std::uint8_t type, Buffer signature);


    /**
    *  Parses an I_MESSAGE HDR payload.
    *  Args: iMessage             - Message to be parsed.
    *        index                - Index of the start of the payload to parse
    *                               within the message. On exit undated to point
    *                               to the next payload type.
    *        nextPayloadTypeIndex - Will be set to the index for the next
    *                               payload type on exit.
    *        csbid                - Will be set to the CSB-ID on exit.
    *        v                    - Will be set to the header V bit on exit.
    *        csMapType            - Will be set to the CS ID map type on exit.
    *  Rets: True if parsing successful, otherwise false.
    */
    bool ProcessHDRPayload(const std::vector<std::uint8_t> &iMessage,
                           std::vector<std::uint8_t>::const_iterator &index,
                           std::vector<std::uint8_t>::const_iterator
                             &nextPayloadTypeIndex,
                           std::uint32_t &csbid, bool &v,
                           std::uint8_t &csMapType);


    /**
    *  Parses an I_MESSAGE T payload.
    *  Args: iMessage             - Message to be parsed.
    *        index                - Index of the start of the payload to parse
    *                               within the message. On exit undated to point
    *                               to the next payload type.
    *        nextPayloadTypeIndex - Will be set to the index for the next
    *                               payload type on exit.
    *        timestamp            - Will be set to the NTP UTC timestamp
    *                               on exit.
    *  Rets: True if parsing successful, otherwise false.
    */
    bool ProcessTPayload(const std::vector<std::uint8_t> &iMessage,
                         std::vector<std::uint8_t>::const_iterator &index,
                         std::vector<std::uint8_t>::const_iterator
                           &nextPayloadTypeIndex,
                         std::uint64_t &timestamp);


    /**
    *  Parses an I_MESSAGE RAND payload, discarding random data.
    *  Args: iMessage             - Message to be parsed.
    *        index                - Index of the start of the payload to parse
    *                               within the message. On exit undated to point
    *                               to the next payload type.
    *        nextPayloadTypeIndex - Will be set to the index for the next
    *                               payload type on exit.
    *  Rets: True if parsing successful, otherwise false.
    */
    bool ProcessRANDPayload(const std::vector<std::uint8_t> &iMessage,
                            std::vector<std::uint8_t>::const_iterator &index,
                            std::vector<std::uint8_t>::const_iterator
                              &nextPayloadTypeIndex);


    /**
    *  Parses an I_MESSAGE RAND payload.
    *  Args: iMessage             - Message to be parsed.
    *        index                - Index of the start of the payload to parse
    *                               within the message. On exit undated to point
    *                               to the next payload type.
    *        nextPayloadTypeIndex - Will be set to the index for the next
    *                               payload type on exit.
    *        rndNum               - Will be set to the RAND payload on exit.
    *  Rets: True if parsing successful, otherwise false.
    */
    bool ProcessRANDPayload(const std::vector<std::uint8_t> &iMessage,
                            std::vector<std::uint8_t>::const_iterator &index,
                            std::vector<std::uint8_t>::const_iterator
                              &nextPayloadTypeIndex,
                            std::vector<std::uint8_t> &rndNum);


    /**
    *  Parses an I_MESSAGE IDR payload.
    *  Args: iMessage             - Message to be parsed.
    *        index                - Index of the start of the payload to parse
    *                               within the message. On exit undated to point
    *                               to the next payload type.
    *        nextPayloadTypeIndex - Will be set to the index for the next
    *                               payload type on exit.
    *        role                 - Will be set to the role on exit.
    *        identity             - Will be set to the identity on exit.
    *  Rets: True if parsing successful, otherwise false.
    */
    bool ProcessIDRPayload(const std::vector<std::uint8_t> &iMessage,
                           std::vector<std::uint8_t>::const_iterator &index,
                           std::vector<std::uint8_t>::const_iterator
                             &nextPayloadTypeIndex,
                           std::uint8_t &role, std::string &identity);


    /**
    *  Parses an I_MESSAGE KEMAC payload.
    *  Args: iMessage             - Message to be parsed.
    *        index                - Index of the start of the payload to parse
    *                               within the message. On exit undated to point
    *                               to the next payload type.
    *        nextPayloadTypeIndex - Will be set to the index for the next
    *                               payload type on exit.
    *        kemacEncType         - Will be set to KEMAC encryption type on
    *                               exit. (Only one should be defined according
    *                               to 3GPP TS 33.303 AES-CM-128.)
    *        kemacEncData         - Will be set to KEMAC encryption data
    *                               (encrypted GSK) on exit.
    *        kemacEncType         - Will be set to KEMAC MAC Type on exit. (Only
    *                               one should be defined according to
    *                               3GPP TS 33.303 HASH-SHA-256.)
    *        kemacEncType         - Will be set to KEMAC MAC on exit.
    *  Rets: True if parsing successful, otherwise false.
    */
    bool ProcessKEMACPayload(const std::vector<std::uint8_t> &iMessage,
                             std::vector<std::uint8_t>::const_iterator &index,
                             std::vector<std::uint8_t>::const_iterator
                               &nextPayloadTypeIndex,
                             std::uint8_t &kemacEncType,
                             std::vector<std::uint8_t> &kemacEnc,
                             std::vector<std::uint8_t> &kemacSalt,
                             std::uint8_t &kemacMacType,
                             std::vector<std::uint8_t> &kemacMac);


    /**
    *  Parses an I_MESSAGE SAKKE payload.
    *  Args: iMessage             - Message to be parsed.
    *        index                - Index of the start of the payload to parse
    *                               within the message. On exit undated to point
    *                               to the next payload type.
    *        nextPayloadTypeIndex - Will be set to the index for the next
    *                               payload type on exit.
    *        sakke                - Will be set to SAKKE payload on exit.
    *  Rets: True if parsing successful, otherwise false.
    */
    bool ProcessSAKKEPayload(const std::vector<std::uint8_t> &iMessage,
                             std::vector<std::uint8_t>::const_iterator &index,
                             std::vector<std::uint8_t>::const_iterator
                               &nextPayloadTypeIndex,
                             std::vector<std::uint8_t> &sakke);


    /**
    *  Parses an I_MESSAGE SIGN (signature) payload.
    *  Args: iMessage             - Message to be parsed.
    *        index                - Index of the start of the payload to parse
    *                               within the message. On exit undated to point
    *                               to the next payload type.
    *        nextPayloadTypeIndex - Will be set to the index for the next
    *                               payload type on exit.
    *        signature            - Will be set to signature on exit.
    *  Rets: True if parsing successful, otherwise false.
    */
    bool ProcessSIGNPayload(const std::vector<std::uint8_t> &iMessage,
                            std::vector<std::uint8_t>::const_iterator &index,
                            std::vector<std::uint8_t>::const_iterator
                              &nextPayloadTypeIndex,
                            std::vector<std::uint8_t> &signature);
};

#endif

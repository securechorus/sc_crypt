// Helper functions that manipulate MIKEY payloads and I_MESSAGES.

// Header file inclusions.
#include <cstddef>
#include <cstdint>
#include <vector>

#include "payload.h"

#include "helper.h"
#include "sc_misc.h"
#include "sc_types.h"


// Using declarations.
using namespace std;


//  Constant definitions.
namespace
{
    const uint8_t MIKEY_RAND_LENGTH = 16;
}


/**
*  Creates a SAKKE I_MESSAGE from supplied data. The message is not signed.
*  An IDRi payload is only present in the I_MESSAGE if sender URI is not
*  empty.
*  Args: iMessage             - Will contain the created I_MESSAGE on exit.
*        nextPayloadTypeIndex - Will contain the index for the next payload
*                               type upon exit.
*        csbId                - Will contain the CSB_ID on exit.
*        rand                 - Will contain the random data in the message
*                               on exit.
*        v                    - Specifies if the V bit is to be set in the
*                               HDR payload.
*        timestamp            - Byte string representation of a UTC-NTP
*                               timestamp to be included in message.
*        senderUri            - Sender URI to be included in message.
*        recipientUri         - Receiver URI to be included in message.
*        sakkeData            - SAKKE encrypted data to be included in
*                               message.
*/
void ScLibMs::CreateIMessage(vector<uint8_t> &iMessage,
                             size_t &nextPayloadTypeIndex, uint32_t &csbId,
                             std::vector<uint8_t> &rand, bool v,
                             String &timestamp, const string &senderUri,
                             const string &recipientUri, Buffer sakkeData)
{
    // Create and assemble the I_MESSAGE from various MIKEY payloads.
    CreateIMessageWithHdrPayload(iMessage, nextPayloadTypeIndex, csbId, v,
                                 defaultMsgType);
    AppendTPayloadToIMessage(iMessage, nextPayloadTypeIndex, timestamp);
    AppendRANDPayloadToIMessage(iMessage, nextPayloadTypeIndex, rand);

    // Append an IDR payload for the sender (initiator) if a sender is
    // specified.
    if (!senderUri.empty())
    {
        AppendIDRPayloadToIMessage(iMessage, nextPayloadTypeIndex,
                                   MIKEY_PAYLOAD_IDR_TYPE_URI,
                                   MIKEY_PAYLOAD_IDR_ROLE_IDRI, senderUri);
    }

    // Append an IDR payload for the recipient.
    AppendIDRPayloadToIMessage(iMessage, nextPayloadTypeIndex,
                               MIKEY_PAYLOAD_IDR_TYPE_URI,
                               MIKEY_PAYLOAD_IDR_ROLE_IDRR, recipientUri);

    // Append a SAKKE payload.
    AppendSAKKEPayloadToIMessage(iMessage, nextPayloadTypeIndex, sakkeData);
}


/**
*  Creates a GROUP GMK SAKKE I_MESSAGE from supplied data. The message is not
*  signed.
*  Args: iMessage             - Will contain the created I_MESSAGE on exit.
*        nextPayloadTypeIndex - Will contain the index for the next payload
*                               type on exit.
*        v                    - Specifies if the V bit is to be set in the HDR
*                               payload.
*        timestamp            - Byte string representation of a UTC-NTP
*                               timestamp to be included in message.
*        senderUri            - Sender URI to be included in message.
*        recipientUri         - Receiver URI to be included in message.
*        sakkeData            - SAKKE encrypted data to be included in message
*  Rets: None. There is no error/failure report.
*/
void ScLibMs::CreateGmkIMessage(vector<uint8_t> &iMessage,
                                size_t &nextPayloadTypeIndex, bool v,
                                String &timestamp,
                                const string &senderUri,
                                const string &recipientUri,
                                Buffer sakkeData)
{
    // Create and assemble the I_MESSAGE from various MIKEY payloads.
    uint32_t csbId;
    CreateIMessageWithHdrPayload(iMessage, nextPayloadTypeIndex, csbId, v,
                                 GMKMsgType);
    AppendTPayloadToIMessage(iMessage, nextPayloadTypeIndex, timestamp);
    std::vector<uint8_t> rand;
    AppendRANDPayloadToIMessage(iMessage, nextPayloadTypeIndex, rand);

    // Refer to Appendix D.2 of 3GPP TS 33.303 Version 12.20.0 Release 12
    // para 5. The IDR_ROLE should be value 3. ID_TYPE field to URI (1).
    // Sets role to KMS MIKEY_PAYLOAD_IDR_ROLE_IDRKMS (3).
    AppendIDRPayloadToIMessage(iMessage, nextPayloadTypeIndex,
                               MIKEY_PAYLOAD_IDR_TYPE_URI,
                               MIKEY_PAYLOAD_IDR_ROLE_IDRKMS, senderUri);

    // Append an IDR payload for the recipient.
    // See TS 33.303 v12..0 Rel 12 Section D.2 para 5.
    AppendIDRPayloadToIMessage(iMessage, nextPayloadTypeIndex,
                               MIKEY_PAYLOAD_IDR_TYPE_URI,
                               MIKEY_PAYLOAD_IDR_ROLE_IDRI, recipientUri);

    // Append a SAKKE payload.
    AppendSAKKEPayloadToIMessage(iMessage, nextPayloadTypeIndex, sakkeData);
}


/**
*  Creates a GROUP GSK SAKKE I_MESSAGE from supplied data. The message is
*  not signed. KEMAC is not added at this stage, the caller will add it.
*  This is because the AES encryption needs the CSB-ID and timestamp
*  from this part of the message construction.
*  Args: iMessage             - Will contain the created I_MESSAGE on exit.
*        nextPayloadTypeIndex - Will contain the index for the next payload
*                               type on exit.
*        v                    - Specifies if the V bit is to be set in the HDR
*                               payload.
*        timestamp            - Byte string representation of a UTC-NTP
*                               timestamp to be included in message.
*        senderUri            - Sender URI to be included in message.
*        recipientUri         - Receiver URI to be included in message.
*  Rets: None. There is no error/failure report.
*/
void ScLibMs::CreateGskIMessage(vector<uint8_t> &iMessage,
                                size_t &nextPayloadTypeIndex, bool v,
                                String &timestamp, const string &senderUri,
                                const string &recipientUri)
{
    // Add header.
    uint32_t csbId;
    CreateIMessageWithHdrPayload(iMessage, nextPayloadTypeIndex, csbId, v,
                                 GSKMsgType);

    // Time must ABSOLUTELY NOT be removed - required for GSK AES enctryption
    // and ending a loop checking partially built I_MESSAGE, see
    // CreateMikeySakkeGSKIMessage.
    AppendTPayloadToIMessage(iMessage, nextPayloadTypeIndex, timestamp);

    // Add RAND to message.
    std::vector<uint8_t> rand;
    AppendRANDPayloadToIMessage(iMessage, nextPayloadTypeIndex, rand);

    // Refer to Appendix D.2 of 3GPP TS 33.303 Version 12.20.0 Release 12
    // para 5. The IDR_ROLE should be value 3. ID_TYPE field to URI (1).
    AppendIDRPayloadToIMessage(iMessage, nextPayloadTypeIndex,
                               MIKEY_PAYLOAD_IDR_TYPE_URI,
                               MIKEY_PAYLOAD_IDR_ROLE_IDRI, senderUri);

    // Append an IDR payload for the recipient.
    AppendIDRPayloadToIMessage(iMessage, nextPayloadTypeIndex,
                               MIKEY_PAYLOAD_IDR_TYPE_URI,
                               MIKEY_PAYLOAD_IDR_ROLE_IDRR, recipientUri);
}


/**
*  Creates a GROUP MC PTT SAKKE I_MESSAGE from supplied data. The message
*  is not signed.
*  Args: iMessage             - Will contain the created I_MESSAGE on exit.
*        nextPayloadTypeIndex - Will contain the index for the next payload
*                               type on exit.
*        v                    - Specifies if the V bit is to be set in the HDR
*                               payload.
*        timestamp            - Byte string representation of a UTC-NTP
*                               timestamp to be included in message.
*        senderUri            - Sender URI to be included in message.
*        recipientUri         - Receiver URI to be included in message.
*       sakkeData            Contains the SAKKE encrypted data.
*  Rets: None. There is no error/failure report.
*/
void ScLibMs::CreateMcPttIMessage(vector<uint8_t> &iMessage,
                                  size_t &nextPayloadTypeIndex, bool v,
                                  String &timestamp, const string &senderUri,
                                  const string &recipientUri, Buffer sakkeData)
{
    // Add headers.
    uint32_t csbId;
    CreateIMessageWithHdrPayload(iMessage, nextPayloadTypeIndex,
                                 csbId, v, MCPTTMsgType);

    AppendTPayloadToIMessage(iMessage, nextPayloadTypeIndex, timestamp);
    std::vector<uint8_t> rand;
    AppendRANDPayloadToIMessage(iMessage, nextPayloadTypeIndex, rand);

    // Refer to Appendix D.2 of 3GPP TS 33.303 Version 12.20.0 Release 12
    // para 5. The IDR_ROLE should be value 3. ID_TYPE field to URI (1).

    // Sets role to KMS MIKEY_PAYLOAD_IDR_ROLE_IDRKMS (3).
    AppendIDRPayloadToIMessage(iMessage, nextPayloadTypeIndex,
                               MIKEY_PAYLOAD_IDR_TYPE_URI,
                               MIKEY_PAYLOAD_IDR_ROLE_IDRKMS, senderUri);

    // Append an IDR payload for the recipient.
    AppendIDRPayloadToIMessage(iMessage, nextPayloadTypeIndex,
                               MIKEY_PAYLOAD_IDR_TYPE_URI,
                               MIKEY_PAYLOAD_IDR_ROLE_IDRR, recipientUri);

    // Append a SAKKE payload.
    AppendSAKKEPayloadToIMessage(iMessage, nextPayloadTypeIndex, sakkeData);
}


/**
* Creates an I_MESSAGE with HDR payload but no crypto sessions.
*  Args: iMessage             - Will contain the created I_MESSAGE on exit.
*        nextPayloadTypeIndex - Will contain the index for the next payload
*                               type on exit.
*        csbId                - Will be set to CSB ID on exit.
*        v                    - Specifies if the V bit is to be set in the HDR
*                               payload.
*        hdrMsgType           - Header message type.
*  Rets: None. There is no error/failure report.
*/
void ScLibMs::CreateIMessageWithHdrPayload(vector<uint8_t> &iMessage,
                                           size_t &nextPayloadTypeIndex,
                                           uint32_t &csbId, bool v,
                                           uint8_t hdrMsgType)
{
    // Initialise message.
    iMessage.clear();

    // Put HDR values into message.
    VectorAppend(iMessage, MIKEY_PAYLOAD_HDR_VERSION);
    VectorAppend(iMessage, MIKEY_PAYLOAD_HDR_TYPE_SAKKE);
    VectorAppend(iMessage, MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST);
    const uint8_t PRFfunc
      = (v ? 0x80 : 0x00) | MIKEY_PAYLOAD_HDR_PRF_HMAC_SHA_256;
    VectorAppend(iMessage, PRFfunc);

    // Create a random CSB ID if required.
    switch (hdrMsgType)
    {
        case GMKMsgType:      // GMK - leave empty.
            break;
        case defaultMsgType:  // P2P.
        case GSKMsgType:      // GSK.
        case MCPTTMsgType:    // PTT (proposal).
        default:
        {
            uint8_t csb_id[sizeof(csbId)];
            Buffer csb_id_buf;
            csb_id_buf.length = sizeof(csbId);
            csb_id_buf.pointer = csb_id;
            GetSecureRandom(csb_id_buf);
            FromNetworkBytes(csb_id, csbId);
            break;
        }
    }

    // Put more HDR values into message.
    VectorAppend(iMessage, csbId);
    const uint8_t numberCS = 0;
    VectorAppend(iMessage, numberCS);
    VectorAppend(iMessage, MIKEY_PAYLOAD_HDR_CS_ID_MAP_TYPE);

    // Set next payload index.
    nextPayloadTypeIndex = 2;
}


/**
*  Creates and appends a MIKEY T payload to an I_MESSAGE.
*  Args: iMessage             - Will contain the created I_MESSAGE on exit.
*        nextPayloadTypeIndex - Will contain the index for the next payload
*                               type on exit.
*        timestampString      - Hex string representation of a UTC-NTP
*                               timestamp.
*  Rets: None. There is no error/failure report.
*/
void ScLibMs::AppendTPayloadToIMessage(vector<uint8_t> &iMessage,
                                       size_t &nextPayloadTypeIndex,
                                       String &timestampString)
{
    // Update next payload
    iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_T;
    nextPayloadTypeIndex = iMessage.size();

    // Append payload, including NTP-UTP timestamp, to the message.
    VectorAppend(iMessage, MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST);
    const uint8_t TStype = MIKEY_PAYLOAD_T_TYPE_NTP_UTC;
    VectorAppend(iMessage, TStype);
    const uint64_t TSvalue = HexStringToU64(timestampString);
    VectorAppend(iMessage, TSvalue);
}


/**
*  Creates and appends a MIKEY RAND payload to an I_MESSAGE.
*  Args: iMessage             - Message to be updated.
*        nextPayloadTypeIndex - Will contain the index for the next payload
*                               type on exit.
*        rand                 - Will contain the random data in the message
*                               on exit.
*  Rets: None. There is no error/failure report.
*/
void ScLibMs::AppendRANDPayloadToIMessage(std::vector<std::uint8_t> &iMessage,
                                          std::size_t &nextPayloadTypeIndex,
                                          std::vector<std::uint8_t> & rand)
{
    // Update next payload.
    iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_RAND;
    nextPayloadTypeIndex = iMessage.size();

    // Create RAND data.
    rand.resize(MIKEY_RAND_LENGTH);
    Buffer rand_buf;
    rand_buf.length = rand.size();
    rand_buf.pointer = rand.data();
    GetSecureRandom(rand_buf);

    // Append the payload to the message.
    const uint8_t nextPayload = MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST;
    VectorAppend(iMessage, nextPayload);
    VectorAppend(iMessage, MIKEY_RAND_LENGTH);
    VectorAppendVector(iMessage, rand);
}


/**
* Creates and appends a MIKEY IDR payload to an I_MESSAGE.
*  Args: iMessage             - Message to be updated.
*        nextPayloadTypeIndex - Will contain the index for the next payload
*                               type on exit.
*        type                 - ID type.
*        role                 - ID role.
*        identity             - Identity.
*  Rets: None. There is no error/failure report.
*/
void ScLibMs::AppendIDRPayloadToIMessage(vector<uint8_t> &iMessage,
                                         size_t &nextPayloadTypeIndex,
                                         const uint8_t type,
                                         const uint8_t role,
                                         const string &identity)
{
    // Verify identity size can be stored in two bytes.
    if (identity.size() > UINT16_MAX)
    {
        return;
    }

    // Update next payload.
    iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_IDR;
    nextPayloadTypeIndex = iMessage.size();

    // Append payload to message.
    VectorAppend(iMessage, MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST);
    VectorAppend(iMessage, role);
    VectorAppend(iMessage, type);
    const uint16_t length = identity.size();
    VectorAppend(iMessage, length);
    VectorAppendString(iMessage, identity);
}


/**
*  Creates and appends a KEMAC payload to an I_MESSAGE.
*  Args: iMessage             - Message to be updated.
*        nextPayloadTypeIndex - Will contain the index for the next payload
*                               type on exit.
*        encryptionAlgorithm  - Algorithm used to encrypt the data.
*        encryptedData        - Encrypted data (encrypted GSK).
*        salt                 - Salt.
*  Rets: None. There is no error/failure report.
*/
void ScLibMs::AppendKEMACPayloadToIMessage(std::vector<uint8_t> &iMessage,
                                           size_t &nextPayloadTypeIndex,
                                           uint8_t encryptionAlgorithm,
                                           Buffer encryptedData, Buffer salt)
{
    // Update next payload.
    iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_KEMAC;
    nextPayloadTypeIndex = iMessage.size();

    // Append the payload to the message.
    VectorAppend(iMessage, MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST);
    int c = 0;
    VectorAppend(iMessage, encryptionAlgorithm);

    // 6 is for nextPayload, encryptionAlgorithm and two lengths.
    const uint16_t length = 6 + salt.length + encryptedData.length;
    VectorAppend(iMessage, length);

    // Add encrypted GSK data sub payload.
    //
    // Type options are:
    //   0  TGK
    //   1  TGK+SALT
    //   2  TEK
    //   3  TEK+SALT
    // Would use TGK+SALT, however TS 33.303 Appendix D paragraph 8
    // states "the Type subfield shall be set to TGK (value 0)."
    //
    // KV options are:
    //   0  No specific usage
    //   1  SPI
    //   2 Interval
    // Here use 0, hence no KV (optional) at end of payload.

    //  Last payload.
    VectorAppend(iMessage, MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST);

    // See RFC 3830 Section 6.13 (key data sub payload).
    const uint8_t typeAndKV = 0x00;
    VectorAppend(iMessage, typeAndKV);
    VectorAppend(iMessage, uint16_t(encryptedData.length));
    VectorAppendBuffer(iMessage, encryptedData);
    VectorAppend(iMessage, uint16_t(salt.length));
    VectorAppendBuffer(iMessage, salt);
}


/**
*  Creates and appends a MAC payload to an I_MESSAGE.
*  Args: iMessage             - Message to be updated.
*        nextPayloadTypeIndex - Will contain the index for the next payload
*                               type on exit.
*        macAlgorithm         - Algorithm used to create the MAC.
*        mac                  - MAC.
*  Rets: None. There is no error/failure report.
*
*  Note: This function appears to be in error, it does not add header
*  information, and the parameter nextPayloadTypeIndex is unused.
*/
void ScLibMs::AppendMACPayloadToIMessage(std::vector<uint8_t> &iMessage,
                                         size_t &nextPayloadTypeIndex,
                                         uint8_t macAlgorithm,
                                         Buffer mac)
{
    VectorAppend(iMessage, macAlgorithm);
    VectorAppendBuffer(iMessage, mac);
}


/**
*  Creates and appends a SAKKE payload to an I_MESSAGE.
*  Args: iMessage             - Message to be updated.
*        nextPayloadTypeIndex - Will contain the index for the next payload
*                               type on exit.
*        sakke                - SAKKE data.
*  Rets: None. There is no error/failure report.
*/
void ScLibMs::AppendSAKKEPayloadToIMessage(vector<uint8_t> &iMessage,
                                           size_t &nextPayloadTypeIndex,
                                           Buffer sakke)
{
    uint8_t nextPayload = MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST;
    uint8_t params = 1;     // Specify using SAKKE parameter set 1. RFC 6509.
    uint8_t idScheme = 1;   // Using tel URI with monthly keys scheme.

    // Update next payload.
    iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_SAKKE;
    nextPayloadTypeIndex = iMessage.size();

    // Append the payload to the message.
    VectorAppend(iMessage, nextPayload);
    VectorAppend(iMessage, params);
    VectorAppend(iMessage, idScheme);
    VectorAppend(iMessage, uint16_t(sakke.length));
    VectorAppendBuffer(iMessage, sakke);
}


/**
*  Creates and appends a SIGN (signature) payload to an I_MESSAGE.
*  Args: iMessage             - Message to be updated.
*        nextPayloadTypeIndex - Will contain the index for the next payload
*                               type on exit.
*        type                 - Lower 4 bits are signature type.
*        signature            - Signature data.
*  Rets: None. There is no error/failure report.
*/
void ScLibMs::AppendSIGNPayloadToIMessage(vector<uint8_t> &iMessage,
                                          size_t nextPayloadTypeIndex,
                                          uint8_t type, Buffer signature)
{
    // Next payload type set before signing. 4 bits type and 12 bits length.
    uint16_t typeAndLength = (type & 0x0F);
    typeAndLength = (typeAndLength << 12) | (signature.length & 0x0FFF);
    VectorAppend(iMessage, typeAndLength);

    // Append the signature to the message.
    VectorAppendBuffer(iMessage, signature);
}


/**
*  Parses an I_MESSAGE HDR payload.
*  Args: iMessage             - Message to be parsed.
*        index                - Index of the start of the payload to parse
*                               within the message. On exit undated to point
*                               to the next payload type.
*        nextPayloadTypeIndex - Will be set to the index for the next
*                               payload type on exit.
*        csbid                - Will be set to the CSB-ID on exit.
*        v                    - Will be set to the header V bit on exit.
*        csMapType            - Will be set to the CS ID map type on exit.
*  Rets: True if parsing successful, otherwise false.
*/
bool ScLibMs::ProcessHDRPayload(const vector<uint8_t> &iMessage,
                                vector<uint8_t>::const_iterator &index,
                                vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
                                uint32_t &csbid,
                                bool &v,
                                uint8_t &csMapType)
{
    // Read version and type and check values.
    uint8_t version;

    if (!VectorRead(iMessage, index + 0, version)
        || version != MIKEY_PAYLOAD_HDR_VERSION)
    {
        return false;
    }

    uint8_t type;
    if (!VectorRead(iMessage, index + 1, type)
        || type != MIKEY_PAYLOAD_HDR_TYPE_SAKKE)
    {
        return false;
    }

    // Read PRF function type and V bit and verify PRF function type.
    uint8_t PRFfunc;

    if (!VectorRead(iMessage, index + 3, PRFfunc))
    {
        return false;
    }

    v = (PRFfunc & 0x80) != 0x00;
    PRFfunc &= 0x7F;

    if (PRFfunc != MIKEY_PAYLOAD_HDR_PRF_HMAC_SHA_256)
    {
        return false;
    }

    // Read CSB ID and number of crypto sessions, and verify latter is zero.
    if (!VectorRead(iMessage, index + 4, csbid))
    {
        return false;
    }

    uint8_t csnum;

    if (!VectorRead(iMessage, index + 8, csnum) || csnum != 0)
    {
        return false;
    }

    // Read map type.
    if (!VectorRead(iMessage, index + 9, csMapType))
    {
        return false;
    }

    // Update iterators and return successful parse.
    nextPayloadTypeIndex = index + 2;
    index += 10;
    return true;
};


/**
*  Parses an I_MESSAGE T payload.
*  Args: iMessage             - Message to be parsed.
*        index                - Index of the start of the payload to parse
*                               within the message. On exit undated to point
*                               to the next payload type.
*        nextPayloadTypeIndex - Will be set to the index for the next
*                               payload type on exit.
*        timestamp            - Will be set to the NTP UTC timestamp
*                               on exit.
*  Rets: True if parsing successful, otherwise false.
*/
bool ScLibMs::ProcessTPayload(const vector<uint8_t> &iMessage,
                              vector<uint8_t>::const_iterator &index,
                              vector<uint8_t>::const_iterator
                                &nextPayloadTypeIndex,
                              uint64_t &timestamp)
{
    // Read and check type; only support NTP_UTC time.
    uint8_t type;

    if (!VectorRead(iMessage, index + 1, type)
        || type != MIKEY_PAYLOAD_T_TYPE_NTP_UTC)
    {
        return false;
    }

    // Read timestamp.
    if (!VectorRead(iMessage, index + 2, timestamp))
    {
        return false;
    }

    // Update iterators and return successfully,
    nextPayloadTypeIndex = index;
    index += 10;
    return true;
}


/**
*  Parses an I_MESSAGE RAND payload, discarding random data.
*  Args: iMessage             - Message to be parsed.
*        index                - Index of the start of the payload to parse
*                               within the message. On exit undated to point
*                               to the next payload type.
*        nextPayloadTypeIndex - Will be set to the index for the next
*                               payload type on exit.
*  Rets: True if parsing successful, otherwise false.
*/
bool ScLibMs::ProcessRANDPayload(const vector<uint8_t> &iMessage,
                                 vector<uint8_t>::const_iterator &index,
                                 vector<uint8_t>::const_iterator
                                   &nextPayloadTypeIndex)
{
    vector<uint8_t> rndNum;
    return ProcessRANDPayload(iMessage, index, nextPayloadTypeIndex, rndNum);
}


/**
*  Parses an I_MESSAGE RAND payload.
*  Args: iMessage             - Message to be parsed.
*        index                - Index of the start of the payload to parse
*                               within the message. On exit undated to point
*                               to the next payload type.
*        nextPayloadTypeIndex - Will be set to the index for the next
*                               payload type on exit.
*        rndNum               - Will be set to the RAND payload on exit.
*  Rets: True if parsing successful, otherwise false.
*/
bool ScLibMs::ProcessRANDPayload(const vector<uint8_t> &iMessage,
                                 vector<uint8_t>::const_iterator &index,
                                 vector<uint8_t>::const_iterator
                                   &nextPayloadTypeIndex,
                                 vector<uint8_t> &rndNum)
{
    // Read RAND length and check (RFC 3830 section 6.11).
    uint8_t length;

    if (!VectorRead(iMessage, index + 1, length) || length < 16)
    {
        return false;
    }

    // Read RAND values.
    if (!VectorReadVector(iMessage, index + 2, length, rndNum))
    {
        return false;
    }

    // Update iterators and return successfully.
    nextPayloadTypeIndex = index;
    index += 2 + length;
    return true;
}


/**
*  Parses an I_MESSAGE IDR payload.
*  Args: iMessage             - Message to be parsed.
*        index                - Index of the start of the payload to parse
*                               within the message. On exit undated to point
*                               to the next payload type.
*        nextPayloadTypeIndex - Will be set to the index for the next
*                               payload type on exit.
*        role                 - Will be set to the role on exit.
*        identity             - Will be set to the identity on exit.
*  Rets: True if parsing successful, otherwise false.
*/
bool ScLibMs::ProcessIDRPayload(const vector<uint8_t> &iMessage,
                                vector<uint8_t>::const_iterator &index,
                                vector<uint8_t>::const_iterator
                                  &nextPayloadTypeIndex,
                                uint8_t &role, string &identity)
{
    // Read role.
    if (!VectorRead(iMessage, index + 1, role))
    {
        return false;
    }

    // Read and check type.
    uint8_t type;

    if (!VectorRead(iMessage, index + 2, type)
        || type != MIKEY_PAYLOAD_IDR_TYPE_URI)
    {
        return false;
    }

    // Read length.
    uint16_t length;

    if (!VectorRead(iMessage, index + 3, length))
    {
        return false;
    }

    // Read identity.
    if (!VectorReadString(iMessage, index + 5, length, identity))
    {
        return false;
    }

    // Update iterators and return successfully.
    nextPayloadTypeIndex = index;
    index += 5 + length;
    return true;
}


/**
*  Parses an I_MESSAGE KEMAC payload.
*  Args: iMessage             - Message to be parsed.
*        index                - Index of the start of the payload to parse
*                               within the message. On exit undated to point
*                               to the next payload type.
*        nextPayloadTypeIndex - Will be set to the index for the next
*                               payload type on exit.
*        kemacEncType         - Will be set to KEMAC encryption type on
*                               exit. (Only one should be defined according
*                               to 3GPP TS 33.303 AES-CM-128.)
*        kemacEncData         - Will be set to KEMAC encryption data
*                               (encrypted GSK) on exit.
*        kemacEncType         - Will be set to KEMAC MAC Type on exit. (Only
*                               one should be defined according to
*                               3GPP TS 33.303 HASH-SHA-256.)
*        kemacEncType         - Will be set to KEMAC MAC on exit.
*  Rets: True if parsing successful, otherwise false.
*/
bool ScLibMs::ProcessKEMACPayload(const vector<uint8_t> &iMessage,
                                  std::vector<std::uint8_t>::const_iterator
                                    &index,
                                  std::vector<std::uint8_t>::const_iterator
                                    &nextPayloadTypeIndex,
                                  std::uint8_t &kemacEncType,
                                  std::vector<std::uint8_t> &kemacEnc,
                                  std::vector<std::uint8_t> &kemacSalt,
                                  std::uint8_t &kemacMacType,
                                  std::vector<std::uint8_t> &kemacMac)
{
    // Read values starting from index.
    nextPayloadTypeIndex = index;
    index += 1;

    if (!VectorRead(iMessage, index, kemacEncType))
    {
        return false;
    }

    index += 1;
    uint16_t encLength;

    if (!VectorRead(iMessage, index, encLength))
    {
        return false;
    }

    index += 2;

    // Read sub payload - see RFC 3830 Section 6.13.

    // Encrypted GSK.
    uint8_t nextPayload;

    if (!VectorRead(iMessage, index, nextPayload))
    {
        return false;
    }

    index += 1;
    uint8_t typeAndKv;

    if (!VectorRead(iMessage, index, typeAndKv))
    {
        return false;
    }

    index += 1;

    // Encrypted Key.
    uint16_t dataLen;

    if (!VectorRead(iMessage, index, dataLen))
    {
        return false;
    }

    index += 2;

    if (!VectorReadVector(iMessage, index, dataLen, kemacEnc))
    {
        return false;
    }

    index += dataLen;

    // Salt.
    uint16_t kemacSaltLen;

    if (!VectorRead(iMessage, index, kemacSaltLen))
    {
        return false;
    }

    index += 2;

    if (!VectorReadVector(iMessage, index, kemacSaltLen, kemacSalt))
    {
        return false;
    }

    index += kemacSaltLen;

    // Only one MAC type defined in 3GPP TS 33.303: HMAC SHA-256-256.
    // So, length is 32 bytes.
    if (!VectorRead(iMessage, index, kemacMacType))
    {
        return false;
    }

    index += 1;

    if (!VectorReadVector(iMessage, index, 32, kemacMac))
    {
        return false;
    }

    index += 32;
    return true;
}


/**
*  Parses an I_MESSAGE SAKKE payload.
*  Args: iMessage             - Message to be parsed.
*        index                - Index of the start of the payload to parse
*                               within the message. On exit undated to point
*                               to the next payload type.
*        nextPayloadTypeIndex - Will be set to the index for the next
*                               payload type on exit.
*        sakke                - Will be set to SAKKE payload on exit.
*  Rets: True if parsing successful, otherwise false.
*/
bool ScLibMs::ProcessSAKKEPayload(const vector<uint8_t> &iMessage,
                                  vector<uint8_t>::const_iterator &index,
                                  vector<uint8_t>::const_iterator
                                    &nextPayloadTypeIndex,
                                  vector<uint8_t> &sakke)
{
    // Read parameters, scheme and length.
    uint8_t params;

    if (!VectorRead(iMessage, index + 1, params) || params != 1)
    {
        return false;
    }

    uint8_t scheme;

    if (!VectorRead(iMessage, index + 2, scheme) || scheme != 1)
    {
        return false;
    }

    uint16_t length;

    if (!VectorRead(iMessage, index + 3, length))
    {
        return false;
    }

    // Read SAKKE payload.
    if (!VectorReadVector(iMessage, index + 5, length, sakke))
    {
        return false;
    }

    // Update iterators and return successfully.
    nextPayloadTypeIndex = index;
    index += 5 + length;
    return true;
}


/**
*  Parses an I_MESSAGE SIGN (signature) payload.
*  Args: iMessage             - Message to be parsed.
*        index                - Index of the start of the payload to parse
*                               within the message. On exit undated to point
*                               to the next payload type.
*        nextPayloadTypeIndex - Will be set to the index for the next
*                               payload type on exit.
*        signature            - Will be set to signature on exit.
*  Rets: True if parsing successful, otherwise false.
*/
bool ScLibMs::ProcessSIGNPayload(const vector<uint8_t> &iMessage,
                                 vector<uint8_t>::const_iterator &index,
                                 vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
                                 vector<uint8_t> &signature)
{
    // Read length and set and check type.
    uint16_t length;

    if (!VectorRead(iMessage, index, length))
    {
        return false;
    }

    const uint8_t type = (length >> 12) & 0x000F;
    length &= 0x0FFF;

    if (type != MIKEY_PAYLOAD_SIGN_TYPE_ECCSI)
    {
        return false;
    }

    // Read signature.
    if (!VectorReadVector(iMessage, index + 2, length, signature))
    {
        return false;
    }

    // Update index and return successfully.
    index += 2 + length;
    return true;
}

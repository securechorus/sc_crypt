// Header file inclusions.
#include <algorithm>
#include <cstdint>
#include <string>
#include <vector>

#include "sc_libms.h"

#include "helper.h"
#include "payload.h"
#include "sc_errno.h"
#include "sc_libmscrypto.h"
#include "sc_misc.h"


//  Constant values.
const std::size_t IMESSAGE_RAND_BYTES = 16;


//  Using directive.
using namespace std;


namespace ScLibMs
{
    namespace
    {
        /**
        *  Creates a MIKEY-SAKKE I_MESSAGE and a shared secret value.
        *  The I_MESSAGE will be signed using ECCSI if a non empty snder URI is
        *  provided.
        *  Args: iMessage    - Vector that will contain the I_MESSAGE (output).
        *        ssv         - Vector that will contain the shared secret value
        *                      (output).
        *        csbId       - Will contain the CSB_ID on exit.
        *        rand        - Vector that will contain the random data in the
        *                      message on exit.
        *        secCtx      - Security context (input).
        *        senderUri   - URI of the sender (input).
        *        recipientUi - URI of the recipient (input).
        *        v           - Required state of the HDR payload v bit (input).
        *  Rets: Standard error code.
        */
        ScErrno CreateMikeySakkeIMessage(vector<uint8_t> &iMessage,
                                         vector<uint8_t> &ssv, uint32_t &csbId,
                                         vector<uint8_t> &rand, SecCtx secCtx,
                                         string const &senderUri,
                                         string const &recipientUri, bool v)
        {
            // Variable to keep track of index of the next payload type byte.
            size_t nextPayloadTypeIndex = 0;

        // String versions of URI.
        String senderUriString;
        String recipientUriString;
        SetStringDescriptor(senderUriString, senderUri);
        SetStringDescriptor(recipientUriString, recipientUri);

        // Buffers for communication with SC Crypto Library.
        Buffer ssvData;
        Buffer sakkeData;

        // Get current timestamp.
        String timestampString;
        int year, month, day;
        ScErrno status = GetDateTime(&timestampString, &year, &month, &day);

        if (status != ScErrno::SUCCESS)
        {
            iMessage.clear();
            return status;
        }

        // Check recipient URI.
        if (recipientUri.empty())
        {
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        else if (!IsTheirUriReady(secCtx, recipientUriString,
                                  timestampString))
        {
            iMessage.clear();
            return ScErrno::RECIPIENT_URI_NOT_PROVISIONED;
        }

        // Check sender URI.
        if (!senderUri.empty()
            && !IsMyUriReady(secCtx, senderUriString, timestampString))
        {
            iMessage.clear();
            return ScErrno::SENDING_URI_NOT_PROVISIONED;
        }

        // Generate a shared secret and SAKKE encrypt.
        status = GenerateSharedSecretAndSakkeEncrypt(secCtx, recipientUriString,
                                                     timestampString, &sakkeData,
                                                     &ssvData);

        if (status != ScErrno::SUCCESS)
        {
            iMessage.clear();
            return status;
        }

        // Update ssv vector with SSV data.
        VectorFromBuffer(ssv, ssvData);

        // Create the MIKEY SAKKE I_MESSAGE.
        CreateIMessage(iMessage, nextPayloadTypeIndex, csbId, rand, v,
                       timestampString, senderUri, recipientUri, sakkeData);

        // Sign message if senderUri is not empty.
        if (!senderUri.empty())
        {
            Buffer iMessageData;
            Buffer signatureData;

            // Set the next payload type field early as it is within the signed
            // message.
            iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_SIGN;

            SetBufferDescriptor(iMessageData, iMessage);

            // Create signature.
            status = EccsiSign(secCtx, senderUriString, timestampString,
                               iMessageData, &signatureData);

            if (status != ScErrno::SUCCESS)
            {
                iMessage.clear();
                return status;
            }

            // Append signature to I_MESSAGE.
            AppendSIGNPayloadToIMessage(iMessage, nextPayloadTypeIndex,
                                        MIKEY_PAYLOAD_SIGN_TYPE_ECCSI,
                                        signatureData);
        }

            return ScErrno::SUCCESS;
        }
    }


    /**
    *  Creates a MIKEY-SAKKE I_MESSAGE and a shared secret value.
    *  The I_MESSAGE will be signed using ECCSI if a non empty snder URI is
    *  provided.
    *  Args: iMessage    - Vector that will contain the I_MESSAGE (output).
    *        ssv         - Vector that will contain the shared secret value
    *                      (output).
    *        secCtx      - Security context (input).
    *        senderUri   - URI of the sender (input).
    *        recipientUi - URI of the recipient (input).
    *        v           - Required state of the HDR payload v bit (input).
    *  Rets: Standard error code.
    */
    ScErrno CreateMikeySakkeIMessage(vector<uint8_t> &iMessage,
                                     vector<uint8_t> &ssv, SecCtx secCtx,
                                     string const &senderUri,
                                     string const &recipientUri, bool v)
    {
        uint32_t csbId;
        std::vector<uint8_t> rand;
        return CreateMikeySakkeIMessage(iMessage, ssv, csbId, rand, secCtx,
                                        senderUri, recipientUri, v);
    }


   /**
    *  Creates a MIKEY-SAKKE I_MESSAGE and an SRTP master key and master salt.
    *  The I_MESSAGE will be signed using ECCSI if a non empty sender URI is
    *  provided.
    *  Args: iMessage    - Vector that will contain the I_MESSAGE (output).
    *        masterKey   - SRTP Master Key (output).
    *        masterSalt  - SRTP Master Salt. If this is 112 bits (14 bytes)
    *                      then that size is used, otherwise resized to 128 bits
    *                      (16 bytes) (output).
    *        secCtx      - Security context (input).
    *        senderUri   - URI of the sender (input).
    *        recipientUi - URI of the recipient (input).
    *        v           - Required state of the HDR payload v bit (input).
    *  Rets: Standard error code.
    */
    ScErrno CreateMikeySakkeIMessageForSRTP(std::vector<std::uint8_t>
                                              &iMessage,
                                            std::vector<std::uint8_t>
                                              &masterKey,
                                            std::vector<std::uint8_t>
                                              &masterSalt,
                                            SecCtx secCtx,
                                            std::string const &senderUri,
                                            std::string const &recipientUri,
                                            bool v)
    {
        const uint8_t csId = 0x00;
        uint32_t csbId;
        std::vector<uint8_t> rand;
        std::vector<uint8_t> ssv;
        const ScErrno error
          = CreateMikeySakkeIMessage(iMessage, ssv, csbId, rand, secCtx,
                                     senderUri, recipientUri, v);

        if (error != ScErrno::SUCCESS)
        {
            return error;
        }

        Buffer rand_buf;
        rand_buf.length = rand.size();
        rand_buf.pointer = rand.data();
        Buffer ssv_buf;
        ssv_buf.length = ssv.size();
        ssv_buf.pointer = ssv.data();
        masterKey.resize(16);
        Buffer key_buf;
        key_buf.length = masterKey.size();
        key_buf.pointer = masterKey.data();

        if (masterSalt.size() != 14)
        {
            masterSalt.resize(16);
        }

        Buffer salt_buf;
        salt_buf.length = masterSalt.size();
        salt_buf.pointer = masterSalt.data();
        return CreateKeySalt(ssv_buf, csbId, csId, rand_buf, key_buf, salt_buf);
    }


    /**
    *  Creates a MIKEY SAKKE I_MESSAGE. The I_MESSAGE will be signed using ECCSI
    *  if a non empty sender URI is provided.
    *  Args: iMessage    - Vector that will contain the I_MESSAGE (output).
    *        secCtx      - Security context (input).
    *        senderUri   - URI of the sender (input).
    *        recipientUi - URI of the recipient (input).
    *        ssv         - Vector containing the shared secret value (input).
    *        v           - Required state of the HDR payload v bit (input).
    *  Rets: Standard error code.
    */
    ScErrno CreateMikeySakkeIMessageWithSsv(vector<uint8_t> &iMessage,
                                            SecCtx secCtx,
                                            string const &senderUri,
                                            string const &recipientUri,
                                            vector<uint8_t> const &ssv, bool v)
    {
        // variable to keep track of index of the next payload type byte
        size_t nextPayloadTypeIndex = 0;

        // String versions of URI
        String senderUriString;
        String recipientUriString;
        SetStringDescriptor(senderUriString, senderUri);
        SetStringDescriptor(recipientUriString, recipientUri);

        // data Buffers for communication with SC Crypto Library
        Buffer ssvData;
        Buffer sakkeData;

        // Temp store for returned status
        ScErrno status;

        // Get current timestamp
        String timestampString;
        int year, month, day;
        status = GetDateTime(&timestampString, &year, &month, &day);
        if (status != ScErrno::SUCCESS)
        {
            // Clean up and return error
            iMessage.clear();
            return status;
        }

        // Check recipient URI
        if (recipientUri.size() < 1)
        {
            // Clean up and return error
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        else
        {
            if (!IsTheirUriReady(secCtx, recipientUriString, timestampString))
            {
                // Clean up and return error
                iMessage.clear();
                return ScErrno::RECIPIENT_URI_NOT_PROVISIONED;
            }
        }

        if (ssv.size() < 1)
        {
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        SetBufferDescriptor(ssvData, ssv);

        // Generate sakke data
        status = SakkeEncrypt(secCtx, recipientUriString, timestampString, ssvData, &sakkeData);
        if (status != ScErrno::SUCCESS)
        {
            // Clean up and return error
            iMessage.clear();
            return status;
        }

        // Check sender URI
        if (senderUri.size() > 0)
        {
            if (!IsMyUriReady(secCtx, senderUriString, timestampString))
            {
                // Clean up and return error
                iMessage.clear();
                return ScErrno::SENDING_URI_NOT_PROVISIONED;
            }
        }

        // Create the MIKEY SAKKE I_MESSAGE
        std::uint32_t csbId;
        std::vector<std::uint8_t> rand;
        CreateIMessage(iMessage, nextPayloadTypeIndex, csbId, rand,
                       v, timestampString, senderUri, recipientUri, sakkeData);

        // Sign message if senderUri is not empty
        if (senderUri.size() > 0)
        {
            Buffer iMessageData;
            Buffer signatureData;

            // Set the next payload field early as it is within the signed message
            iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_SIGN;

            SetBufferDescriptor(iMessageData, iMessage);

            // Create signature
            status = EccsiSign(secCtx, senderUriString, timestampString,
                iMessageData, &signatureData);
            if (status != ScErrno::SUCCESS)
            {
                // Clean up and return error
                iMessage.clear();
                return status;
            }

            // Append signature to I_MESSAGE
            AppendSIGNPayloadToIMessage(iMessage, nextPayloadTypeIndex,
                MIKEY_PAYLOAD_SIGN_TYPE_ECCSI, signatureData);

        }

        return ScErrno::SUCCESS;
    }


    /*
     *  Creates a Group (GMK) MIKEY SAKKE I_MESSAGE and a GMK value.
     *  The I_MESSAGE will be signed using ECCSI with sender URI.
     *  Args:   iMessage    - Vector that will contain the I_MESSAGE (output).
     *          gmk         - Vector that will contain the Group Master Key
     *                        (output).
     *          secCtx      - Security context (input).
     *          senderUri   - URI of the sender (input).
     *          recipientUi - URI of the recipient (input).
     *          v           - Required state of the HDR payload v bit (input).
     *  Rets: Standard error code.
     */
    ScErrno CreateMikeySakkeGMKIMessage(vector<uint8_t> &iMessage,
                                        vector<uint8_t> &gmk, SecCtx secCtx,
                                        string const &senderUri,
                                        string const &recipientUri, bool v)
    {
        size_t nextPayloadTypeIndex = 0; /* variable to keep track of index
                                           * of the next payload type byte.
                                           */

        /* String versions of URI */
        String senderUriString;
        String recipientUriString;
        SetStringDescriptor(senderUriString, senderUri);
        SetStringDescriptor(recipientUriString, recipientUri);

        /* data Buffers for communication with SC Crypto Library */
        Buffer gmkData;
        Buffer sakkeData;
        Buffer iMessageData;
        Buffer signatureData;

        /* Temp store for returned status */
        ScErrno status;

        /* Get current timestamp */
        String timestampString;
        int year, month, day;
        status = GetDateTime(&timestampString, &year, &month, &day);
        if (status != ScErrno::SUCCESS)
        {
            /* Clean up and return error */
            iMessage.clear();
            return status;
        }

        /* Check recipient URI */
        if (recipientUri.size() < 1)
        {
            /* Clean up and return error */
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        else
        {
            if (!IsTheirUriReady(secCtx, recipientUriString, timestampString))
            {
                return ScErrno::RECIPIENT_URI_NOT_PROVISIONED;
            }
        }

        /* Generate a shared secret and SAKKE encrypt */
        status = GenerateSharedSecretAndSakkeEncrypt(
            secCtx,
            recipientUriString,
            timestampString,
            &sakkeData,
            &gmkData);
        if (status != ScErrno::SUCCESS)
        {
            /* Clean up and return error */
            iMessage.clear();
            return status;
        }

        /* Update gmk vector with gmk data */
        VectorFromBuffer(gmk, gmkData);

        /* Check sender URI */
        if (senderUri.size() < 1) /* Mandatory for GMK */
        {
            /* Clean up and return error */
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        else
        {
            if (!IsMyUriReady(secCtx, senderUriString, timestampString))
            {
                /* Clean up and return error */
                iMessage.clear();
                return ScErrno::SENDING_URI_NOT_PROVISIONED;
            }
        }

        /* Create the MIKEY SAKKE I_MESSAGE */
        CreateGmkIMessage(iMessage,
            nextPayloadTypeIndex,
            v,
            timestampString,
            senderUri,
            recipientUri,
            sakkeData);

        /* Sign message, senderUri is already checked for empty above */

        /* Set the next payload type field early as it is within the signed
        * message.
        */
        iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_SIGN;

        SetBufferDescriptor(iMessageData, iMessage);

        /* Create signature */
        status = EccsiSign(secCtx,
            senderUriString,
            timestampString,
            iMessageData,
            &signatureData);
        if (status != ScErrno::SUCCESS)
        {
            /* Clean up and return error */
            iMessage.clear();
            return status;
        }

        /* Append signature to I_MESSAGE */
        AppendSIGNPayloadToIMessage(iMessage, nextPayloadTypeIndex,
            MIKEY_PAYLOAD_SIGN_TYPE_ECCSI, signatureData);

        return ScErrno::SUCCESS;
    }


    /**
    *  Creates a Group (GMK) MIKEY SAKKE I_MESSAGE with specified GMK.
    *  The I_MESSAGE will be signed using ECCSI with sender URI.
    *  Args: iMessage    - Vector that will contain the I_MESSAGE (output).
    *        secCtx      - Security context (input).
    *        senderUri   - URI of the sender (input).
    *        recipientUi - URI of the recipient (input).
    *        gmk         - vVctor containing the Group Master Key (input).
    *        v           - Required state of the HDR payload v bit (input).
    *  Rets: Standard error code.
    */
    ScErrno CreateMikeySakkeGMKIMessageWithGmk(vector<uint8_t> &iMessage,
                                               SecCtx secCtx,
                                               string const &senderUri,
                                               string const &recipientUri,
                                               vector<uint8_t> const &gmk,
                                               bool v)
    {
        /* variable to keep track of index of the next payload type byte */
        size_t nextPayloadTypeIndex = 0;

        /* String versions of URI */
        String senderUriString;
        String recipientUriString;
        SetStringDescriptor(senderUriString, senderUri);
        SetStringDescriptor(recipientUriString, recipientUri);

        /* data Buffers for communication with SC Crypto Library */
        Buffer gmkData;
        Buffer sakkeData;
        Buffer iMessageData;
        Buffer signatureData;

        /* Temp store for returned status */
        ScErrno status;

        /* Get current timestamp */
        String timestampString;
        int year, month, day;
        status = GetDateTime(&timestampString, &year, &month, &day);
        if (status != ScErrno::SUCCESS)
        {
            /* Clean up and return error */
            iMessage.clear();
            return status;
        }

        /* Check recipient URI */
        if (recipientUri.size() < 1)
        {
            /* Clean up and return error */
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        else
        {
            if (!IsTheirUriReady(secCtx, recipientUriString, timestampString))
            {
                /* Clean up and return error */
                iMessage.clear();
                return ScErrno::RECIPIENT_URI_NOT_PROVISIONED;
            }
        }

        if (gmk.size() < 1)
        {
            /* Clean up and return error */
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        SetBufferDescriptor(gmkData, gmk);

        /* Generate sakke data */
        status = SakkeEncrypt(secCtx,
            recipientUriString,
            timestampString,
            gmkData,
            &sakkeData);
        if (status != ScErrno::SUCCESS)
        {
            /* Clean up and return error */
            iMessage.clear();
            return status;
        }

        /* Check sender URI */
        if (senderUri.size() < 1) /* Mandatory for GMK */
        {
            /* Clean up and return error */
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        else
        {
            if (!IsMyUriReady(secCtx, senderUriString, timestampString))
            {
                /* Clean up and return error */
                iMessage.clear();
                return ScErrno::SENDING_URI_NOT_PROVISIONED;
            }
        }

        /* Create the MIKEY SAKKE I_MESSAGE */
        CreateGmkIMessage(iMessage,
            nextPayloadTypeIndex,
            v,
            timestampString,
            senderUri,
            recipientUri,
            sakkeData);

        /* Sign message, senderUri is already checked for empty above */

        /* Set the next payload field early as it is within the signed
        * message
        */
        iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_SIGN;

        SetBufferDescriptor(iMessageData, iMessage);

        /* Create signature */
        status = EccsiSign(secCtx, senderUriString, timestampString,
            iMessageData, &signatureData);
        if (status != ScErrno::SUCCESS)
        {
            /* Clean up and return error */
            iMessage.clear();
            return status;
        }

        /* Append signature to I_MESSAGE */
        AppendSIGNPayloadToIMessage(iMessage, nextPayloadTypeIndex,
            MIKEY_PAYLOAD_SIGN_TYPE_ECCSI, signatureData);

        return ScErrno::SUCCESS;
    }


    /**
        *  Creates a Group (GSK) MIKEY SAKKE I_MESSAGE and a GSK value.
        *  The I_MESSAGE will be signed using ECCSI if a non empty
        *  sender URI is provided.
        *  Args: iMessage    - Vector that will contain the I_MESSAGE (output).
        *        gmk         - Vector containing the Group Master Key used to
        *                      encrypt the GSK (input).
        *        gsk         - Vector that will contain the Group Session Key
        *                      (output).
        *        secCtx      - Security context (input).
        *        senderUri   - URI of the sender (input).
        *        recipientUi - URI of the recipient (input).
        *        v           - Required state of the HDR payload v bit (input).
        *  Rets: Standard error code.
        */
    ScErrno CreateMikeySakkeGSKIMessage(std::vector<uint8_t> &iMessage,
                                        std::vector<uint8_t> const &gmk,
                                        std::vector<uint8_t> const &gsk,
                                        SecCtx secCtx,
                                        std::string const &senderUri,
                                        std::string const &recipientUri,
                                        bool v)
    {
        size_t nextPayloadTypeIndex = 0;   /* Variable to keep track of index
                                             * of the next payload type byte
                                             */
        vector<uint8_t>::const_iterator index;
        vector<uint8_t>::const_iterator tmp_nextPayloadTypeIndex;
        uint16_t nextPayloadType = 256; /* Unused type to indicate HDR
                                        * record next
                                        */
        vector<uint8_t> rndNum;

        /* String versions of URI */
        String senderUriString;
        String recipientUriString;
        SetStringDescriptor(senderUriString, senderUri);
        SetStringDescriptor(recipientUriString, recipientUri);

        /* data Buffers for communication with SC Crypto Library */
        Buffer gmkData;
        Buffer gskData;
        Buffer kemacEncData;
        Buffer kemacMacData;
        Buffer rndNumData;
        Buffer salt;
        Buffer iMessageData;
        String messageTimeString;

        uint8_t kmacArr[16];
        uint8_t kmacMacArr[32];
        uint8_t saltArr[32];

        kemacEncData.length = 16;
        kemacEncData.pointer = (unsigned char *)&kmacArr;
        memset((void *)&kmacArr, 0, sizeof(kmacArr));

        kemacMacData.length = 32;
        kemacMacData.pointer = (unsigned char *)&kmacMacArr;
        memset((void *)&kmacMacArr, 0, sizeof(kmacMacArr));

        salt.length = 16;
        salt.pointer = (unsigned char *)&saltArr;
        memset((void *)&saltArr, 0, sizeof(saltArr));

        uint32_t csbid = 0;
        uint64_t messageTime = 0;
        bool     success = true;
        uint8_t  hasT = 0;
        uint8_t  hasRAND = 0;
        bool     tmp_v = false;
        uint8_t  tmp_map;

        ScErrno status;

        /* Initialisation for iterator of partial iMessage parse (below) */

        /* Get current timestamp */
        String timestampString;
        int year, month, day;
        status = GetDateTime(&timestampString, &year, &month, &day);
        if (status != ScErrno::SUCCESS)
        {
            /* Clean up and return error */
            iMessage.clear();
            return status;
        }

        /* Check recipient URI */
        if (recipientUri.size() < 1)
        {
            /* Clean up and return error */
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        else
        {
            if (!IsTheirUriReady(secCtx, recipientUriString, timestampString))
            {
                /* Clean up and return error */
                iMessage.clear();
                return ScErrno::RECIPIENT_URI_NOT_PROVISIONED;
            }
        }
        /* Check GMK */
        if (gmk.size() < 1)
        {
            /* Clean up and return error */
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        SetBufferDescriptor(gmkData, gmk);

        /* Check GSK */
        if (gsk.size() < 1)
        {
            /* Clean up and return error */
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        SetBufferDescriptor(gskData, gsk);

        /* Check sender URI */
        if (senderUri.size() < 1) /* Mandatory for GMK   */
        {
            /* Clean up and return error */
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }

        /* Create the MIKEY SAKKE I_MESSAGE without KEMAC
        * Why? Well, in order to add KEMAC we need some of the terms
        * added to the message when first constructed, namely CSB-ID,
        * RAND and Timestamp.
        */
        CreateGskIMessage(iMessage, nextPayloadTypeIndex,
            v, timestampString, senderUri, recipientUri);

        /* Now we have created the initial iMessage we need a couple of things
        * from it in order to generate the KEMAC (CSB-ID, RAND and timestamp).
        * So, parse the iMessage created so far to retrieve these data, then
        * create the KEMAC and add to the iMessage.
        */
        index = iMessage.begin();
        tmp_nextPayloadTypeIndex = index + 2;
        do
        {
            switch (nextPayloadType)
            {
            case 256:
                success = ProcessHDRPayload(iMessage, index, tmp_nextPayloadTypeIndex, csbid, tmp_v, tmp_map);
                break;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_T:
                success = ProcessTPayload(iMessage, index, tmp_nextPayloadTypeIndex, messageTime);
                hasT++;
                break;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_RAND:
                success = ProcessRANDPayload(iMessage, index, tmp_nextPayloadTypeIndex, rndNum);
                SetBufferDescriptor(rndNumData, rndNum);
                hasRAND++;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST: /* Exit condition */
                success = false;
                break;
            default: /* Ignore, only interested in CSB and timestamp. */
                break;
            }
            nextPayloadType = *tmp_nextPayloadTypeIndex;
        } while (success);

        // Convert the iMessage timestamp into hex string form
        U64ToHexString(messageTimeString, messageTime);

        /* Create some space for KEMAC AES encrypted GSK and HMAC */
        iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_KEMAC;
        ScErrno res = KemacEncrypt(
            csbid,
            messageTimeString,
            rndNumData,
            gmkData,
            gskData,
            kemacEncData,
            salt);

        /* Append  KEMAC payload */
        AppendKEMACPayloadToIMessage(
            iMessage, nextPayloadTypeIndex,
            1, /* enc alg - AES-CM-128, see RFC 3830 table 6.2.a */
            kemacEncData,
            salt);
        /* Calculate HMAC across entiore message and add it to the message. */
        SetBufferDescriptor(iMessageData, iMessage);

        if ((res = calculateKemacHMAC(
            iMessageData,
            csbid,
            rndNumData,
            gmkData,
            kemacMacData)) == SUCCESS)
        {
            AppendMACPayloadToIMessage(
                iMessage,
                nextPayloadTypeIndex,
                2, /* mac alg - HMAC-SHA-256-256, see RFC 6043 table 6.6 */
                /* 0 NULL (restricted)
                * 1 HMAC-SHA-1-160
                * 2 HMAC-SHA-256-256
                */
                kemacMacData);
        }

        memset((void *)&kmacArr, 0, sizeof(kmacArr));
        memset((void *)&kmacMacArr, 0, sizeof(kmacMacArr));
        memset((void *)&saltArr, 0, sizeof(saltArr));

        /* If we use KEMAC we don't sign (with ECCSI) - RFC 3830 Section 6.2
        * states, "If the transport method used is the pre-shared key method,
        * this Key data transport payload is the last payload in the
        * message...
        */

        return ScErrno::SUCCESS;
    }


    /**
    *  Creates a MIKEY SAKKE MC-PTT I_MESSAGE and a gmk value.
    *  The I_MESSAGE will be signed using ECCSI if a non empty
    *  sender URI is provided.
    *  Args: iMessage    - Vector that will contain the I_MESSAGE (output).
    *        gmk         - Vector that will contain the GMK (output).
    *        secCtx      - Security context (input).
    *        senderUri   - URI of the sender (input).
    *        recipientUi - URI of the recipient (input).
    *        v           - Required state of the HDR payload v bit (input).
    *  Rets: Standard error code.
    */
    ScErrno CreateMikeySakkeMCPTTIMessage(vector<uint8_t> &iMessage,
                                          vector<uint8_t> &gmk, SecCtx secCtx,
                                          string const &senderUri,
                                          string const &recipientUri, bool v)
    {
        size_t nextPayloadTypeIndex = 0; /* variable to keep track of index
                                           * of the next payload type byte.
                                           */

        /* String versions of URI */
        String senderUriString;
        String recipientUriString;
        SetStringDescriptor(senderUriString, senderUri);
        SetStringDescriptor(recipientUriString, recipientUri);

        /* data Buffers for communication with SC Crypto Library */
        Buffer gmkData;
        Buffer sakkeData;

        ScErrno status;

        String timestampString;
        int year, month, day;

        /* Get current timestamp */
        status = GetDateTime(&timestampString, &year, &month, &day);
        if (status != ScErrno::SUCCESS)
        {
            /* Clean up and return error */
            iMessage.clear();
            return status;
        }

        /* Check recipient URI */
        if (recipientUri.size() < 1)
        {
            /* Clean up and return error */
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        else
        {
            if (!IsTheirUriReady(secCtx, recipientUriString, timestampString))
            {
                return ScErrno::RECIPIENT_URI_NOT_PROVISIONED;
            }
        }

        /* Generate a shared secret and SAKKE encrypt */
        status = GenerateSharedSecretAndSakkeEncrypt(
            secCtx,
            recipientUriString,
            timestampString,
            &sakkeData,
            &gmkData);
        if (status != ScErrno::SUCCESS)
        {
            /* Clean up and return error */
            iMessage.clear();
            return status;
        }

        /* Update gmk vector with gmk data */
        VectorFromBuffer(gmk, gmkData);

        /* Check sender URI */
        if (senderUri.size() < 1) /* Mandatory for GMK */
        {
            /* Clean up and return error */
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        else
        {
            if (!IsMyUriReady(secCtx, senderUriString, timestampString))
            {
                /* Clean up and return error */
                iMessage.clear();
                return ScErrno::SENDING_URI_NOT_PROVISIONED;
            }
        }


        /* Create the MIKEY SAKKE I_MESSAGE */
        CreateMcPttIMessage(
            iMessage,
            nextPayloadTypeIndex,
            v,
            timestampString,
            senderUri,
            recipientUri,
            sakkeData);

        /* Sign message, senderUri is already checked for empty above */
        Buffer iMessageData;
        Buffer signatureData;

        /* Set the next payload type field early as it is within the signed
        * message
        */
        iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_SIGN;

        SetBufferDescriptor(iMessageData, iMessage);

        /* Create signature */
        status = EccsiSign(secCtx, senderUriString, timestampString,
            iMessageData, &signatureData);
        if (status != ScErrno::SUCCESS)
        {
            /* Clean up and return error */
            iMessage.clear();
            return status;
        }

        /* Append signature to I_MESSAGE */
        AppendSIGNPayloadToIMessage(iMessage, nextPayloadTypeIndex,
            MIKEY_PAYLOAD_SIGN_TYPE_ECCSI, signatureData);

        return ScErrno::SUCCESS;
    }


    /**
    *  Creates a MIKEY SAKKE MC-PTT I_MESSAGE.
    *  The I_MESSAGE will be signed using ECCSI if a non empty
    *  sender URI is provided.
    *  Args: iMessage    - Vector that will contain the I_MESSAGE (output).
    *        secCtx      - Security context (input).
    *        senderUri   - URI of the sender (input).
    *        recipientUi - URI of the recipient (input).
    *        gmk         - Vector containing the GMK (input).
    *        v           - Required state of the HDR payload v bit (input).
    *  Rets: Standard error code.
    */
    ScErrno CreateMikeySakkeMCPTTIMessageWithGmk(vector<uint8_t> &iMessage,
                                                 SecCtx secCtx,
                                                 string const &senderUri,
                                                 string const &recipientUri,
                                                 vector<uint8_t> const &gmk,
                                                 bool v)
    {
        size_t nextPayloadTypeIndex = 0; /* variable to keep track of index
                                           * of the next payload type byte.
                                           */

        /* String versions of URI */
        String senderUriString;
        String recipientUriString;
        Buffer iMessageData;
        Buffer signatureData;

        SetStringDescriptor(senderUriString, senderUri);
        SetStringDescriptor(recipientUriString, recipientUri);

        /* data Buffers for communication with SC Crypto Library */
        Buffer gmkData;
        Buffer sakkeData;

        ScErrno status;

        /* Get current timestamp */
        String timestampString;
        int year, month, day;

        status = GetDateTime(&timestampString, &year, &month, &day);
        if (status != ScErrno::SUCCESS)
        {
            /* Clean up and return error */
            iMessage.clear();
            return status;
        }

        /* Check recipient URI */
        if (recipientUri.size() < 1)
        {
            /* Clean up and return error */
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        else
        {
            if (!IsTheirUriReady(secCtx, recipientUriString, timestampString))
            {
                /* Clean up and return error */
                iMessage.clear();
                return ScErrno::RECIPIENT_URI_NOT_PROVISIONED;
            }
        }

        if (gmk.size() < 1)
        {
            /* Clean up and return error */
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        SetBufferDescriptor(gmkData, gmk);

        /* Generate sakke data */
        status = SakkeEncrypt(secCtx, recipientUriString, timestampString, gmkData, &sakkeData);
        if (status != ScErrno::SUCCESS)
        {
            /* Clean up and return error */
            iMessage.clear();
            return status;
        }

        /* Check sender URI */
        if (senderUri.size() < 1) /* Mandatory for GMK */
        {
            /* Clean up and return error */
            iMessage.clear();
            return ScErrno::GENERAL_FAILURE;
        }
        else
        {
            if (!IsMyUriReady(secCtx, senderUriString, timestampString))
            {
                /* Clean up and return error */
                iMessage.clear();
                return ScErrno::SENDING_URI_NOT_PROVISIONED;
            }
        }

        /* Create the MIKEY SAKKE I_MESSAGE */
        CreateMcPttIMessage(iMessage,
            nextPayloadTypeIndex,
            v,
            timestampString,
            senderUri,
            recipientUri,
            sakkeData);


        /* Sign message, senderUri is already checked for empty above */

        /* Set the next payload field early as it is within the signed
        * message
        */
        iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_SIGN;

        SetBufferDescriptor(iMessageData, iMessage);

        /* Create signature */
        status = EccsiSign(secCtx,
            senderUriString,
            timestampString,
            iMessageData,
            &signatureData);
        if (status != ScErrno::SUCCESS)
        {
            /* Clean up and return error */
            iMessage.clear();
            return status;
        }

        /* Append signature to I_MESSAGE */
        AppendSIGNPayloadToIMessage(iMessage, nextPayloadTypeIndex,
            MIKEY_PAYLOAD_SIGN_TYPE_ECCSI, signatureData);

        return ScErrno::SUCCESS;
    }


    /**
    *  Processes a MIKEY SAKKE I_MESSAGE with optional signature verification
    *  and returns the extracted shared secret value.
    *  Args: recipientUi    - URI of the recipient (output).
    *        senderUri      - URI of the sender, if present (output).
    *        ssv            - Vector that will contain the extracted shared
    *                         secret value (output).
    *        v              - State of the HDR payload v bit (output).
    *        secCtx         - Security context (input).
    *        iMessage       - Vector containing the I_MESSAGE (input).
    *        checkSignature - Signature is verified if true (input).
    *  Rets: Standard error code.
    */
    ScErrno ProcessMikeySakkeIMessage(string &recipientUri, string &senderUri,
                                      vector<uint8_t> &ssv, bool &v,
                                      SecCtx secCtx,
                                      vector<uint8_t> const &iMessage,
                                      bool checkSignature)
    {
        // Get current timestamp
        String timestampString;
        int year, month, day;
        ScErrno status = GetDateTime(&timestampString, &year, &month, &day);
        if (status != ScErrno::SUCCESS)
        {
            // Clean up and return error
            return status;
        }
        uint64_t timestamp = HexStringToU64(timestampString);

        return ProcessMikeySakkeIMessageForTime(recipientUri,
            senderUri,
            ssv,
            v,
            secCtx,
            iMessage,
            timestamp,
            checkSignature);
    }


    /**
    *  Processes a MIKEY SAKKE I_MESSAGE with optional signature verification
    *  as if the validation was happening at some other time given as a
    *  parameter. Returns the extracted shared secret value.
    *  Args: recipientUi    - URI of the recipient (output).
    *        senderUri      - URI of the sender, if present (output).
    *        ssv            - Vector that will contain the extracted shared
    *                         secret value (output).
    *        v              - State of the HDR payload v bit (output).
    *        secCtx         - Security context (input).
    *        iMessage       - vector containing the I_MESSAGE (input).
    *        timestamp      - Verification timestamp (input).
    *        checkSignature - Signature is verified if true (input).
    *  Rets: Standard error code.
    */
    ScErrno ProcessMikeySakkeIMessageForTime(string &recipientUri,
                                             string &senderUri,
                                             vector<uint8_t> &ssv, bool &v,
                                             SecCtx secCtx,
                                             vector<uint8_t> const &iMessage,
                                             uint64_t timestamp,
                                             bool checkSignature)
    {
        ScErrno status;

        // Flags for payload types
        uint8_t payloadFlags = 0;

        // Local variables
        uint32_t csbid = 0;
        uint64_t messageTime = 0;
        vector<uint8_t> sakke;
        vector<uint8_t> signature;
        String senderUriString;
        String recipientUriString;
        Buffer sakkeData;
        Buffer ssvData;
        Buffer signatureData;
        Buffer messageData;
        String messageTimeString;
        size_t messageLength = 0;

        status = ParseMikeySakkeIMessage(iMessage, v, recipientUri, senderUri,
            csbid, messageTime, messageLength, sakke, signature, payloadFlags);

        // Return an error if we have failed
        if (status != ScErrno::SUCCESS)
        {
            return status;
        }

        // Check that we have all the required payloads
        if (checkSignature && ((payloadFlags & MIKEY_PAYLOAD_HAS_SIGN) == 0))
        {
            // No signature
            return ScErrno::IMESSAGE_NOT_SIGNED;
        }

        if (((payloadFlags & MIKEY_PAYLOAD_HAS_T) == 0)
            || ((payloadFlags & MIKEY_PAYLOAD_HAS_RAND) == 0)  // RAND is mandated to exist in a MIKEY SAKKE I_MESSAGE
            || (checkSignature && ((payloadFlags & MIKEY_PAYLOAD_HAS_IDRI) == 0))
            || ((payloadFlags & MIKEY_PAYLOAD_HAS_IDRR) == 0)  // Without this we cannot decode the SAKKE data
            || ((payloadFlags & MIKEY_PAYLOAD_HAS_SAKKE) == 0)
            )
        {
            return ScErrno::IMESSAGE_MALFORMED;
        }

        // Check the time difference
        if (timestamp > messageTime)
        {
            if ((timestamp - messageTime) > MIKEY_MESSAGE_MAX_TIME_ERROR)
            {
                return ScErrno::TIMESTAMP_OLD;
            }
        }
        else
        {
            if ((messageTime - timestamp) > MIKEY_MESSAGE_MAX_TIME_ERROR)
            {
                return ScErrno::TIMESTAMP_FUTURE;
            }
        }

        // Convert the iMessage timestamp into hex string form
        U64ToHexString(messageTimeString, messageTime);

        // Verify the signature
        if (checkSignature)
        {
            SetStringDescriptor(senderUriString, senderUri);
            SetBufferDescriptor(signatureData, signature);
            SetBufferDescriptor(messageData, iMessage); // This will contain the signature
            messageData.length = messageLength;
            status = EccsiVerify(secCtx, senderUriString, messageTimeString, messageData, signatureData);
            if (status != ScErrno::SUCCESS)
            {
                return status;
            }
        }

        // Decrypt the SAKKE encrypted data
        SetStringDescriptor(recipientUriString, recipientUri);
        SetBufferDescriptor(sakkeData, sakke);
        status = SakkeDecrypt(secCtx, recipientUriString, messageTimeString, sakkeData, &ssvData);
        if (status == ScErrno::SUCCESS)
        {
            VectorFromBuffer(ssv, ssvData);
        }

        return status;
    }


    /**
    *  Parse a MIKEY SAKKE I_MESSAGE and extract the key fields.
    *  No verification or decryption of the fields is performed.
    *  Args: iMessage      - I_MESSAGE contents to be parsed (input).
    *        v             - State of the HDR payload  v bit (output).
    *        recipientUri  - String that will contain the extracted recipient
    *                        URI (output).
    *        senderUri     - String that will contain the extracted sender URI
    *                        (output).
    *        csbid         - Crypto Session Bundle ID (output).
    *        messageTime   - Timestamp of the message (output).
    *        messageLength - Length of the message up to the signature (output).
    *        sakke         - Vector that will contain the extracted SAKKE
    *                        encapsulated data (output).
    *        signature     - Vector that will contain the extracted signature
    *                        data (output).
    *        payloadFlags  - Bitfield of header fields present in the message
    *                        (output).
    *  Rets: Standard error code.
    */
    ScErrno ParseMikeySakkeIMessage(vector<uint8_t> const &iMessage,
                                    bool &v,
                                    string &recipientUri,
                                    string &senderUri,
                                    uint32_t &csbid,
                                    uint64_t &messageTime,
                                    size_t &messageLength,
                                    vector<uint8_t> &sakke,
                                    vector<uint8_t> &signature,
                                    uint8_t &payloadFlags)
    {
        // Check the first 2 bytes of the HDR record
        // Process each payload in the iMessage
        vector<uint8_t>::const_iterator index(iMessage.begin());
        vector<uint8_t>::const_iterator nextPayloadTypeIndex = index + 2;
        uint16_t nextPayloadType = 256; // Unused type to indicate HDR record next

        // Error state
        bool success = true;

        // local temp variables
        string identity;
        uint8_t role;

        // Set payload flags to zero
        payloadFlags = 0;

        do
        {
            switch (nextPayloadType)
            {
            case 256:
                uint8_t tmp_map;
                success = ProcessHDRPayload(iMessage, index, nextPayloadTypeIndex, csbid, v, tmp_map);
                break;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_T:
                success = ProcessTPayload(iMessage, index, nextPayloadTypeIndex, messageTime);
                if (payloadFlags & MIKEY_PAYLOAD_HAS_T)
                    return ScErrno::IMESSAGE_MALFORMED;
                else
                    payloadFlags ^= MIKEY_PAYLOAD_HAS_T;
                break;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_RAND:
                success = ProcessRANDPayload(iMessage, index, nextPayloadTypeIndex);
                if (payloadFlags & MIKEY_PAYLOAD_HAS_RAND)
                    return ScErrno::IMESSAGE_MALFORMED;
                else
                    payloadFlags ^= MIKEY_PAYLOAD_HAS_RAND;
                break;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_IDR:
                success = ProcessIDRPayload(iMessage, index, nextPayloadTypeIndex, role, identity);
                if (success)
                {
                    switch (role)
                    {
                    case MIKEY_PAYLOAD_IDR_ROLE_IDRI:
                        senderUri = identity;
                        if (payloadFlags & MIKEY_PAYLOAD_HAS_IDRI)
                            return ScErrno::IMESSAGE_MALFORMED;
                        else
                            payloadFlags ^= MIKEY_PAYLOAD_HAS_IDRI;
                        break;
                    case MIKEY_PAYLOAD_IDR_ROLE_IDRR:
                        recipientUri = identity;
                        if (payloadFlags & MIKEY_PAYLOAD_HAS_IDRR)
                            return ScErrno::IMESSAGE_MALFORMED;
                        else
                            payloadFlags ^= MIKEY_PAYLOAD_HAS_IDRR;
                        break;
                    case MIKEY_PAYLOAD_IDR_ROLE_IDRKMSI:
                    case MIKEY_PAYLOAD_IDR_ROLE_IDRKMSR:
                        // Accepted but unused role
                        break;
                    default:
                        // Unexpected role
                        return ScErrno::IMESSAGE_MALFORMED;
                    }
                }
                break;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_SAKKE:
                success = ProcessSAKKEPayload(iMessage, index, nextPayloadTypeIndex, sakke);
                if (payloadFlags & MIKEY_PAYLOAD_HAS_SAKKE)
                    return ScErrno::IMESSAGE_MALFORMED;
                else
                    payloadFlags ^= MIKEY_PAYLOAD_HAS_SAKKE;
                break;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_SIGN:
                messageLength = index - iMessage.begin(); // Record message length
                success = ProcessSIGNPayload(iMessage, index, nextPayloadTypeIndex, signature);
                if (payloadFlags & MIKEY_PAYLOAD_HAS_SIGN)
                    return ScErrno::IMESSAGE_MALFORMED;
                else
                    payloadFlags ^= MIKEY_PAYLOAD_HAS_SIGN;
                break;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST:
                // Should not get here
                return ScErrno::IMESSAGE_MALFORMED;
            default:
                // Unexpected payload
                return ScErrno::IMESSAGE_MALFORMED;
            }

            // It was the last payload if we processed a SIGN payload
            if (nextPayloadType == MIKEY_PAYLOAD_NEXT_PAYLOAD_SIGN)
            {
                nextPayloadType = MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST;
            }
            else
            {
                // Find out which payload to process next
                nextPayloadType = *nextPayloadTypeIndex;
            }
        } while (success && nextPayloadType != MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST);

        if (success)
        {
            return ScErrno::SUCCESS;
        }
        else
        {
            return ScErrno::IMESSAGE_MALFORMED;
        }
    }


    /**
    *  Parse a MIKEY SAKKE I_MESSAGE and extract a subset of the key fields.
    *  No verification or decryption of the fields is performed.
    *  Args: iMessage     - I_MESSAGE contents to be parsed (input).
    *        recipientUri - String that will contain the extracted recipient
    *                       URI (output).
    *        senderUri    - String that will contain the extracted sender URI
    *                       (output).
    *        messageTime    Timestamp of the message (output).
    *  Rets: Standard error code.
    */
    ScErrno ParseMikeySakkeIMessage(std::vector<uint8_t> const &iMessage,
        std::string &recipientUri,
        std::string &senderUri,
        uint64_t &messageTime)
    {
        uint32_t csbid;
        size_t messageLength;
        vector<uint8_t> sakke;
        vector<uint8_t> signature;
        uint8_t payloadFlags;
        bool v_bit = false;

        return ParseMikeySakkeIMessage(iMessage, v_bit, recipientUri, senderUri,
            csbid, messageTime, messageLength, sakke, signature, payloadFlags);
    }


    /**
    *  Processes a MIKEY SAKKE I_MESSAGE with optional signature verification
    *  and returns the extracted shared secret or GMK or GSK value.
    *
    *  This implementation of the ProcessMikeySakkeIMessage method handles
    *  messages that would also be handled by the method implemented above
    *  However, this implementation also handles Group Messages and, as such,
    *  also returns an IDRiRole indication for the caller to know whethe the
    *  I_MESSAGE was group related.
    *
    *  Of course, unlike when creating a GMK or GSK I-Message where the intent
    *  is known, it is not possible to know 'a priori' whether a received
    *  I_MESSAGE is an original P2P or group (GMK/GSK) I_MESSAGE. Additionally,
    *  I_MESSAGEs don't have 'type' i.e. this is a GMK etc, so this has to be
    *  inferred.
    *
    *  Args: recipientUi    - URI of the recipient (output).
    *        senderUri      - URI of the sender, if present (output).
    *        secretValue    - Vector that will contain the extracted secret
    *                         value; this may be SSV, GMK or GSK depending
    *                         on the type of received message (output).
    *        rndNum         - I_MESSAGE RAND payload (output).
    *        v              - State of the HDR payload v bit (output).
    *        csbId          - Crypto session bundle ID (output).
    *        msgType        - Initiator role (see RFC 6043 section 6.6).
    *                         Used to determine which secret value is being
    *                         passed (output).
    *        secCtx         - Security context (input).
    *        iMessage       - Vector containing the I_MESSAGE (input).
    *        checkSignature - Signature is verified if true (input).
    *  Rets: Standard error code.
    */
    ScErrno ProcessMikeySakkeIMessage(std::string &recipientUri,
                                      std::string &senderUri,
                                      std::vector<uint8_t> &secretValue,
                                      std::vector<uint8_t> &rndNum,
                                      bool &v, uint32_t &csbId,
                                      IMsgType &msgType, SecCtx secCtx,
                                      std::vector<uint8_t> const &iMessage,
                                      bool checkSignature)
    {
        // Check the first 2 bytes of the HDR record
        // Process each payload in the iMessage
        vector<uint8_t>::const_iterator index(iMessage.begin());
        vector<uint8_t>::const_iterator nextPayloadTypeIndex = index + 2;
        uint16_t nextPayloadType = 256; // Unused type to indicate HDR record next

        // Error state
        bool success = true;
        ScErrno status;

        // Counters for payload types
        uint8_t hasRAND = 0;
        uint8_t hasT = 0;
        uint8_t hasIDRi = 0;
        uint8_t hasIDRr = 0;
        uint8_t hasKEMAC = 0;
        uint8_t hasSAKKE = 0;
        uint8_t hasSIGN = 0;

        // Local variables
        uint64_t messageTime = 0;
        uint8_t role;
        uint8_t IDRiRole = 0;
        string identity;
        vector<uint8_t> kemacEnc;
        vector<uint8_t> kemacSalt;
        vector<uint8_t> kemacMac;
        vector<uint8_t> sakke;
        vector<uint8_t> signature;
        String senderUriString;
        String recipientUriString;
        uint8_t kemacEncType = 0;
        uint8_t kemacMacType = 0;
        Buffer kemacEncData;
        Buffer kemacMacData;
        Buffer rndNumData;
        Buffer kemacSaltData;
        Buffer gmkSsv;
        Buffer gskSsv;
        Buffer sakkeData;
        Buffer secretValueData;
        Buffer signatureData;
        Buffer messageData;
        String messageTimeString;
        size_t messageLength = 0;
        size_t iMsgLenForHash = 0;

        // Get current timestamp
        String timestampString;
        int year, month, day;
        status = GetDateTime(&timestampString, &year, &month, &day);
        if (status != ScErrno::SUCCESS)
        {
            // Clean up and return error
            return status;
        }
        uint64_t timestamp = HexStringToU64(timestampString);

        uint8_t csMapType = defaultMsgType;
        msgType = ScLibMs::defaultMsgType;

        do
        {
            switch (nextPayloadType)
            {
            case 256:
                success = ProcessHDRPayload(iMessage, index, nextPayloadTypeIndex, csbId, v, csMapType);
                break;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_T:
                success = ProcessTPayload(iMessage, index, nextPayloadTypeIndex, messageTime);
                hasT++;
                break;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_RAND:
                success = ProcessRANDPayload(iMessage, index, nextPayloadTypeIndex, rndNum);
                hasRAND++;
                break;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_IDR:
                success = ProcessIDRPayload(iMessage, index, nextPayloadTypeIndex, role, identity);
                if (success)
                {
                    switch (role)
                    {
                    case MIKEY_PAYLOAD_IDR_ROLE_IDRI:
                        if (hasIDRi) {
                            recipientUri = identity;
                            hasIDRr++;
                        }
                        else {
                            IDRiRole = role; /* Set returned role value for IDRi */
                            senderUri = identity;
                            hasIDRi++;
                        }
                        break;
                    case MIKEY_PAYLOAD_IDR_ROLE_IDRKMS:
                    case MIKEY_PAYLOAD_IDR_ROLE_IDRPSK:
                    case MIKEY_PAYLOAD_IDR_ROLE_IDRAPP:
                        IDRiRole = role; /* Set returned role value for IDRi */
                        senderUri = identity;
                        hasIDRi++;
                        break;
                    case MIKEY_PAYLOAD_IDR_ROLE_IDRR:
                        recipientUri = identity;
                        hasIDRr++;
                        break;
                    case MIKEY_PAYLOAD_IDR_ROLE_IDRKMSI:
                    case MIKEY_PAYLOAD_IDR_ROLE_IDRKMSR:
                        // Accepted but unused role
                        break;
                    default:
                        // Unexpected role
                        success = false;
                        break;
                    }
                }
                break;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_KEMAC:
                iMsgLenForHash = index - iMessage.begin() + 4; /* Before index is updated */
                success = ProcessKEMACPayload(iMessage,
                    index,
                    nextPayloadTypeIndex,
                    kemacEncType,
                    kemacEnc,
                    kemacSalt,
                    kemacMacType,
                    kemacMac);
                iMsgLenForHash += 1 /* Next payload */
                    + 1 /* Type and KV */
                    + 2 /* kemac enc data len */
                    + kemacEnc.size()
                    + 2 /* Salt len */
                    + kemacSalt.size();

                hasKEMAC++;
                break;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_SAKKE:
                success = ProcessSAKKEPayload(iMessage, index, nextPayloadTypeIndex, sakke);
                hasSAKKE++;
                break;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_SIGN:
                messageLength = index - iMessage.begin(); // Record message length
                success = ProcessSIGNPayload(iMessage, index, nextPayloadTypeIndex, signature);
                hasSIGN++;
                break;
            case MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST:
                // Should not get here
                break;
            default:
                // Unexpected payload
                success = false;
                break;
            }

            // It was the last payload if we processed a SIGN payload
            if (nextPayloadType == MIKEY_PAYLOAD_NEXT_PAYLOAD_SIGN)
            {
                nextPayloadType = MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST;
            }
            else
            {
                // Find out which payload to process next
                nextPayloadType = *nextPayloadTypeIndex;
            }
        } while (success && nextPayloadType != MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST);
        // Return an error if we have failed
        if (!success)
        {
            return ScErrno::GENERAL_FAILURE;
        }

        // Check that we have all the required payloads
        // If we received a message with KEMAC we assume it was Gsk.
        // RFC 3830 states if KEMAC is included it should be the last entry in the iMessage
        if (!hasKEMAC && checkSignature && (hasSIGN != 1))
        {
            // No signature
            return ScErrno::IMESSAGE_NOT_SIGNED;
        }

        /**************************************************************/
        /* Message Type disambiguation - what message did we receive? */
        /**************************************************************/

        /* we can can infer the Mikey-Sakke Message type from the
        * received data as follows:
        *
        *   Normal (original) P2P
        *       CS-Map-Type == 0 (SRTP)
        *       IDRi-Role   == 1 (Initiator)
        *       hasSAKKE    == TRUE
        *   GMK
        *       CS-Map-Type == 1 (Empty)
        *       IDRi-Role   == 3 (KMS)
        *       hasSAKKE    == TRUE
        *   GSK
        *       CS-Map-Type == 0 (SRTP)
        *       IDRi-Role   == 1 (Initiator)
        *       hasSAKKE    == FALSE (uses KEMAC instead)
        *   MC-PTT
        *       CS-Map-Type == 0 (SRTP)
        *       IDRi-Role   == 3 (KMS)
        *       hasSAKKE    == TRUE
        */
        if (!csMapType)
        {
            if (IDRiRole == 1)
            {
                if (hasSAKKE)
                {
                    msgType = ScLibMs::defaultMsgType;
                }
                else
                {
                    msgType = ScLibMs::GSKMsgType;
                }
            }
            else
            {
                if ((IDRiRole == 3) && (hasSAKKE))
                {
                    msgType = ScLibMs::MCPTTMsgType;
                }
            }
        }
        else
        {
            if ((IDRiRole == 3) && (hasSAKKE))
            {
                msgType = ScLibMs::GMKMsgType;
            }
        }

        switch (msgType) {
        case (ScLibMs::defaultMsgType) :
            if ((hasT != 1)
                || (hasRAND != 1)  // RAND is mandated to exist in a MIKEY SAKKE I_MESSAGE
                || (checkSignature && hasIDRi != 1)
                || (hasIDRr != 1)  // Without this we cannot decode the SAKKE data
                || (hasSAKKE != 1))
            {
                return ScErrno::GENERAL_FAILURE;
            }
            break;

        case (ScLibMs::GMKMsgType) :
            /* RAND is optional in GMK */
            if ((hasT != 1)
                || (checkSignature && hasIDRi != 1)
                || (hasIDRr != 1)  // Without this we cannot decode the SAKKE data
                || (hasSAKKE != 1))
            {
                return ScErrno::GENERAL_FAILURE;
            }
            break;

        case (ScLibMs::GSKMsgType) :
        {
            if ((hasT != 1)
                || (hasRAND != 1)
                || (hasIDRi != 1)
                || (hasIDRr != 1)
                || (hasKEMAC != 1))
            {
                return ScErrno::GENERAL_FAILURE;
            }

            // Also need to swap sender and recipient uri's
            // the different Group Messages have the Group
            // in different IDR's
            string tmp(senderUri);
            senderUri = recipientUri;
            recipientUri = tmp;
        }
        break;

        case (ScLibMs::MCPTTMsgType) :
            if ((hasT != 1)
                || (hasRAND != 1)  // RAND is not used but is mandated to exist in a MIKEY SAKKE I_MESSAGE
                || (checkSignature && hasIDRi != 1)
                || (hasIDRr != 1)  // Without this we cannot decode the SAKKE data
                || (hasSAKKE != 1))
            {
                return ScErrno::GENERAL_FAILURE;
            }
            break;

        default:
            break;
        }

        // Check the time difference
        if (timestamp > messageTime)
        {
            if ((timestamp - messageTime) > MIKEY_MESSAGE_MAX_TIME_ERROR)
            {
                return ScErrno::TIMESTAMP_OLD;
            }
        }
        else
        {
            if ((messageTime - timestamp) > MIKEY_MESSAGE_MAX_TIME_ERROR)
            {
                return ScErrno::TIMESTAMP_FUTURE;
            }
        }

        // Convert the iMessage timestamp into hex string form
        U64ToHexString(messageTimeString, messageTime);

        // Verify the signature
        if (!hasKEMAC && checkSignature)
        {
            SetStringDescriptor(senderUriString, senderUri);
            SetBufferDescriptor(signatureData, signature);
            SetBufferDescriptor(messageData, iMessage);  // This will contain the signature
            messageData.length = messageLength;
            status = EccsiVerify(secCtx, senderUriString, messageTimeString,
                                 messageData, signatureData);
            if (status != ScErrno::SUCCESS)
            {
                return status;
            }
        }

        // Decrypt the SAKKE encrypted data
        SetStringDescriptor(recipientUriString, recipientUri);
        if (hasSAKKE)
        {
            SetBufferDescriptor(sakkeData, sakke);
            status = SakkeDecrypt(secCtx, recipientUriString, messageTimeString,
                                  sakkeData, &secretValueData);
            if (status == ScErrno::SUCCESS)
            {
                VectorFromBuffer(secretValue, secretValueData);
            }
        }
        else if (hasKEMAC) /* Assume we are receiving GSK message */
        {
            SetBufferDescriptor(kemacEncData, kemacEnc);
            SetBufferDescriptor(kemacMacData, kemacMac);
            SetBufferDescriptor(rndNumData, rndNum);
            SetBufferDescriptor(kemacSaltData, kemacSalt);
            SetBufferDescriptor(gmkSsv, secretValue);

            std::vector<uint8_t> secretValueOut(16, 0);
            SetBufferDescriptor(gskSsv, secretValueOut);

            // This should be run after we receive a GMK message therefore, the
            // SSV data passed in should still be in GMK SSV.
            std::vector<unsigned char> gsk_ssv(16);
            gskSsv.length = gsk_ssv.size();
            gskSsv.pointer = gsk_ssv.data();
            status = KemacDecrypt(csbId, messageTimeString, rndNumData,
                                  kemacSaltData, kemacEncData, gmkSsv, gskSsv);

            if (checkSignature)
            {
                /* The MAC must be at the end of the message */
                Buffer iMessageData;
                SetBufferDescriptor(iMessageData, iMessage);
                status = verifyKemacHMAC(iMessageData, iMsgLenForHash, csbId,
                                         rndNumData, gmkSsv, kemacMacData);
            }

            if (status == SUCCESS)
            {
                secretValue.clear();
                VectorFromBuffer(secretValue, gskSsv);
            }

            std::fill(gsk_ssv.begin(), gsk_ssv.end(), 0);
            gskSsv.length = 0;
            gskSsv.pointer = 0;
        }

        return status;
    }


    /**
      *  Processes a MIKEY SAKKE I_MESSAGE with optional signature verification
      *  and return the SRTP Master Key and Master Salt.
      *  Args: recipientUi    - URI of the recipient (output).
      *        senderUri      - URI of the sender, if present (output).
      *        masterKey      - SRTP Master Key (output).
      *        masterSalt     - SRTP Master Salt. If this is 112 bits (14 bytes)
      *                         then that size is used, otherwise resized to
      *                         128 bits (16 bytes) (output).
      *        v              - State of the HDR payload v bit (output).
      *        secCtx         - Security context (input).
      *        iMessage       - Vector containing the I_MESSAGE (input).
      *        checkSignature   - Signature is verified if true (input).
      *  Rets: Standard error code.
      */
    ScErrno ProcessMikeySakkeIMessageForSRTP(std::string &recipientUri,
                                             std::string &senderUri,
                                             std::vector<uint8_t> &masterKey,
                                             std::vector<uint8_t> &masterSalt,
                                             bool &v, SecCtx secCtx,
                                             std::vector<uint8_t> const
                                               &iMessage,
                                             bool checkSignature)
    {
        vector<uint8_t> secret;
        vector<uint8_t> rand;
        ScLibMs::IMsgType msgType;
        uint32_t csbId;
        const uint8_t csId = 0x00;
        const ScErrno error
          = ProcessMikeySakkeIMessage(recipientUri, senderUri, secret, rand, v,
                                      csbId, msgType, secCtx, iMessage,
                                      checkSignature);

        if (error != ScErrno::SUCCESS)
        {
            return error;
        }

        const Buffer secret_buf = { secret.size(), secret.data() };
        const Buffer rand_buf = { rand.size(), rand.data() };
        masterKey.resize(16);
        const Buffer key_buf = { masterKey.size(), masterKey.data() };

        if (masterSalt.size() != 14)
        {
           masterSalt.resize(16);
        }

        const Buffer salt_buf = { masterSalt.size(), masterSalt.data() };
        return CreateKeySalt(secret_buf, csbId, csId, rand_buf, key_buf, salt_buf);
    }


    /**
    *  Creates a MIKEY ECCSI signature for the supplied data.
    *  Args: mikeyEccsi   - Following successful operation this vector
    *                       will contain the created MIKEY-SAKKE ECCSI
    *                       signature, otherwise it will be empty (output).
    *        secCtx       - Security context (input).
    *        signingUri   - String containing the URI to use when signing the
    *                       message (input).
    *        msgTimestamp - String containing the message timestamp to use when
    *                       signing the message (input).
    *       data          - Data to be signed (input).
    *  Rets: Standard error code.
    */
    ScErrno CreateMikeyEccsiSig(vector<uint8_t> &mikeyEccsi,
                                SecCtx secCtx, string const &signingUri,
                                string const &msgTimestamp,
                                vector<uint8_t> const &data)
    {
        String signingUriString;
        String timestampString;
        Buffer messageData;
        Buffer signatureData;
        ScErrno status;

        // Set up data for call
        SetStringDescriptor(signingUriString, signingUri);
        SetStringDescriptor(timestampString, msgTimestamp);
        SetBufferDescriptor(messageData, data);

        status = EccsiSign(secCtx, signingUriString, timestampString,
            messageData, &signatureData);

        if (status == ScErrno::SUCCESS)
        {
            // Copy signature to output vector
            VectorFromBuffer(mikeyEccsi, signatureData);
        }
        else
        {
            // An error occured so clean up
            mikeyEccsi.clear();
        }

        return status;
    }


    /**
    *  Verifies a MIKEY ECCSI signature against supplied data.
    *  Args: secCtx       - Security context (input).
    *        signingUri   - String containing the URI to use when signing the
    *                       message (input).
    *        msgTimestamp - String containing the message timestamp to use when
    *                       signing the message (input).
    *        signedData   - Data that is signed (input).
    *        signature    - ECCSI signature (input).
    *  Rets: Standard error code. SUCCESS indicates successful verification.
    */
    ScErrno VerifyMikeyEccsiSig(SecCtx secCtx, string const &signingUri,
                                string const &msgTimestamp,
                                vector<uint8_t> const &signedData,
                                vector<uint8_t> const &signature)
    {
        String signingUriString;
        String timestampString;
        Buffer messageData;
        Buffer signatureData;

        // Set up data for call
        SetStringDescriptor(signingUriString, signingUri);
        SetStringDescriptor(timestampString, msgTimestamp);
        SetBufferDescriptor(messageData, signedData);
        SetBufferDescriptor(signatureData, signature);

        return EccsiVerify(secCtx,
            signingUriString, timestampString,
            messageData, signatureData);
    }
}

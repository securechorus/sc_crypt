/*
 *  Type definition for Secure Chorus security context.
 */

#ifndef SC_CONTEXT_H
#define SC_CONTEXT_H

#include "sc_dataref.h"

typedef DataRef SecCtx;

#endif

#ifndef SC_LIBMSKMS_H
#define SC_LIBMSKMS_H

#include <string>
#include <vector>

#include "sc_context.h"

enum ScErrno;

namespace ScLibMsKMS
{
	/*
	* Pass through function, to libmscrypto, to allow libmskms to manage
	* transport keys.
	* 
	* Args: secCtx         - Reference to the Security Context to be created
	*                        (output).
	*		 KMSUri        - The URI to which the Security Context is to be 
	*                        associated (input).
	*       TransportKeyId - The ID of the transport key associated with the
	*                        Security Context (input).
	*       TransportKey   - Buffer containg the transport key (input).
	* Rets: Standard error code. 
	*/	
	ScErrno CreateKmsSecurityContext(SecCtx & secCtx, std::string KMSUri,
		                             const std::string & TransportKeyId,
		                             const std::vector<unsigned char> &
		                               transportKey);

	/*
	* Returns XML, as a vector, containing an initialisation request.
	* Args: secCtx         - Reference to the Security Context (input).
	*	    KMSUrl         - The URL of the KMS this request is for
	*                        (input).
	*       UserUri        - The URI of the user for the specific KMS
	*                        (input).
	*       KMSInitReq     - Vector containing the XML request (output).
	* Rets: Standard error code. 
	*/
	ScErrno RequestKmsInitXML(SecCtx secCtx, std::string KMSUrl,
		                      const std::string & UserUri,
		                      std::vector<unsigned char> & KmsInitReq);

	/*
	* Returns XML, as a vector, containing an key provision request.
	* Args: secCtx        - Reference to the Security Context (input).
	*		KMSUrl        - The URL of the KMS this request is for 
	*                       (input).
	*       UserUri       - The URI of the user for the specific KMS
	*                       (input).
	*       KMSKeyProvReq - Vector containing the XML request (output).
	* Rets: Standard error code. 
	*/
	ScErrno RequestKmsKeyProvXML(SecCtx secCtx, std::string KMSUrl,
		                         const std::string & UserUri,
		                         std::vector<unsigned char> &
								   KmsKeyProvReq);

	/*
	* Returns XML, as a vector, containing an certificate cache request.
	* Args: secCtx          - Reference to the Security Context (input).
	*	    KMSUrl          - The URL of the KMS this request is for
	*                         (input).
	*       UserUri         - The URI of the user for the specific KMS
	*                         (input).
	*       KMSCertCacheReq - Vector containing the XML request (output).
	* Rets: Standard error code. 
	*/
	ScErrno RequestKmsCertCacheXML(SecCtx secCtx, std::string KMSUrl,
		                           const std::string & UserUri,
								   std::vector<unsigned char> &
								     KmsCertCacheReq);

	/*
	* Main function to process information received from the KMS.
	* Args: secCtc      - Reference to the associated Security Context
	*                     (input).
	*	    KMSResponse - Buffer holding data from KMS (input).
	* Rets: Standard error code.  
	*/
	ScErrno ProcessKmsRespXML(SecCtx secCtx,
		                      const std::vector<unsigned char> &
							    KMSResponse);

	/*
	* Main function to process information received from the KMS.
	* Args: secCtc			- Reference to the associated Security Context
	*						(input).
	*	    KMSResponse		- Buffer holding data from KMS (input).
	*		schemaDirectory - The directory containing the KMS response XML schema file
	*						if this parameter is empty the current directory will be used.
	*						If a directory is specified it must be suffixed with a directory seperator.
	* Rets: Standard error code.
	*/
	ScErrno ProcessKmsRespXML(SecCtx secCtx,
	const std::vector<unsigned char> &
	KMSResponse,
	std::string schemaDirectory);
}

#endif

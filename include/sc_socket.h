/* Provide cross platform socket capability. */
#pragma once

#if defined (_WIN32) || defined (_WIN64)
#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>
#include <WS2tcpip.h>
#pragma comment(lib,"ws2_32")
int sc_select(int nfds, fd_set * readfds, fd_set * writefds, fd_set * exceptfds, const struct timeval * timeout)
{
    return select(0, readfds, writefds, exceptfds, timeout);
}
#else
#include <cerrno>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/uio.h>
#define SOCKET int
#define SOCKET_ERROR -1
#define INVALID_SOCKET -1
int WSAGetLastError()
{
    return errno;
}
void WSACleanup() {}
#define NO_ERROR 0
#define MAKEWORD(a, b) 0
#define WSADATA int
int WSAStartup(int dummy1, int * dummy2)
{
    return NO_ERROR;
}
#define closesocket close
#define SOCKADDR sockaddr
#define sc_select select
#endif 
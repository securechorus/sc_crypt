/*
 *  Header file for Secure Chorus crypto library.
 */

#ifndef SC_CRYPTO_H
#define SC_CRYPTO_H

#include "sc_context.h"
#include "sc_errno.h"
#include "sc_types.h"
#include <stdint.h>


/*
 *  To allow use by both C and C++ code (start).
 */
#ifdef __cplusplus
extern "C"
{
#endif


/*
 *  Creates a new security context for KMS URI.
 *  Args: kmsUri         - KMS URI (input).
 *        transportKeyId - Transport key identity (input).
 *        transportKey   - Transport key (input).
 *        secCtx_p       - *secCtx_p is set to new security context
 *                         matching input parameters.
 *  Rets: Standard error code.
 */
ScErrno CreateKmsSecurityContext(String kmsUri, String transportKeyId,
                                 Buffer transportKey, SecCtx *secCtx_p);


/*
 *  Get KMS URI.
 *  Args: secCtx   - Security context (input).
 *        kmsUri_p - *kmsUri_p is set to KMS URI.
 *  Rets: Standard error code.
 */
ScErrno GetKmsUri(SecCtx secCtx, String *kmsUri_p);


/*
 *  Stores root KMS certificate.
 *  Args: secCtx      - Security context (input).
 *        rootKmsCert - Root KMS certificate (input).
 *  Rets: Standard error code.
 */
ScErrno StoreRootCert(SecCtx secCtx, Buffer rootKmsCert);


/*
 *  Stores certificate for an external domain.
 *  Args: secCtx          - Security context (input).
 *        externalKmsCert - External domain certificate (input).
 *  Rets: Standard error code.
 */
ScErrno StoreExternalCert(SecCtx secCtx, Buffer externalKmsCert);


/*
 *  Get root KMS certificate.
 *  Args: secCtx        - Security context (input).
 *        rootKmsCert_p - *rootKmsCert_p is set to root KMS certificate.
 *  Rets: Standard error code.
 */
ScErrno GetRootCert(SecCtx secCtx, Buffer *rootKmsCert_p);


/*
 *  Get KMS certificate.
 *  Args: secCtx    - Security context (input).
 *        certUri   - URI for required certificate.
 *        kmsCert_p - *kmsCert_p is set to KMS certificate.
 *  Rets: Standard error code.
 */
ScErrno GetCert(SecCtx secCtx, String certUri, Buffer *kmsCert_p);


/*
 *  Get list of KMS certificate URIs.
 *  Args: secCtx              - Security context (input).
 *        listofKmsCertUris_p - *listofKmsCertUris_p is set to indicate
 *                              list of KMS certificate URIs, starting
 *                              with the root KMS certificate.
 *  Rets: Standard error code.
 */
ScErrno ListKmsCertUris(SecCtx secCtx, StringList *listofKmsCertUris_p);


/*
 *  Converts a user URI into a Mikey-Sakke UID.
 *  Args: secCtx    - Security context (input).
 *        userUri   - User URI (input).
 *        timestamp - UTC timestamp (input). If string is empty then uses
 *                    current time.
 *        userId_p  - *userId_p is set to Mikey-Sakke UID.
 *  Rets: Standard error code.
 */
ScErrno GetMikeySakkeUid(SecCtx secCtx, String userUri, String timestamp,
                         String *userId_p);


/*
 *  Returns if timestamp and URI combination is provisioned for use
 *  (decryption and signing).
 *  Args: secCtx    - Security context (input).
 *        userUri   - User URI (input).
 *        timestamp - UTC timestamp (input).
 *  Returns true if provisioned, false if not.
 */
Boolean IsMyUriReady(SecCtx secCtx, String userUri, String timestamp);


/*
 *  Returns if timestamp and URI combination is provisioned for use
 *  (encryption and verification).
 *  Args: secCtx    - Security context (input).
 *        userUri   - User URI (input).
 *        timestamp - UTC timestamp (input).
 *  Returns true if provisioned, false if not.
 */
Boolean IsTheirUriReady(SecCtx secCtx, String userUri, String timestamp);


/*
 *  Get list of user URIs with stored keys.
 *  Args: secCtx           - Security context (input).
 *        timestamp        - UTC timestamp (input).
 *        listofUserUris_p - *listofUserUris_p is set to indicate
 *                           list of user URIs with valid keys at
 *                           indicated time (unless timestamp is
 *                           empty).
 *  Rets: Standard error code.
 */
ScErrno ListKeyedUserUris(SecCtx secCtx, String timestamp,
                          StringList *listofUserUris_p);


/*
 *  Get list of user UIDs with stored keys.
 *  Args: secCtx                 - Security context (input).
 *        userUri                - User URI (input). If empty then consider
 *                                 all user UIDs.
 *        timestamp              - UTC timestamp (input). If empty then do
 *                                 not validate timestamp
 *        listofMikeySakkeUids_p - *listofMikeySakkeUids_p is set to indicate
 *                                 list of user UIDs with valid keys matching
 *                                 parameters as indicated.
 *  Rets: Standard error code.
 */
ScErrno ListKeyedMikeySakkeUids(SecCtx secCtx, String userUri, String timestamp,
                                StringList *listofMikeySakkeUids_p);


/*
 *  Store UDK, SSK and PVT for given user ID. These are provided encrypted
 *  using indicated transport key using indicated transport algorithm.
 *  Args: secCtx                       - Security context (input).
 *        userUri                      - User URI (input).
 *        userId                       - User ID (input).
 *        transportAlg                 - Indicates algorithm used to encrypt
 *                                       UDK, SSK and PVT, either "kw-aes128"
 *                                       or "kw-aes-256" (input).
 *        transportKeyId               - Identity of key used to encrypt UDK,
 *                                       SSK and PVT (input).
 *        validFrom                    - Timestamp from which key is valid
 *                                       (input).
 *        validTo                      - Timestamp from which key is invalid
 *                                       (input).
 *        encryptedSakkeUserDecryptKey - Encrypted SAKKE UDK (input).
 *        encryptedEccsiSsk            - Encrypted ECCSI SSK (input).
 *        encryptedEccsiPvt            - Encrypted ECCSI PVT (input).
 *  Rets: Standard error code.
 */
ScErrno StoreKeyPack(SecCtx secCtx, String userUri, String userId,
                     const char *transportAlg, String transportKeyId,
					 String validFrom, String validTo,
                     Buffer encryptedSakkeUserDecryptKey,
                     Buffer encryptedEccsiSsk, Buffer encryptedEccsiPvt);


/*
 *  Remove user (all keys).
 *  Args: secCtx  - Security context (input).
 *        userUri - User URI (input).
 *  Rets: Standard error code.
 */
ScErrno RemoveUser(SecCtx secCtx, String userUri);


/*
 *  Remove UID (single key set).
 *  Args: secCtx  - Security context (input).
 *        userUid - User identity (input).
 *  Rets: Standard error code.
 */
ScErrno RemoveUid(SecCtx secCtx, String userId);


/*
 *  Store new transport key.
 *  Args: secCtx                   - Security context (input).
 *        transportAlg             - Indicates algorithm used, either
 *                                   "kw-aes128" or "kw-aes-256" (input).
 *        transportKeyId           - Transport key identity (input).
 *        newTransportKeyId        - New transport key identity (input).
 *        encryptedNewTransportKey - Encrypted new transport key.
 *  Rets: Standard error code.
 */
ScErrno StoreTk(SecCtx secCtx, String userUri, const char *transportAlg,
                String transportKeyId, String newTransportKeyId,
                Buffer encryptedNewTransportKey);


/*
 *  Get list of transport key identities.
 *  Args: secCtx        - Security context (input).
 *        listofTkIds_p - *listofTkIds_p is set to indicate
 *                        list of transport key identities.
 *  Rets: Standard error code.
 */
ScErrno ListTkIds(SecCtx secCtx, StringList *listofTkIds_p);


/*
 *  Remove a stored transport key.
 *  Args: secCtx         - Security context (input).
 *        transportKeyId - Transport key identity (input).
 *  Rets: Standard error code.
 */
ScErrno PurgeTk(SecCtx secCtx, String transportKeyId);


/*
 *  Remove all keys and all certificates for a context.
 *  Args: secCtx - Security context (input).
 *  Rets: if successful.
 */
Boolean PurgeKeys(SecCtx secCtx);


/*
 *  Generates SSV and encrypts using SAKKE. (Used at start of
 *  communications.)
 *  Args: secCtx       - Security context (input).
 *        recipientUri - URI of intended recipient (input).
 *        timestamp    - UTC timestamp (input).
 *        sed_p        - *sed_p is set to encrypted SSV.
 *        ssv_p        - *ssv_p is set to SSV.
 *  Rets: Standard error code.
 */
ScErrno GenerateSharedSecretAndSakkeEncrypt(SecCtx secCtx, String recipientUri,
                                            String timestamp, Buffer *sed_p,
                                            Buffer *ssv_p);


/*
 *  Encrypts data using SAKKE.
 *  Args: secCtx          - Security context (input).
 *        recipientUri    - URI of intended recipient (input).
 *        timestamp       - UTC timestamp (input).
 *        data            - Data to be encrypted (input).
 *        encryptedData_p - *encryptedData_p is set to
 *                          encrypted data.
 *  Rets: Standard error code.
 */
ScErrno SakkeEncrypt(SecCtx secCtx, String recipientUri, String timestamp,
					 Buffer data, Buffer *encryptedData_p);


/*
 *  Decrypts data using SAKKE.
 *  Args: secCtx          - Security context (input).
 *        recipientUri    - URI of sender (input).
 *        timestamp       - UTC timestamp (input).
 *        data            - Data to be decrypted (input).
 *        decryptedData_p - *decryptedData_p is set to
 *                          decrypted data.
 *  Rets: Standard error code.
 */
ScErrno SakkeDecrypt(SecCtx secCtx, String recipientUri, String timestamp,
					 Buffer data, Buffer *decryptedData_p);


/*
*  Creates a key and salt from a master key and session information as
*  defined in RFC 3830 (MIKEY), but using SHA-256 rather than SHA-1 for
*  larger key and salt values.
*  Args: masterKey    - Master key (input).
*        csbId        - Crypto session bundle ID (input).
*        csId         - Crypto session ID (input).
*        rndNum       - Random data (RAND), must be of length 16 bytes (input).
*        key          - Key, must provide length of 16 bytes (output).
*        salt         - Salt, must provide length of 14, 16 or 32 bytes
*                       (output).
*  Rets: Standard error code.
*/
ScErrno CreateKeySalt(Buffer masterKey, uint32_t csbId, uint8_t csId,
					  Buffer rndNum, Buffer key, Buffer salt);


/*
*  Encrypts data using KEMAC, as defined in RFC 3830.
*  Args: csbId        - CSB-ID needed for AES calculation (input).
*        timestamp    - Timestamp needed for AES calculation (input).
*        rndNum         RAND needed for AES calculation (input).
*        gmk            Group Master Key held by all group members (input).
*        gsk            Group Session Key to be encrypted and distributed
*                       (input).
*        kemacEncData - Encrypted data containing the group session key
*                       (output).
*        salt         - Salt used in calculations, to be passed to peers
*                       (output).
*  Rets: Standard error code.
*/
ScErrno KemacEncrypt(uint32_t csbId, String timestamp, Buffer rndNum,
					 Buffer gmk, Buffer gsk, Buffer kemacEncData, Buffer salt);


/*
*  Decrypts data for KEMAC, as defined in RFC 3830.
*  Args: csbId        - CSB-ID needed for AES calculation (input).
*        timestamp    - Timestamp needed for AES calculation (input).
*        rndNum         RAND needed for AES calculation (input).
*        salt         - Salt used in calculations (input).
*        kemacEncData - Encrypted data containing the group session key
*                       (input).
*        gmk            Group Master Key previously distributed (input).
*        gsk            Group Session Key (output).
*  Rets: Standard error code.
*/
ScErrno KemacDecrypt(uint32_t csbId, String timestamp, Buffer rndNum,
	                 Buffer salt, Buffer kemacEncData, Buffer gmk, Buffer gsk);


/*
*  Calculates the MAC part of a message header, as defined in RFC 3830.
*  Args: iMessage - The message we are creating the MAC for (input).
*        csbId    - CSB-ID needed for AES calculation (input).
*        rndNum   - RAND needed for AES calculation (input).
*        gmk      - Group Master Key, previously distributed (input).
*        kemacMacData The result MAC (output).
*  Rets: Standard error code.
*/
ScErrno calculateKemacHMAC(Buffer iMessage, uint32_t csbId, Buffer rndNum,
						   Buffer gmk, Buffer kemacMacData);


/*
*  Verifies the MAC part of a message header, as defined in RFC 3830.
*  Args: iMessage       - The message verifying the MAC for (input).
*        iMsgLenForHash - The length of the part of the message over
*                         which the hash must be calculated. A received
*                         message also contains the hash, which must not
*                         be included (input).
*        csbId          - CSB-ID, from received message (input).
*        rndNum         - RAND, from received message (input).
*        gmk            - Group Master Key, previously distributed (input).
*        kemacMacData   - The received MAC against which the calculated
*                         MAC will be verified (input).
*  Rets: Standard error code.
*/
ScErrno verifyKemacHMAC(Buffer iMessage, size_t iMsgLenForHash, uint32_t csbId,
	                    Buffer rndNum, Buffer gmk, Buffer kemacMacData);


/*
 *  ECCSI verify.
 *  Args: secCtx     - Security context (input).
 *        signingUri - URI of signer (input).
 *        timestamp  - UTC timestamp (input).
 *        data       - Data to be verified (input).
 *        signature  - Signature (input).
 *  Rets: Standard error code.
 */
ScErrno EccsiVerify(SecCtx secCtx, String signingUri, String timestamp,
					Buffer data, Buffer signature);


/*
 *  ECCSI sign.
 *  Args: secCtx      - Security context (input).
 *        signingUri  - URI of intended recipient (input).
 *        timestamp   - UTC timestamp (input).
 *        data        - Data to be signed (input).
 *        signature_p - *signature_p is set to signature.
 *  Rets: Standard error code.
 */
ScErrno EccsiSign(SecCtx secCtx, String signingUri, String timestamp,
				  Buffer data, Buffer *signature_p);

/*
 *  ECDSA verify.
 *  Args: secCtx    - Security context (input).
 *        data      - Data to be verified (input).
 *        signature - Signature (input).
 *  Rets: Standard error code.
 */
ScErrno EcdsaVerify(SecCtx secCtx, Buffer data, Buffer signature);


/*
 *  Calculate HMAC using stored transport key.
 *  Args: secCtx         - Security context (input).
 *        transportAlg   - Indicates algorithm used, must be
 *                         "hmac-sha256" (input).
 *        transportKeyId - Transport key identity (input).
 *        data           - Data to be signed (input).
 *        length         - HMAC will be truncated to this length
 *                         (before base 64 encoding). A value of
 *                         zero is interpreted as no truncation.
 *        hmac_p         - *hmac_p is set to HMAC (base64 encoded).
 *  Rets: Standard error code.
 */
ScErrno HmacTk(SecCtx secCtx, const char *transportAlg,
               String transportKeyId, Buffer data,
               size_t length, Buffer *hmac_p);


/*
 *  Calculate hash.
 *  Args: hashAlg - Indicates algorithm used, must be "sha256" (input).
 *        data    - Data to be hashed (input).
 *        length  - Hash will be truncated to this length
 *                  (before base 64 encoding). A value of
 *                  zero is interpreted as no truncation.
 *        hash_p  - *hash_p is set to hash (base64 encoded).
 *  Rets: Standard error code.
 */
ScErrno HashData(const char *hashAlg, Buffer data, size_t length,
	             Buffer *hash_p);


/*
 *  To allow use by both C and C++ code (finish).
 */
#ifdef __cplusplus
}
#endif

#endif

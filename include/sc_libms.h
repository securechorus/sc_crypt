#ifndef SC_LIBMS_H
#define SC_LIBMS_H

// Header file inclusions.
#include <cstdint>
#include <string>
#include <vector>

#include "sc_context.h"
#include "sc_errno.h"


/**
 *  Secure Chorus MIKEY-SAKKE Library namespace.
 */
namespace ScLibMs
{
    /**
	* Enumerated type for messages.
	*/
	enum IMsgType { defaultMsgType = 0, GMKMsgType, GSKMsgType, MCPTTMsgType };


	/**
	*  Creates a MIKEY-SAKKE I_MESSAGE and a shared secret value.
	*  The I_MESSAGE will be signed using ECCSI if a non empty snder URI is
	*  provided.
	*  Args: iMessage    - Vector that will contain the I_MESSAGE (output).
	*        ssv         - Vector that will contain the shared secret value
	*                      (output).
	*        secCtx      - Security context (input).
	*        senderUri   - URI of the sender (input).
	*        recipientUi - URI of the recipient (input).
	*        v           - Required state of the HDR payload v bit (input).
	*  Rets: Standard error code.
	*/
	ScErrno CreateMikeySakkeIMessage(std::vector<std::uint8_t> &iMessage,
		                             std::vector<std::uint8_t> &ssv,
		                             SecCtx secCtx,
		                             std::string const &senderUri,
		                             std::string const &recipientUri,
		                             bool v = false);


    /**
    *  Creates a MIKEY-SAKKE I_MESSAGE and an SRTP master key and master salt.
    *  The I_MESSAGE will be signed using ECCSI if a non empty sender URI is
    *  provided.
    *  Args: iMessage    - Vector that will contain the I_MESSAGE (output).
    *        masterKey   - SRTP Master Key (output).
    *        masterSalt  - SRTP Master Salt. If this is 112 bits (14 bytes)
	*                      then that size is used, otherwise resized to 128 bits
	*                      (16 bytes) (output).
    *        secCtx      - Security context (input).
    *        senderUri   - URI of the sender (input).
    *        recipientUi - URI of the recipient (input).
    *        v           - Required state of the HDR payload v bit (input).
    *  Rets: Standard error code.
    */
    ScErrno CreateMikeySakkeIMessageForSRTP(std::vector<std::uint8_t>
		                                      &iMessage,
                                            std::vector<std::uint8_t>
											  &masterKey,
                                            std::vector<std::uint8_t>
											  &masterSalt,
                                            SecCtx secCtx,
                                            std::string const &senderUri,
                                            std::string const &recipientUri,
                                            bool v = false);


	/**
	*  Creates a MIKEY SAKKE I_MESSAGE. The I_MESSAGE will be signed using ECCSI
	*  if a non empty sender URI is provided.
	*  Args: iMessage    - Vector that will contain the I_MESSAGE (output).
	*        secCtx      - Security context (input).
	*        senderUri   - URI of the sender (input).
	*        recipientUi - URI of the recipient (input).
	*        ssv         - Vector containing the shared secret value (input).
	*        v           - Required state of the HDR payload v bit (input).
	*  Rets: Standard error code.
	*/
	ScErrno CreateMikeySakkeIMessageWithSsv(std::vector<std::uint8_t> &iMessage,
		                                    SecCtx secCtx,
		                                    std::string const &senderUri,
		                                    std::string const &recipientUri,
		                                    std::vector<std::uint8_t> const
											  &ssv,
		                                    bool v = false);


	/*
	 *  Creates a Group (GMK) MIKEY SAKKE I_MESSAGE and a GMK value.
	 *  The I_MESSAGE will be signed using ECCSI with sender URI.
	 *  Args:	iMessage	- Vector that will contain the I_MESSAGE (output).
	 *			gmk			- Vector that will contain the Group Master Key
	 *                        (output).
	 *			secCtx		- Security context (input).
	 *			senderUri	- URI of the sender (input).
	 *			recipientUi - URI of the recipient (input).
	 *			v			- Required state of the HDR payload v bit (input).
	 *	Rets: Standard error code.
	 */
	ScErrno CreateMikeySakkeGMKIMessage(std::vector<std::uint8_t> &iMessage,
		                                std::vector<std::uint8_t> &gmk,
		                                SecCtx secCtx,
		                                std::string const &senderUri,
		                                std::string const &recipientUri,
		                                bool v = false);


	/**
	*  Creates a Group (GMK) MIKEY SAKKE I_MESSAGE with specified GMK.
	*  The I_MESSAGE will be signed using ECCSI with sender URI.
	*  Args: iMessage    - Vector that will contain the I_MESSAGE (output).
	*        secCtx      - Security context (input).
	*        senderUri   - URI of the sender (input).
	*        recipientUi - URI of the recipient (input).
	*        gmk         - vVctor containing the Group Master Key (input).
	*        v           - Required state of the HDR payload v bit (input).
	*  Rets: Standard error code.
	*/
	ScErrno CreateMikeySakkeGMKIMessageWithGmk(std::vector<std::uint8_t>
		                                         &iMessage,
		                                       SecCtx secCtx,
		                                       std::string const &senderUri,
		                                       std::string const &recipientUri,
		                                       std::vector<std::uint8_t> const
											     &gmk,
		                                       bool v = false);


	/**
	*  Creates a Group (GSK) MIKEY SAKKE I_MESSAGE and a GSK value.
	*  The I_MESSAGE will be signed using ECCSI if a non empty
	*  sender URI is provided.
	*  Args: iMessage    - Vector that will contain the I_MESSAGE (output).
	*        gmk         - Vector containing the Group Master Key used to
	*                      encrypt the GSK (input).
	*        gsk         - Vector that will contain the Group Session Key
	*                      (output).
	*        secCtx      - Security context (input).
	*        senderUri   - URI of the sender (input).
	*        recipientUi - URI of the recipient (input).
	*        v           - Required state of the HDR payload v bit (input).
	*  Rets: Standard error code.
	*/
	ScErrno CreateMikeySakkeGSKIMessage(std::vector<std::uint8_t> &iMessage,
		                                std::vector<std::uint8_t> const &gmk,
		                                std::vector<std::uint8_t> const &gsk,
		                                SecCtx secCtx,
		                                std::string const &senderUri,
		                                std::string const &recipientUri,
		                                bool v);


    /**
    *  Creates a MIKEY SAKKE MC-PTT I_MESSAGE and a gmk value.
	*  The I_MESSAGE will be signed using ECCSI if a non empty
	*  sender URI is provided.
	*  Args: iMessage    - Vector that will contain the I_MESSAGE (output).
	*        gmk         - Vector that will contain the GMK (output).
	*        secCtx      - Security context (input).
	*        senderUri   - URI of the sender (input).
	*        recipientUi - URI of the recipient (input).
	*        v           - Required state of the HDR payload v bit (input).
	*  Rets: Standard error code.
	*/
	ScErrno CreateMikeySakkeMCPTTIMessage(std::vector<std::uint8_t> &iMessage,
		                                  std::vector<std::uint8_t> &gmk,
		                                  SecCtx secCtx,
		                                  std::string const &senderUri,
		                                  std::string const &recipientUri,
		                                  bool v = false);


    /**
	*  Creates a MIKEY SAKKE MC-PTT I_MESSAGE.
	*  The I_MESSAGE will be signed using ECCSI if a non empty
	*  sender URI is provided.
	*  Args: iMessage    - Vector that will contain the I_MESSAGE (output).
	*        secCtx      - Security context (input).
	*        senderUri   - URI of the sender (input).
	*        recipientUi - URI of the recipient (input).
	*        gmk         - Vector containing the GMK (input).
	*        v           - Required state of the HDR payload v bit (input).
	*  Rets: Standard error code.
	*/
	ScErrno CreateMikeySakkeMCPTTIMessageWithGmk(std::vector<std::uint8_t>
		                                           &iMessage,
		                                         SecCtx secCtx,
		                                         std::string const &senderUri,
		                                         std::string const
												   &recipientUri,
		                                         std::vector<std::uint8_t> const
												   &gmk,
		                                         bool v = false);


	/**
	*  Processes a MIKEY SAKKE I_MESSAGE with optional signature verification
	*  and returns the extracted shared secret value.
	*  Args: recipientUi    - URI of the recipient (output).
	*        senderUri      - URI of the sender, if present (output).
	*        ssv            - Vector that will contain the extracted shared
	*                         secret value (output).
	*        v              - State of the HDR payload v bit (output).
	*        secCtx         - Security context (input).
	*        iMessage       - Vector containing the I_MESSAGE (input).
	*        checkSignature - Signature is verified if true (input).
	*  Rets: Standard error code.
	*/
	ScErrno ProcessMikeySakkeIMessage(std::string &recipientUri,
		                              std::string &senderUri,
		                              std::vector<std::uint8_t> &ssv,
		                              bool &v, SecCtx secCtx,
		                              std::vector<std::uint8_t> const &iMessage,
		                              bool checkSignature = true);


	/**
	*  Processes a MIKEY SAKKE I_MESSAGE with optional signature verification
	*  as if the validation was happening at some other time given as a
	*  parameter. Returns the extracted shared secret value.
	*  Args: recipientUi    - URI of the recipient (output).
	*        senderUri      - URI of the sender, if present (output).
	*        ssv            - Vector that will contain the extracted shared
	*                         secret value (output).
	*        v              - State of the HDR payload v bit (output).
	*        secCtx         - Security context (input).
	*        iMessage       - vector containing the I_MESSAGE (input).
	*        timestamp      - Verification timestamp (input).
	*        checkSignature - Signature is verified if true (input).
	*  Rets: Standard error code.
	*/
	ScErrno ProcessMikeySakkeIMessageForTime(std::string &recipientUri,
		                                     std::string &senderUri,
		                                     std::vector<std::uint8_t> &ssv,
		                                     bool &v, SecCtx secCtx,
		                                     std::vector<std::uint8_t> const
											   &iMessage,
		                                     std::uint64_t timestamp,
		                                     bool checkSignature = true);


	/**
	*  Parse a MIKEY SAKKE I_MESSAGE and extract the key fields.
	*  No verification or decryption of the fields is performed.
	*  Args: iMessage      - I_MESSAGE contents to be parsed (input).
	*        v             - State of the HDR payload  v bit (output).
	*        recipientUri  - String that will contain the extracted recipient
	*                        URI (output).
	*        senderUri     - String that will contain the extracted sender URI
	*                        (output).
	*        csbid         - Crypto Session Bundle ID (output).
	*        messageTime   - Timestamp of the message (output).
	*        messageLength - Length of the message up to the signature (output).
	*        sakke         - Vector that will contain the extracted SAKKE
	*                        encapsulated data (output).
	*        signature     - Vector that will contain the extracted signature
	*                        data (output).
	*        payloadFlags  - Bitfield of header fields present in the message
	*                        (output).
	*  Rets: Standard error code.
	*/
	ScErrno ParseMikeySakkeIMessage(std::vector<std::uint8_t> const &iMessage,
		                            bool &v, std::string &recipientUri,
		                            std::string &senderUri,
		                            std::uint32_t &csbid,
		                            std::uint64_t &messageTime,
		                            size_t &messageLength,
		                            std::vector<std::uint8_t> &sakke,
		                            std::vector<std::uint8_t> &signature,
		                            std::uint8_t &payload);

	
	/**
	*  Parse a MIKEY SAKKE I_MESSAGE and extract a subset of the key fields.
	*  No verification or decryption of the fields is performed.
	*  Args: iMessage     - I_MESSAGE contents to be parsed (input).
	*        recipientUri - String that will contain the extracted recipient
	*                       URI (output).
	*        senderUri    - String that will contain the extracted sender URI
	*                       (output).
	*        messageTime    Timestamp of the message (output).
	*  Rets: Standard error code.
	*/
	ScErrno ParseMikeySakkeIMessage(std::vector<std::uint8_t> const &iMessage,
		                            std::string &recipientUri,
		                            std::string &senderUri,
		                            std::uint64_t &messageTime);


	/**
	*  Processes a MIKEY SAKKE I_MESSAGE with optional signature verification
	*  and returns the extracted shared secret or GMK or GSK value.
	*
	*  This implementation of the ProcessMikeySakkeIMessage method handles
	*  messages that would also be handled by the method implemented above
	*  However, this implementation also handles Group Messages and, as such,
	*  also returns an IDRiRole indication for the caller to know whethe the
	*  I_MESSAGE was group related.
	*
	*  Of course, unlike when creating a GMK or GSK I-Message where the intent
	*  is known, it is not possible to know 'a priori' whether a received
	*  I_MESSAGE is an original P2P or group (GMK/GSK) I_MESSAGE. Additionally,
	*  I_MESSAGEs don't have 'type' i.e. this is a GMK etc, so this has to be
	*  inferred.
	*
	*  Args: recipientUi    - URI of the recipient (output).
	*        senderUri      - URI of the sender, if present (output).
	*        secretValue    - Vector that will contain the extracted secret
	*                         value; this may be SSV, GMK or GSK depending
	*                         on the type of received message (output).
	*        rndNum         - I_MESSAGE RAND payload (output).
	*        v              - State of the HDR payload v bit (output).
	*        csbId          - Crypto session bundle ID (output).
	*        msgType        - Initiator role (see RFC 6043 section 6.6).
	*        		          Used to determine which secret value is being
	*        		          passed (output).
	*        secCtx         - Security context (input).
	*        iMessage       - Vector containing the I_MESSAGE (input).
	*        checkSignature - Signature is verified if true (input).
	*  Rets: Standard error code.
	*/
	ScErrno ProcessMikeySakkeIMessage(std::string &recipientUri,
		                              std::string &senderUri,
		                              std::vector<std::uint8_t> &secretValue,
		                              std::vector<std::uint8_t> &rndNum,
		                              bool &v, std::uint32_t & csbId,
		                              IMsgType &msgType, SecCtx secCtx,
		                              std::vector<std::uint8_t> const &iMessage,
		                              bool checkSignature = true);


    /**
      *  Processes a MIKEY SAKKE I_MESSAGE with optional signature verification
      *  and return the SRTP Master Key and Master Salt.
      *  Args: recipientUi    - URI of the recipient (output).
      *        senderUri      - URI of the sender, if present (output).
      *        masterKey      - SRTP Master Key (output).
      *        masterSalt     - SRTP Master Salt. If this is 112 bits (14 bytes)
	  *                         then that size is used, otherwise resized to
	  *                         128 bits (16 bytes) (output).
      *        v              - State of the HDR payload v bit (output).
      *        secCtx         - Security context (input).
      *        iMessage       - Vector containing the I_MESSAGE (input).
	  *        checkSignature   - Signature is verified if true (input).
      *  Rets: Standard error code.
      */
    ScErrno ProcessMikeySakkeIMessageForSRTP(std::string &recipientUri,
                                             std::string &senderUri,
                                             std::vector<std::uint8_t> &masterKey,
                                             std::vector<std::uint8_t> &masterSalt,
                                             bool &v, SecCtx secCtx,
                                             std::vector<std::uint8_t> const
											   &iMessage,
                                             bool checkSignature = true);


	/**
	*  Creates a MIKEY ECCSI signature for the supplied data.
	*  Args: mikeyEccsi	  - Following successful operation this vector
	*                       will contain the created MIKEY-SAKKE ECCSI
	*	                    signature, otherwise it will be empty (output).
    *        secCtx	      - Security context (input).
    *        signingUri   - String containing the URI to use when signing the
	*                       message (input).
    *        msgTimestamp - String containing the message timestamp to use when
	*                       signing the message (input).
    *       data          - Data to be signed (input).
    *  Rets: Standard error code.
	*/
	ScErrno CreateMikeyEccsiSig(std::vector<std::uint8_t> &mikeyEccsi,
		                       SecCtx secCtx,
		                       std::string const &signingUri,
		                       std::string const &msgTimestamp,
		                       std::vector<std::uint8_t> const &data);


	/**
	*  Verifies a MIKEY ECCSI signature against supplied data.
	*  Args: secCtx	      - Security context (input).
	*		 signingUri	  - String containing the URI to use when signing the
	*                       message (input).
    *        msgTimestamp - String containing the message timestamp to use when
	*                       signing the message (input).
    *        signedData   - Data that is signed (input).
    *        signature    - ECCSI signature (input).
	*  Rets: Standard error code. SUCCESS indicates successful verification.
	*/
	ScErrno VerifyMikeyEccsiSig(SecCtx secCtx,  std::string const &signingUri,
		                        std::string const &msgTimestamp,
		                        std::vector<std::uint8_t> const &signedData,
		                        std::vector<std::uint8_t> const &signature);
}

#endif


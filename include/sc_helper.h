#pragma once

// Include file to expose the C structs for Buffer and String to the managed C++ wrapper

#include <string>

// Forward declarations for the C structs in libmscrypto
struct String;
struct Buffer;

// Aliases for the C structs to prevent name conflicts in managed C++
typedef struct String LibMSCryptoString;

typedef struct Buffer LibMSCryptoBuffer;

typedef Boolean LibMsCryptoBoolean;

namespace ScLibMs
{

	// Configures a String descriptor to use data from a string
	void SetStringDescriptor(LibMSCryptoString &descriptor, const std::string &s);

}
/*
 *  Definitions of types for C interface to Secure Chorus.
 */

#ifndef SC_TYPES_H
#define SC_TYPES_H

#include <stddef.h>

/*
 *  Define boolean type. There are potential problems mixing C++ and C99 boolean
 *  types. See for example http://www.stroustrup.com/sibling_rivalry.pdf. Thus
 *  define the type used as Boolean. Also ensure true and false are defined.
 *
 *  The header stdbool.h is available in C99, but not supported by e.g. Visual
 *  Studio 2012. Define STDBOOL_H as 1 if stdbool.h is available when using C
 *  and its bool type is compatible with C++ bool type. If not then define
 *  STDBOOL_H as 0. Boolean will then be bool or int. Note that in the latter
 *  case there may be warnings when using C++ and converting from Boolean to
 *  bool.
 */

#define STDBOOL_H  0

#if STDBOOL_H
typedef bool Boolean;
#else
typedef int Boolean;
#endif

#ifndef __cplusplus
#if STDBOOL_H
#include <stdbool.h>
#else
#define true  1
#define false 0
#endif
#endif


/*
 *  Structure to record information about used buffer.
 */
struct Buffer
{
  size_t length;
  const unsigned char *pointer;
};

typedef struct Buffer Buffer;


/*
 *  Structure to record information about used string.
 *  It is not assumed that the string is null terminated.
 */
struct String
{
  size_t     length;
  const char *pointer;
};

typedef struct String String;


/*
 *  Structure to record list of strings.
 */
struct StringList
{
  size_t       length;
  const String *pointer;
};

typedef struct StringList StringList;


#endif

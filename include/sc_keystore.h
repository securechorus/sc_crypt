/*
 *  Header file for Secure Chorus key store.
 */

#ifndef SC_KEYSTORE_H
#define SC_KEYSTORE_H

#include "sc_dataref.h"
#include "sc_errno.h"
#include "sc_types.h"


/*
 *  To allow use by both C and C++ code (start).
 */

#ifdef __cplusplus
extern "C"
{
#endif

/*
 *  Data reference returned by first stored data.
 */
#define KEYSTORE_FIRST 1

/*
 *  Sets the file path of the keystore file which defaults to sc_keystore.txt
 *  Args: newFilePath	- The new file path
 *  Rets: Standard error code.
*/
ScErrno SetFilePath(const char * newFilePath);

/*
 *  Stores data securely in data store, returning reference.
 *  Args: data          - Data to be stored (input).
 *        updateAllowed - If a subsequent update of this data
 *                        is allowed (input).
 *        dataRef_p     - *dataRef_p is set to a copyable
 *                        "handle" to the stored data. It is
 *                        unchanged in the event of an error.
 *  Rets: Standard error code.
 */
ScErrno StoreSecureData(Buffer data, Boolean updateAllowed,
                        DataRef *dataRef_p);


/*
 *  Updates data in data store.
 *  Args: dataRef - Handle to the stored data to be updated (input).
 *        data    - Data to be stored (input).
 *  Rets: Standard error code.
 */
ScErrno UpdateData(DataRef dataRef, Buffer data);


/*
 *  Gets data from data store.
 *  Args: dataRef - Handle to the stored data to be returned (input).
 *        data_p  - *data_p is set to indicate the stored data.
 *                  Unspecified value if error.
 *  Rets: Standard error code.
 */
ScErrno GetData(DataRef dataRef, Buffer *data_p);


/*
 *  Purge single item of data in data store.
 *  Args: dataRef - Handle to the stored data to be purged (input).
 *  Rets: Standard error code.
 */
ScErrno PurgeData(DataRef dataRef);


/*
 *  Purges all data in data store.
 *  Rets: Standard error code.
 */
ScErrno PurgeAllData();


/*
 *  To allow use by both C and C++ code (finish).
 */

#ifdef __cplusplus
}
#endif

#endif

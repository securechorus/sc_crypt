#if defined (_WIN32) || defined (_WIN64)
// For rand_s windows build
#define _CRT_RAND_S
#endif

#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

// Secure Chorus includes
#include <sc_libms.h>
#include <sc_libmskms.h>

// Local utility includes
#include "sc_app_init.h"
#include "sc_app_crypto.h"
#include "sc_errno.h"
#include "sc_misc.h"
#include "sc_socket.h"

using namespace std;

const string configfile = "sc.cfg";

#define CONFERENCE_RECV_PORT     8300  // The port that the group mode will listen on
#define GROUP_MANAGER_RECV_PORT  8301

const int SEND_PORT = 8453; // The port that the send mode will listen on
const int RECV_PORT = 8454; // The port that the recv mode will listen on

// Outputs usage information
// Args: name - the name of the application
void usage(string &name)
{
	cout << "Usage: " << name << " <operation> <arg>..." << endl;
	cout << "  where operation is one of init, prov, cert, send or recv." << endl;
	cout << "    init  - Bootstraps the client transport key." << endl;
	cout << "    prov  - Provision keys from the KMS." << endl;
	cout << "    cert  - Updates client certificate cache from the KMS." << endl;
	cout << "    recv  - Updates client key and waits for connection for secure messaging." << endl;
	cout << "    send  - Updates client key and initiates a connection for secure messaging." << endl;
	cout << "    group - Accepts and/or uses group key material for GMK/GSK and/or MCPTT variants" << endl;
	cout << "For each operation the args are as follows:" << endl;
	cout << "  init <bootstap filename> <KMS URL> <KMS URI> <User URI>" << endl;
	cout << "  prov <KMS URL>" << endl;
	cout << "  cert <KMS URL>" << endl;
	cout << "  send <remote URL> <remote URI> [-srtp] [-seed <num>]" << endl;
	cout << "  recv <remote URL> <remote URI> [-srtp] [-seed <num>]" << endl;
	cout << "  group <local-ip-addr> <local-listen-port> [<key-filename>]... " << endl;
	cout << "      key-filename optional if first run but " << endl;
	cout << "      MUST end [.gsk|.mcptt] for message transfer" << endl;
	cout << "      Key material can be updated (by Group Manager) in this mode." << endl;
}

#if ! defined (_WIN32) && ! defined (_WIN64)
// Linux no kbhit so create one...
int _kbhit(void)
{
	struct timeval tv;
	fd_set read_fd;
	tv.tv_sec = 0;
	tv.tv_usec = 0;
	/* Initialize read_fd */
	FD_ZERO(&read_fd);
	FD_SET(0, &read_fd);
	if (select(1, &read_fd, NULL, NULL, &tv) == -1)
	{
		return 0; /* An error occurred */
	}
	if (FD_ISSET(0, &read_fd))
	{
		/* Something waiting to be read from stdin */
		return 1;
	}
	/* no characters were pending */
	return 0;
}
#endif // if ! defined (_WIN32) && ! defined (_WIN64)

// Saves configuration information to a config file
// arg: ctx - the secure context to save
//      user - the users uri to save
// rets: true if successful otherwise false
bool saveConfig( SecCtx const &ctx, string const &user)
{
	ofstream config(configfile);
	// check the file is open
	if( !config.is_open())
	{
		return false;
	}

	// Save the configuration
	config << ctx << endl;
	config << user << endl;

	// Close the file
	config.close();

	return true;
}

// Saves configuration information to a config file
// arg: ctx - the secure context to put read configuration into
//      user - the users uri to put read configuration into
// rets: true if successful otherwise false
bool loadConfig( SecCtx &ctx, string &user)
{
	ifstream config(configfile);
	// check the file is open
	if( !config.is_open())
	{
		return false;
	}

	// Read the configuration
	config >> ctx;
	config >> user;

	// close the file
	config.close();

	return true;
}

// Gets a UDP socket address for the supplied domain name and service.
// arg: domainname - the name to get an ip address for.
//      service - the UDP service (port).
//      sockaddr - the socker address to populate. Contains socket address on exit
//                 if successful.
// Ret: True if successful, otherwise false.
bool getInetUdpSocketAddress(const string &domainname, const string &service, sockaddr_in &sockaddr)
{
	int result;
	bool success = false;
	struct addrinfo hints;
	struct addrinfo *infolist = NULL;
	struct addrinfo *info_p = NULL;

	// Set up the hints for an IPV4 UDP socket
	memset( &hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_protocol = IPPROTO_UDP;

	result = getaddrinfo( domainname.c_str(), service.c_str(), &hints, &infolist);
	if( result == 0)
	{
		// Search through the results for a suitable socket
		for( info_p = infolist; info_p != NULL; info_p = info_p->ai_next)
		{
			if( info_p->ai_family == AF_INET)
			{
				// Copy the sockaddr which we know is IPV4 sockaddr.
				memcpy( &sockaddr, info_p->ai_addr, sizeof(sockaddr_in));
				success = true;
				break;
			}
		}
	}

	// Cleanup and exit
	freeaddrinfo( infolist);
	return success;
}

std::string binToHex(const vector<uint8_t> &bin)
{
	std::stringstream hex_string;
	for (uint8_t s : bin)
	{
		char temp[3] = { 0 };
		sprintf_s(temp, 2, "%02x", s);
		if (s == 0)
		{
			hex_string << "00";
		}
		else
		{
			hex_string << temp;
		}
	}
	return hex_string.str();
}

// Seed the random number generator
// arg: num_seed_bytes - the number of bytes of entropy data to use
// rets: true if successful otherwise false
bool seedRandom(int num_seed_bytes)
{
	vector<uint8_t> entropy(num_seed_bytes);

#if defined (_WIN32) || defined (_WIN64)
	// The rand_s function on windows writes a pseudorandom integer in the
	// range 0 to UINT_MAX to the input pointer. It uses the operating system
	// to generate cryptographically secure random numbers. It does not use
	// the seed generated by the srand function, nor does it affect the random
	// number sequence used by rand.
	for (int i = 0; i < num_seed_bytes; ++i)
	{
		unsigned int rand_value;
		rand_s(&rand_value);
		entropy[i] = rand_value / (UINT_MAX / 256);
	}
#else
	// If it exists try using /dev/random as the entropy source.
	// Note that this is blocking until full entropy available.
	if (FILE *fp = fopen("/dev/random", "r"))
	{
		fread(&entropy[0], sizeof(uint8_t), num_seed_bytes, fp);
	    fclose(fp);
	}
	else
	{
		return false;
	}
#endif

	Buffer entropy_buf;
	entropy_buf.pointer = entropy.data();
	entropy_buf.length = entropy.size();
	return InitSecureRandom(entropy_buf) == SUCCESS;
}

bool saveGroupKeyMaterial(const string &filename, const vector<uint8_t> &keyData)
{
	/* Filename contains (postfix) of GMK, GSK or MCPTT */
	ofstream group(filename);
	/* check the file is open */
	if (!group.is_open())
	{
		cout << "cannot open file <" << filename << ">" << endl;
		return false;
	}
	string hex_string = binToHex(keyData);
	cout << "Key Data is <" << hex_string << ">" << endl;
	group << hex_string;
	
	/* Close the file */
	group.close();
	cout << "    Key Data saved in <" << filename << ">" << endl;
	cout << "    Using new key!";
	return true;

} /* saveGroupKeyMaterial */

bool getGroupKeyMaterial(const string &filename, vector<uint8_t> &keyData, const string title)
{
	string key_str;
	
	/* Filename contains (postfix) of GMK, GSK or MCPTT */
	ifstream group(filename);
	
	/* check the file is open */
	if (!group.is_open()) 
	{
		return false;
	}
	
	/* Retrieve the key */
	group >> key_str;
	group.close();

	if (key_str.length() != 32)
	{
		cout << "Key is incorrect length";
		exit(1);
	}
    else
	{
        for (int c = 0; c < 32; c += 2)
        {
			const unsigned long byte = std::strtoul(key_str.substr(c, 2).c_str(), 0, 16);
            keyData.push_back(static_cast<uint8_t>(byte));
        }
    }
	return true;

} /* getGroupKeyMaterial */


/* Group together all the socket-related activities and variables. */
typedef struct SocketsStruct
{
	fd_set fds;
	timeval timeout;
	SOCKET listen_socket;
	SOCKET gm_listen_socket;
	SOCKET sending_socket;
	sockaddr_in sender_address;
	socklen_t  sender_address_size;
	sockaddr_in conference_address;
	sockaddr_in remote_address;
	SocketsStruct()
	{
		timeout.tv_sec = 0;
		timeout.tv_usec = 1000; // 0.1 second timeout
		listen_socket = 0;
		gm_listen_socket = 0;
		sending_socket = 0;
		sender_address_size = 0;
	}
	int setup(int listen_port, string remote_addr, int remote_port)
	{
		// Initialise Winsock
		WSADATA wsaData;
		int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (result != NO_ERROR)
		{
			cout << " failed to initialise Winsock. Result = " << result;
			return -1;
		}
		// Create listener socket
		listen_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (listen_socket == INVALID_SOCKET)
		{
			cout << " unable to create listener socket: " << WSAGetLastError();
			WSACleanup();
			return -1;
		}
		// Create Group manager listener socket
		gm_listen_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (gm_listen_socket == INVALID_SOCKET)
		{
			cout << " unable to create group manager listener socket: " << WSAGetLastError();
			WSACleanup();
			return -1;
		}
		// Create sender socket
		sending_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (sending_socket == INVALID_SOCKET)
		{
			cout << " unable to create sender socket: " << WSAGetLastError();
			cleanup();
			return -1;
		}
		// Create socket addresses
		sockaddr_in listen_address;
		sender_address_size = sizeof(sender_address);

		// Create listener socket 
		listen_address.sin_family = AF_INET;
		listen_address.sin_port = htons(listen_port);
		listen_address.sin_addr.s_addr = htonl(INADDR_ANY);
		result = bind(listen_socket, (SOCKADDR *)&listen_address, sizeof(listen_address));
		if (result != 0)
		{
			cout << " failed to bind listener socket. result = " << result;
			cleanup();
			return -1;
		}
		if (remote_port == 0)
		{
			printf("    Creating group socket\n");
			/* Assume for testing same machine but different port */
			if (!getInetUdpSocketAddress(remote_addr, to_string(CONFERENCE_RECV_PORT), conference_address))
			{
				cout << "Error creating conference connection";
				cleanup();
				return -1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			// Set up a remote UDP socket address
			if (!getInetUdpSocketAddress(remote_addr, to_string(remote_port), remote_address))
			{
				cout << " Unable to get socket address for " << remote_addr << ":" << remote_port;
				cout << "Closing socket!";
				cleanup();
				return -1;
			}
			else
			{
				return 0;
			}
		}
	}
	int wait()
	{
		// Set up the file descriptor for the socket
		FD_ZERO(&fds);
		FD_SET(listen_socket, &fds);

		// Wait for input
		int select_result = sc_select(listen_socket + 1, &fds, NULL, NULL, &timeout);
		if (select_result == SOCKET_ERROR)
		{
			cout << "Error while waiting in select is " << WSAGetLastError();
			return -1;
		}

		return select_result;
	}
	int receive_message(vector<uint8_t> & message)
	{
		int result = recvfrom(listen_socket, (char *)&message.front(), (int)message.size(), 0,
			(SOCKADDR *)&sender_address, &sender_address_size);

		if (result == SOCKET_ERROR)
		{
			cout << "Error while receiving message. Error is " << WSAGetLastError();
			cleanup();
			return -1;
		}
		return result;
	}
	void send_to_bridge(vector<uint8_t> & ciphertext)
	{
		// Send the message
		/* Group mode uses same port for send and receive */
		if (sendto(listen_socket, /*sending_socket,*/
			(char *)&ciphertext.front(), (int)ciphertext.size(), 0,
			(SOCKADDR *)&conference_address, sizeof(remote_address))
			== SOCKET_ERROR)
		{
			cout << "Send to conference address failed. Error is " << WSAGetLastError();
		}
	}
	int send_to_peer(vector<uint8_t> & message)
	{
		int result = sendto(sending_socket, (const char *)&message.front(), (int)message.size(), 0,
			(SOCKADDR *)&remote_address, sizeof(remote_address));
		if (result == SOCKET_ERROR)
		{
			cout << "Failed to send MIKEY-SAKKE I_MESSAGE. Error is " << WSAGetLastError();
			closesocket(listen_socket);
			closesocket(sending_socket);
			WSACleanup();
			return -1;
		}
		return 0;
	}
	void cleanup()
	{
		closesocket(gm_listen_socket);
		closesocket(listen_socket);
		closesocket(sending_socket);
		WSACleanup();
	}
} Sockets;

/* Go into peer to peer mode. */
int peer_to_peer(bool send, string remote_addr, string remote_uri, bool srtp)
{
	int listen_port = 0; // The port this app will listen on
	int remote_port = 0; // The port this app will send messages to
	Sockets sockets;
	SecCtx secCtx;
	string my_uri;
	std::vector<uint8_t> ssv;
	std::vector<uint8_t> masterKey;
	std::vector<uint8_t> masterSalt;
	ScErrno sc_result;
	loadConfig(secCtx, my_uri);

	// Assign port numbers
	if (send)
	{
		listen_port = SEND_PORT;
		remote_port = RECV_PORT;
	}
	else
	{
		listen_port = RECV_PORT;
		remote_port = SEND_PORT;
	}

	if (sockets.setup(listen_port, remote_addr, remote_port) == -1)
	{
		return -1;
	}
	
	// Do the MIKEY exchange activity to set up a shared secret
	if (send)
	{
		// Send mode operation.
		// In this mode it sends a MIKEY message to the remote host set up shared secret.
		// Secure messaging mode will then operate using the shared secret.
		// Create a MIKEY-SAKKE I_MESSAGE
		string sender_uri = my_uri;
		string recipient_uri = remote_uri;
		vector<uint8_t> iMessage;
		
		if (srtp)
		{
			sc_result = ScLibMs::CreateMikeySakkeIMessageForSRTP(iMessage, masterKey, masterSalt, secCtx, sender_uri, recipient_uri, false);
		}
		else
		{
			sc_result = ScLibMs::CreateMikeySakkeIMessage(iMessage, ssv, secCtx, sender_uri, recipient_uri, false);
		}

		if (sc_result != ScErrno::SUCCESS)
		{
			cout << " Failed to create MIKEY-SAKKE I_MESSAGE. SC Error = "
				<< sc_result << " (" << ScErrName(sc_result) << ")";
			sockets.cleanup();
			return -1;
		}
		// Send the MIKEY-SAKKE I_MESSAGE
		if (sockets.send_to_peer(iMessage) == -1)
		{
			return -1;
		}
	}
	else /* Receiver */
	{
		cout << "Waiting to receive a MIKEY-SAKKE I_MESSAGE to establish a shared secret." << endl;
		vector<uint8_t> iMessage(2000);
		int result = sockets.receive_message(iMessage);
		if (result == SOCKET_ERROR)
		{
			return -1;
		}
		else
		{
			iMessage.resize(result);

			/* We need to know from the following call
			* whether the IMessage was from a Group
			* Manager. We chck the returned receivedMsgType.
			*/
			ScLibMs::IMsgType receivedMsgType = ScLibMs::defaultMsgType;
			string sender_uri;
			string responder_uri;
/*
			uint64_t messageTime;
*/
			bool v_bit;
/*
			sc_result = ScLibMs::ParseMikeySakkeIMessage(iMessage, responder_uri, sender_uri, messageTime);
			if (sc_result != ScErrno::SUCCESS)
			{
				cout << "Error: Failed to parse MIKEY-SAKKE I_MESSAGE. SC Error = " << sc_result << " (" << ScErrName(sc_result) << ")" << endl;
				return -1;
			}
			cout << "Received I_MESSAGE from initiator <" << sender_uri << "> to responder <" << responder_uri << "> with timestamp <" << messageTime << ">" << endl;
*/			
			if (srtp)
			{
				sc_result = ScLibMs::ProcessMikeySakkeIMessageForSRTP(
					my_uri, sender_uri, masterKey, masterSalt, v_bit, secCtx, iMessage, true);
			}
			else
			{
				vector<uint8_t> rand;
				uint32_t csbId;
				sc_result = ScLibMs::ProcessMikeySakkeIMessage(
					my_uri, sender_uri, ssv, rand, v_bit, csbId, receivedMsgType, secCtx, iMessage, true);
				/* This is the new ProcessMikeySakkeIMessage */
			}
			cout << "  my uri: " << my_uri;
			cout << "  remote_uri: " << remote_uri;
			cout << "  secCtx: " << secCtx;
			cout << "  sender uri: " << sender_uri;
			if (sc_result != ScErrno::SUCCESS)
			{
				cout << " Failed to process MIKEY-SAKKE I_MESSAGE. SC Error = "
					<< sc_result << " (" << ScErrName(sc_result) << ")";
				sockets.cleanup();
				return -1;
			}
			cout << "Parsed MIKEY-SAKKE I_MESSAGE." << endl;
			if (v_bit)
			{
				cout << "Warning: Verification response requested (not supported)." << endl;
			}
			if (receivedMsgType != ScLibMs::defaultMsgType)
			{
				sockets.cleanup();
				return -1;
			}
		}
	}
	
	// Secure messaging loop
	cout << "Begin secure messaging. Note, whilst typing you cannot" << endl;
	cout << "    receive messages. Type 'exit' to exit. " << endl;
	
	if (srtp)
	{
		// If the SRTP test, user the SRTP master key as the shared secret encryption key.
		ssv = masterKey;
		cout << "Using SRTP masterkey for encryption\n";
	}

	// Do until we have no more input
	bool newline = true;
	bool exit = false;
	while (!exit)
	{
		if (newline)
		{
			cout << "SC> ";
			fflush(stdout);
			newline = false;
		}
		// Wait for input
		int select_result = sockets.wait();
		if (select_result == -1)
		{
			break;
		}
		// Check if any messages were received on the listening socket
		else if (select_result > 0)
		{
			// Read the message
			vector<uint8_t> ciphertext(2000);
			int result = sockets.receive_message(ciphertext);
			ciphertext.resize(result); /* To actual size */
			
			/* Decrypt the message */
			vector<uint8_t> plaintext;
			if (decrypt(ssv, ciphertext, plaintext))
			{
				// Display the message
				cout << endl << "Received Message: "
					<< string(reinterpret_cast<char *>(&plaintext.front()), plaintext.size()) << endl;
			}
			else
			{
				cout << "Error: Failed to decrypt received message." << endl;
			}
			newline = true;
		}
		else
		{
			// No messages so check if there are any characters typed by the user
			if (_kbhit())
			{
				// Read the message line
				string message;
				getline(cin, message);
				if (message.compare("exit") == 0)
				{
					exit = true;
				}
				else if (message.size() > 0)
				{
					cout << "Sending Message: " + message << endl;
					/* Encrypt the message */
					vector<uint8_t> plaintext(reinterpret_cast<uint8_t *>(&message.front()),
						reinterpret_cast<uint8_t *>(&message.back() + 1));
					vector<uint8_t> ciphertext;

					if (encrypt(ssv, plaintext, ciphertext))
					{
						sockets.send_to_peer(ciphertext);
					}
					else
					{
						cout << " Failed to encrypt message. Not sending.";
					}
				}
				newline = true;
			}
		}
	}
	sockets.cleanup();
	return 0;
}

int group(string remote_addr, int listen_port, string group_id)
{
	Sockets sockets;
	sockets.setup(listen_port, remote_addr, 0);
	
	// Create storage for the I_MESSAGE and the shared secret
	vector<uint8_t> ssv; /* previous gsk or mcptt */
	vector<uint8_t> gmk;
	
	/* The user MUST specify if they wish to be using GMK/ GSK or MCPTT
	* mechanism. To do this they specify either .gmk or .mcptt postfix
	* for key filename.
	*/
	if ((group_id.length() != 0) && ((group_id.find(".gsk") == std::string::npos) &&
		(group_id.find(".mcptt") == std::string::npos)))
	{
		cerr << "key file MUST end in .gsk or .mcptt" << endl;
		exit(1);
	}
	
	/* If we're doing group communications the group key is
	* assumed to have been already transmitted and stored.
	*/
	/* We don't need to wait for key material from peer,
	* as it should have already been received via a
	* Group (GMK/GSK or MC-PTT message and stored
	* locally.
	*/
	if (group_id.find(".gsk") != std::string::npos)
	{
		if (!getGroupKeyMaterial(group_id, ssv, "GSK"))
		{
			cout << "!!!!! Cannot load GSK !!!!!" << endl;
			cout << "Will let you continue, but get the Group Manager to send some keys" << endl;
		}
		/* Try and get an associated GSK we can also use? */
		group_id.replace(group_id.length() - 4, 4, ".gmk");
		if (!getGroupKeyMaterial(group_id, gmk, "GMK SSV"))
		{
			cout << "!!!!! Cannot load GMK !!!!!" << endl;
			cout << "Will let you continue, but get the Group Manager to send some keys" << endl;
		}
	}
	else /* Should be mcptt */
	{
		if (!getGroupKeyMaterial(group_id, ssv, "MCPTT SSV"))
		{
			cout << "!!!!! No key material !!!!!" << endl;
			cout << "Will let you continue, but get the Group Manager to send some keys" << endl;
		}
	}
	
	// Secure messaging loop
	cout << "Begin secure messaging. Note, whilst typing you cannot" << endl;
	cout << "    receive messages. Type 'exit' to exit. " << endl;

	// Do until we have no more input
	SecCtx secCtx;
	string my_uri;
	loadConfig(secCtx, my_uri);
	bool newline = true;
	bool exit = false;
	while (!exit)
	{
		if (newline)
		{
			cout << "SC> ";
			fflush(stdout);
			newline = false;
		}
		int select_result = sockets.wait();
		if (select_result == -1)
		{
			break;
		}
		// Check if any messages were received on the listening socket
		else if (select_result > 0)
		{
			// Read the message
			vector<uint8_t> message;
			message.resize(2000);
			int message_size = sockets.receive_message(message);
			if (message_size == -1)
			{
				return -1;
			}
			if (ntohs(sockets.sender_address.sin_port) == GROUP_MANAGER_RECV_PORT)
			{
				bool v_bit;
				ScLibMs::IMsgType receivedMsgType = ScLibMs::defaultMsgType;
				vector<uint8_t> rand;
				uint32_t csbId;
				ScErrno sc_result = ScLibMs::ProcessMikeySakkeIMessage(
					my_uri, group_id, ssv, rand, v_bit, csbId,
					receivedMsgType, secCtx, message, true);
				if (sc_result != ScErrno::SUCCESS)
				{
					cout << " Failed to process MIKEY-SAKKE I_MESSAGE. SC Error = "
						<< sc_result << " (" << ScErrName(sc_result) << ")";
					sockets.cleanup();
					return -1;
				}
				cout << "Parsed MIKEY-SAKKE I_MESSAGE.";
				if (v_bit)
				{
					cout << "Warning: Verification response requested (not supported).";
				}
				switch (receivedMsgType) {
				case (ScLibMs::GMKMsgType) :
					cout << endl << "Received a GMK (Group Master Key) from a Group Manager" << endl;
					/* Save the Group GMK */
					gmk = ssv;
					saveGroupKeyMaterial(group_id + ".gmk", gmk);
					break;
				case (ScLibMs::GSKMsgType) :
					cout << endl << "Received a GSK (Group Session Key) from a Group Manager" << endl;
					/* Save the Group GSK */
					saveGroupKeyMaterial(group_id + ".gsk", ssv);
					break;
				case (ScLibMs::MCPTTMsgType) :
					cout << endl << "Received a MCPTT (Key) from a Group Manager" << endl;
					/* Save the Group MCPTT */
					saveGroupKeyMaterial(group_id + ".mcptt", ssv);
					break;
				default:
					cout << " Unknown message from Group Manager";
					break;
				}
				newline = true;
			}
			else
			{
				message.resize(message_size); /* To actual size */

				/* Decrypt the message */
				vector<uint8_t> plaintext;
				if (decrypt(ssv, message, plaintext))
				{
					// Display the message
					cout << "Received Message: "
						<< string(reinterpret_cast<char *>(&plaintext.front()), plaintext.size()) << endl;
				}
				else
				{
					cout << "Error: Failed to decrypt received message.";
				}
				newline = true;
			}
		}
		else
		{
			// No messages so check if there are any characters typed by the user
			if (_kbhit())
			{
				// Read the message line
				string message;
				getline(cin, message);
				if (message.compare("exit") == 0)
				{
					exit = true;
				}
				else if (message.size() > 0)
				{
					cout << "Sending Message: " + message << endl;
					/* Encrypt the message */
					vector<uint8_t> plaintext(reinterpret_cast<uint8_t *>(&message.front()),
						reinterpret_cast<uint8_t *>(&message.back() + 1));
					vector<uint8_t> ciphertext;
					if (encrypt(ssv, plaintext, ciphertext))
					{
						sockets.send_to_bridge(ciphertext);
					}
					else
					{
						cout << " Failed to encrypt message. Not sending.";
					}
				}
				newline = true;
			}
		}
	}
	sockets.cleanup();
	return 0;
}

// Main entry point
// Args: argc - number of command line arguments.
//       argv[] - array of command line arguments.
int main(int argc, char* argv[])
{
	SecCtx secCtx;
	string my_uri;
	ScErrno sc_result;

	cout << "Secure Chorus test client" << endl;
	cout << "=========================" << endl;
	string app(argv[0]);


	if (argc < 2)
	{
		// Insufficient arguments
		usage(app);
		return -1;
	}

	// Parse the command line command
	string operation(argv[1]);

	// Load the configuration
	if (operation != "init" && !loadConfig(secCtx, my_uri))
	{
		// Failed to read configuration
		cout << " failed to read configuration file - "
			<< configfile << endl;
		return -1;
	}

	// Init mode
	if (operation == "init")
	{
		// Perform initial bootstrapping
		// Check command line
		// app init <bootstap filename> <KMS URL> <KMS URI> <User URI>
		if (argc != 6)
		{
			cout << "Error: invalid number of parameters for init operation." << endl;
			usage(app);
			return -1;
		}

		// Set up parameters
		string filename(argv[2]);
		string kms_url(argv[3]);
		string kms_uri(argv[4]);
		my_uri = argv[5];

		// Perform the intitialisation
		secCtx = init(kms_uri, kms_url, my_uri, filename.c_str());

		// Check if it suceeded
		if (secCtx == 0)
		{
			// Failed to initialise
			cout << "Error: init operation failed." << endl;
			return -1;
		}

		// Write out config
		if (!saveConfig(secCtx, my_uri))
		{
			// Failed to save configuration
			cout << "Error: failed to write configuration file - "
				<< configfile << endl;
			return -1;
		}

		// Success
		cout << "Initialisation successful" << endl;
	}
	else if (operation == "prov")
	{
		// check and decode command line arguments
		if (argc != 3)
		{
			cout << "Error: invalid number of parameters for "
				<< operation << " operation." << endl;
			usage(app);
			return -1;
		}
		string kms_url(argv[2]);

		// Request keys from KMS
		cout << "Provisioning keys from KMS." << endl;
		if ((sc_result = ProvisionKeys(secCtx, kms_url, my_uri)) != ScErrno::SUCCESS)
		{
			cout << "Error: Failed to provision keys. SC Error = " << sc_result << " (" << ScErrName(sc_result) << ")" << endl;
			return -1;
		}

		// Success
		cout << "Key provisioning suceeded." << endl;
	}
	else if (operation == "cert")
	{
		// check and decode command line arguments
		if (argc != 3)
		{
			cout << "Error: invalid number of parameters for "
				<< operation << " operation." << endl;
			usage(app);
			return -1;
		}
		string kms_url(argv[2]);

		// Request certificate update from KMS server
		cout << "Updating certificate cache from KMS." << endl;
		if ((sc_result = updateCertCache(secCtx, kms_url, my_uri)) != ScErrno::SUCCESS)
		{
			cout << "Error: Failed to update certificate cache. SC Error = " << sc_result << " (" << ScErrName(sc_result) << ")" << endl;
			return -1;
		}

		// Success
		cout << "Certificate cache update suceeded." << endl;
	}
	else if (operation == "group")
	{
		string remote_addr = argv[2]; /* Conference Bridge */
		int listen_port = atoi(argv[3]); /* Used for sending and receiving */
		cout << "Listening on Port: " << listen_port << endl;
		cout << "Using group address: " << remote_addr << endl;
		cout << "Username is: " << my_uri << endl;
		string group_id;
		if (argc == 5)
		{
			group_id = argv[4];
		}
		cout << "    Operation is group" << endl;
		return group(remote_addr, listen_port, group_id);
	}
	else if (operation == "send" || operation == "recv")
	{
		// get command line arguments
		string remote_addr = argv[2];
		string remote_uri = argv[3];
		bool srtp_mode = false;
		int num_seed_bytes = 0;

		// Parse optional command line args
		for (int i = 4; i < argc; ++i)
		{
			string arg(argv[i]);

			if (arg.compare("-srtp") == 0)
			{
				srtp_mode = true;
			}
			else if (arg.compare("-seed") == 0 && i + 1 < argc)
			{
				++i;
				num_seed_bytes = atoi(argv[i]);
			}
			else
			{
				// Invalid option
				cout << "Error: " + arg + " is not a valid " << operation
					 << " option." << endl;
				usage(app);
				return -1;
			}
		}

		if (num_seed_bytes > 0)
		{
			// Initialise random number generator.
			cout << "Random number generator initialisation ";
			if (seedRandom(num_seed_bytes))
			{
				cout << "successful\n";
			}
			else
			{
				cout << "FAILED\n";
			}
		}

		if (operation == "send")
		{
			cout << "Operation is send\n";
			return peer_to_peer(true, remote_addr, remote_uri, srtp_mode);
		}
		else
		{
			cout << "Operation is receive\n";
			return peer_to_peer(false, remote_addr, remote_uri, srtp_mode);
		}
	}
	else
	{
		// Invalid operation
		cout << "Error: " + operation + " is not a valid operation." << endl;
		usage(app);
		return -1;
	}

	return 0;
}

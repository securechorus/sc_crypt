#include <cstring>
#include <fstream>
#include <istream>
#include <iostream>
#include <string>
#include <vector>

#include <curl/curl.h>

#include "sc_app_init.h"

#include "sc_libmskms.h"
#include "sc_context.h"
#include "sc_types.h"
#include "sc_errno.h"

void dumptofile( std::vector<unsigned char> &data, std::string &filename)
{
	if (data.size() > 0){
		std::string content(reinterpret_cast<char *>(&data.front()), data.size());
		std::ofstream xmldump;
		xmldump.open(filename);
		xmldump << content << std::endl;
		xmldump.close();
	}
}

//  Variables and functions used only in this file.
namespace
{
  //  Transport key information. Length is in bytes.
  const std::string tk_algorithm_256 = "kw-aes256";
  const std::size_t tk_length_256    = 32;
  const std::string tk_algorithm_128 = "kw-aes128";
  const std::size_t tk_length_128    = 16;

  //  Initialise bootstrap key from bootstrap file.
  //  Args: ist              - Input stream (bootstrap file).
  //        bootstrap_key    - Set to bootstrap key.
  //        bootstrap_key_id - Set to bootstrap key ID.
  //  Reys: True if successful, false if not.
  bool init_bootstrap(std::istream &ist,
                      std::vector<unsigned char> &bootstrap_key,
                      std::string &bootstrap_key_id)
  {
    //  Get hexadecimal key string from stream, verify stream
    //  state and string length.
    std::string key;
    getline(ist, key);
	std::string tk_algorithm;
    std::size_t tk_length;

    if (!ist)
    {
	  // failed to read from input stream
      return false;
    }
	else if( key.size() == 2 * tk_length_128)
	{
		tk_length = tk_length_128;
		tk_algorithm = tk_algorithm_128;
	}
	else if( key.size() == 2 * tk_length_256)
	{
		tk_length = tk_length_256;
		tk_algorithm = tk_algorithm_256;
	}
	else
	{
		// Unexpected key size
		return false;
	}

    // Convert key to binary, verifying all characters are
    // hexadecimal. Allow either case of A-F or a-f.
    bootstrap_key.resize(tk_length);
    const char *xdigits = "0123456789ABCDEFabcdef";

    for (size_t i = 0; i != tk_length; ++i)
    {
      const char *p1 = std::strchr(xdigits, key[2 * i]);
      const char *p2 = std::strchr(xdigits, key[2 * i + 1]);

      if (p1 == 0 || p2 == 0)
      {
        return false;
      }

	  const unsigned char d1 = (const unsigned char)(p1 - xdigits);
	  const unsigned char d2 = (const unsigned char)(p2 - xdigits);
      bootstrap_key[i] = 16 * (d1 >= 16 ? d1 - 6 : d1)
                         + (d2 >= 16 ? d2 - 6 : d2);
    }

    //  Get key ID from stream, verify stream and ID not empty.
    getline(ist, bootstrap_key_id);
    return ist && !bootstrap_key_id.empty();
  }

  //  Callback function for CURL write. Appends the data contained
  //  in the referenced buffer to the supplied unsigned char vector.
  //  Args: buffer - Pointer to buffer returned data was put in.
  //        size   - Size of data items.
  //        number - Number of data items.
  //        user_p - User pointer. Must be the address of a
  //                 std::vector<unsigned char>.
  //  Rets: Number of bytes handled (all of them).
  std::size_t curl_write(void *buffer, std::size_t size, std::size_t number,
	                     void *user_p)
  {
	std::size_t data_length = size * number;
	if( data_length > 0)
	{
		std::vector<unsigned char> *vec_p
			= reinterpret_cast<std::vector<unsigned char> *>(user_p);
		unsigned const char *buf_p
			= reinterpret_cast<unsigned const char*>(buffer);
		vec_p->insert( vec_p->end(), buf_p, buf_p + data_length);
	}
	return data_length;
  }

  //  Post XML data and get XML data returned.
  //  Args: xml - On entry XML to be posted, on return XML received.
  //        url - URL to which XML is to be posted.
  //  Rets: True if succeeded, false if not.
  //  Note: This function will block until data is returned.
  bool curl_post(std::vector<unsigned char> &xml, const std::string &url)
  {
	//  Initialise CURL.
    CURL * const curl = curl_easy_init();

    if (curl == 0)
    {
      return false;
    }

	//  Set URL and to be used for posting.
    if (curl_easy_setopt(curl, CURLOPT_URL, url.c_str()))
    {
      curl_easy_cleanup(curl); 
      return false;
    }

	//  Set CURL to send POST message
	if (curl_easy_setopt(curl, CURLOPT_POST, 1))
    {
      curl_easy_cleanup(curl); 
      return false;
    }

	// Set up the http header content type.
	struct curl_slist *slist =
		curl_slist_append(NULL, "Content-Type: text/xml; charset=utf-8");
	if (curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist))
    {
      curl_easy_cleanup(curl); 
      return false;
    }

	// Set up CURL to post the XML data.
	if (curl_easy_setopt(curl, CURLOPT_POSTFIELDS, &xml.front()))
    {
      curl_easy_cleanup(curl); 
      return false;
    }

	if (curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, xml.size()))
    {
      curl_easy_cleanup(curl); 
      return false;
    }

	//  Setup buffer for return and callback function to transfer
	//  read data to it.
	std::vector<unsigned char> buffer;
    
	if (curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer))
    {
      curl_easy_cleanup(curl); 
      return false;
    }

	if (curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curl_write))
	{
      curl_easy_cleanup(curl); 
      return false;
	}

	//  Post and get returned data set in buffer.
	if (curl_easy_perform(curl))
	{
      curl_easy_cleanup(curl);
      return false;
	}

	//  Swap buffer with XML for return, cleanup and exit.
	xml.swap(buffer);
    curl_easy_cleanup(curl);
	return true;
  }
}

//  Initialise cryptographic functions, using KMS client and server.
//  Args: kms_uri  - KMS URI.
//        kms_auth - KMS URL authority (optional user, host, optional
//                   port).
//        user_uri - User URI.
//        filename - Bootstrap file name, containing bootstrap key.
//                   Opened and closed by this function.
//  Rets: Security context, tests false (equal to zero) if
//        initialisation fails.
SecCtx init(const std::string &kms_uri, const std::string &kms_auth,
            const std::string &user_uri, const char *filename)
{
  // Open bootstrap file.
  std::ifstream ifs(filename);
  ScErrno result;

  if (!ifs)
  {
    return SecCtx(0);
  }

  //  Read bootstrap key from file.
  std::vector<unsigned char> bootstrap_key;
  std::string bootstrap_key_id;

  if (!init_bootstrap(ifs, bootstrap_key, bootstrap_key_id))
  {
	std::cout << "Failed to read bootstrap key" << std::endl;
    return SecCtx(0);
  }

  //  Create security context.
  SecCtx context;

  if (result = ScLibMsKMS::CreateKmsSecurityContext(context, kms_uri, bootstrap_key_id,
	                                       bootstrap_key))
  {
	std::cout << "ScLibMsKMS::CreateKmsSecurityContext failed. Returned "
		<< result << " (" << ScErrName(result) << ")" << std::endl;
    return SecCtx(0);
  }

  //  KMS client creates initialisation XML.
  std::string
    kms_url = "http://" + kms_auth + "/keymanagement/identity/v1/init";
  std::vector<unsigned char> xml;
  if (result = ScLibMsKMS::RequestKmsInitXML(context, kms_url, user_uri, xml))
  {
	std::cout << "ScLibMsKMS::RequestKmsInitXML failed. Returned "
		<< result << " (" << ScErrName(result) << ")" << std::endl;
    return SecCtx(0);
  }
  dumptofile( xml, std::string("KmsInitRequest.xml"));

  //  Post initialisation XML to KMS server using CURL and get
  //  XML response.
  if (!curl_post(xml, kms_url))
  {
    return SecCtx(0);
  }
  dumptofile( xml, std::string("KmsInitResponse.xml"));

  //  Pass initialisation response to KMS client.
  if (result = ScLibMsKMS::ProcessKmsRespXML(context, xml))
  {
	std::cout << "ScLibMsKMS::ProcessKmsRespXML failed. Returned "
		<< result << " (" << ScErrName(result) << ")" << std::endl;
    return SecCtx(0);
  }
  return context;
}

//  Provisions keys from the KMS, using KMS client and KMS server.
//  Args: secCtx   - the security context.
//        kms_auth - KMS URL authority (optional user, host, optional
//                   port).
//        user_uri - User URI.
//  Rets: Standard return codes
ScErrno ProvisionKeys(const SecCtx &ctx, const std::string &kms_auth,
					  const std::string &user_uri)
{
	ScErrno result;
	std::vector<unsigned char> xml;

	//  KMS client creates key provision XML.
	std::string
		kms_url = "http://" + kms_auth + "/keymanagement/identity/v1/keyprov";
	if (result = ScLibMsKMS::RequestKmsKeyProvXML(ctx, kms_url, user_uri, xml))
	{
		std::cout << "ScLibMsKMS::RequestKmsKeyProvXML failed. Returned "
			<< result << " (" << ScErrName(result) << ")" << std::endl;
		return result;
	}
	dumptofile( xml, std::string("KmsKeyProvRequest.xml"));

	//  Post key provision XML to KMS server using CURL and get
	//  XML response.
	if (!curl_post(xml, kms_url))
	{
		return ScErrno::GENERAL_FAILURE;
	}
	dumptofile( xml, std::string("KmsKeyProvResponse.xml"));

	//  Pass key provision response to KMS client.
	if ( result = ScLibMsKMS::ProcessKmsRespXML(ctx, xml))
	{
		std::cout << "ScLibMsKMS::ProcessKmsRespXML failed. Returned "
			<< result << " (" << ScErrName(result) << ")" << std::endl;
		return result;
	}

	//  Successful initialisation.
	return result;
}

//  Updates the certificate cache, using KMS client and KMS server.
//  Args: secCtx   - the security context.
//        kms_auth - KMS URL authority (optional user, host, optional
//                   port).
//        user_uri - User URI.
//  Rets: Standard return codes
ScErrno updateCertCache(const SecCtx &ctx, const std::string &kms_auth,
						const std::string &user_uri)
{
	ScErrno result;
	std::vector<unsigned char> xml;
	std::string kms_url =
		"http://" + kms_auth + "/keymanagement/identity/v1/certcache";

	//  KMS client creates update certificate cache request.
	if (result = ScLibMsKMS::RequestKmsCertCacheXML(ctx, kms_url, user_uri, xml))
	{
		std::cout << "ScLibMsKMS::RequestKmsCertCacheXML failed. Returned "
			<< result << " (" << ScErrName(result) << ")" << std::endl;
		return result;
	}

	dumptofile( xml, std::string("KmsCertCacheRequest.xml"));

	//  Post key provision XML to KMS server using CURL and get
	//  XML response.
	if (!curl_post(xml, kms_url))
	{
		return ScErrno::GENERAL_FAILURE;
	}
	dumptofile( xml, std::string("KmsCertCacheResponse.xml"));

	//  Pass KMS Cert Cache response to KMS client.
	if ( result = ScLibMsKMS::ProcessKmsRespXML(ctx, xml))
	{
		std::cout << "ScLibMsKMS::RequestKmsKeyProvXML failed. Returned "
			<< result << " (" << ScErrName(result) << ")" << std::endl;
	}
	return result;
}

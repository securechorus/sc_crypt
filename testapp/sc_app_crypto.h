#ifndef SC_APP_CRYPTO_H
#define SC_APP_CRYPTO_H

#include <vector>


//  Authenticated encrypt data using AES-128 in GCM mode.
//  Args: key        - Key. Must have length 16 bytes.
//        plaintext  - Plaintext to be encrypted.
//        ciphertext - Ciphertext after encryption.
//  Rets: True if encryption succeeds, false otherwise.
bool encrypt(const std::vector<unsigned char> &key,
	         const std::vector<unsigned char> &plaintext,
			 std::vector<unsigned char> &ciphertext);


//  Authenticated decrypt data using AES-128 in GCM mode.
//  Args: key        - Key. Must have length 16 bytes.
//        ciphertext - Ciphertext to be decrypted.
//        ciphertext - Plaintext after decryption.
//  Rets: True if decryption succeeds, false otherwise.
bool decrypt(const std::vector<unsigned char> &key,
	         const std::vector<unsigned char> &ciphertext,
			 std::vector<unsigned char> &plaintext);


#endif


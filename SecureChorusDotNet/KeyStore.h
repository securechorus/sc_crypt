#pragma once

#include "Errno.h"

#include <msclr\marshal_cppstd.h>

namespace SecureChorus {

	using namespace System;
	using namespace System::Collections::Generic;
	using namespace System::Runtime::InteropServices;

	public ref class KeyStore
	{
	public:

		static ScErrorCode SetStoreFilePath(System::String^ newFilePath);

	private:

		static std::string* mFilePath = 0;

	};

};
#include "stdafx.h"
#include "InteropUtils.h"

#include <msclr\marshal_cppstd.h>

#include "sc_helper.h"

namespace SecureChorus {

	using namespace System;
	using namespace System::Runtime::InteropServices;

	void InteropUtils::CopyVectorToByteArray(std::vector<unsigned char>& sourceVector, [Out] array<Byte>^% byteArray)
	{

		// Create managed byte array
		byteArray = gcnew array<Byte>(sourceVector.size());

		// Copy unmanaged vector into the managed array
		// Note that vectors are guaranteed to be contiguous
		Marshal::Copy(IntPtr(&sourceVector.front()), byteArray, 0, sourceVector.size());

	}

	void InteropUtils::CopyByteArrayToVector(array<Byte>^ byteArray, std::vector<unsigned char>& destVector)
	{
		
		destVector.resize(byteArray->Length);
		for (int i = 0; i < byteArray->Length; i++)
		{
			destVector[i] = static_cast<unsigned char>(byteArray[i]);
		}

	}

	void InteropUtils::CopyNativeToManagedByteArray(const unsigned char* nativeArray, unsigned int length, [Out] array<System::Byte>^% byteArray)
	{

		byteArray = gcnew array<Byte>(length);

		for (unsigned int i = 0; i < length; i++)
		{
			byteArray[i] = nativeArray[i];
		}

	}

	void InteropUtils::StringListToManagedArray(StringList& stringList, [Out] array<System::String^>^% managedStringArray)
	{

			// Create managed String array
			managedStringArray = gcnew array<System::String^>(stringList.length);

			// Iterate through each String struct in the list
			for (size_t i = 0; i < stringList.length; i++)
			{

				// Get the native String struct
				LibMSCryptoString nativeString = static_cast<const LibMSCryptoString*>(stringList.pointer)[i];

				// Convert native string to a managed one and add to the array
				System::String^ managedString = gcnew System::String(nativeString.pointer, 0, nativeString.length);
				managedStringArray[i] = managedString;

			}

	}

};

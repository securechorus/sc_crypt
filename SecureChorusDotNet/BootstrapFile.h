#pragma once

namespace SecureChorus {

	using namespace System;
	using namespace System::IO;
	using namespace System::Collections::Generic;

	public ref class BootstrapFile
	{
	public:
		BootstrapFile(FileStream^ booststrapFileStream);

		property List<Byte>^ BootstrapKey;

		property String^ KeyId;

	private:

		void parse_file(FileStream^ ist);

		//  Transport key information. Length is in bytes.
		const static String^ tk_algorithm_256 = "kw-aes256";
		const static unsigned int tk_length_256    = 32;
		const static String^ tk_algorithm_128 = "kw-aes128";
		const static unsigned int tk_length_128    = 16;

	};

};

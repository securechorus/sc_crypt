#pragma once

#include "Errno.h"

#include <msclr\marshal_cppstd.h>

namespace SecureChorus {

	using namespace System;
	using namespace System::Collections::Generic;
	using namespace System::Runtime::InteropServices;

	public ref class MikeySakkeCrypto
	{
	public:
		
		static ScErrorCode GenerateSSVAndSakkeEncrypt(unsigned int secCtx, 
			System::String^ recipientUri,
			System::String^ timestamp, 
			[Out] array<System::Byte>^% sed,
			[Out] array<System::Byte>^% ssv);

		static ScErrorCode DecryptSakkeEncapsulatedPayload(unsigned int secCtx,
			System::String^ recipientUri, 
			System::String^ timestamp,
			array<System::Byte>^ data, 
			[Out] array<System::Byte>^% decryptedData);

		/*
		 *  Get list of user UIDs with stored keys.
		 *  Args: secCtx                 - Security context (input).
		 *        userUri                - User URI (input). If empty then consider
		 *                                 all user UIDs.
		 *        timestamp              - UTC timestamp (input). If empty then do
		 *                                 not validate timestamp
		 *        listofMikeySakkeUids_p - *listofMikeySakkeUids_p is set to indicate
		 *                                 list of user UIDs with valid keys matching
		 *                                 parameters as indicated.
		 *  Rets: Standard error code.
		 */
		static ScErrorCode GetKeyedMikeySakkeUIDsList(unsigned int secCtx,
			System::String^ userUri, 
			System::String^ timestamp,
			[Out] array<System::String^>^% listOfMikaySakkeUids);


		/*
		 *  Get list of user URIs with stored keys.
		 *  Args: secCtx           - Security context (input).
		 *        timestamp        - UTC timestamp (input).
		 *        listofUserUris_p - *listofUserUris_p is set to indicate
		 *                           list of user URIs with valid keys at
		 *                           indicated time (unless timestamp is
		 *                           empty).
		 *  Rets: Standard error code.
		 */
		static ScErrorCode GetKeyedUserUris(unsigned int secCtx,
			System::String^ timestamp,
			[Out] array<System::String^>^% listOfMikaySakkeUris);

		/*
		 *  Remove user (all keys).
		 *  Args: secCtx  - Security context (input).
		 *        userUri - User URI (input).
		 *  Rets: Standard error code.
		 */
		static ScErrorCode RemoveUserByURI(unsigned int secCtx,
			System::String^ userUri);

		/*
		 *  Returns if timestamp and URI combination is provisioned for use
		 *  (encryption and verification).
		 *  Args: secCtx    - Security context (input).
		 *        userUri   - User URI (input).
		 *        timestamp - UTC timestamp (input).
		 *  Returns true if provisioned, false if not.
		 */
		static bool IsRecipientURIReady(unsigned int secCtx,
			System::String^ userUri,
			System::String^ timestamp);

	};

};

#include "stdafx.h"
#include "MikeySakkeCrypto.h"

#include <msclr\marshal.h>
#include <msclr\marshal_cppstd.h>

// Include headers from the Secure Chorus include directory
#include "sc_libmscrypto.h"
#include "sc_context.h"
#include "sc_types.h"
#include "sc_errno.h"
#include "sc_helper.h"

#include "Errno.h"
#include "InteropUtils.h"

namespace SecureChorus {

	using namespace System;
	using namespace System::Collections::Generic;
	using namespace msclr::interop;
	using namespace System::Runtime::InteropServices;

	ScErrorCode MikeySakkeCrypto::GenerateSSVAndSakkeEncrypt(unsigned int secCtx, 
			System::String^ recipientUri,
			System::String^ timestamp, 
			[Out] array<System::Byte>^% sed,
			[Out] array<System::Byte>^% ssv)
	{

		ScErrno errorNum;

		LibMSCryptoString recipientUriNative;
		LibMSCryptoString timestampiNative;

		std::string recipientUriStdString = marshal_as<std::string>(recipientUri);
		std::string timestampStdString = marshal_as<std::string>(timestamp);

		ScLibMs::SetStringDescriptor(recipientUriNative, recipientUriStdString);
		ScLibMs::SetStringDescriptor(timestampiNative, timestampStdString);

		LibMSCryptoBuffer sedNative;
		LibMSCryptoBuffer ssvNative;

		// Initialise to 0 so if the function doesn't use the buffers, we know not to copy anything
		sedNative.length = 0;
		sedNative.pointer = 0;
		ssvNative.length = 0;
		ssvNative.pointer = 0;

		errorNum = GenerateSharedSecretAndSakkeEncrypt(static_cast<SecCtx>(secCtx),
			recipientUriNative,
			timestampiNative,
			&sedNative,
			&ssvNative);

		// Convert the libmscrypto Buffers into managed byte arrays
		
		InteropUtils::CopyNativeToManagedByteArray(sedNative.pointer, static_cast<unsigned int>(sedNative.length), sed);

		InteropUtils::CopyNativeToManagedByteArray(ssvNative.pointer, static_cast<unsigned int>(ssvNative.length), ssv);

		return static_cast<ScErrorCode>(errorNum);

	}

	ScErrorCode MikeySakkeCrypto::DecryptSakkeEncapsulatedPayload(unsigned int secCtx,
			System::String^ recipientUri, 
			System::String^ timestamp,
			array<System::Byte>^ data, 
			[Out] array<System::Byte>^% decryptedData)
	{

		ScErrno errorNum;

		LibMSCryptoString recipientUriNative;
		LibMSCryptoString timestampiNative;

		std::string recipientUriStdString = marshal_as<std::string>(recipientUri);
		std::string timestampStdString = marshal_as<std::string>(timestamp);

		ScLibMs::SetStringDescriptor(recipientUriNative, recipientUriStdString);
		ScLibMs::SetStringDescriptor(timestampiNative, timestampStdString);

		LibMSCryptoBuffer dataNative;

		// Convert data to a native LibMSCryptoBuffer

		dataNative.length = static_cast<size_t>(data->Length);
		Byte* nativeDataCopy = new Byte[data->Length];

		// Copy the managed bytes
		for (int i = 0; i < data->Length; i++)
		{
			nativeDataCopy[i] = data[i];
		}

		dataNative.pointer = nativeDataCopy;


		LibMSCryptoBuffer decryptedNative;
		// Initialise to 0 so if the function doesn't use the buffer, we know not to copy anything
		decryptedNative.length = 0;
		decryptedNative.pointer = 0;

		errorNum = SakkeDecrypt(static_cast<SecCtx>(secCtx),
			recipientUriNative,
			timestampiNative,
			dataNative,
			&decryptedNative);

		delete[] dataNative.pointer;

		// Convert the libmscrypto Buffer into a managed byte array
		InteropUtils::CopyNativeToManagedByteArray(decryptedNative.pointer, static_cast<unsigned int>(decryptedNative.length), decryptedData);

		return static_cast<ScErrorCode>(errorNum);

	}

	ScErrorCode MikeySakkeCrypto::GetKeyedMikeySakkeUIDsList(unsigned int secCtx,
			System::String^ userUri, 
			System::String^ timestamp,
			[Out] array<System::String^>^% listOfMikaySakkeUids)
	{

		ScErrno errorNum;

		LibMSCryptoString userUriNative;
		LibMSCryptoString timestampiNative;

		// The std::strings need to stay in scope as SetStringDescriptor sets the String struct to point to their contents
		std::string userUriStdString = marshal_as<std::string>(userUri);
		std::string timestampStdString = marshal_as<std::string>(timestamp);

		// userUri can be passed as empty to ListKeyedMikeySakkeUids which expects pointer to be null when empty
		if (userUri->Length > 0) {
			ScLibMs::SetStringDescriptor(userUriNative, userUriStdString);
		} else {
			userUriNative.length = 0;
			userUriNative.pointer = 0;
		}
		
		// timestamp can be passed as empty to ListKeyedMikeySakkeUids which expects pointer to be null when empty
		if (timestamp->Length > 0) {
			ScLibMs::SetStringDescriptor(timestampiNative, timestampStdString);
		} else {
			timestampiNative.length = 0;
			timestampiNative.pointer = 0;
		}

		StringList nativeList;
		// Zero so we know if ListKeyedMikeySakkeUids has put a result in it
		nativeList.length = 0;
		nativeList.pointer = 0;

		errorNum = ListKeyedMikeySakkeUids(static_cast<SecCtx>(secCtx),
			userUriNative,
			timestampiNative,
			&nativeList);

		if (nativeList.pointer != 0)
		{
			InteropUtils::StringListToManagedArray(nativeList, listOfMikaySakkeUids);
		}

		return static_cast<ScErrorCode>(errorNum);

	}

	ScErrorCode MikeySakkeCrypto::GetKeyedUserUris(unsigned int secCtx,
			System::String^ timestamp,
			[Out] array<System::String^>^% listOfMikaySakkeUris)
	{

		ScErrno errorNum;

		LibMSCryptoString timestampiNative;

		// The std::strings need to stay in scope as SetStringDescriptor sets the String struct to point to their contents
		std::string timestampStdString = marshal_as<std::string>(timestamp);
		
		if (timestamp->Length > 0) {
			ScLibMs::SetStringDescriptor(timestampiNative, timestampStdString);
		} else {
			timestampiNative.length = 0;
			timestampiNative.pointer = 0;
		}

		StringList nativeList;
		// Zero so we know if ListKeyedMikeySakkeUids has put a result in it
		nativeList.length = 0;
		nativeList.pointer = 0;

		errorNum = ListKeyedUserUris(static_cast<SecCtx>(secCtx),
			timestampiNative,
			&nativeList);

		if (nativeList.pointer != 0)
		{
			InteropUtils::StringListToManagedArray(nativeList, listOfMikaySakkeUris);
		}

		return static_cast<ScErrorCode>(errorNum);

	}

	ScErrorCode MikeySakkeCrypto::RemoveUserByURI(unsigned int secCtx,
			System::String^ userUri)
	{

		ScErrno errorNum;

		LibMSCryptoString userUriNative;

		// The std::strings need to stay in scope as SetStringDescriptor sets the String struct to point to their contents
		std::string userUriStdString = marshal_as<std::string>(userUri);

		ScLibMs::SetStringDescriptor(userUriNative, userUriStdString);

		errorNum = RemoveUser(static_cast<SecCtx>(secCtx),
			userUriNative);

		return static_cast<ScErrorCode>(errorNum);

	}

	/*
	*  Returns if timestamp and URI combination is provisioned for use
	*  (encryption and verification).
	*  Args: secCtx    - Security context (input).
	*        userUri   - User URI (input).
	*        timestamp - UTC timestamp (input).
	*  Returns true if provisioned, false if not.
	*/
	bool MikeySakkeCrypto::IsRecipientURIReady(unsigned int secCtx,
			System::String^ userUri,
			System::String^ timestamp)
	{
		
		LibMSCryptoString recipientURINative;
		LibMSCryptoString timestampNative;

		// The std::strings need to stay in scope as SetStringDescriptor sets the String struct to point to their contents
		std::string userUriStdString = marshal_as<std::string>(userUri);
		std::string timestampStdString = marshal_as<std::string>(timestamp);

		ScLibMs::SetStringDescriptor(recipientURINative, userUriStdString);
		ScLibMs::SetStringDescriptor(timestampNative, timestampStdString);

		LibMsCryptoBoolean result = IsTheirUriReady(static_cast<SecCtx>(secCtx),
			recipientURINative,
			timestampNative
			);

		// LibMsCryptoBoolean is just a typedef of an int
		return (result != 0);

	}

};
